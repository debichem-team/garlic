/* Copyright (C) 2001-2006 Damir Zucic */

/*=============================================================================

			   open_template_file.c

Purpose:
	Open the template file.  This is the file with template atoms. These
	atoms are used to create new peptides, to replace or insert residues
	and for similar operations.  Up to six directories are searched.  If
	all attempts fail, this function will return NULL.

Input:
	(1) Environment variable HOME used.

Output:
	(1) Return value. 

Return value:
	(1) File pointer, if file is found.
	(2) NULL, if all attempts to find file fail.

Notes:
	(1) The following six pathnames in the given order  will be checked,
	    if necessary:

	    (1) residues.pdb                (current working directory)
	    (2) $HOME/residues.pdb          (home directory)
	    (3) $HOME/garlic/residues.pdb
	    (4) /usr/share/garlic/residues.pdb (recom. system-wide default!)
	    (5) /usr/local/lib/garlic/residues.pdb
	    (6) /usr/lib/garlic/residues.pdb

========includes:============================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "defines.h"

/*======try to find and open residues.pdb file:==============================*/

FILE *OpenTemplateFile_ (void)
{
static FILE	*fileP;
static char	tildaA[10] = "~";
char		*home_dirP;
int		n;
char		template_file_nameA[STRINGSIZE];

/* The first attempt: try to find the file in the current working directory: */
if ((fileP = fopen ("residues.pdb", "r")) != NULL)
	{
	return fileP;
	}

/* The second attempt - try to find residues.pdb file in the home directory: */
do
	{
	if ((home_dirP = getenv ("HOME")) != NULL) break;
	if ((home_dirP = getenv ("home")) != NULL) break;
	home_dirP = tildaA;
	} while (0);

n = STRINGSIZE - 100;
strncpy (template_file_nameA, home_dirP, n);
template_file_nameA[n] = '\0';
strncat (template_file_nameA, "/residues.pdb", 13);
template_file_nameA[STRINGSIZE - 1] = '\0';
if ((fileP = fopen (template_file_nameA, "r")) != NULL)
	{
	return fileP;
	}

/* The third attempt - try to find personal file in $HOME/garlic directory: */
n = STRINGSIZE - 100;
strncpy (template_file_nameA, home_dirP, n);
template_file_nameA[n] = '\0';
strncat (template_file_nameA, "/garlic/residues.pdb", 20);
template_file_nameA[STRINGSIZE - 1] = '\0';
if ((fileP = fopen (template_file_nameA, "r")) != NULL)
	{
	return fileP;
	}

/* The fourth attempt  -  /usr/share/garlic  directory. */
/* This pathname is recommended as system-wide default! */
if ((fileP = fopen ("/usr/share/garlic/residues.pdb", "r")) != NULL)
        {
        return fileP;
        }

/* The fifth attempt - /usr/local/lib/garlic directory: */
if ((fileP = fopen ("/usr/local/lib/garlic/residues.pdb", "r")) != NULL)
	{
	return fileP;
	}

/* The sixth attempt - /usr/lib/garlic directory: */
if ((fileP = fopen ("/usr/lib/garlic/residues.pdb", "r")) != NULL)
        {
        return fileP;
        }

/* If this point is reached, all attempts have failed: */
return NULL;
}

/*===========================================================================*/


