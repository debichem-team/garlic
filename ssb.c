/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				ssb.c

Purpose:
	Execute ssb command: change drawing style for disulfide bonds.

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure, with some runtime data.
	(4) Pointer to ConfigS structure, with configuration data.
	(5) Pointer to GUIS structure, with GUI data.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
	(9) String which contains the bond drawing style name.

Output:
	(1) The hiddenF set for each disulfide bond in each caught complex.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute ssb command:=================================================*/

int SSB_ (MolComplexS *mol_complexSP, int mol_complexesN,
	  RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
	  NearestAtomS *nearest_atomSP, size_t pixelsN,
	  unsigned int *refreshIP, char *stringP)
{
char		*remainderP;
char		tokenA[SHORTSTRINGSIZE];
int		bond_styleI;
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;
size_t		atomsN, atomI;
AtomS		*curr_atomSP;
int		bondI, bondsN;

/* Extract the bond style name: */
remainderP = ExtractToken_ (tokenA, SHORTSTRINGSIZE, stringP, " \t\n");
if (!remainderP)
	{
	strcpy (runtimeSP->messageA,
		"Bond drawing style specification missing!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_NO_ATOM_STYLE;
	}

/* Try to recognize the requested style: */
if      (strcmp (tokenA, "0") == 0) bond_styleI = 0;
else if (strcmp (tokenA, "1") == 0) bond_styleI = 1;
else if (strcmp (tokenA, "2") == 0) bond_styleI = 2;
else if (strcmp (tokenA, "3") == 0) bond_styleI = 3;
else if (strcmp (tokenA, "4") == 0) bond_styleI = 4;
else if (strcmp (tokenA, "5") == 0) bond_styleI = 5;
else if (strstr (tokenA, "NIC") == tokenA) bond_styleI = NICE_BONDS;
else
	{
	sprintf (runtimeSP->messageA,
		 "Bond drawing style \"%s\" not available!", tokenA);
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_BOND_STYLE;
	}

/* Scan every macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/** Pointer to the current macromolecular complex: **/
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/** Check is the current macromolecular complex caught: **/
	if (curr_mol_complexSP->catchF == 0) continue;

	/** Number of bonds in a macromolecular complex: **/
	atomsN = curr_mol_complexSP->atomsN;

	/** Scan all bonds in the current complex: **/
	for (atomI = 0; atomI < atomsN; atomI++)
		{
		/** Pointer to the current bond: **/
		curr_atomSP = curr_mol_complexSP->atomSP + atomI;

		/** Check is atom selected: **/
		if (curr_atomSP->selectedF == 0) continue;

		/** The number of bonds associated with a given atoms: **/
		bondsN = curr_atomSP->bondsN;

		/** Scan all bonds: **/
		for (bondI = 0; bondI < bondsN; bondI++)
			{
			/*** If this is not a disulfide bond, skip it: ***/
			if (curr_atomSP->true_bondSA[bondI].bond_typeI != 2)
				{
				continue;
				}

			/*** Change the bond drawing style: ***/
			curr_atomSP->true_bondSA[bondI].bond_styleI =
								bond_styleI;
			}
		}

	/* Reset the position_changedF: */
	curr_mol_complexSP->position_changedF = 1;
	}

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN, runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return positive value on success: */
return COMMAND_DISULFIDE;
}

/*===========================================================================*/


