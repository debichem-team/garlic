/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

				print_config.c

Purpose:
	Print configuration data (for debugging purpose).

Input:
	(1) Pointer to ConfigS structure, with configuration data.
	(2) Pointer to GUIS structure, with GUI data.

Output:
	Configuration data and part of GUI data written to stdout.

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======print configuration data:============================================*/

void PrintConfig_ (ConfigS *configSP, GUIS *guiSP)
{
int		n;

printf ("========================\n");

/* Main window geometry: */
printf ("main window geometry: <%s>\n", configSP->geometryA);

/* Font name and cursor name: */
printf ("font name: <%s>\n", configSP->font_nameA);
printf ("cursor name: <%s>\n", configSP->cursor_nameA);

/* Coordinate system flag: */
printf ("coordinate system flag: %d\n", (int) configSP->show_coord_systemF);

/* Control window flag: */
printf ("control window flag: %d\n", (int) configSP->show_control_windowF);

/* Sequence neighborhood flag: */
printf ("sequence neighborhood flag: %d\n",
	(int) configSP->show_sequence_neighborhoodF);

/** Stereo flag: **/
printf ("stereo flag: %d\n", (int) configSP->stereoF);

/** Light source theta angle: **/
printf ("light source theta angle: %f\n", configSP->light_theta);

/** Light source phi angle: **/
printf ("light source phi angle: %f\n", configSP->light_phi);

/** Default slab mode flag: */
printf ("default slab mode: %d\n", (int) configSP->default_slab_modeI);

/** Default color fading mode: **/
printf ("default color fading mode: %d\n",
	(int) configSP->default_fading_modeI);

/** Strings specifying default colors for all windows: **/
printf ("window background color: <%s>\n", configSP->bg_colorA);
printf ("window foreground color: <%s>\n", configSP->fg_colorA);
printf ("text background color: <%s>\n", configSP->text_bg_colorA);
printf ("text foreground color: <%s>\n", configSP->text_fg_colorA);

printf ("Number of fading surfaces: %d\n", configSP->default_surfacesN);

/** Strings specifying default basic colors for atoms and bonds: **/
for (n = 0; n < MAXCOLORSURFACES; n++)
	{
	printf ("Fading surface %d:\n", n);
	printf ("Left color: <%s>\n", configSP->left_colorAA[n]);
	printf ("Left color RGB values: %d %d %d\n",
		(int) configSP->left_rgbSA[n].red,
		(int) configSP->left_rgbSA[n].green,
		(int) configSP->left_rgbSA[n].blue);
	printf ("Middle color: <%s>\n", configSP->middle_colorAA[n]);
	printf ("Middle color RGB values: %d %d %d\n",
		(int) configSP->middle_rgbSA[n].red,
		(int) configSP->middle_rgbSA[n].green,
		(int) configSP->middle_rgbSA[n].blue);
	printf ("Right color: <%s>\n", configSP->right_colorAA[n]);
	printf ("Right color RGB values: %d %d %d\n",
		(int) configSP->right_rgbSA[n].red,
		(int) configSP->right_rgbSA[n].green,
		(int) configSP->right_rgbSA[n].blue);
	}

/** Default color scheme for the outer side of molecular surfaces: **/
printf ("default surface color scheme (outer side): <%s>\n",
	 configSP->surface_outer_color_schemeA);

/** Default color scheme for the inner side of molecular surfaces: **/
printf ("default surface color scheme (inner side): <%s>\n",
	 configSP->surface_inner_color_schemeA);

/** Default drawing style for atoms, bonds and backbone: **/
printf ("default atom drawing style: %d\n", configSP->default_atom_styleI);
printf ("default bond drawing style: %d\n", configSP->default_bond_styleI);
printf ("default backbone drawing style: %d\n",
	configSP->default_backbone_styleI);

/** Bond parameters: **/
printf ("maximal bond length: %f\n", configSP->max_bond_length);
printf ("minimal C-C bond length: %f\n", configSP->C_C_bond_length_min);
printf ("maximal C-C bond length: %f\n", configSP->C_C_bond_length_max);
printf ("minimal C-N bond length: %f\n", configSP->C_N_bond_length_min);
printf ("maximal C-N bond length: %f\n", configSP->C_N_bond_length_max);
printf ("minimal C-O bond length: %f\n", configSP->C_O_bond_length_min);
printf ("maximal C-O bond length: %f\n", configSP->C_O_bond_length_max);
printf ("minimal C-S bond length: %f\n", configSP->C_S_bond_length_min);
printf ("maximal C-S bond length: %f\n", configSP->C_S_bond_length_max);
printf ("minimal C-H bond length: %f\n", configSP->C_H_bond_length_min);
printf ("maximal C-H bond length: %f\n", configSP->C_H_bond_length_max);
printf ("minimal N-O bond length: %f\n", configSP->N_O_bond_length_min);
printf ("maximal N-O bond length: %f\n", configSP->N_O_bond_length_max);
printf ("minimal N-H bond length: %f\n", configSP->N_H_bond_length_min);
printf ("maximal N-H bond length: %f\n", configSP->N_H_bond_length_max);
printf ("minimal O-H bond length: %f\n", configSP->O_H_bond_length_min);
printf ("maximal O-H bond length: %f\n", configSP->O_H_bond_length_max);
printf ("minimal S-H bond length: %f\n", configSP->S_H_bond_length_min);
printf ("maximal S-H bond length: %f\n", configSP->S_H_bond_length_max);
printf ("minimal O-P bond length: %f\n", configSP->O_P_bond_length_min);
printf ("maximal O-P bond length: %f\n", configSP->O_P_bond_length_max);
printf ("minimal S-S bond length: %f\n", configSP->S_S_bond_length_min);
printf ("maximal S-S bond length: %f\n", configSP->S_S_bond_length_max);
printf ("minimal generic bond length:  %f\n",
				configSP->generic_bond_length_min);
printf ("maximal generic bond length:  %f\n",
				configSP->generic_bond_length_max);
printf ("minimal hydrogen bond length: %f\n", configSP->hydro_bond_length_min);
printf ("maximal hydrogen bond length: %f\n", configSP->hydro_bond_length_max);
printf ("minimal C-O...N angle (radians): %f\n",
				configSP->hydro_bond_angle_min);
printf ("maximal C-O...N angle (radians): %f\n",
				configSP->hydro_bond_angle_max);

/** The calculated (practical) bond parameters: **/
printf ("minimal C-C bond length squared: %f\n", configSP->C_C_min_squared);
printf ("maximal C-C bond length squared: %f\n", configSP->C_C_max_squared);
printf ("minimal C-N bond length squared: %f\n", configSP->C_N_min_squared);
printf ("maximal C-N bond length squared: %f\n", configSP->C_N_max_squared);
printf ("minimal C-O bond length squared: %f\n", configSP->C_O_min_squared);
printf ("maximal C-O bond length squared: %f\n", configSP->C_O_max_squared);
printf ("minimal C-S bond length squared: %f\n", configSP->C_S_min_squared);
printf ("maximal C-S bond length squared: %f\n", configSP->C_S_max_squared);
printf ("minimal C-H bond length squared: %f\n", configSP->C_H_min_squared);
printf ("maximal C-H bond length squared: %f\n", configSP->C_H_max_squared);
printf ("minimal N-O bond length squared: %f\n", configSP->N_O_min_squared);
printf ("maximal N-O bond length squared: %f\n", configSP->N_O_max_squared);
printf ("minimal N-H bond length squared: %f\n", configSP->N_H_min_squared);
printf ("maximal N-H bond length squared: %f\n", configSP->N_H_max_squared);
printf ("minimal O-H bond length squared: %f\n", configSP->O_H_min_squared);
printf ("maximal O-H bond length squared: %f\n", configSP->O_H_max_squared);
printf ("minimal S-H bond length squared: %f\n", configSP->S_H_min_squared);
printf ("maximal S-H bond length squared: %f\n", configSP->S_H_max_squared);
printf ("minimal O-P bond length squared: %f\n", configSP->O_P_min_squared);
printf ("maximal O-P bond length squared: %f\n", configSP->O_P_max_squared);
printf ("minimal S-S bond length squared: %f\n", configSP->S_S_min_squared);
printf ("maximal S-S bond length squared: %f\n", configSP->S_S_max_squared);
printf ("minimal generic bond length squared: %f\n",
				configSP->generic_min_squared);
printf ("maximal generic bond length squared: %f\n",
				configSP->generic_max_squared);
printf ("minimal hydrogen bond length squared: %f\n",
				configSP->hydro_min_squared);
printf ("maximal hydrogen length squared: %f\n",
                                configSP->hydro_max_squared);

/** Backbone CA-CA distance: **/
printf ("maximal CA-CA distance: %f\n", configSP->CA_CA_dist_max);
printf ("maximal CA-CA distance squared: %f\n",
				configSP->CA_CA_dist_max_squared);

/** Atomic radii: **/
printf ("radius of H atom: %f\n", configSP->H_radius);
printf ("radius of C atom: %f\n", configSP->C_radius);
printf ("radius of N atom: %f\n", configSP->N_radius);
printf ("radius of O atom: %f\n", configSP->O_radius);
printf ("radius of S atom: %f\n", configSP->S_radius);
printf ("radius of P atom: %f\n", configSP->P_radius);
printf ("generic radius:   %f\n", configSP->generic_radius);

/** Covalent radii: **/
printf ("covalent radius of H atom: %f\n", configSP->H_covalent_radius);
printf ("covalent radius of C atom: %f\n", configSP->C_covalent_radius);
printf ("covalent radius of N atom: %f\n", configSP->N_covalent_radius);
printf ("covalent radius of O atom: %f\n", configSP->O_covalent_radius);
printf ("covalent radius of S atom: %f\n", configSP->S_covalent_radius);
printf ("covalent radius of P atom: %f\n", configSP->P_covalent_radius);
printf ("generic covalent radius:   %f\n", configSP->generic_covalent_radius);

/** Small radii: **/
printf ("small radius of H atom: %f\n", configSP->H_small_radius);
printf ("small radius of C atom: %f\n", configSP->C_small_radius);
printf ("small radius of N atom: %f\n", configSP->N_small_radius);
printf ("small radius of O atom: %f\n", configSP->O_small_radius);
printf ("small radius of S atom: %f\n", configSP->S_small_radius);
printf ("small radius of P atom: %f\n", configSP->P_small_radius);
printf ("generic small radius:   %f\n", configSP->generic_small_radius);

/** Van der Waals radii: **/
printf ("van der Waals radius of H atom: %f\n",
	configSP->H_van_der_Waals_radius);
printf ("van der Waals radius of C atom: %f\n",
	configSP->C_van_der_Waals_radius);
printf ("van der Waals radius of N atom: %f\n",
	configSP->N_van_der_Waals_radius);
printf ("van der Waals radius of O atom: %f\n",
	configSP->O_van_der_Waals_radius);
printf ("van der Waals radius of S atom: %f\n",
	configSP->S_van_der_Waals_radius);
printf ("van der Waals radius of P atom: %f\n",
	configSP->P_van_der_Waals_radius);
printf ("generic van der Waals radius:   %f\n",
	configSP->generic_van_der_Waals_radius);

/** Ball radius: **/
printf ("default ball radius (used to draw balls and sticks): %f\n",
	configSP->default_ball_radius);

/** Default bond probe radius: **/
printf ("default bond probe radius (used to draw nice bonds): %f\n",
	configSP->default_bond_probe_radius);

/** Default stick radius: **/
printf ("default stick radius (used to draw bonds as sticks): %f\n",
	configSP->default_stick_radius);

/** Number of neighbors which should be checked as bond candidates: **/
printf ("number of bond candidates (backward): %d\n",
	configSP->bond_candidates_backward);
printf ("number of bond candidates (forward): %d\n",
	configSP->bond_candidates_forward);

/** Blur rectangle default width and height: **/
printf ("Blur rectangle default width:  %d\n", configSP->blur_default_width);
printf ("Blur rectangle default height: %d\n", configSP->blur_default_height);

/** Miscelaneous geometric parameters, related to drawing: **/
printf ("main window left margin: %d\n", configSP->main_margin_left);
printf ("main window right margin: %d\n", configSP->main_margin_right);
printf ("main window top margin: %d\n", configSP->main_margin_top);
printf ("main window bottom margin: %d\n", configSP->main_margin_bottom);
printf ("maximal main window width: %d\n", configSP->max_main_win_width);
printf ("maximal main window height: %d\n", configSP->max_main_win_height);
printf ("the nearest line thickness: %d\n", configSP->nearest_line_thickness);
printf ("screen width (physical): %f\n", configSP->screen_real_width);
printf ("screen height (physical): %f\n", configSP->screen_real_height);
printf ("user to screen distance (physical): %f\n",
	configSP->user_screen_real_distance);
printf ("screen width (atomic): %f\n", configSP->screen_atomic_width);
printf ("user position (atomic): %f\n", configSP->user_atomic_position);
printf ("stereo margin (screen): %d\n", configSP->stereo_screen_margin);
printf ("stereo angle (degrees): %f\n", configSP->stereo_angle * RAD_TO_DEG);
printf ("rotation steps (degrees): %7.3f %7.3f %7.3f %7.3f %7.3f\n",
	configSP->rotation_stepA[0], configSP->rotation_stepA[1],
	configSP->rotation_stepA[2], configSP->rotation_stepA[3],
	configSP->rotation_stepA[4]);
printf ("translation steps (angstroms): %7.3f %7.3f %7.3f %7.3f %7.3f\n",
	configSP->translation_stepA[0], configSP->translation_stepA[1],
	configSP->translation_stepA[2], configSP->translation_stepA[3],
	configSP->translation_stepA[4]);
printf ("slab steps (angstroms): %7.3f %7.3f %7.3f %7.3f %7.3f\n",
	configSP->slab_stepA[0], configSP->slab_stepA[1],
	configSP->slab_stepA[2], configSP->slab_stepA[3],
	configSP->slab_stepA[4]);
printf ("fading steps (angstroms): %7.3f %7.3f %7.3f %7.3f %7.3f\n",
	configSP->fading_stepA[0], configSP->fading_stepA[1],
	configSP->fading_stepA[2], configSP->fading_stepA[3],
	configSP->fading_stepA[4]);

printf ("------------------------\n");

/** Parameters calculated from geometric data. **/
printf ("user to screen distance (atomic): %f\n",
	configSP->user_screen_atomic_distance);
printf ("screen height (atomic): %f\n", configSP->screen_atomic_height);
printf ("screen z coordinate (atomic): %f\n", configSP->screen_atomic_z);
printf ("atomic to screen scale factor (x): %f\n",
	configSP->atomic_to_screen_scale_x);
printf ("atomic to screen scale factor (y): %f\n",
	configSP->atomic_to_screen_scale_y);
printf ("screen to atomic scale factor (x): %f\n",
	configSP->screen_to_atomic_scale_x);
printf ("window free width (atomic): %f\n", configSP->win_atomic_free_width);
printf ("window free height (atomic): %f\n", configSP->win_atomic_free_height);
printf ("window left edge position (atomic): %f\n", configSP->win_atomic_x0);
printf ("window top edge position (atomic): %f\n", configSP->win_atomic_y0);
printf ("window z coordinate (atomic): %f\n", configSP->win_atomic_z0);
printf ("window right edge position (atomic): %f\n", configSP->win_atomic_x1);
printf ("window bottom edge position (atomic): %f\n", configSP->win_atomic_y1);
printf ("rationalized x0: %f\n", configSP->rationalized_x0);
printf ("rationalized y0: %f\n", configSP->rationalized_y0);
printf ("rationalized x1: %f\n", configSP->rationalized_x1);
printf ("rationalized y1: %f\n", configSP->rationalized_y1);
printf ("stereo margin (atomic): %f\n", configSP->stereo_atomic_margin);
printf ("left image center x (screen): %d\n", configSP->center_screen_x[0]);
printf ("right image center x (screen): %d\n", configSP->center_screen_x[1]);
printf ("both images center y (screen): %d\n", configSP->center_screen_y);
printf ("left image left edge x (screen): %d\n",
	configSP->image_screen_x0[0]);
printf ("left image right edge x (screen): %d\n",
	configSP->image_screen_x1[0]);
printf ("right image left edge x (screen): %d\n",
        configSP->image_screen_x0[1]);
printf ("right image right edge x (screen): %d\n",
        configSP->image_screen_x1[1]);
printf ("both images top edge y (screen): %d\n",
	configSP->image_screen_y0);
printf ("both images bottom edge y (screen): %d\n",
	configSP->image_screen_y1);

printf ("-----some GUI data:-----\n");

printf ("screen width (pixels): %u\n", guiSP->screen_width);
printf ("screen height (pixels): %u\n", guiSP->screen_height);
printf ("control window x0 (pixels): %u\n", guiSP->control_winS.x0);
printf ("input window y0 (pixels): %u\n", guiSP->input_winS.y0);
printf ("main window free area width: %u\n",
	guiSP->main_win_free_area_width);
printf ("main window free area height: %u\n",
	guiSP->main_win_free_area_height);

/* Flush it: */
fflush (stdout);

}

/*===========================================================================*/


