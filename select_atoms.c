/* Copyright (C) 2001-2003 Damir Zucic */

/*=============================================================================

				select_atoms.c

Purpose:
	Try to interpret the selection string as the request to select one
	or more atoms.  Atomic serial numbers  are used  to specify atoms.
	Selection  modes:   0 = overwrite,  1 = restrict,  2 = expand  the
	previous selection. This function expects that the second token in
	the command line is the keyword ATOM (short version is ATO). If it
	is not found, the function will return negative value.

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) Number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) The selection string.
	(5) Selection mode index  (0 = overwr., 1 = restrict, 2 = expand).

Output:
	(1) The flag selectedF will be equal to one for selected atoms and
	    equal to zero for all other atoms.
	(2) Return value.

Return value:
	(1) The number of  selected atoms,  if selection  is done  in this
	    function.
	(2) Negative value on failure.

Notes:
	(1) If there are two atoms with the same serial number, both atoms
	    will be selected.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
int		ExtractSerials_ (int *, int *, char *);

/*======select atom(s) by serial number(s):==================================*/

long SelectAtoms_ (MolComplexS *mol_complexSP, int mol_complexesN,
		   RuntimeS *runtimeSP, char *stringP, int selection_modeI)
{
long		selected_atomsN = 0; 
char		*remainderP;
char		tokenA[SHORTSTRINGSIZE];
int		max;
int		n1, n2;
int		start_atomIA[MAXFIELDS], end_atomIA[MAXFIELDS];
int		rangeI = 0, rangesN;
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;
size_t		atomsN, atomI;
AtomS		*curr_atomSP;
int		serial_numberF;
int		serialI;
int		startI, endI;

/* Extract the first token and check it (search for substring ATO): */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, stringP, " \t\n");
if (!strstr (tokenA, "ATO")) return (long) -1;

/* Parse the list of ranges: */
max = SHORTSTRINGSIZE;
while ((remainderP = ExtractToken_ (tokenA, max, remainderP, " \t,;")) != NULL)
	{
	/* Extract serial numbers: */
	if (ExtractSerials_ (&n1, &n2, tokenA) < 0) return (long) -2;

	/* Copy serial numbers: */
	start_atomIA[rangeI] = n1;
	end_atomIA[rangeI]   = n2;

	/* Increment and check the number of ranges: */
	rangeI++;
	if (rangeI >= MAXFIELDS) break;
	}

/* Store the number of ranges: */
rangesN = rangeI;

/* Scan every macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/* Pointer to the current macromolecular complex: */
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/* Check is the current macromolecular complex caught: */
	if (curr_mol_complexSP->catchF == 0) continue;

	/* Number of atoms in a macromolecular complex: */
	atomsN = curr_mol_complexSP->atomsN;

	/* Scan all atoms in the current complex: */
	for (atomI = 0; atomI < atomsN; atomI++)
		{
		/* Pointer to the current atom: */
		curr_atomSP = curr_mol_complexSP->atomSP + atomI;

		/* Copy the serial number of the current atom: */
		serialI = curr_atomSP->raw_atomS.serialI;

		/* Reset the flag: */
		serial_numberF = 0;

		/* Scan all ranges and check the atomic serial number: */
		for (rangeI = 0; rangeI < rangesN; rangeI++)
			{
			startI = start_atomIA[rangeI];
			endI   = end_atomIA[rangeI];
			if ((serialI >= startI) && (serialI <= endI))
				{
				serial_numberF = 1;
				break;
				}
			}

		/* Set the selection flag for the current atom: */
		switch (selection_modeI)
			{
			/* Overwrite the previous selection: */
			case 0:
				curr_atomSP->selectedF = serial_numberF;
				break;

			/* Restrict the previous selection: */
			case 1:
				curr_atomSP->selectedF &= serial_numberF;
				break;

			/* Expand the previous selection: */
			case 2:
				curr_atomSP->selectedF |= serial_numberF;
				break;

			default:
				;
			}

		/* Check the selection flag; increase */
		/* the count if flag is equal to one: */
		if (curr_atomSP->selectedF) selected_atomsN++;
		}

	/* Update the position_changedF: */
	curr_mol_complexSP->position_changedF = 1;
	}

/* Return the number of selected atoms: */
return selected_atomsN;
}

/*===========================================================================*/


