/* Copyright (C) 2001-2005 Damir Zucic */


/* Command codes - return values from ExecuteCommand_ () function. */


/* Positive (success) codes: */

#define COMMAND_QUIT           1
#define COMMAND_IGNORE         2
#define COMMAND_LOAD           3
#define COMMAND_DISCARD        4
#define COMMAND_CATCH          5
#define COMMAND_SELECT         6
#define COMMAND_SHOW           7
#define COMMAND_HIDE           8
#define COMMAND_COLOR          9
#define COMMAND_ATOMS         10
#define COMMAND_BONDS         11
#define COMMAND_CONTINUE      12
#define COMMAND_PAUSE         13
#define COMMAND_EXECUTE       14
#define COMMAND_ROTATE        15
#define COMMAND_TRANSLATE     16
#define COMMAND_SLAB          17
#define COMMAND_FADING        18
#define COMMAND_LOG           19
#define COMMAND_FOREGROUND    20
#define COMMAND_BACKGROUND    21
#define COMMAND_TBG           22
#define COMMAND_TFG           23
#define COMMAND_FONT          24
#define COMMAND_BACKBONE      25
#define COMMAND_HYBONDS       26
#define COMMAND_STEREO        27
#define COMMAND_MONO          28
#define COMMAND_POSITION      29
#define COMMAND_TITLE         30
#define COMMAND_LABELS        31
#define COMMAND_REGISTER      32
#define COMMAND_CENTER        33
#define COMMAND_SAVE          34
#define COMMAND_TAG           35
#define COMMAND_SEQUENCE      36
#define COMMAND_RAMACHANDRAN  37
#define COMMAND_WHEEL         38
#define COMMAND_ANGLE         39
#define COMMAND_RADIUS        40
#define COMMAND_VENN          41
#define COMMAND_DISULFIDE     42
#define COMMAND_WINDOW        43
#define COMMAND_PLOT          44
#define COMMAND_MOVE          45
#define COMMAND_PLANE         46
#define COMMAND_SCALE         47
#define COMMAND_PATTERN       48
#define COMMAND_DOCK          49
#define COMMAND_COMPARE       50
#define COMMAND_NEIGHBORHOOD  51
#define COMMAND_STRUCTURE     52
#define COMMAND_CREATE        53
#define COMMAND_REPLACE       54
#define COMMAND_EDIT          55
#define COMMAND_MEMBRANE      56
#define COMMAND_SET           57
#define COMMAND_PROBE         58
#define COMMAND_REFRESH       59
#define COMMAND_STICKS        60
#define COMMAND_BALLS         61
#define COMMAND_BLUR          62
#define COMMAND_GROUP         63
#define COMMAND_DUMP          64


/* Negative (error) codes: */

#define ERROR_UNREC_COMM      -1
#define ERROR_LOAD            -2
#define ERROR_NO_FILE_NAME    -3
#define ERROR_OPEN_FAILURE    -4
#define ERROR_NO_ID           -5
#define ERROR_BAD_ID          -6
#define ERROR_SELECT          -7
#define ERROR_NO_SCHEME       -8
#define ERROR_BAD_SCHEME      -9
#define ERROR_NO_ATOM_STYLE  -10
#define ERROR_ATOM_STYLE     -11
#define ERROR_NO_BOND_STYLE  -12
#define ERROR_BOND_STYLE     -13
#define ERROR_EXECUTE        -14
#define ERROR_NO_AXIS        -15
#define ERROR_BAD_AXIS       -16
#define ERROR_NO_ANGLE       -17
#define ERROR_NO_SHIFT       -18
#define ERROR_BAD_SURFACE    -19
#define ERROR_SLAB           -20
#define ERROR_FADING         -21
#define ERROR_LOG            -22
#define ERROR_FOREGROUND     -23
#define ERROR_BACKGROUND     -24
#define ERROR_NO_COLOR       -25
#define ERROR_TBG            -26
#define ERROR_TFG            -27
#define ERROR_FONT           -28
#define ERROR_NO_FONT_NAME   -29
#define ERROR_BACKBONE       -30
#define ERROR_HYBONDS        -31
#define ERROR_STEREO         -32
#define ERROR_MONO           -33
#define ERROR_POSITION       -34
#define ERROR_TITLE          -35
#define ERROR_LABELS         -36
#define ERROR_CENTER         -37
#define ERROR_SAVE           -38
#define ERROR_TAG            -39
#define ERROR_SEQUENCE       -40
#define ERROR_RAMACHANDRAN   -41
#define ERROR_WHEEL          -42
#define ERROR_ANGLE          -43
#define ERROR_RADIUS         -44
#define ERROR_VENN           -45
#define ERROR_WINDOW         -46
#define ERROR_PLOT           -47
#define ERROR_MOVE           -48
#define ERROR_PLANE          -49
#define ERROR_SCALE          -50
#define ERROR_PATTERN        -51
#define ERROR_DOCK           -52
#define ERROR_COMPARE        -53
#define ERROR_NEIGHBORHOOD   -54
#define ERROR_STRUCTURE      -55
#define ERROR_CREATE         -56
#define ERROR_REPLACE        -57
#define ERROR_EDIT           -58
#define ERROR_MEMBRANE       -59
#define ERROR_SET            -60
#define ERROR_PROBE          -61
#define ERROR_REFRESH        -62
#define ERROR_STICKS         -63
#define ERROR_BALLS          -64
#define ERROR_BLUR           -65
#define ERROR_GROUP          -66
#define ERROR_DUMP           -67


