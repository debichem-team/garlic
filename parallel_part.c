/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

				parallel_part.c

Purpose:
	Find the part of the vector which is parallel with the reference
	vector.

Input:
	(1) Pointer to VectorS (where the result will be stored).
	(2) Pointer to VectorS structure (the reference vector).
	(3) Pointer to VectorS structure (the vector which should be
	    projected).

Output:
	(1) Parallel part of the specified vector.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>

#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

double		ScalarProduct_ (VectorS *, VectorS *);

/*======find parallel part of input vector:==================================*/

int ParallelPart_ (VectorS *parallel_vectorSP,
		   VectorS *reference_vectorSP, VectorS *input_vectorSP)
{
double		abs_value_squared;
double		reciprocal_denominator;
double		scalar_product;
double		scale_factor;

/* Check the squared absolute value of the reference vector: */
abs_value_squared = ScalarProduct_ (reference_vectorSP, reference_vectorSP);
if (abs_value_squared == 0.0) return -1;

/* Prepare the reciprocal denominator: */
reciprocal_denominator = 1.0 / abs_value_squared;

/* Prepare the scalar product of two vectors: */
scalar_product = ScalarProduct_ (input_vectorSP, reference_vectorSP);

/* Prepare the scale factor: */
scale_factor = scalar_product * reciprocal_denominator;

/* Prepare the result: */
parallel_vectorSP->x = scale_factor * reference_vectorSP->x;
parallel_vectorSP->y = scale_factor * reference_vectorSP->y;
parallel_vectorSP->z = scale_factor * reference_vectorSP->z;

/* Return positive value (success indicator): */
return 1;
}

/*===========================================================================*/


