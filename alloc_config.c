/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				alloc_config.c

Purpose:
	Allocate memory for ConfigS structure. The calloc function is used
	to zero-initialize all bytes.

Input:
	No arguments.

Output:
	(1) Memory for ConfigS structure allocated.
	(2) Return value.

Return value:
	(1) Pointer to allocated memory, on success.
	(2) NULL on failure.

========includes:============================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

void		ErrorMessage_ (char *, char *, char *,
			       char *, char *, char *, char *);

/*======allocate memory for ConfigS structure:===============================*/

ConfigS *AllocateConfigS_ (void)
{
static ConfigS		*configSP;

/* Allocate zero-initialized memory: */
configSP = (ConfigS *) calloc (1, sizeof (ConfigS));

if (configSP == NULL)
	{
	ErrorMessage_ ("garlic", "AllocateConfigS_", "",
		"Failed to allocate memory for Config structure!\n",
		"", "", "");
	}

/* Return pointer to allocated memory (NULL on failure): */
return configSP;
}

/*===========================================================================*/


