/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				eat_right_char.c

Purpose:
	Eat right character. Copy the command line to the history buffer.

Input:
	(1) Pointer to RuntimeS structure.

Output:
	(1) One character removed.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======eat right character:=================================================*/

int EatRightChar_ (RuntimeS *runtimeSP)
{
int			comm_length, old_comm_length, carriage_pos;
char			stringA[COMMSTRINGSIZE];
char			*currP;

/* Copy the command string length and carriage (keyboard cursor) position: */
comm_length = runtimeSP->command_length;
old_comm_length = comm_length;
carriage_pos = runtimeSP->carriage_position;

/* Check is there anything on the right side: */
if (carriage_pos >= comm_length) return -1;

/* Reduce the command length: */
comm_length--;

/* Copy the left part: */
strncpy (stringA, runtimeSP->curr_commandA, carriage_pos);
stringA[carriage_pos] = '\0';

/* Copy the right part, but skip the first character: */
strncat (stringA,
	 runtimeSP->curr_commandA + carriage_pos + 1,
	 old_comm_length - carriage_pos - 1);

/* Ensure the proper termination: */
stringA[comm_length] = '\0';

/* Copy the command string: */
strcpy (runtimeSP->curr_commandA, stringA);

/* Set the command length: */
runtimeSP->command_length = comm_length;

/* Copy the command string to the output buffer: */
currP = runtimeSP->commandsP + runtimeSP->next_commandI * COMMSTRINGSIZE;
strcpy (currP, runtimeSP->curr_commandA);

/* Return positive value (success): */
return 1;
}

/*===========================================================================*/


