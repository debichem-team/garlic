/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				atoms_style3.c

Purpose:
	Draw atoms, using style 3 (3x3 square).

Input:
	(1) Pointer to MolComplexS structure.
	(2) Number of macromolecular complexes.
	(3) Pointer to ConfigS structure, with configuration data.
	(4) Pointer to GUIS structure.
	(5) Pointer to NearestAtomS structure, with information about the
	    atom occupying the given pixel.
	(6) The number of pixels in the main window free area.
	(7) The refreshI, used to check the  NearestAtomS associated with
	    a given pixel.

Output:
	(1) Atoms drawn to the hidden pixmap.
	(2) Return value.

Return value:
	(1) The number of atoms drawn.

Notes:
	(1) Indentation is exceptionally 4 spaces.

=============================================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======draw atoms using style 3 (3x3 square):===============================*/

size_t DrawAtomsStyle3_ (MolComplexS *mol_complexSP, int mol_complexesN,
			 ConfigS *configSP, GUIS *guiSP,
			 NearestAtomS *nearest_atomSP, size_t pixelsN,
			 unsigned int refreshI)
{
size_t			atoms_drawnN = 0;
int			imageI, imagesN;
int			left_edge[2], right_edge[2];
int			mol_complexI;
MolComplexS		*curr_mol_complexSP;
size_t			atomsN, atomI;
AtomS			*curr_atomSP;
int			atom_drawnF;
int			screen_x, screen_y;
size_t			pixelI;
NearestAtomS		*curr_pixelSP;
double			z;
int			bondI;

/* Initialize the bond index. The negative value means that pixels */
/* filled  in this function  are not a part of some chemical bond. */
bondI = -1;

/* Number of images: */
if (configSP->stereoF) imagesN = 2;
else imagesN = 1;

/* Left and right image edge (in stereo mode there are two images): */
left_edge[0]  = configSP->image_screen_x0[0];
right_edge[0] = configSP->image_screen_x1[0];
left_edge[1]  = configSP->image_screen_x0[1];
right_edge[1] = configSP->image_screen_x1[1];

/* Draw each macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
    {
    /* Pointer to current macromolecular complex: */
    curr_mol_complexSP = mol_complexSP + mol_complexI;

    /* Prepare and check the number of atoms: */
    atomsN = curr_mol_complexSP->atomsN;
    if (atomsN == 0) continue;

    /* Draw atoms which have the given style: */
    for (atomI = 0; atomI < atomsN; atomI++)
	{
	/* Pointer to the current atom: */
	curr_atomSP = curr_mol_complexSP->atomSP + atomI;

	/* Check style: */
	if (curr_atomSP->raw_atomS.atom_styleI != 3) continue;

	/* Check is atom hidden: */
	if (curr_atomSP->hiddenF) continue;

	/* Check is atom inside slab: */
	if (!curr_atomSP->inside_slabF) continue;

	/* Check is atom inside window: */
	if (!curr_atomSP->inside_windowF) continue;

	/* Reset the flag: */
	atom_drawnF = 0;

/*------pixels which are colored in "left" color:----------------------------*/

	/* Prepare the color: */
	XSetForeground (guiSP->displaySP, guiSP->theGCA[0],
			curr_atomSP->left_colorID);

/*......pixel (-1,0).........................................................*/

	/* Draw one (mono) or two pixels (stereo): */
	for (imageI = 0; imageI < imagesN; imageI++)
            {
	    /* Single pass loop: */
	    do
		{
		/* Prepare the coordinates: */
		screen_x = curr_atomSP->raw_atomS.screen_x[imageI] - 1;
		screen_y = curr_atomSP->raw_atomS.screen_y;

		/* Check is this pixel inside the area reserved for the */
		/* current image (there are two images in stereo mode): */
		if (screen_x <  left_edge[imageI])  break;
		if (screen_x >= right_edge[imageI]) break;

		/* Prepare index  to the array */
		/* of NearestAtomS structures: */
		pixelI = guiSP->main_win_free_area_width * screen_y + screen_x;

		/* Check the pixel index: */
		if (pixelI >= pixelsN) break;

		/* Pointer to  NearestAtomS struct. */
		/* assigned to current coordinates: */
		curr_pixelSP = nearest_atomSP + pixelI;

		/* Current atom z: */
		z = curr_atomSP->raw_atomS.z[imageI];

		/* Check was  this pixel used  already in */
		/* this drawing step;  if it was, compare */
		/* the z value of the current atom with z */
		/* value previously stored to this pixel: */
		if (refreshI == curr_pixelSP->last_refreshI)
		    {
		    if (z >= curr_pixelSP->z) break;
		    }

		/* If all test were passed, draw the pixel: */
		XDrawPoint (guiSP->displaySP, guiSP->main_hidden_pixmapID,
			    guiSP->theGCA[0], screen_x, screen_y);

		/* Refresh the content of NearestAtomS */
		/* array  associated  with this pixel: */
		curr_pixelSP->styleI = 3;
		curr_pixelSP->last_refreshI = refreshI;
		curr_pixelSP->mol_complexI = mol_complexI;
		curr_pixelSP->atomI = atomI;
		curr_pixelSP->bondI = bondI;
		curr_pixelSP->z = z;
		curr_pixelSP->colorID = curr_atomSP->left_colorID;

		/* Set the flag: */
		atom_drawnF = 1;

		} while (0);
	    }

/*......pixel (-1,-1)........................................................*/

	/* Draw one (mono) or two pixels (stereo): */
	for (imageI = 0; imageI < imagesN; imageI++)
	    {
	    /* Single pass loop: */
	    do
		{
		/* Prepare the coordinates: */
		screen_x = curr_atomSP->raw_atomS.screen_x[imageI] - 1;
		screen_y = curr_atomSP->raw_atomS.screen_y - 1;

		/* Check is this pixel inside the area reserved for the */
		/* current image (there are two images in stereo mode): */
		if (screen_x < left_edge[imageI]) break;
		if (screen_x > right_edge[imageI]) break;

		/* Prepare index  to the array */
		/* of NearestAtomS structures: */
		pixelI = guiSP->main_win_free_area_width * screen_y + screen_x;

		/* Check the pixel index: */
		if (pixelI >= pixelsN) break;

		/* Pointer to  NearestAtomS struct. */
		/* assigned to current coordinates: */
		curr_pixelSP = nearest_atomSP + pixelI;

		/* Current atom z: */
		z = curr_atomSP->raw_atomS.z[imageI];

		/* Check was  this pixel used  already in */
		/* this drawing step;  if it was, compare */
		/* the z value of the current atom with z */
		/* value previously stored to this pixel: */
		if (refreshI == curr_pixelSP->last_refreshI)
		    {
		    if (z >= curr_pixelSP->z) break;
		    }

		/* If all test were passed, draw the pixel: */
		XDrawPoint (guiSP->displaySP, guiSP->main_hidden_pixmapID,
			    guiSP->theGCA[0], screen_x, screen_y);

		/* Refresh the content of NearestAtomS */
		/* array  associated  with this pixel: */
		curr_pixelSP->styleI = 3;
		curr_pixelSP->last_refreshI = refreshI;
		curr_pixelSP->mol_complexI = mol_complexI;
		curr_pixelSP->atomI = atomI;
		curr_pixelSP->bondI = bondI;
		curr_pixelSP->z = z;
		curr_pixelSP->colorID = curr_atomSP->left_colorID;

		/* Set the flag: */
		atom_drawnF = 1;

		} while (0);
	    }

/*......pixel (0,-1).........................................................*/

	/* Draw one (mono) or two pixels (stereo): */
	for (imageI = 0; imageI < imagesN; imageI++)
	    {
	    /* Single pass loop: */
	    do
		{
		/* Prepare the coordinates: */
		screen_x = curr_atomSP->raw_atomS.screen_x[imageI];
		screen_y = curr_atomSP->raw_atomS.screen_y - 1;

		/* Check is this pixel inside the area reserved for the */
		/* current image (there are two images in stereo mode): */
		if (screen_x < left_edge[imageI]) break;
		if (screen_x > right_edge[imageI]) break;

		/* Prepare index  to the array */
		/* of NearestAtomS structures: */
		pixelI = guiSP->main_win_free_area_width * screen_y + screen_x;

		/* Check the pixel index: */
		if (pixelI >= pixelsN) break;

		/* Pointer to  NearestAtomS struct. */
		/* assigned to current coordinates: */
		curr_pixelSP = nearest_atomSP + pixelI;

		/* Current atom z: */
		z = curr_atomSP->raw_atomS.z[imageI];

		/* Check was  this pixel used  already in */
		/* this drawing step;  if it was, compare */
		/* the z value of the current atom with z */
		/* value previously stored to this pixel: */
		if (refreshI == curr_pixelSP->last_refreshI)
		    {
		    if (z >= curr_pixelSP->z) break;
		    }

		/* If all test were passed, draw the pixel: */
		XDrawPoint (guiSP->displaySP, guiSP->main_hidden_pixmapID,
			    guiSP->theGCA[0], screen_x, screen_y);

		/* Refresh the content of NearestAtomS */
		/* array  associated  with this pixel: */
		curr_pixelSP->styleI = 3;
		curr_pixelSP->last_refreshI = refreshI;
		curr_pixelSP->mol_complexI = mol_complexI;
		curr_pixelSP->atomI = atomI;
		curr_pixelSP->bondI = bondI;
		curr_pixelSP->z = z;
		curr_pixelSP->colorID = curr_atomSP->left_colorID;

		/* Set the flag: */
		atom_drawnF = 1;

		} while (0);
	    }

/*------pixels which are colored in "middle" color:--------------------------*/

	/* Prepare the color: */
	XSetForeground (guiSP->displaySP, guiSP->theGCA[1],
			curr_atomSP->middle_colorID);

/*......pixel (-1,1).........................................................*/

	/* Draw one (mono) or two pixels (stereo): */
	for (imageI = 0; imageI < imagesN; imageI++)
	    {
	    /* Single pass loop: */
	    do
		{
		/* Prepare the coordinates: */
		screen_x = curr_atomSP->raw_atomS.screen_x[imageI] - 1;
		screen_y = curr_atomSP->raw_atomS.screen_y + 1;

		/* Check is this pixel inside the area reserved for the */
		/* current image (there are two images in stereo mode): */
		if (screen_x < left_edge[imageI]) break;
		if (screen_x > right_edge[imageI]) break;

		/* Prepare index  to the array */
		/* of NearestAtomS structures: */
		pixelI = guiSP->main_win_free_area_width * screen_y + screen_x;

		/* Check the pixel index: */
		if (pixelI >= pixelsN) break;

		/* Pointer to  NearestAtomS struct. */
		/* assigned to current coordinates: */
		curr_pixelSP = nearest_atomSP + pixelI;

		/* Current atom z: */
		z = curr_atomSP->raw_atomS.z[imageI];

		/* Check was  this pixel used  already in */
		/* this drawing step;  if it was, compare */
		/* the z value of the current atom with z */
		/* value previously stored to this pixel: */
		if (refreshI == curr_pixelSP->last_refreshI)
		    {
		    if (z >= curr_pixelSP->z) break;
		    }

		/* If all test were passed, draw the pixel: */
		XDrawPoint (guiSP->displaySP, guiSP->main_hidden_pixmapID,
			    guiSP->theGCA[1], screen_x, screen_y);

		/* Refresh the content of NearestAtomS */
		/* array  associated  with this pixel: */
		curr_pixelSP->styleI = 3;
		curr_pixelSP->last_refreshI = refreshI;
		curr_pixelSP->mol_complexI = mol_complexI;
		curr_pixelSP->atomI = atomI;
		curr_pixelSP->bondI = bondI;
		curr_pixelSP->z = z;
		curr_pixelSP->colorID = curr_atomSP->middle_colorID;

		/* Set the flag: */
		atom_drawnF = 1;

		} while (0);
	    }

/*......pixel (0,0)..........................................................*/

	/* Draw one (mono) or two pixels (stereo): */
	for (imageI = 0; imageI < imagesN; imageI++)
	    {
	    /* Single pass loop: */
	    do
		{
		/* Prepare the coordinates: */
		screen_x = curr_atomSP->raw_atomS.screen_x[imageI];
		screen_y = curr_atomSP->raw_atomS.screen_y;

		/* Check is this pixel inside the area reserved for the */
		/* current image (there are two images in stereo mode): */
		if (screen_x < left_edge[imageI]) break;
		if (screen_x > right_edge[imageI]) break;

		/* Prepare index  to the array */
		/* of NearestAtomS structures: */
		pixelI = guiSP->main_win_free_area_width * screen_y + screen_x;

		/* Check the pixel index: */
		if (pixelI >= pixelsN) break;

		/* Pointer to  NearestAtomS struct. */
		/* assigned to current coordinates: */
		curr_pixelSP = nearest_atomSP + pixelI;

		/* Current atom z: */
		z = curr_atomSP->raw_atomS.z[imageI];

		/* Check was  this pixel used  already in */
		/* this drawing step;  if it was, compare */
		/* the z value of the current atom with z */
		/* value previously stored to this pixel: */
		if (refreshI == curr_pixelSP->last_refreshI)
		    {
		    if (z >= curr_pixelSP->z) break;
		    }

		/* If all test were passed, draw the pixel: */
		XDrawPoint (guiSP->displaySP, guiSP->main_hidden_pixmapID,
			    guiSP->theGCA[1], screen_x, screen_y);

		/* Refresh the content of NearestAtomS */
		/* array  associated  with this pixel: */
		curr_pixelSP->styleI = 3;
		curr_pixelSP->last_refreshI = refreshI;
		curr_pixelSP->mol_complexI = mol_complexI;
		curr_pixelSP->atomI = atomI;
		curr_pixelSP->bondI = bondI;
		curr_pixelSP->z = z;
		curr_pixelSP->colorID = curr_atomSP->middle_colorID;

		/* Set the flag: */
		atom_drawnF = 1;

		} while (0);
	    }

/*......pixel (1,-1).........................................................*/

	/* Draw one (mono) or two pixels (stereo): */
	for (imageI = 0; imageI < imagesN; imageI++)
	    {
	    /* Single pass loop: */
	    do
		{
		/* Prepare the coordinates: */
		screen_x = curr_atomSP->raw_atomS.screen_x[imageI] + 1;
		screen_y = curr_atomSP->raw_atomS.screen_y - 1;

		/* Check is this pixel inside the area reserved for the */
		/* current image (there are two images in stereo mode): */
		if (screen_x < left_edge[imageI]) break;
		if (screen_x > right_edge[imageI]) break;

		/* Prepare index  to the array */
		/* of NearestAtomS structures: */
		pixelI = guiSP->main_win_free_area_width * screen_y + screen_x;

		/* Check the pixel index: */
		if (pixelI >= pixelsN) break;

		/* Pointer to  NearestAtomS struct. */
		/* assigned to current coordinates: */
		curr_pixelSP = nearest_atomSP + pixelI;

		/* Current atom z: */
		z = curr_atomSP->raw_atomS.z[imageI];

		/* Check was  this pixel used  already in */
		/* this drawing step;  if it was, compare */
		/* the z value of the current atom with z */
		/* value previously stored to this pixel: */
		if (refreshI == curr_pixelSP->last_refreshI)
		    {
		    if (z >= curr_pixelSP->z) break;
		    }

		/* If all test were passed, draw the pixel: */
		XDrawPoint (guiSP->displaySP, guiSP->main_hidden_pixmapID,
			    guiSP->theGCA[1], screen_x, screen_y);

		/* Refresh the content of NearestAtomS */
		/* array  associated  with this pixel: */
		curr_pixelSP->styleI = 3;
		curr_pixelSP->last_refreshI = refreshI;
		curr_pixelSP->mol_complexI = mol_complexI;
		curr_pixelSP->atomI = atomI;
		curr_pixelSP->bondI = bondI;
		curr_pixelSP->z = z;
		curr_pixelSP->colorID = curr_atomSP->middle_colorID;

		/* Set the flag: */
		atom_drawnF = 1;

		} while (0);
	    }

/*------pixels which are colored in "right" color:---------------------------*/

	/* Prepare the color: */
	XSetForeground (guiSP->displaySP, guiSP->theGCA[2],
			curr_atomSP->right_colorID);

/*......pixel (0,1)..........................................................*/

	/* Draw one (mono) or two pixels (stereo): */
	for (imageI = 0; imageI < imagesN; imageI++)
	    {
	    /* Single pass loop: */
	    do
		{
		/* Prepare the coordinates: */
		screen_x = curr_atomSP->raw_atomS.screen_x[imageI];
		screen_y = curr_atomSP->raw_atomS.screen_y + 1;

		/* Check is this pixel inside the area reserved for the */
		/* current image (there are two images in stereo mode): */
		if (screen_x < left_edge[imageI]) break;
		if (screen_x > right_edge[imageI]) break;

		/* Prepare index  to the array */
		/* of NearestAtomS structures: */
		pixelI = guiSP->main_win_free_area_width * screen_y + screen_x;

		/* Check the pixel index: */
		if (pixelI >= pixelsN) break;

		/* Pointer to  NearestAtomS struct. */
		/* assigned to current coordinates: */
		curr_pixelSP = nearest_atomSP + pixelI;

		/* Current atom z: */
		z = curr_atomSP->raw_atomS.z[imageI];

		/* Check was  this pixel used  already in */
		/* this drawing step;  if it was, compare */
		/* the z value of the current atom with z */
		/* value previously stored to this pixel: */
		if (refreshI == curr_pixelSP->last_refreshI)
		    {
		    if (z >= curr_pixelSP->z) break;
		    }

		/* If all test were passed, draw the pixel: */
		XDrawPoint (guiSP->displaySP, guiSP->main_hidden_pixmapID,
			    guiSP->theGCA[2], screen_x, screen_y);

		/* Refresh the content of NearestAtomS */
		/* array  associated  with this pixel: */
		curr_pixelSP->styleI = 3;
		curr_pixelSP->last_refreshI = refreshI;
		curr_pixelSP->mol_complexI = mol_complexI;
		curr_pixelSP->atomI = atomI;
		curr_pixelSP->bondI = bondI;
		curr_pixelSP->z = z;
		curr_pixelSP->colorID = curr_atomSP->right_colorID;

		/* Set the flag: */
		atom_drawnF = 1;

		} while (0);
	    }

/*......pixel (1,1)..........................................................*/

	/* Draw one (mono) or two pixels (stereo): */
	for (imageI = 0; imageI < imagesN; imageI++)
	    {
	    /* Single pass loop: */
	    do
		{
		/* Prepare the coordinates: */
		screen_x = curr_atomSP->raw_atomS.screen_x[imageI] + 1;
		screen_y = curr_atomSP->raw_atomS.screen_y + 1;

		/* Check is this pixel inside the area reserved for the */
		/* current image (there are two images in stereo mode): */
		if (screen_x < left_edge[imageI]) break;
		if (screen_x > right_edge[imageI]) break;

		/* Prepare index  to the array */
		/* of NearestAtomS structures: */
		pixelI = guiSP->main_win_free_area_width * screen_y + screen_x;

		/* Check the pixel index: */
		if (pixelI >= pixelsN) break;

		/* Pointer to  NearestAtomS struct. */
		/* assigned to current coordinates: */
		curr_pixelSP = nearest_atomSP + pixelI;

		/* Current atom z: */
		z = curr_atomSP->raw_atomS.z[imageI];

		/* Check was  this pixel used  already in */
		/* this drawing step;  if it was, compare */
		/* the z value of the current atom with z */
		/* value previously stored to this pixel: */
		if (refreshI == curr_pixelSP->last_refreshI)
		    {
		    if (z >= curr_pixelSP->z) break;
		    }

		/* If all test were passed, draw the pixel: */
		XDrawPoint (guiSP->displaySP, guiSP->main_hidden_pixmapID,
			    guiSP->theGCA[2], screen_x, screen_y);

		/* Refresh the content of NearestAtomS */
		/* array  associated  with this pixel: */
		curr_pixelSP->styleI = 3;
		curr_pixelSP->last_refreshI = refreshI;
		curr_pixelSP->mol_complexI = mol_complexI;
		curr_pixelSP->atomI = atomI;
		curr_pixelSP->bondI = bondI;
		curr_pixelSP->z = z;
		curr_pixelSP->colorID = curr_atomSP->right_colorID;

		/* Set the flag: */
		atom_drawnF = 1;

		} while (0);
	    }

/*......pixel (1,0)..........................................................*/

	/* Draw one (mono) or two pixels (stereo): */
	for (imageI = 0; imageI < imagesN; imageI++)
	    {
	    /* Single pass loop: */
	    do
		{
		/* Prepare the coordinates: */
		screen_x = curr_atomSP->raw_atomS.screen_x[imageI] + 1;
		screen_y = curr_atomSP->raw_atomS.screen_y;

		/* Check is this pixel inside the area reserved for the */
		/* current image (there are two images in stereo mode): */
		if (screen_x < left_edge[imageI]) break;
		if (screen_x > right_edge[imageI]) break;

		/* Prepare index  to the array */
		/* of NearestAtomS structures: */
		pixelI = guiSP->main_win_free_area_width * screen_y + screen_x;

		/* Check the pixel index: */
		if (pixelI >= pixelsN) break;

		/* Pointer to  NearestAtomS struct. */
		/* assigned to current coordinates: */
		curr_pixelSP = nearest_atomSP + pixelI;

		/* Current atom z: */
		z = curr_atomSP->raw_atomS.z[imageI];

		/* Check was  this pixel used  already in */
		/* this drawing step;  if it was, compare */
		/* the z value of the current atom with z */
		/* value previously stored to this pixel: */
		if (refreshI == curr_pixelSP->last_refreshI)
		    {
		    if (z >= curr_pixelSP->z) break;
		    }

		/* If all test were passed, draw the pixel: */
		XDrawPoint (guiSP->displaySP, guiSP->main_hidden_pixmapID,
			    guiSP->theGCA[2], screen_x, screen_y);

		/* Refresh the content of NearestAtomS */
		/* array  associated  with this pixel: */
		curr_pixelSP->styleI = 3;
		curr_pixelSP->last_refreshI = refreshI;
		curr_pixelSP->mol_complexI = mol_complexI;
		curr_pixelSP->atomI = atomI;
		curr_pixelSP->bondI = bondI;
		curr_pixelSP->z = z;
		curr_pixelSP->colorID = curr_atomSP->right_colorID;

		/* Set the flag: */
		atom_drawnF = 1;

		} while (0);
	    }

/*---------------------------------------------------------------------------*/

	/* Check is at least  one pixel drawn; */
	/* increase the counter if it is true: */
	if (atom_drawnF != 0) atoms_drawnN++;

	}
    }

return atoms_drawnN;
}

/*===========================================================================*/


