/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				skip_keyword.c

Purpose:
	Skip keyword: find the pointer which points to the first character
	which belongs to  the set of separators.  Used to skip keyword and
	reach the content following the keyword. This function is required
	because some keywords may be abbreviated.

Input:
	(1) Pointer to the string which has a keyword at the beginning.
	(2) Pointer to the list (string) of separators.

Output:
	(1) Return value.

Return value:
	(1) Pointer to the remainder of the string, following the keyword.
	(2) NULL on failure.

========includes:============================================================*/

#include <stdio.h>

/*======skip keyword:========================================================*/

char *SkipKeyword_ (char *stringP, char *separatorsP)
{
int		current_char, current_separator;
int		separator_foundF = 0;
char		*P;

/* Check the input string pointer: */
if (stringP == NULL) return NULL;

/* Find the first separator: */
while ((current_char = *stringP++) != '\0')
	{
	/** Search the set of separators: **/
	P = separatorsP;
	separator_foundF = 0;
	while ((current_separator = *P++) != '\0')
		{
		if (current_char == current_separator)
			{
			separator_foundF = 1;
			break;
			}
		}

	/** If the current character belongs to **/
	/** the set of separators, job is done: **/
	if (separator_foundF) break;
	}

/* If end of string is reached  but separator was */
/* not found, there is nothing after the keyword: */
if (!separator_foundF) return NULL;

/* Return the pointer to the remainder of the string: */
return --stringP;
}

/*===========================================================================*/


