/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				alloc_gui.c

Purpose:
	Allocate memory for GUIS structure. The calloc function is used to
	zero-initialize all bytes.

Input:
	No arguments.

Output:
	(1) Memory for GUIS structure allocated.
	(2) Return value.

Return value:
	(1) Pointer to allocated memory, on success.
	(2) NULL on failure.

========includes:============================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

void		ErrorMessage_ (char *, char *, char *,
			       char *, char *, char *, char *);

/*======allocate memory for GUIS structure:==================================*/

GUIS *AllocateGUIS_ (void)
{
static GUIS		*guiSP;

/* Allocate zero-initialized memory: */
guiSP = (GUIS *) calloc (1, sizeof (GUIS));

if (guiSP == NULL)
	{
	ErrorMessage_ ("garlic", "AllocateGUIS_", "",
		"Failed to allocate memory for GUIS structure!\n",
		"", "", "");
	}

/* Return pointer to allocated memory (equal to NULL on failure): */
return guiSP;
}

/*===========================================================================*/


