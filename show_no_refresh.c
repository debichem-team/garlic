/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				show_no_refresh.c

Purpose:
        Show selected atoms but do not refresh graphics.

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) The number of macromolecular complexes.

Output:
	(1) The hiddenF set for each atom in each caught complex.
	(2) Return value.

Return value:
	(1) Always positive (trivial).

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======show selected atoms:=================================================*/

int ShowNoRefresh_ (MolComplexS *mol_complexSP, int mol_complexesN)
{
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;
size_t		atomsN, atomI;
AtomS		*curr_atomSP;

/* Check every macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/** Pointer to the current macromolecular complex: **/
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/** Check is the current macromolecular complex caught: **/
	if (curr_mol_complexSP->catchF == 0) continue;

	/** Number of atoms in a macromolecular complex: **/
	atomsN = curr_mol_complexSP->atomsN;

	/** Scan all atoms in the current complex: **/
	for (atomI = 0; atomI < atomsN; atomI++)
		{
		/** Pointer to the current atom: **/
		curr_atomSP = curr_mol_complexSP->atomSP + atomI;

		/** Check the selection flag; show selected atoms: **/
		if (curr_atomSP->selectedF) curr_atomSP->hiddenF = 0;
		}
	}

/* Return positive value (trivial): */
return 1;
}

/*===========================================================================*/


