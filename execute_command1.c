/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

				execute_command1.c

Purpose:
	Parse and execute command.  Refresh the message  (reset or fill
	with a new content). This function is capable to execute garlic
	scripts.

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) Pointer to the number of macromolecular complexes.
	(3) Pointer to the next macromolecular complex identifier.
	(4) Pointer to RuntimeS structure, with some runtime data.
	(5) Pointer to ConfigS structure, with configuration data.
	(6) Pointer to GUIS structure, with GUI data.
	(7) Pointer to pointer to NearestAtomS structure.
	(8) Pointer to the number of pixels in the main win. free area.
	(9) Pointer to refreshI.

Output:
	(1) The latest command executed.
	(2) Return value.

Return value:
	(1) Positive on success (command code).
	(2) Zero, if command string is empty.
	(3) Negative on failure.

Notes:
	(1) The value of refreshI  (*refreshIP)  may be changed in this
	    function or in some function called by this function. It is
	    checked in  EventLoop_  (see event_loop.c).  It is  assumed
	    that  refreshI  is updated no more  than once in each event
	    handling function. If this is not the case, the care should
	    be taken to avoid refreshI overflow.

	(2) The command string is converted to uppercase. Do not forget
	    that  file names are  case sensitive.  Use the copy  of the
	    original command string to prepare the proper file name.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>
#include <ctype.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		ExecuteScript_ (MolComplexS *, int *, int *,
				RuntimeS *, ConfigS *, GUIS *,
				NearestAtomS **, size_t *, unsigned int *);
char		*ExtractToken_ (char *, int, char *, char *);
int		Load_ (MolComplexS *, int *, int *,
		       RuntimeS *, ConfigS *, GUIS *,
		       NearestAtomS *, size_t, unsigned int *);
int		Discard_ (MolComplexS *, int *, int *,
			  RuntimeS *, ConfigS *, GUIS *,
			  NearestAtomS *, size_t, unsigned int *, char *);
int		Catch_ (MolComplexS *, int,
			RuntimeS *, ConfigS *, GUIS *,
			NearestAtomS *, size_t, unsigned int *, char *);
int		Select_ (MolComplexS *, int,
			 RuntimeS *, ConfigS *, GUIS *,
			 NearestAtomS *, size_t, unsigned int *,
			 char *, int);
int		Show_ (MolComplexS *, int,
		       RuntimeS *, ConfigS *, GUIS *,
		       NearestAtomS *, size_t, unsigned int *);
int		Hide_ (MolComplexS *, int,
		       RuntimeS *, ConfigS *, GUIS *,
		       NearestAtomS *, size_t, unsigned int *);
int		Color_ (MolComplexS *, int,
			RuntimeS *, ConfigS *, GUIS *,
			NearestAtomS *, size_t, unsigned int *, char *);
int		Atoms_ (MolComplexS *, int,
			RuntimeS *, ConfigS *, GUIS *,
			NearestAtomS *, size_t, unsigned int *, char *);
int		Bonds_ (MolComplexS *, int,
			RuntimeS *, ConfigS *, GUIS *,
			NearestAtomS *, size_t, unsigned int *, char *);
int		CommandRotate_ (MolComplexS *, int,
				RuntimeS *, ConfigS *, GUIS *,
				NearestAtomS *, size_t, unsigned int *,
				char *);
int		CommandTranslate_ (MolComplexS *, int,
				   RuntimeS *, ConfigS *, GUIS *,
				   NearestAtomS *, size_t, unsigned int *,
				   char *);
int		CommandSlab_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int *,
			      char *);
int		CommandFading_ (MolComplexS *, int,
				RuntimeS *, ConfigS *, GUIS *,
				NearestAtomS *, size_t, unsigned int *,
				char *);
int		Log_ (ConfigS *, RuntimeS *);
int		Foreground_ (MolComplexS *, int,
			     RuntimeS *, ConfigS *, GUIS *,
			     NearestAtomS *, size_t, unsigned int *);
int		Background_ (MolComplexS *, int,
			     RuntimeS *, ConfigS *, GUIS *,
			     NearestAtomS *, size_t, unsigned int *);
int		TextBackground_ (MolComplexS *, int,
				 RuntimeS *, ConfigS *, GUIS *,
				 NearestAtomS *, size_t, unsigned int *);
int		TextForeground_ (MolComplexS *, int,
				 RuntimeS *, ConfigS *, GUIS *,
				 NearestAtomS *, size_t, unsigned int *);
int		Font_ (MolComplexS *, int,
		       RuntimeS *, ConfigS *, GUIS *,
		       NearestAtomS **, size_t *, unsigned int *);
int		CommandBackbone_ (MolComplexS *, int,
				  RuntimeS *, ConfigS *, GUIS *,
				  NearestAtomS *, size_t, unsigned int *,
				  char *);
int		HydrogenBonds_ (MolComplexS *, int,
				RuntimeS *, ConfigS *, GUIS *,
				NearestAtomS *, size_t, unsigned int *,
				char *);
int		CommandStereo_ (MolComplexS *, int,
				RuntimeS *, ConfigS *, GUIS *,
				NearestAtomS *, size_t, unsigned int *,
				char *);
int		CommandMono_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int *);
int		CommandPosition_ (MolComplexS *, int,
				  RuntimeS *, ConfigS *, GUIS *,
				  NearestAtomS *, size_t, unsigned int *,
				  char *);
int		Title_ (MolComplexS *, int,
			RuntimeS *, ConfigS *, GUIS *,
			NearestAtomS *, size_t, unsigned int *);
int		Labels_ (MolComplexS *, int,
			 RuntimeS *, ConfigS *, GUIS *,
			 NearestAtomS *, size_t, unsigned int *,
			 char *);
int		Register_ (void);
int		Center_ (MolComplexS *, int,
			 RuntimeS *, ConfigS *, GUIS *,
			 NearestAtomS *, size_t, unsigned int *);
int		Save_ (MolComplexS *, int, RuntimeS *);
int		Tag_ (MolComplexS *, int,
		      RuntimeS *, ConfigS *, GUIS *,
		      NearestAtomS *, size_t, unsigned int *);
int		Sequence_ (MolComplexS *, int, RuntimeS *, char *);
int		Ramachandran_ (MolComplexS *, int,
			       RuntimeS *, ConfigS *, GUIS *,
			       NearestAtomS *, size_t, unsigned int *,
			       char *);
int		Wheel_ (MolComplexS *, int,
			RuntimeS *, ConfigS *, GUIS *,
			NearestAtomS *, size_t, unsigned int *,
			char *);
int		Angle_ (RuntimeS *, char *);
int		Radius_ (RuntimeS *, char *);
int		Venn_ (MolComplexS *, int,
		       RuntimeS *, ConfigS *, GUIS *,
		       NearestAtomS *, size_t, unsigned int *,
		       char *);
int		SSB_ (MolComplexS *, int,
		      RuntimeS *, ConfigS *, GUIS *,
		      NearestAtomS *, size_t, unsigned int *, char *);
int		Window_ (RuntimeS *);
int		Plot_ (MolComplexS *, int,
		       RuntimeS *, ConfigS *, GUIS *,
		       NearestAtomS *, size_t, unsigned int *,
		       char *);
int		CommandMove_ (MolComplexS *, int, RuntimeS *, char *);
int		Plane_ (MolComplexS *, int,
			RuntimeS *, ConfigS *, GUIS *,
			NearestAtomS *, size_t, unsigned int *,
			char *);
int		Scale_ (MolComplexS *, int, RuntimeS *, char *);
int		Pattern_ (MolComplexS *, int, RuntimeS *, char *);
int		Dock_ (MolComplexS *, int,
		       RuntimeS *, ConfigS *, GUIS *,
		       NearestAtomS *, size_t, unsigned int *,
		       char *);
int		Compare_ (MolComplexS *, int,
			  RuntimeS *, ConfigS *, GUIS *,
			  NearestAtomS *, size_t, unsigned int *,
			  char *);
int		Neighborhood_ (MolComplexS *, int,
			       RuntimeS *, ConfigS *, GUIS *,
			       NearestAtomS *, size_t, unsigned int *,
			       char *);
int		Structure_ (MolComplexS *, int, RuntimeS *, char *);
int		Create_ (MolComplexS *, int *, int *,
			 RuntimeS *, ConfigS *, GUIS *,
			 NearestAtomS *, size_t, unsigned int *);
int		Replace_ (MolComplexS *, int, int,
			  RuntimeS *, ConfigS *, GUIS *,
			  NearestAtomS *, size_t, unsigned int *);
int		Edit_ (MolComplexS *, int,
		       RuntimeS *, ConfigS *, GUIS *,
		       NearestAtomS *, size_t, unsigned int *,
		       char *);
int		Membrane_ (MolComplexS *, int,
			   RuntimeS *, ConfigS *, GUIS *,
			   NearestAtomS *, size_t, unsigned int *,
			   char *);
int		Set_ (MolComplexS *, int,
		      RuntimeS *, ConfigS *, GUIS *,
		      NearestAtomS *, size_t, unsigned int *,
		      char *);
int		Probe_ (MolComplexS *, int,
			RuntimeS *, ConfigS *, GUIS *,
			NearestAtomS *, size_t, unsigned int *,
			char *);
int		CommandRefresh_ (MolComplexS *, int,
				 RuntimeS *, ConfigS *, GUIS *,
				 NearestAtomS *, size_t, unsigned int *,
				 char *);
int		Sticks_ (MolComplexS *, int,
			 RuntimeS *, ConfigS *, GUIS *,
			 NearestAtomS *, size_t, unsigned int *,
			 char *);
int		Balls_ (MolComplexS *, int,
			RuntimeS *, ConfigS *, GUIS *,
			NearestAtomS *, size_t, unsigned int *,
			char *);
int		Blur_ (MolComplexS *, int,
		       RuntimeS *, ConfigS *, GUIS *,
		       NearestAtomS *, size_t, unsigned int *,
		       char *);
int		Group_ (MolComplexS *, int,
			RuntimeS *, ConfigS *, GUIS *,
			NearestAtomS *, size_t, unsigned int *, char *);
int		Dump_ (MolComplexS *, int, RuntimeS *, char *);

/*======execute command (1):=================================================*/

int ExecuteCommand1_ (MolComplexS *mol_complexSP, int *mol_complexesNP,
		      int *next_mol_complexIDP,
		      RuntimeS *runtimeSP,
		      ConfigS *configSP, GUIS *guiSP,
		      NearestAtomS **nearest_atomSPP, size_t *pixelsNP,
		      unsigned int *refreshIP)
{
char		commandA[STRINGSIZE];
int		command_length;
int		i;
char		*remainderP;
char		tokenA[STRINGSIZE];

/* If the previous command was PAUSE, take a special care: */
if (runtimeSP->pauseF == 1)
	{

	/* Script mode: */
	if (runtimeSP->scriptF)
		{
		return ExecuteScript_ (mol_complexSP, mol_complexesNP,
				       next_mol_complexIDP,
				       runtimeSP, configSP, guiSP,
				       nearest_atomSPP, pixelsNP, refreshIP);
		}
	/* Interactive mode: */
	else
		{
		runtimeSP->pauseF = 0;
		runtimeSP->messageA[0] = '\0';
		runtimeSP->message_length = 0;
		return COMMAND_CONTINUE;
		}
	}

/* Copy both command length and command string: */
command_length = runtimeSP->command_length;
strncpy (commandA, runtimeSP->curr_commandA, runtimeSP->command_length);
commandA[command_length] = '\0';

/* Convert the command string to uppercase: */
for (i = 0; i < command_length; i++) commandA[i] = toupper (commandA[i]);

/* Truncate the message and reset the message length: */
runtimeSP->messageA[0] = '\0';
runtimeSP->message_length = 0;

/* Take the first token - it should be the command word: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, commandA, " \t\n");

/* If there is nothing, return zero - the command string is empty: */
if (remainderP == NULL) return 0; 

/* Parse and execute the command: */

/*------quit, exit:----------------------------------------------------------*/

if ((strstr (tokenA, "QUI") == tokenA) || (strstr (tokenA, "EXI") == tokenA))
	{
	return COMMAND_QUIT;
	}

/*------pause:---------------------------------------------------------------*/

else if (strstr (tokenA, "PAU") == tokenA)
	{
	runtimeSP->pauseF = 1;
	strcpy (runtimeSP->messageA, "Press enter to continue!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return COMMAND_PAUSE;
	}

/*------ignore:--------------------------------------------------------------*/

/* This command does nothing - it is added for debugging purpose! */
else if (strstr (tokenA, "IGN") == tokenA)
	{
	return COMMAND_IGNORE;
	}

/*------select:--------------------------------------------------------------*/

/* Overwrite the previous selection: */
else if (strstr (tokenA, "SEL") == tokenA)
	{
	return Select_ (mol_complexSP, *mol_complexesNP,
			runtimeSP, configSP, guiSP,
			*nearest_atomSPP, *pixelsNP, refreshIP,
			remainderP, 0);
	}

/*------restrict:------------------------------------------------------------*/

/* Restrict the previous selection: */
else if (strstr (tokenA, "RES") == tokenA)
	{
	return Select_ (mol_complexSP, *mol_complexesNP,
			runtimeSP, configSP, guiSP,
			*nearest_atomSPP, *pixelsNP, refreshIP,
			remainderP, 1);
	}

/*------add:-----------------------------------------------------------------*/

/* Expand (add to) the previous selection: */
else if ((strstr (tokenA, "ADD") == tokenA) ||
	 (strstr (tokenA, "EXP") == tokenA))
	{
	return Select_ (mol_complexSP, *mol_complexesNP,
			runtimeSP, configSP, guiSP,
			*nearest_atomSPP, *pixelsNP, refreshIP,
			remainderP, 2);
	}

/*------load, read:----------------------------------------------------------*/

else if ((strstr (tokenA, "LOA") == tokenA) ||
	 (strstr (tokenA, "REA") == tokenA))
	{
	/* The original  command string  should be used */
	/* because the copy was converted to uppercase: */
	return Load_ (mol_complexSP, mol_complexesNP, next_mol_complexIDP,
		      runtimeSP, configSP, guiSP,
		      *nearest_atomSPP, *pixelsNP, refreshIP);
	}

/*------discard:-------------------------------------------------------------*/

else if (strstr (tokenA, "DIS") == tokenA)
	{
	return Discard_ (mol_complexSP, mol_complexesNP, next_mol_complexIDP,
			 runtimeSP, configSP, guiSP,
			 *nearest_atomSPP, *pixelsNP, refreshIP, remainderP);
	}

/*------catch, take:---------------------------------------------------------*/

else if ((strstr (tokenA, "CAT") == tokenA) ||
	 (strstr (tokenA, "TAK") == tokenA))
	{
	return Catch_ (mol_complexSP, *mol_complexesNP,
		       runtimeSP, configSP, guiSP,
		       *nearest_atomSPP, *pixelsNP, refreshIP,
		       remainderP);
	}

/*------show:----------------------------------------------------------------*/

else if (strstr (tokenA, "SHO") == tokenA)
	{
	return Show_ (mol_complexSP, *mol_complexesNP,
		      runtimeSP, configSP, guiSP,
		      *nearest_atomSPP, *pixelsNP, refreshIP);
	}

/*------hide:----------------------------------------------------------------*/

else if (strstr (tokenA, "HID") == tokenA)
	{
	return Hide_ (mol_complexSP, *mol_complexesNP,
		      runtimeSP, configSP, guiSP,
		      *nearest_atomSPP, *pixelsNP, refreshIP);
	}

/*------color:---------------------------------------------------------------*/

else if (strstr (tokenA, "COL") == tokenA)
	{
	return Color_ (mol_complexSP, *mol_complexesNP,
		       runtimeSP, configSP, guiSP,
		       *nearest_atomSPP, *pixelsNP, refreshIP,
		       remainderP);
	}

/*------atoms:---------------------------------------------------------------*/

else if (strstr (tokenA, "ATO") == tokenA)
	{
	return Atoms_ (mol_complexSP, *mol_complexesNP,
		       runtimeSP, configSP, guiSP,
		       *nearest_atomSPP, *pixelsNP, refreshIP,
		       remainderP);
	}

/*------bonds:---------------------------------------------------------------*/

else if (strstr (tokenA, "BON") == tokenA)
	{
	return Bonds_ (mol_complexSP, *mol_complexesNP,
		       runtimeSP, configSP, guiSP,
		       *nearest_atomSPP, *pixelsNP, refreshIP,
		       remainderP);
	}

/*------rotate:--------------------------------------------------------------*/

else if (strstr (tokenA, "ROT") == tokenA)
	{
	return CommandRotate_ (mol_complexSP, *mol_complexesNP,
			       runtimeSP, configSP, guiSP,
			       *nearest_atomSPP, *pixelsNP, refreshIP,
			       remainderP);
	}

/*------translate:-----------------------------------------------------------*/

else if (strstr (tokenA, "TRA") == tokenA)
	{
	return CommandTranslate_ (mol_complexSP, *mol_complexesNP,
				  runtimeSP, configSP, guiSP,
				  *nearest_atomSPP, *pixelsNP, refreshIP,
				  remainderP);
	}

/*------slab:----------------------------------------------------------------*/

else if (strstr (tokenA, "SLA") == tokenA)
	{
	return CommandSlab_ (mol_complexSP, *mol_complexesNP,
			     runtimeSP, configSP, guiSP,
			     *nearest_atomSPP, *pixelsNP, refreshIP,
			     remainderP);
	}

/*------fading:--------------------------------------------------------------*/

else if (strstr (tokenA, "FAD") == tokenA)
	{
	return CommandFading_ (mol_complexSP, *mol_complexesNP,
			       runtimeSP, configSP, guiSP,
			       *nearest_atomSPP, *pixelsNP, refreshIP,
			       remainderP);
	}

/*------log:-----------------------------------------------------------------*/

else if (strstr (tokenA, "LOG") == tokenA)
	{
	/* The original  command string  should be used */
	/* because the copy was converted to uppercase: */
	return Log_ (configSP, runtimeSP);
	}

/*------fg:------------------------------------------------------------------*/

else if ((strcmp (tokenA, "FG") == 0) || (strstr (tokenA, "FOREG") == tokenA))
	{
	/* The original  command string  should be used */
	/* because the copy was converted to uppercase: */
	return Foreground_ (mol_complexSP, *mol_complexesNP,
			    runtimeSP, configSP, guiSP,
			    *nearest_atomSPP, *pixelsNP, refreshIP);
	}

/*------bg:------------------------------------------------------------------*/

else if ((strcmp (tokenA, "BG") == 0) || (strstr (tokenA, "BACKG") == tokenA))
	{
	/* The original  command string  should be used */
	/* because the copy was converted to uppercase: */
	return Background_ (mol_complexSP, *mol_complexesNP,
			    runtimeSP, configSP, guiSP,
			    *nearest_atomSPP, *pixelsNP, refreshIP);
	}

/*------tbg:-----------------------------------------------------------------*/

else if (strcmp (tokenA, "TBG") == 0)
	{
	/* The original  command string  should be used */
	/* because the copy was converted to uppercase: */
	return TextBackground_ (mol_complexSP, *mol_complexesNP,
				runtimeSP, configSP, guiSP,
				*nearest_atomSPP, *pixelsNP, refreshIP);
	}

/*------tfg:-----------------------------------------------------------------*/

else if (strcmp (tokenA, "TFG") == 0)
	{
	/* The original  command string  should be used */
	/* because the copy was converted to uppercase: */
	return TextForeground_ (mol_complexSP, *mol_complexesNP,
				runtimeSP, configSP, guiSP,
				*nearest_atomSPP, *pixelsNP, refreshIP);
	}

/*------font:----------------------------------------------------------------*/

else if (strstr (tokenA, "FON") == tokenA)
	{
	/* The original  command string  should be used */
	/* because the copy was converted to uppercase: */
	return Font_ (mol_complexSP, *mol_complexesNP,
		      runtimeSP, configSP, guiSP,
		      nearest_atomSPP, pixelsNP, refreshIP);
	}

/*------backbone:------------------------------------------------------------*/

else if (strstr (tokenA, "BAC") == tokenA)
        {
	return CommandBackbone_ (mol_complexSP, *mol_complexesNP,
				 runtimeSP, configSP, guiSP,
				 *nearest_atomSPP, *pixelsNP, refreshIP,
				 remainderP);
	}

/*------hybonds:-------------------------------------------------------------*/

else if (strstr (tokenA, "HYB") == tokenA)
	{
	return HydrogenBonds_ (mol_complexSP, *mol_complexesNP,
			       runtimeSP, configSP, guiSP,
			       *nearest_atomSPP, *pixelsNP, refreshIP,
			       remainderP);
	}

/*------stereo:--------------------------------------------------------------*/

else if (strstr (tokenA, "STE") == tokenA)
	{
	return CommandStereo_ (mol_complexSP, *mol_complexesNP,
			       runtimeSP, configSP, guiSP,
			       *nearest_atomSPP, *pixelsNP, refreshIP,
			       remainderP);
	}

/*------mono:----------------------------------------------------------------*/

else if (strstr (tokenA, "MON") == tokenA)
	{
	return CommandMono_ (mol_complexSP, *mol_complexesNP,
			     runtimeSP, configSP, guiSP,
			     *nearest_atomSPP, *pixelsNP, refreshIP);
	}

/*------position:------------------------------------------------------------*/

else if (strstr (tokenA, "POS") == tokenA)
	{
	return CommandPosition_ (mol_complexSP, *mol_complexesNP,
				 runtimeSP, configSP, guiSP,
				 *nearest_atomSPP, *pixelsNP, refreshIP,
				 remainderP);
	}

/*------title:---------------------------------------------------------------*/

else if (strstr (tokenA, "TIT") == tokenA)
	{
	/* The original  command string  should be used */
	/* because the copy was converted to uppercase: */
	return Title_ (mol_complexSP, *mol_complexesNP,
		       runtimeSP, configSP, guiSP,
		       *nearest_atomSPP, *pixelsNP, refreshIP);
	}

/*------labels:--------------------------------------------------------------*/

else if (strstr (tokenA, "LAB") == tokenA)
	{
	return Labels_ (mol_complexSP, *mol_complexesNP,
			runtimeSP, configSP, guiSP,
			*nearest_atomSPP, *pixelsNP, refreshIP,
			remainderP);
	}

/*------register:------------------------------------------------------------*/

else if (strstr (tokenA, "REG") == tokenA)
	{
	strcpy (runtimeSP->messageA,
		"A short e-mail message was send to the author.");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	Register_ ();
	return COMMAND_REGISTER;
	}

/*------center:--------------------------------------------------------------*/

else if (strstr (tokenA, "CEN") == tokenA)
	{
	return Center_ (mol_complexSP, *mol_complexesNP,
			runtimeSP, configSP, guiSP,
			*nearest_atomSPP, *pixelsNP, refreshIP);
	}

/*------save:----------------------------------------------------------------*/

else if (strstr (tokenA, "SAV") == tokenA)
	{
	/* The original  command string  should be used */
	/* because the copy was converted to uppercase: */
	return Save_ (mol_complexSP, *mol_complexesNP, runtimeSP);
	}

/*------tag:-----------------------------------------------------------------*/

else if (strstr (tokenA, "TAG") == tokenA)
	{
	/* The original  command string  should be used */
	/* because the copy was converted to uppercase: */
	return Tag_ (mol_complexSP, *mol_complexesNP,
		     runtimeSP, configSP, guiSP,
		     *nearest_atomSPP, *pixelsNP, refreshIP);
	}

/*------sequence:------------------------------------------------------------*/

else if (strstr (tokenA, "SEQ") == tokenA)
	{
	return Sequence_ (mol_complexSP, *mol_complexesNP,
			  runtimeSP, remainderP);
	}

/*------ramachandran:--------------------------------------------------------*/

else if (strstr (tokenA, "RAM") == tokenA)
	{
	return Ramachandran_ (mol_complexSP, *mol_complexesNP,
			      runtimeSP, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, refreshIP,
			      remainderP);
	}

/*------wheel:---------------------------------------------------------------*/

else if (strstr (tokenA, "WHE") == tokenA)
	{
	return Wheel_ (mol_complexSP, *mol_complexesNP,
		       runtimeSP, configSP, guiSP,
		       *nearest_atomSPP, *pixelsNP, refreshIP,
		       remainderP);
	}

/*------angle:---------------------------------------------------------------*/

else if (strstr (tokenA, "ANG") == tokenA)
	{
	return Angle_ (runtimeSP, remainderP);
	}

/*------radius:--------------------------------------------------------------*/

else if (strstr (tokenA, "RAD") == tokenA)
	{
	return Radius_ (runtimeSP, remainderP);
	}

/*------venn:----------------------------------------------------------------*/

else if (strstr (tokenA, "VEN") == tokenA)
	{
	return Venn_ (mol_complexSP, *mol_complexesNP,
		      runtimeSP, configSP, guiSP,
		      *nearest_atomSPP, *pixelsNP, refreshIP,
		      remainderP);
	}

/*------ssb:-----------------------------------------------------------------*/

else if (strstr (tokenA, "SSB") == tokenA)
	{
	return SSB_ (mol_complexSP, *mol_complexesNP,
		     runtimeSP, configSP, guiSP,
		     *nearest_atomSPP, *pixelsNP, refreshIP,
		     remainderP);
	}

/*------window:--------------------------------------------------------------*/

else if (strstr (tokenA, "WIN") == tokenA)
	{
	return Window_ (runtimeSP);
	}

/*------plot:----------------------------------------------------------------*/

else if (strstr (tokenA, "PLO") == tokenA)
	{
	return Plot_ (mol_complexSP, *mol_complexesNP,
		      runtimeSP, configSP, guiSP,
		      *nearest_atomSPP, *pixelsNP, refreshIP,
		      remainderP);
	}

/*------move:----------------------------------------------------------------*/

else if (strstr (tokenA, "MOV") == tokenA)
	{
	return CommandMove_ (mol_complexSP, *mol_complexesNP,
			     runtimeSP, remainderP);
	}

/*------plane:---------------------------------------------------------------*/

else if (strstr (tokenA, "PLA") == tokenA)
	{
	return Plane_ (mol_complexSP, *mol_complexesNP,
		       runtimeSP, configSP, guiSP,
		       *nearest_atomSPP, *pixelsNP, refreshIP,
		       remainderP);
	}

/*------scale:---------------------------------------------------------------*/

else if (strstr (tokenA, "SCA") == tokenA)
	{
	return Scale_ (mol_complexSP, *mol_complexesNP, runtimeSP, remainderP);
	}

/*------pattern:-------------------------------------------------------------*/

else if (strstr (tokenA, "PAT") == tokenA)
        {
        return Pattern_ (mol_complexSP, *mol_complexesNP,
			 runtimeSP, remainderP);
        }

/*------dock:----------------------------------------------------------------*/

else if (strstr (tokenA, "DOC") == tokenA)
	{
	return Dock_ (mol_complexSP, *mol_complexesNP,
		      runtimeSP, configSP, guiSP,
		      *nearest_atomSPP, *pixelsNP, refreshIP,
		      remainderP);
	}

/*------compare:-------------------------------------------------------------*/

else if (strstr (tokenA, "COM") == tokenA)
	{
	return Compare_ (mol_complexSP, *mol_complexesNP,
			 runtimeSP, configSP, guiSP,
			 *nearest_atomSPP, *pixelsNP, refreshIP,
			 remainderP);
	}

/*------neighborhood:--------------------------------------------------------*/

else if (strstr (tokenA, "NEI") == tokenA)
	{
	return Neighborhood_ (mol_complexSP, *mol_complexesNP,
			      runtimeSP, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, refreshIP,
			      remainderP);
	}

/*------structure:-----------------------------------------------------------*/

else if (strstr (tokenA, "STR") == tokenA)
	{
	return Structure_ (mol_complexSP, *mol_complexesNP,
			   runtimeSP, remainderP);
	}

/*------create:--------------------------------------------------------------*/

else if (strstr (tokenA, "CRE") == tokenA)
	{
	return Create_ (mol_complexSP, mol_complexesNP, next_mol_complexIDP,
			runtimeSP, configSP, guiSP,
			*nearest_atomSPP, *pixelsNP, refreshIP);
	}

/*------replace:-------------------------------------------------------------*/

else if (strstr (tokenA, "REP") == tokenA)
	{
	return Replace_ (mol_complexSP, *mol_complexesNP, *next_mol_complexIDP,
			 runtimeSP, configSP, guiSP,
			 *nearest_atomSPP, *pixelsNP, refreshIP);
	}

/*------edit:----------------------------------------------------------------*/

else if (strstr (tokenA, "EDI") == tokenA)
	{
	return Edit_ (mol_complexSP, *mol_complexesNP,
		      runtimeSP, configSP, guiSP,
		      *nearest_atomSPP, *pixelsNP, refreshIP,
		      remainderP);
	}

/*------membrane:------------------------------------------------------------*/

else if (strstr (tokenA, "MEM") == tokenA)
	{
	return Membrane_ (mol_complexSP, *mol_complexesNP,
			  runtimeSP, configSP, guiSP,
			  *nearest_atomSPP, *pixelsNP, refreshIP,
			  remainderP);
	}

/*------set:-----------------------------------------------------------------*/

else if (strstr (tokenA, "SET") == tokenA)
	{
	return Set_ (mol_complexSP, *mol_complexesNP,
		     runtimeSP, configSP, guiSP,
		     *nearest_atomSPP, *pixelsNP, refreshIP,
		     remainderP);
	}

/*------probe:---------------------------------------------------------------*/

else if (strstr (tokenA, "PRO") == tokenA)
	{
	return Probe_ (mol_complexSP, *mol_complexesNP,
		       runtimeSP, configSP, guiSP,
		       *nearest_atomSPP, *pixelsNP, refreshIP,
		       remainderP);
	}

/*------refresh:-------------------------------------------------------------*/

else if (strstr (tokenA, "REF") == tokenA)
	{
	return CommandRefresh_ (mol_complexSP, *mol_complexesNP,
				runtimeSP, configSP, guiSP,
				*nearest_atomSPP, *pixelsNP, refreshIP,
				remainderP);
	}

/*------sticks:--------------------------------------------------------------*/

else if (strstr (tokenA, "STI") == tokenA)
	{
	return Sticks_ (mol_complexSP, *mol_complexesNP,
			runtimeSP, configSP, guiSP,
			*nearest_atomSPP, *pixelsNP, refreshIP,
			remainderP);
	}

/*------balls:---------------------------------------------------------------*/

else if (strstr (tokenA, "BAL") == tokenA)
        {
        return Balls_ (mol_complexSP, *mol_complexesNP,
		       runtimeSP, configSP, guiSP,
		       *nearest_atomSPP, *pixelsNP, refreshIP,
		       remainderP);
        }

/*------blur:----------------------------------------------------------------*/

else if (strstr (tokenA, "BLU") == tokenA)
	{
	return Blur_ (mol_complexSP, *mol_complexesNP,
		      runtimeSP, configSP, guiSP,
		      *nearest_atomSPP, *pixelsNP, refreshIP,
		      remainderP);
	}

/*------group:---------------------------------------------------------------*/

else if (strstr (tokenA, "GRO") == tokenA)
	{
	return Group_ (mol_complexSP, *mol_complexesNP,
		       runtimeSP, configSP, guiSP,
		       *nearest_atomSPP, *pixelsNP, refreshIP,
		       remainderP);
	}

/*------dump:----------------------------------------------------------------*/

else if (strstr (tokenA, "DUM") == tokenA)
	{
	return Dump_ (mol_complexSP, *mol_complexesNP, runtimeSP, remainderP);
	}

/*------execute:-------------------------------------------------------------*/

else if (strstr (tokenA, "EXE") == tokenA)
	{
	/* The original  command string  should be used */
	/* because the copy was converted to uppercase: */
	return ExecuteScript_ (mol_complexSP, mol_complexesNP,
			       next_mol_complexIDP,
			       runtimeSP, configSP, guiSP,
			       nearest_atomSPP, pixelsNP, refreshIP);
	}

/*------end of command recognition section.----------------------------------*/

/* If this point is reached, the command was not recognized! */

/* Prepare error message: */
strcpy (runtimeSP->messageA, "Command not recognized!");
runtimeSP->message_length = strlen (runtimeSP->messageA);

/* Return the error code: */
return ERROR_UNREC_COMM;
}

/*===========================================================================*/


