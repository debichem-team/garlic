/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				count_residues.c

Purpose:
	Count residues.

Input:
	(1) Pointer to  MolComplexS structure,  with macromolecular data.

Output:
	(1) Return value.

Return value:
	(1) The number of residues (positive or zero).

Notes:
	(1) The function which calls this function should check are there
	    any atoms at all. Thus, there should be at least one atom.

	(2) Some hetero atoms may be counted  as separate residues.  This
	    is typical for solvent molecules.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======count residues:======================================================*/

size_t CountResidues_ (MolComplexS *mol_complexSP)
{
size_t		residuesN = 0;
size_t		atomI, atomsN;
RawAtomS	*raw_atomSP;
int		previous_residueI = -9999, current_residueI;
int		previous_insertion_code = '\0', current_insertion_code;

/* Prepare the number of atoms: */
atomsN = mol_complexSP->atomsN;

/* Scan the macromolecular complex: */
for (atomI = 0; atomI < atomsN; atomI++)
	{
	/* Pointer to raw atomic data: */
	raw_atomSP = &(mol_complexSP->atomSP + atomI)->raw_atomS;

	/* Copy the residue sequence number and residue insertion code: */
	current_residueI = raw_atomSP->residue_sequenceI;
	current_insertion_code = raw_atomSP->residue_insertion_code;

	/* Compare the current residue sequence number with the old one */
	/* and  the current residue insertion code  with the  old code: */
	if ((current_residueI == previous_residueI) &&
	    (current_insertion_code == previous_insertion_code)) continue;

	/* If this point is reached,  the current sequence number */
	/* is different from the previous one, or insertion codes */
	/* are different. This means that a new residue is found: */
	residuesN++;

	/* Update the residue sequence index and residue insertion code: */
	previous_residueI = current_residueI;
	previous_insertion_code = current_insertion_code;
	}

/* Return the number of residues: */
return residuesN;
}

/*===========================================================================*/


