/* Copyright (C) 2001-2003 Damir Zucic */

/*=============================================================================

			   save_sec_structure.c

Purpose:
	Save  secondary structure to  the specified file.  This function
	outputs one letter secondary structure codes in a format similar
	to FASTA.

Input:
	(1) Pointer to RuntimeS structure.

Output:
	(1) Secondary structure stored to the specified file.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);

/*======save secondary structure to file:====================================*/

int SaveSecondaryStructure_ (RuntimeS *runtimeSP)
{
char		lineA[STRINGSIZE];
char		*remainderP;
char		tokenA[STRINGSIZE];
FILE		*fileP;
int		codeI;
int		code;

/* Copy the original command string: */
strncpy (lineA, runtimeSP->curr_commandA, STRINGSIZE - 1);
lineA[STRINGSIZE - 1] = '\0';

/* Skip two tokens: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, lineA, " \t\n");
if (!remainderP) return -1;
remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, " \t\n");
if (!remainderP) return -2;

/* The third token should contain the file name: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, " \t\n");
if (!remainderP)
	{
	strcpy (runtimeSP->messageA, "File name missing!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return -3;
	}

/* Try to open file: */
fileP = fopen (tokenA, "w");
if (fileP == NULL)
	{
	strcpy (runtimeSP->messageA, "Failed to open file!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return -4;
	}

/* Write the first line to the output file: */
fprintf (fileP, ">1xxx     %d codes\n", runtimeSP->sec_structure_length);

/* Write secondary structure codes to file: */
for (codeI = 0; codeI < (int) runtimeSP->sec_structure_length; codeI++)
	{
	/* Prepare the current secondary structure code: */
	code = *(runtimeSP->sec_structureP + codeI);

	/* Print residue name: */
	fprintf (fileP, "%c", code);

	/* Add newline after each 75 residues: */
	if (((codeI + 1) % 75) == 0) fprintf (fileP, "\n");
	}

/* Add newline at the end of file: */
fprintf (fileP, "\n");

/* Close file: */
fclose (fileP);

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


