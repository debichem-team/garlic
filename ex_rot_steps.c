/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

				ex_rot_steps.c

Purpose:
	Extract rotation steps (five doubles) from a string.

Input:
	(1) Pointer to ConfigS structure, where values will be stored.
	(2) Input string pointer.

Output:
	Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======extract rotation steps from a string:================================*/

int ExtractRotationSteps_ (ConfigS *configSP, char *sP)
{
char		*P0, *P1;
int		n;
double		d1, d2, d3, d4, d5;

/* Colon should be separator: */
if ((P0 = strstr (sP, ":")) == NULL) P0 = sP;

/* Replace each non-numeric character (except */
/* minus sign and  decimal point) with space: */
P1 = P0;
while ((n = *P1++) != '\0')
	{
	if (!isdigit (n) && (n != '-') && (n != '.')) *(P1 - 1) = ' ';
	}

/* Try to read five doubles: */
if (sscanf (P0, "%lf %lf %lf %lf %lf", &d1, &d2, &d3, &d4, &d5) != 5)
	{
	return -1;
	}

/* Convert degrees to radians: */
d1 *= DEG_TO_RAD;
d2 *= DEG_TO_RAD;
d3 *= DEG_TO_RAD;
d4 *= DEG_TO_RAD;
d5 *= DEG_TO_RAD;

/* Copy rotation steps to ConfigS: */
configSP->rotation_stepA[0] = d1;
configSP->rotation_stepA[1] = d2;
configSP->rotation_stepA[2] = d3;
configSP->rotation_stepA[3] = d4;
configSP->rotation_stepA[4] = d5;

/* If everything worked fine, return positive number: */
return 1;
}

/*===========================================================================*/


