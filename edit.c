/* Copyright (C) 2001, 2002 Damir Zucic */

/*=============================================================================

				edit.c

Purpose:
	Execute edit command: set the editing mode index. The actual editing
	is not done here.  The functions which are doing this job are called
	by the functions which handle KeyPress events.  If using the keyword
	BOND, two atomic serial numbers have to be specified.

Input:
	(1) Pointer to MolComplexS structure.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to ConfigS structure.
	(5) Pointer to GUIS structure.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
	(9) Pointer to  the remainder of  the  command  string.  The command
	    edit must be accompanied by one of the following keywords:  OFF,
	    ATOMS, PHI, PSI, OMEGA, MAIN, BOND, SIDE, DIMENSIONS  or  CLICK.
	    Long  keywords  may be  abbreviated  to  three  characters.  The
	    keyword  BOND  should be followed  by the serial numbers  of two
	    atoms which form the bond.

Output:
	(1) The specified editing mode switched on or off.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

Notes:
	(1) The keyword  BOND  always forces editing of  a single bond.  The
	    keywords PHI, PSI, OMEGA, MAIN and SIDE may be used to edit more
	    than one bond. The flag edit_single_bondF decides how many bonds
	    should be modified.

	(2) The keyword  DIMENSIONS may be used to edit special objects.  It
	    is not suitable  for molecular structures.  It should be used to
	    change  the size of the object  which consists  of special atoms
	    (quarkonium and jellium).

	(3) The keyword CLICK is used to set the click_modeI index.  If this
	    index is set to one,  the pointer  may be used  to pick one bond
	    for editing. The command EDIT CLICK resets the edit_modeI index.
	    All other combinations reset the click_modeI to zero.

	(4) Each call to this function resets the edit mode index.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
int		ExtractTwoIntegers_ (int *, int *, char *);
int		BondPartners_ (RuntimeS *,
			       MolComplexS *, ResidueS *, char *, char *);
int		AreAtomsBound_ (size_t *, size_t *,
				MolComplexS *, int, int, int);
int		IsPartOfRing_ (MolComplexS *, int, size_t, size_t);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute edit command:================================================*/

int Edit_ (MolComplexS *mol_complexSP, int mol_complexesN,
	   RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
	   NearestAtomS *nearest_atomSP, size_t pixelsN,
	   unsigned int *refreshIP, char *stringP)
{
int			old_edit_modeI;
char			*remainderP;
char			tokenA[SHORTSTRINGSIZE];
MolComplexS		*default_mol_complexSP;
int			residuesN, residueI;
int			selected_residue_foundF;
static ResidueS		*curr_residueSP;
static size_t		startI, endI, atomI;
AtomS			*atomSP;
int			n;
ResidueS		*previous_residueSP;
size_t			previous_startI, previous_endI;
int			atom1_serialI, atom2_serialI;
size_t			atom1_arrayI, atom2_arrayI;
AtomS			*atom1SP, *atom2SP;
char			*atom1_pure_nameP, *atom2_pure_nameP;
int			atom_foundF;

/* Copy the old edit_modeI: */
old_edit_modeI = runtimeSP->edit_modeI;

/* Each call to this function resets the edit_modeI: */
runtimeSP->edit_modeI = 0;

/* Reset the edit_single_bondF flag: */
runtimeSP->edit_single_bondF = 0;

/* Reset the click_modeI; CLICK is the only */
/* keyword which may set this index to one: */
runtimeSP->click_modeI = 0;

/* Extract the first token: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, stringP, " \t\n");

/* If not available, inform user that some keyword is mandatory: */
if (!remainderP)
	{
	strcpy (runtimeSP->messageA,
		"What do you want to edit?");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	if (old_edit_modeI != 0)
		{
		(*refreshIP)++;
		MainRefresh_ (mol_complexSP, mol_complexesN,
			      runtimeSP, configSP, guiSP,
			      nearest_atomSP, pixelsN, *refreshIP);
		ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
				 configSP, guiSP);
		}
	return ERROR_EDIT;
	}

/* If this point is reached, the keyword is present. */

/* Prepare the pointer to default macromolecular complex: */
default_mol_complexSP = mol_complexSP + runtimeSP->default_complexI;

/* The number of residues in default macromolecular complex: */
residuesN = default_mol_complexSP->residuesN;

/* Check is the keyword OFF specified: */
if (strstr (tokenA, "OFF") == tokenA)
	{
	/* Set the edit mode index: */
	runtimeSP->edit_modeI = 0;
	}

/* Keyword ATOMS: */
else if (strstr (tokenA, "ATO") == tokenA)
	{
	/* Set the edit mode index: */
	runtimeSP->edit_modeI = 1;
	}

/* Keyword PHI: */
else if (strstr (tokenA, "PHI") == tokenA)
	{
	/* Find the first selected residue: */
	selected_residue_foundF = 0;
	for (residueI = 0; residueI < residuesN; residueI++)
		{
		/* Pointer to the current residue: */
		curr_residueSP = default_mol_complexSP->residueSP + residueI;

		/* Pointer to the first atom: */
		atomSP = default_mol_complexSP->atomSP +
			 curr_residueSP->residue_startI;

		/* If the first atom is not selected, do */
		/* not treat  this residue  as selected: */
		if (atomSP->selectedF)
			{
			selected_residue_foundF = 1;
			break;
			}
		}

	/* At least one residue should be selected: */
	if (!selected_residue_foundF)
		{
		strcpy (runtimeSP->messageA,
			"At least one residue should be selected!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		if (old_edit_modeI != 0)
			{
			(*refreshIP)++;
			MainRefresh_ (mol_complexSP, mol_complexesN,
				      runtimeSP, configSP, guiSP,
				      nearest_atomSP, pixelsN, *refreshIP);
			ControlRefresh_ (mol_complexSP +
					 runtimeSP->default_complexI,
					 configSP, guiSP);
			}
		return ERROR_EDIT;
		}

	/* Extract serial numbers and array indices of */
	/* the atoms which form this bond  (N and CA): */
	n = BondPartners_ (runtimeSP,
			   default_mol_complexSP, curr_residueSP, "N", "CA");
	if (n < 0)
		{
		strcpy (runtimeSP->messageA,
			"Unable to extract data for atoms N and CA!");
	 	runtimeSP->message_length = strlen (runtimeSP->messageA);
		if (old_edit_modeI != 0)
			{
			(*refreshIP)++;
			MainRefresh_ (mol_complexSP, mol_complexesN,
				      runtimeSP, configSP, guiSP,
				      nearest_atomSP, pixelsN, *refreshIP);
			ControlRefresh_ (mol_complexSP +
					 runtimeSP->default_complexI,
					 configSP, guiSP);
			}
		return ERROR_EDIT;
		}

	/* Set the edit mode index: */
	runtimeSP->edit_modeI = 2;
	}

/* Keyword PSI: */
else if (strstr (tokenA, "PSI") == tokenA)
	{
	/* Find the first selected residue: */
	selected_residue_foundF = 0;
	for (residueI = 0; residueI < residuesN; residueI++)
		{
		/* Pointer to the current residue: */
		curr_residueSP = default_mol_complexSP->residueSP + residueI;

		/* Pointer to the first atom: */
		atomSP = default_mol_complexSP->atomSP +
			 curr_residueSP->residue_startI;

		/* If the first atom is not selected, do */
		/* not treat  this residue  as selected: */
		if (atomSP->selectedF)
			{
			selected_residue_foundF = 1;
			break;
			}
		}

	/* At least one residue should be selected: */
	if (!selected_residue_foundF)
		{
		strcpy (runtimeSP->messageA,
			"At least one residue should be selected!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		if (old_edit_modeI != 0)
			{
			(*refreshIP)++;
			MainRefresh_ (mol_complexSP, mol_complexesN,
				      runtimeSP, configSP, guiSP,
				      nearest_atomSP, pixelsN, *refreshIP);
			ControlRefresh_ (mol_complexSP +
					 runtimeSP->default_complexI,
					 configSP, guiSP);
			}
		return ERROR_EDIT;
		}

	/* Extract serial numbers and array indices of */
	/* the atoms which form this bond  (CA and C): */
	n = BondPartners_ (runtimeSP,
			   default_mol_complexSP, curr_residueSP, "CA", "C");
	if (n < 0)
		{
		strcpy (runtimeSP->messageA,
			"Unable to extract data for atoms CA and C!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		if (old_edit_modeI != 0)
			{
			(*refreshIP)++;
			MainRefresh_ (mol_complexSP, mol_complexesN,
				      runtimeSP, configSP, guiSP,
				      nearest_atomSP, pixelsN, *refreshIP);
			ControlRefresh_ (mol_complexSP +
					 runtimeSP->default_complexI,
					 configSP, guiSP);
			}
		return ERROR_EDIT;
		}

	/* Set the edit mode index: */
	runtimeSP->edit_modeI = 3;
	}

/* Keyword OMEGA: */
else if (strstr (tokenA, "OME") == tokenA)
	{
	/* Find the first selected residue, but skip the first one: */
	selected_residue_foundF = 0;
	for (residueI = 1; residueI < residuesN; residueI++)
		{
		/* Pointer to the current residue: */
		curr_residueSP = default_mol_complexSP->residueSP + residueI;

		/* Prepare the atomic index range: */
		startI = curr_residueSP->residue_startI;
		endI   = curr_residueSP->residue_endI;

		/* Pointer to the first atom: */
		atomSP = default_mol_complexSP->atomSP +
			 curr_residueSP->residue_startI;

		/* If the first atom is not selected, do */
		/* not treat  this residue  as selected: */
		if (atomSP->selectedF)
			{
			selected_residue_foundF = 1;
			break;
			}
		}

	/* At least one residue should be selected: */
	if (!selected_residue_foundF)
		{
		strcpy (runtimeSP->messageA,
			"Select at least one residue");
		strcat (runtimeSP->messageA,
			" (but not the first one)!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		if (old_edit_modeI != 0)
			{
			(*refreshIP)++;
			MainRefresh_ (mol_complexSP, mol_complexesN,
				      runtimeSP, configSP, guiSP,
				      nearest_atomSP, pixelsN, *refreshIP);
			ControlRefresh_ (mol_complexSP +
					 runtimeSP->default_complexI,
					 configSP, guiSP);
			}
		return ERROR_EDIT;
		}

	/* If this point is reached, the current residue is selected. */
	/* To define the C-N bond,  the previous residue is required. */

	/* Prepare the pointer to the previous residue: */
	previous_residueSP = default_mol_complexSP->residueSP + residueI - 1;

	/* Prepare the atomic index range: */
	previous_startI = previous_residueSP->residue_startI;
	previous_endI   = previous_residueSP->residue_endI;

	/* Prepare the array and serial indices for C */
	/* and N atom. C atom belongs to the previous */
	/* residue  and  N  to  the current  residue. */

        /* Find the serial number of C atom of the previous residue: */

	/* Try to find atom: */
	atom_foundF = 0;
	for (atomI = previous_startI; atomI <= previous_endI; atomI++)
		{
		/* Pointer to the current atom: */
		atomSP = mol_complexSP->atomSP + atomI;

		/* Check the purified name of this atom: */
		if (strcmp (atomSP->raw_atomS.pure_atom_nameA, "C") == 0)
			{
			atom1_serialI = atomSP->raw_atomS.serialI;
			atom1_arrayI = atomI;
			atom_foundF = 1;
			break;
			}
		}

	/* If C atom is missing write error message and return: */
	if (!atom_foundF)
		{
		strcpy (runtimeSP->messageA,
			"C atom (required for C-N bond) is missing!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		if (old_edit_modeI != 0)
			{
			(*refreshIP)++;
			MainRefresh_ (mol_complexSP, mol_complexesN,
				      runtimeSP, configSP, guiSP,
				      nearest_atomSP, pixelsN, *refreshIP);
			ControlRefresh_ (mol_complexSP +
					 runtimeSP->default_complexI,
					 configSP, guiSP);
			}
		return ERROR_EDIT;
		}

	/* The serial number of N atom of the current residue: */
	atom_foundF = 0;
	for (atomI = startI; atomI <= endI; atomI++)
		{
		/* Pointer to the current atom: */
		atomSP = mol_complexSP->atomSP + atomI;

		/* Check the purified name of this atom: */
		if (strcmp (atomSP->raw_atomS.pure_atom_nameA, "N") == 0)
			{
			atom2_serialI = atomSP->raw_atomS.serialI;
			atom2_arrayI = atomI;
			atom_foundF = 1;
			break;
			}
		}

	/* If N atom is missing write error message and return: */
	if (!atom_foundF)
		{
		strcpy (runtimeSP->messageA,
			"N atom (required for C-N bond) is missing!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		if (old_edit_modeI != 0)
			{
			(*refreshIP)++;
			MainRefresh_ (mol_complexSP, mol_complexesN,
				      runtimeSP, configSP, guiSP,
				      nearest_atomSP, pixelsN, *refreshIP);
			ControlRefresh_ (mol_complexSP +
					 runtimeSP->default_complexI,
					 configSP, guiSP);
			}
		return ERROR_EDIT;
		}

        /* Store the serial numbers and the array indices: */
        runtimeSP->atom1_serialI = atom1_serialI;
        runtimeSP->atom2_serialI = atom2_serialI;
        runtimeSP->atom1_arrayI  = atom1_arrayI;
        runtimeSP->atom2_arrayI  = atom2_arrayI;

	/* Set the edit mode index: */
	runtimeSP->edit_modeI = 4;
	}

/* Keyword MAIN: */
else if (strstr (tokenA, "MAI") == tokenA)
	{
	/* Find the first selected residue: */
	selected_residue_foundF = 0;
	for (residueI = 0; residueI < residuesN; residueI++)
		{
		/* Pointer to the current residue: */
		curr_residueSP = default_mol_complexSP->residueSP + residueI;

		/* Pointer to the first atom: */
		atomSP = default_mol_complexSP->atomSP +
			 curr_residueSP->residue_startI;

		/* If the first atom is not selected, do */
		/* not treat  this residue  as selected: */
		if (atomSP->selectedF)
			{
			selected_residue_foundF = 1;
			break;
			}
		}

	/* At least one residue should be selected: */
	if (!selected_residue_foundF)
		{
		strcpy (runtimeSP->messageA,
			"At least one residue should be selected!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		if (old_edit_modeI != 0)
			{
			(*refreshIP)++;
			MainRefresh_ (mol_complexSP, mol_complexesN,
				      runtimeSP, configSP, guiSP,
				      nearest_atomSP, pixelsN, *refreshIP);
			ControlRefresh_ (mol_complexSP +
					 runtimeSP->default_complexI,
					 configSP, guiSP);
			}
		return ERROR_EDIT;
		}

	/* Extract the serial numbers  and the array indices */
	/* of N and CA atoms for the first selected residue: */
	n = BondPartners_ (runtimeSP,
			   default_mol_complexSP, curr_residueSP, "N", "CA");
	if (n < 0)
		{
		strcpy (runtimeSP->messageA,
			"Unable to extract data for atoms N and CA!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		if (old_edit_modeI != 0)
			{
			(*refreshIP)++;
			MainRefresh_ (mol_complexSP, mol_complexesN,
				      runtimeSP, configSP, guiSP,
				      nearest_atomSP, pixelsN, *refreshIP);
			ControlRefresh_ (mol_complexSP +
					 runtimeSP->default_complexI,
					 configSP, guiSP);
			}
		return ERROR_EDIT;
		}

	/* Set the edit mode index: */
	runtimeSP->edit_modeI = 5;
	}

/* Keyword BOND: */
else if (strstr (tokenA, "BON") == tokenA)
	{
	/* Try to extract serial numbers for two atoms: */
	if (ExtractTwoIntegers_ (&atom1_serialI, &atom2_serialI, stringP) < 0)
		{
		strcpy (runtimeSP->messageA,
			"Failed to extract serial numbers");
		strcat (runtimeSP->messageA,
			" of two atoms which form the bond!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		if (old_edit_modeI != 0)
			{
			(*refreshIP)++;
			MainRefresh_ (mol_complexSP, mol_complexesN,
				      runtimeSP, configSP, guiSP,
				      nearest_atomSP, pixelsN, *refreshIP);
			ControlRefresh_ (mol_complexSP +
					 runtimeSP->default_complexI,
					 configSP, guiSP);
			}
		return ERROR_EDIT;
		}

	/* Check signs: */
	if ((atom1_serialI < 0) || (atom2_serialI < 0))
		{
		strcpy (runtimeSP->messageA, "Both indices must be positive!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		if (old_edit_modeI != 0)
			{
			(*refreshIP)++;
			MainRefresh_ (mol_complexSP, mol_complexesN,
				      runtimeSP, configSP, guiSP,
				      nearest_atomSP, pixelsN, *refreshIP);
			ControlRefresh_ (mol_complexSP +
					 runtimeSP->default_complexI,
					 configSP, guiSP);
			}
		return ERROR_EDIT;
		}

	/* Check are the atoms specified by the */
	/* given  serial numbers  bound at all: */
	n = AreAtomsBound_ (&atom1_arrayI, &atom2_arrayI,
			    mol_complexSP, runtimeSP->default_complexI,
			    atom1_serialI, atom2_serialI);
	if (n < 0)
		{
		strcpy (runtimeSP->messageA,
			"The specified atoms are not covalently bound!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		if (old_edit_modeI != 0)
			{
			(*refreshIP)++;
			MainRefresh_ (mol_complexSP, mol_complexesN,
				      runtimeSP, configSP, guiSP,
				      nearest_atomSP, pixelsN, *refreshIP);
			ControlRefresh_ (mol_complexSP +
					 runtimeSP->default_complexI,
					 configSP, guiSP);
			}
		return ERROR_EDIT;
		}

	/* Check is it possible to rotate the specified bond. */
	/* It is not possible to edit  the bond which is part */
	/* of a ring or some other closed structural element. */
	n = IsPartOfRing_ (mol_complexSP, runtimeSP->default_complexI,
			   atom1_arrayI, atom2_arrayI);
	if (n > 0)
		{
		strcpy (runtimeSP->messageA,
			"The specified bond is part of a ring!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		if (old_edit_modeI != 0)
			{
			(*refreshIP)++;
			MainRefresh_ (mol_complexSP, mol_complexesN,
				      runtimeSP, configSP, guiSP,
				      nearest_atomSP, pixelsN, *refreshIP);
			ControlRefresh_ (mol_complexSP +
					 runtimeSP->default_complexI,
					 configSP, guiSP);
			}
		return ERROR_EDIT;
		}

	/* Store the serial numbers and the array indices: */
	runtimeSP->atom1_serialI = atom1_serialI;
	runtimeSP->atom2_serialI = atom2_serialI;
	runtimeSP->atom1_arrayI  = atom1_arrayI;
	runtimeSP->atom2_arrayI  = atom2_arrayI;

	/* Prepare the purified names for two given atoms: */
	atom1SP = default_mol_complexSP->atomSP + atom1_arrayI;
	atom2SP = default_mol_complexSP->atomSP + atom2_arrayI;
	atom1_pure_nameP = atom1SP->raw_atomS.pure_atom_nameA;
	atom2_pure_nameP = atom2SP->raw_atomS.pure_atom_nameA;

	/* If two atoms form the  N-CA bond,  the current command */
	/* is equivalent to EDIT PHI. If they form the CA-C bond, */
	/* the command  is equivalent to  EDIT PSI.  If they form */
	/* the C-N bond, the command is equivalent to EDIT OMEGA. */

	/* The first combination equivalent to EDIT PHI: */
	if ((strcmp (atom1_pure_nameP, "N" ) == 0) &&
	    (strcmp (atom2_pure_nameP, "CA") == 0))
		{
		runtimeSP->edit_modeI = 2;
		runtimeSP->edit_single_bondF = 1;
		}

	/* The second combination equivalent to EDIT PHI: */
	else if ((strcmp (atom1_pure_nameP, "CA") == 0) &&
		 (strcmp (atom2_pure_nameP, "N" ) == 0))
		{
		runtimeSP->edit_modeI = 2;
		runtimeSP->edit_single_bondF = 1;
		}

	/* The first combination equivalent to EDIT PSI: */
	else if ((strcmp (atom1_pure_nameP, "CA") == 0) &&
		 (strcmp (atom2_pure_nameP, "C" ) == 0))
		{
		runtimeSP->edit_modeI = 3;
		runtimeSP->edit_single_bondF = 1;
		}

	/* The second combination equivalent to EDIT PSI: */
	else if ((strcmp (atom1_pure_nameP, "CA") == 0) &&
		 (strcmp (atom2_pure_nameP, "C" ) == 0))
		{
		runtimeSP->edit_modeI = 3;
		runtimeSP->edit_single_bondF = 1;
		}

	/* The first combination equivalent to EDIT OMEGA: */
	else if ((strcmp (atom1_pure_nameP, "C") == 0) &&
		 (strcmp (atom2_pure_nameP, "N") == 0))
		{
		runtimeSP->edit_modeI = 4;
		runtimeSP->edit_single_bondF = 1;
		}

	/* The second combination equivalent to EDIT OMEGA: */
	else if ((strcmp (atom1_pure_nameP, "N") == 0) &&
		 (strcmp (atom2_pure_nameP, "C") == 0))
		{
		runtimeSP->edit_modeI = 4;
		runtimeSP->edit_single_bondF = 1;

		/* Swap the array and serial indices. The second array */
		/* index  will be used in  small_omega.c  to recognize */
		/* the residue  to which  the nitrogen  atom  belongs: */
		runtimeSP->atom2_arrayI = atom1_arrayI;
		runtimeSP->atom1_arrayI = atom2_arrayI;
		runtimeSP->atom1_serialI = atom2_serialI;
		runtimeSP->atom2_serialI = atom1_serialI;
		}

	/* All  other combinations  are  not  equivalent to */
	/* special cases EDIT PHI, EDIT PSI and EDIT OMEGA: */
	else
		{
		runtimeSP->edit_modeI = 6;
		runtimeSP->edit_single_bondF = 1;
		}
	}

/* Keyword SIDE: */
else if (strstr (tokenA, "SID") == tokenA)
	{
	/* Find the first selected residue: */
	selected_residue_foundF = 0;
	for (residueI = 0; residueI < residuesN; residueI++)
		{
		/* Pointer to the current residue: */
		curr_residueSP = default_mol_complexSP->residueSP + residueI;

		/* Pointer to the first atom: */
		atomSP = default_mol_complexSP->atomSP +
			 curr_residueSP->residue_startI;

		/* If the first atom is not selected, do */
		/* not treat  this residue  as selected: */
		if (atomSP->selectedF)
			{
			selected_residue_foundF = 1;
			break;
			}
		}

	/* At least one residue should be selected: */
	if (!selected_residue_foundF)
		{
		strcpy (runtimeSP->messageA,
			"At least one residue should be selected!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		if (old_edit_modeI != 0)
			{
			(*refreshIP)++;
			MainRefresh_ (mol_complexSP, mol_complexesN,
				      runtimeSP, configSP, guiSP,
				      nearest_atomSP, pixelsN, *refreshIP);
			ControlRefresh_ (mol_complexSP +
					 runtimeSP->default_complexI,
					 configSP, guiSP);
			}
		return ERROR_EDIT;
		}

	/* Extract serial numbers and array indices of CA and CB atoms: */
	n = BondPartners_ (runtimeSP,
			   default_mol_complexSP, curr_residueSP, "CA", "CB");
	if (n < 0)
		{
		strcpy (runtimeSP->messageA,
			"Unable to extract data for atoms CA and CB!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		if (old_edit_modeI != 0)
			{
			(*refreshIP)++;
			MainRefresh_ (mol_complexSP, mol_complexesN,
				      runtimeSP, configSP, guiSP,
				      nearest_atomSP, pixelsN, *refreshIP);
			ControlRefresh_ (mol_complexSP +
					 runtimeSP->default_complexI,
					 configSP, guiSP);
			}
		return ERROR_EDIT;
		}

	/* Set the edit mode index: */
	runtimeSP->edit_modeI = 7;
	}

/* Keyword DIMENSIONS: */
else if (strstr (tokenA, "DIM") == tokenA)
	{
	/* Set the edit mode index: */
	runtimeSP->edit_modeI = 101;
	}

/* Keyword CLICK: */
else if (strstr (tokenA, "CLI") == tokenA)
	{
	/* Reset the edit mode index: */
	runtimeSP->edit_modeI = 0;

	/* Set the click mode index to one: */
	runtimeSP->click_modeI = 1;
	}

/* If keyword recognition failed: */
else
	{
	strcpy (runtimeSP->messageA, "Keyword not recognized!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	if (old_edit_modeI != 0)
		{
		(*refreshIP)++;
		MainRefresh_ (mol_complexSP, mol_complexesN,
			      runtimeSP, configSP, guiSP,
			      nearest_atomSP, pixelsN, *refreshIP);
		ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
				 configSP, guiSP);
			}
	return ERROR_EDIT;
	}

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return the command code: */
return COMMAND_EDIT;
}

/*===========================================================================*/


