/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

				bond_partners.c

Purpose:
	Extract  serial numbers and  array indices of two atoms which both
	should belong to the specified residue.

Input:
	(1) Pointer to RuntimeS structure.
	(2) Pointer to MolComplexS structure.
	(3) Pointer to ResidueS structure.
	(4) The purified name of the first atom.
	(5) The purified name of the second atom.

Output:
	(1) Serial numbers and array indices stored to RuntimeS structure.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======extract serial numbers and array indices for two atoms:==============*/

int BondPartners_ (RuntimeS *runtimeSP,
		   MolComplexS *mol_complexSP, ResidueS *residueSP,
		   char *atom1_nameP, char *atom2_nameP)
{
int			atom_foundF;
size_t			atom_startI, atom_endI, atomI;
AtomS			*curr_atomSP;
static int		atom1_serialI, atom2_serialI;
static size_t		atom1_arrayI, atom2_arrayI;

/* The range for atomic index: */
atom_startI = residueSP->residue_startI;
atom_endI   = residueSP->residue_endI;

/* Try to find the atom specified by the first name: */
atom_foundF = 0;
for (atomI = atom_startI; atomI <= atom_endI; atomI++)
	{
	/* Pointer to the current atom: */
	curr_atomSP = mol_complexSP->atomSP + atomI;

	/* Check the purified name of this atom: */
	if (strcmp (curr_atomSP->raw_atomS.pure_atom_nameA, atom1_nameP) == 0)
		{
		atom1_serialI = curr_atomSP->raw_atomS.serialI;
		atom1_arrayI = atomI;
		atom_foundF = 1;
		break;
		}
	}

/* If atom is not present in this residue: */
if (!atom_foundF) return -1;

/* Try to find the atom specified by the second name: */
atom_foundF = 0;
for (atomI = atom_startI; atomI <= atom_endI; atomI++)
        {
        /* Pointer to the current atom: */
        curr_atomSP = mol_complexSP->atomSP + atomI;

        /* Check the purified name of this atom: */
        if (strcmp (curr_atomSP->raw_atomS.pure_atom_nameA, atom2_nameP) == 0)
                {
		atom2_serialI = curr_atomSP->raw_atomS.serialI;
		atom2_arrayI = atomI;
                atom_foundF = 1;
                break;
                }
        }

/* If atom is not present in this residue: */
if (!atom_foundF) return -2;

/* If this point is reached, both atoms were found. */

/* Store the serial numbers and the array indices: */
runtimeSP->atom1_serialI = atom1_serialI;
runtimeSP->atom2_serialI = atom2_serialI;
runtimeSP->atom1_arrayI  = atom1_arrayI;
runtimeSP->atom2_arrayI  = atom2_arrayI;

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


