/* Copyright (C) 2000-2006 Damir Zucic */

/*=============================================================================

				write_hints.c

Purpose:
	Write some hints to the hidden pixmap.

Input:
	(1) Pointer to GUIS structure, with GUI data.

Output:
	(1) Some hints (textual information) written to hidden pixmap.
	(2) Return value.

Return value:
	(1) Positive always (trivial).

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======write some hints to the hidden pixmap:===============================*/

int WriteHints_ (GUIS *guiSP)
{
int		screen_x, screen_y, delta_y;
char		stringA[STRINGSIZE];

/* Prepare the text color (orange): */
XSetForeground (guiSP->displaySP, guiSP->theGCA[0], guiSP->orange_colorID);

/* Draw hints: */
delta_y = guiSP->input_winS.text_line_height;
screen_x = 10;

screen_y = 60 + 2 * delta_y;
strcpy (stringA,
	"Home page (HTML documentation and GNU GPL license is there):");
XDrawString (guiSP->displaySP, guiSP->main_hidden_pixmapID, guiSP->theGCA[0],
	     screen_x, screen_y, stringA, strlen (stringA));

screen_x = 40;

screen_y += delta_y;
strcpy (stringA, "http://garlic.mefos.hr/garlic-1.6");
XDrawString (guiSP->displaySP, guiSP->main_hidden_pixmapID, guiSP->theGCA[0],
	     screen_x, screen_y, stringA, strlen (stringA));

screen_x = 10;

screen_y += 2 * delta_y;
strcpy (stringA, "Author: http://garlic.mefos.hr/zucic");
XDrawString (guiSP->displaySP, guiSP->main_hidden_pixmapID, guiSP->theGCA[0],
	     screen_x, screen_y, stringA, strlen (stringA));

/* Change the text color (yellow): */
XSetForeground (guiSP->displaySP, guiSP->theGCA[0], guiSP->yellow_colorID);

screen_x = 10;

screen_y += 2 * delta_y;
strcpy (stringA, "References:");
XDrawString (guiSP->displaySP, guiSP->main_hidden_pixmapID, guiSP->theGCA[0],
	     screen_x, screen_y, stringA, strlen (stringA));

screen_x = 40;

screen_y += delta_y;
strcpy (stringA, "Damir Zucic and Davor Juretic,");
XDrawString (guiSP->displaySP, guiSP->main_hidden_pixmapID, guiSP->theGCA[0],
	     screen_x, screen_y, stringA, strlen (stringA));

screen_y += delta_y;
strcpy (stringA, "Precise Annotation of Transmembrane Segments");
XDrawString (guiSP->displaySP, guiSP->main_hidden_pixmapID, guiSP->theGCA[0],
	     screen_x, screen_y, stringA, strlen (stringA));

screen_y += delta_y;
strcpy (stringA, "with Garlic - a Free Molecular Visualization Program,");
XDrawString (guiSP->displaySP, guiSP->main_hidden_pixmapID, guiSP->theGCA[0],
	     screen_x, screen_y, stringA, strlen (stringA));

screen_y += delta_y;
strcpy (stringA, "Croatica Chemica Acta Vol. 77, No. 1-2, pp. 397-401 (2004).");
XDrawString (guiSP->displaySP, guiSP->main_hidden_pixmapID, guiSP->theGCA[0],
	     screen_x, screen_y, stringA, strlen (stringA));

screen_y += delta_y;
strcpy (stringA, "Free PDF:");
XDrawString (guiSP->displaySP, guiSP->main_hidden_pixmapID, guiSP->theGCA[0],
	     screen_x, screen_y, stringA, strlen (stringA));

screen_y += delta_y;
strcpy (stringA, "http://public.carnet.hr/ccacaa/CCA-PDF/cca2004/v77-n1_n2/CCA_77_2004_397-401_zucic.pdf");
XDrawString (guiSP->displaySP, guiSP->main_hidden_pixmapID, guiSP->theGCA[0],
	     screen_x, screen_y, stringA, strlen (stringA));

screen_y += 2.0 * delta_y;
strcpy (stringA, "Damir Zucic,");
XDrawString (guiSP->displaySP, guiSP->main_hidden_pixmapID, guiSP->theGCA[0],
	     screen_x, screen_y, stringA, strlen (stringA));

screen_y += delta_y;
strcpy (stringA, "Annotation of Transmembrane Segments of Experimentally");
XDrawString (guiSP->displaySP, guiSP->main_hidden_pixmapID, guiSP->theGCA[0],
	     screen_x, screen_y, stringA, strlen (stringA));

screen_y += delta_y;
strcpy (stringA, "Solved Bacterial Porins and Adhesins,");
XDrawString (guiSP->displaySP, guiSP->main_hidden_pixmapID, guiSP->theGCA[0],
	     screen_x, screen_y, stringA, strlen (stringA));

screen_y += delta_y;
strcpy (stringA, "Croatica Chemica Acta Vol. 78, No. 2, pp. 151-158 (2005).");
XDrawString (guiSP->displaySP, guiSP->main_hidden_pixmapID, guiSP->theGCA[0],
	     screen_x, screen_y, stringA, strlen (stringA));

screen_y += delta_y;
strcpy (stringA, "Free PDF:");
XDrawString (guiSP->displaySP, guiSP->main_hidden_pixmapID, guiSP->theGCA[0],
	     screen_x, screen_y, stringA, strlen (stringA));

screen_y += delta_y;
strcpy (stringA, "http://jagor.srce.hr/ccacaa/CCA-PDF/cca2005/v78-n2/CCA_78_2005_151-158_Zucic.pdf");
XDrawString (guiSP->displaySP, guiSP->main_hidden_pixmapID, guiSP->theGCA[0],
	     screen_x, screen_y, stringA, strlen (stringA));

/* Restore the text color (orange): */
XSetForeground (guiSP->displaySP, guiSP->theGCA[0], guiSP->orange_colorID);

screen_x = 10;

screen_y += 2 * delta_y;
strcpy (stringA, "Encourage garlic development - type reg to register!");
XDrawString (guiSP->displaySP, guiSP->main_hidden_pixmapID, guiSP->theGCA[0],
	     screen_x, screen_y, stringA, strlen (stringA));

/* Return positive value (trivial): */
return 1;
}

/*===========================================================================*/


