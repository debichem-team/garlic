/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				is_old.c

Purpose:
	Convert the string to uppercase and compare it with the word "OLD".
	If they are equal, return 1, otherwise return 0.

Input:
	(1) Output string pointer.

Output:
	(1) Return value.

Return value:
	(1) Positive, if input string contains the word "OLD".
	(2) Zero otherwise.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>
#include <ctype.h>

#include "defines.h"

/*======compare the string with the word "OLD":==============================*/

int IsOld_ (char *stringP)
{
char		string_copyA[SHORTSTRINGSIZE];
char		*P;
int		n;

/* Copy the input string: */
strncpy (string_copyA, stringP, SHORTSTRINGSIZE - 1);
string_copyA[SHORTSTRINGSIZE - 1] = '\0';

/* Convert the string copy to uppercase: */
P = string_copyA;
while ((n = *P++) != '\0') *(P - 1) = toupper (n);

/* Check the string content - compare it with the word "OLD": */
if (strcmp (string_copyA, "OLD") != 0) return 0;

/* If this point is reached, string contains the word "OLD": */
return 1;
}

/*===========================================================================*/


