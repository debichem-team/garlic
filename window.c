/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				window.c

Purpose:
	Execute window command: set the sliding window width. The hard-coded
	default is 11. The minimal value is 1.

Input:
	(1) Pointer to RuntimeS structure.

Output:
	(1) The sliding window width set.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>
#include <ctype.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"


/*======execute window command:==============================================*/

int Window_ (RuntimeS *runtimeSP)
{
char		string_copyA[STRINGSIZE];
char		*P;
int		n;
int		value;

/* Copy the original command string: */
strncpy (string_copyA, runtimeSP->curr_commandA, STRINGSIZE - 1);
string_copyA[STRINGSIZE - 1] = '\0';

/* Replace each non-numeric character (except */
/* minus sign and  decimal point) with space: */
P = string_copyA;
while ((n = *P++) != '\0')
	{
	if (!isdigit (n) && (n != '-') && (n != '.')) *(P - 1) = ' ';
	}

/* Try to extract the sliding window width: */
if (sscanf (string_copyA, "%d", &value) != 1)
	{
	strcpy (runtimeSP->messageA,
		"Failed to extract the sliding window width!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_WINDOW;
	}

/* Check the value: */
if (value < 1)
	{
	strcpy (runtimeSP->messageA, "Positive value expected!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_WINDOW;
	}

/* On success, copy the extracted value to RuntimeS structure: */
runtimeSP->sliding_window_width = value;

/* Return the command code: */
return COMMAND_WINDOW;
}

/*===========================================================================*/


