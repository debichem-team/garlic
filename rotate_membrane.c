/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

				rotate_membrane.c

Purpose:
	Rotate the membrane associated with the given macromolecular complex.

Input:
	(1) Pointer to MolComplexS structure.
	(2) Pointer to ConfigS structure.
	(3) Rotation angle.
	(4) Rotation axis identifier (1 = x, 2 = y, 3 = z).

Output:
	(1) The membrane associated with the specified macromolecular complex
	    will be rotated.

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>

#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======rotate membrane:=====================================================*/

void RotateMembrane_ (MolComplexS *curr_mol_complexSP, ConfigS *configSP,
		      double rotation_angle, int axisID)
{
double		cos_angle, sin_angle;
double		cos_stereo_angle, sin_stereo_angle;
MembraneS	*membraneSP;
double		x0, y0, z0;
double		x, y, z;
static double	x_new, y_new, z_new;
PlaneS		*plane1SP, *plane2SP;

/* Calculate cosine and sine of the rotation angle: */
cos_angle = cos (rotation_angle);
sin_angle = sin (rotation_angle);

/* Calculate cosine and sine of the stereo angle: */
cos_stereo_angle = cos (configSP->stereo_angle);
sin_stereo_angle = sin (configSP->stereo_angle);

/* Pointer to the membrane: */
membraneSP = &curr_mol_complexSP->membraneS;

/*------rotate the membrane center, if required:-----------------------------*/

/* If membrane if moved together with the */
/* structure, rotate the membrane center: */
if (curr_mol_complexSP->move_bits & STRUCTURE_MASK)
	{
	/* Copy the rotation center coordinates: */
	x0 = curr_mol_complexSP->rotation_center_vectorS.x;
	y0 = curr_mol_complexSP->rotation_center_vectorS.y;
	z0 = curr_mol_complexSP->rotation_center_vectorS.z;

	/* Position of the membrane center relative to the rotation center: */
	x = membraneSP->center_x - x0;
	y = membraneSP->center_y - y0;
	z = membraneSP->center_z - z0;

	/* Rotate these coordinates: */
	switch (axisID)
		{
		/* Rotate around x: */
		case 1:
			x_new =  x;
			y_new =  y * cos_angle - z * sin_angle;
			z_new =  y * sin_angle + z * cos_angle;
			break;

		/* Rotate around y: */
		case 2:
			x_new =  x * cos_angle + z * sin_angle;
			y_new =  y;
			z_new = -x * sin_angle + z * cos_angle;
			break;

		/* Rotate around z: */
		case 3:
			x_new =  x * cos_angle - y * sin_angle;
			y_new =  x * sin_angle + y * cos_angle;
			z_new =  z;
			break;

		/* The impossible case: */
		default:
			;
			break;
		}
	/* Translate and store new coordinates: */
	membraneSP->center_x = x_new + x0;
	membraneSP->center_y = y_new + y0;
	membraneSP->center_z = z_new + z0;
	}

/*------rotate the first plane:----------------------------------------------*/

/* Pointer to the first plane: */
plane1SP = &membraneSP->plane1S;

/* Copy the normal vector components: */
x = plane1SP->normal_x[0];
y = plane1SP->normal_y;
z = plane1SP->normal_z[0];

/* Rotate the normal vector: */
switch (axisID)
	{
	/* Rotate around x: */
	case 1:
		x_new =  x;
		y_new =  y * cos_angle - z * sin_angle;
		z_new =  y * sin_angle + z * cos_angle;
		break;

	/* Rotate around y: */
	case 2:
		x_new =  x * cos_angle + z * sin_angle;
		y_new =  y;
		z_new = -x * sin_angle + z * cos_angle;
		break;

	/* Rotate around z: */
	case 3:
		x_new =  x * cos_angle - y * sin_angle;
		y_new =  x * sin_angle + y * cos_angle;
		z_new =  z;
		break;

	/* The impossible case: */
	default:
		;
		break;
	}

/* Store the rotated vector: */
plane1SP->normal_x[0] = x_new;
plane1SP->normal_y    = y_new;
plane1SP->normal_z[0] = z_new;

/* Prepare stereo data for normal vector: */
plane1SP->normal_x[1] =  plane1SP->normal_x[0] * cos_stereo_angle +
			 plane1SP->normal_z[0] * sin_stereo_angle;
plane1SP->normal_z[1] = -plane1SP->normal_x[0] * sin_stereo_angle +
			 plane1SP->normal_z[0] * cos_stereo_angle;

/*------rotate the second plane:---------------------------------------------*/

/* Pointer to the second plane: */
plane2SP = &membraneSP->plane2S;

/* Copy the normal vector components: */
x = plane2SP->normal_x[0];
y = plane2SP->normal_y;
z = plane2SP->normal_z[0];

/* Rotate the normal vector: */
switch (axisID)
	{
	/* Rotate around x: */
	case 1:
		x_new =  x;
		y_new =  y * cos_angle - z * sin_angle;
		z_new =  y * sin_angle + z * cos_angle;
		break;

	/* Rotate around y: */
	case 2:
		x_new =  x * cos_angle + z * sin_angle;
		y_new =  y;
		z_new = -x * sin_angle + z * cos_angle;
		break;

	/* Rotate around z: */
	case 3:
		x_new =  x * cos_angle - y * sin_angle;
		y_new =  x * sin_angle + y * cos_angle;
		z_new =  z;
		break;

	/* The impossible case: */
	default:
		;
		break;
	}

/* Store the rotated vector: */
plane2SP->normal_x[0] = x_new;
plane2SP->normal_y    = y_new;
plane2SP->normal_z[0] = z_new;

/* Prepare stereo data for normal vector: */
plane2SP->normal_x[1] =  plane2SP->normal_x[0] * cos_stereo_angle +
			 plane2SP->normal_z[0] * sin_stereo_angle;
plane2SP->normal_z[1] = -plane2SP->normal_x[0] * sin_stereo_angle +
			 plane2SP->normal_z[0] * cos_stereo_angle;

/*---------------------------------------------------------------------------*/

/* Set the position_changedF: */
curr_mol_complexSP->position_changedF = 1;
}

/*===========================================================================*/


