/* Copyright (C) 2000-2006 Damir Zucic */

/*=============================================================================

				size_hints.c

Purpose:
	Set size hints  for main window.  Part of this job  could be done
	before.  This function just copies data from one reserved storage
	to another.

Input:
	(1) Pointer to GUIS structure, where input data will be read from
	    and output data will be stored.
	(2) Pointer to ConfigS structure, with some input data.

Output:
	(1) Some data stored to GUIS structure.

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======set size hints for the main window:==================================*/

void SetSizeHints_ (GUIS *guiSP, ConfigS *configSP)
{

/* Set main window size hints (obsolete parameters are not initialized): */
guiSP->main_winS.size_hintsS.flags        = PAllHints;
guiSP->main_winS.size_hintsS.min_width    = 300;
guiSP->main_winS.size_hintsS.min_height   = 200;

if ((int) guiSP->screen_width <= configSP->max_main_win_width)
	{
	guiSP->main_winS.size_hintsS.max_width = guiSP->screen_width;
	}
else guiSP->main_winS.size_hintsS.max_width = configSP->max_main_win_width;

if ((int) guiSP->screen_height <= configSP->max_main_win_height)
	{
	guiSP->main_winS.size_hintsS.max_height = guiSP->screen_height;
	}
else guiSP->main_winS.size_hintsS.max_height = configSP->max_main_win_height;

guiSP->main_winS.size_hintsS.width_inc    = 1;
guiSP->main_winS.size_hintsS.height_inc   = 1;
guiSP->main_winS.size_hintsS.min_aspect.x = 1;
guiSP->main_winS.size_hintsS.min_aspect.y = 4;
guiSP->main_winS.size_hintsS.max_aspect.x = 4;
guiSP->main_winS.size_hintsS.max_aspect.y = 1;
guiSP->main_winS.size_hintsS.base_width   = guiSP->main_winS.width;
guiSP->main_winS.size_hintsS.base_height  = guiSP->main_winS.height;
guiSP->main_winS.size_hintsS.win_gravity  = NorthWestGravity;
}

/*===========================================================================*/


