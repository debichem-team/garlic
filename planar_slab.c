/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				planar_slab.c

Purpose:
	Check which atoms of a given macromolecular complex are inside the
	planar slab.  Planar slab is defined by two parallel planes. It is
	always perpendicular to z axis.  If you need different planar slab
	orientation, write a new function.
	
Input:
	(1) Pointer to MolComplexS structure.

Output:
	(1) Slab flag  set for  each atom.  The value  one  is assigned to
	    atoms inside the slab, zero to the rest.
	(2) Return value.

Return value:
	The number of atoms inside the slab.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======apply planar slab:===================================================*/

size_t PlanarSlab_ (MolComplexS *curr_mol_complexSP)
{
size_t		atoms_inside_slabN = 0;
size_t		atomsN, atomI;
AtomS		*curr_atomSP;
double		z0, z1;

/* The number of atoms in a complex: */
atomsN = curr_mol_complexSP->atomsN;

/* The z coordinate of the front (near) surface: */
z0 = curr_mol_complexSP->slab_front_relative_position +
     curr_mol_complexSP->slab_center_vectorS.z;

/* The z coordinate of the back (far) surface: */
z1 = curr_mol_complexSP->slab_back_relative_position +
     curr_mol_complexSP->slab_center_vectorS.z;

/* Set the slab flag for each atom: */
for (atomI = 0; atomI < atomsN; atomI++)
	{
	/** Pointer to the current atom: **/
	curr_atomSP = curr_mol_complexSP->atomSP + atomI;

	/** Set the initial values of slab flags to one; this **/
	/** will be changed later for atoms outside the slab: **/
	curr_atomSP->inside_slabF = 1;
	curr_atomSP->inside_projected_slabF = 1;

	/** If current atom is closer to the observer than the front **/
	/** slab plane,  set flag to zero  and  check the next atom: **/
	if (curr_atomSP->raw_atomS.z[0] < z0)
		{
		curr_atomSP->inside_slabF = 0;
		curr_atomSP->inside_projected_slabF = 0;
		continue;
		}

	/** If atom is behind the back plane: **/
	if (curr_atomSP->raw_atomS.z[0] > z1)
		{
		curr_atomSP->inside_slabF = 0;
		curr_atomSP->inside_projected_slabF = 0;
		continue;
		}

	/** If this points is reached, current atom is inside the slab: **/
	atoms_inside_slabN++;
	}

/* Return the number of atoms inside the slab: */
return atoms_inside_slabN;
}

/*===========================================================================*/

