/* Copyright (C) 2002 Damir Zucic */

/*=============================================================================

				command_refresh.c

Purpose:
	Execute refresh command: turn refreshing ON or OFF.

Input:
	(1) Pointer to MolComplexS structure.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to ConfigS structure.
	(5) Pointer to GUIS structure.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
	(9) Pointer to the remainder of the command string. The only
	    keyword is OFF.

Output:
	(1) The main window refresh flag set to zero (OFF) or one (ON).
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

void		InformUser_ (GUIS *, char *);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute plot command:================================================*/

int CommandRefresh_ (MolComplexS *mol_complexSP, int mol_complexesN,
		     RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
		     NearestAtomS *nearest_atomSP, size_t pixelsN,
		     unsigned int *refreshIP, char *stringP)
{

/* If keyword OFF is present, switch refreshing */
/* off and inform user  that refreshing is off: */
if (strstr (stringP, "OFF"))
	{
	guiSP->main_window_refreshF = 0;
        InformUser_ (guiSP, "Refreshing off!");
	return COMMAND_REFRESH;
	}

/* If this point is reached, there is no keyword OFF: */

/* Set flag to one: */
guiSP->main_window_refreshF = 1;

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return the command code: */
return COMMAND_REFRESH;
}

/*===========================================================================*/


