/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				rgbs_from_xcolor.c

Purpose:
	Copy RGB values from Xcolor structure to RGBS structure.

Input:
	(1) Pointer to RGBS structure (output goes there).
	(2) Pointer to XColor structure (contains input data).

Output:
	(1) Data stored to RGBS.

Return value:
	No return value.

=============================================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======copy RGB values from XColor to RGBS structure:=======================*/

void RGBSFromXColor_ (RGBS *rgbSP, XColor *colorSP)
{
rgbSP->red   = colorSP->red;
rgbSP->green = colorSP->green;
rgbSP->blue  = colorSP->blue;
}

/*===========================================================================*/


