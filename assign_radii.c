/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				assign_radii.c

Purpose:
	Assign atomic, covalent and van der Waals radius to each atom. Assign
	generic radii to unrecognized atoms.

Input:
	(1) Pointer to  MolComplexS structure.
	(2) Pointer to ConfigS structure.

Output:
	(1) Atomic radius,  covalent radius and van der Waals radius assigned
	    to each atom.
	(2) Return value.

Return value:
	(1) Positive if there are some atoms.
	(2) Zero if there are no atoms.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======assign two radii to each atom:=======================================*/

int AssignRadii_ (MolComplexS *mol_complexSP, ConfigS *configSP)
{
size_t		atomI, atomsN;
AtomS		*curr_atomSP;

/* Return zero if there are no atoms in this macromolecular complex: */
atomsN = mol_complexSP->atomsN;
if (atomsN == 0) return 0;

/* Scan the macromolecular complex: */
for (atomI = 0; atomI < atomsN; atomI++)
	{
	/** Pointer to the current atom: **/
	curr_atomSP = mol_complexSP->atomSP + atomI;

	/** Assign default ball radius to all atoms: **/
	curr_atomSP->raw_atomS.ball_radius = configSP->default_ball_radius;

	/** Check the chemical symbol and assign four additional radii: **/
	if      (strcmp (curr_atomSP->raw_atomS.chemical_symbolA, " H") == 0)
		{
		curr_atomSP->raw_atomS.radius = configSP->H_radius;
		curr_atomSP->raw_atomS.covalent_radius =
				configSP->H_covalent_radius;
		curr_atomSP->raw_atomS.small_radius = configSP->H_small_radius;
		curr_atomSP->raw_atomS.van_der_Waals_radius =
				configSP->H_van_der_Waals_radius;
		}
	else if (strcmp (curr_atomSP->raw_atomS.chemical_symbolA, " C") == 0)
		{
		curr_atomSP->raw_atomS.radius = configSP->C_radius;
		curr_atomSP->raw_atomS.covalent_radius =
				configSP->C_covalent_radius;
		curr_atomSP->raw_atomS.small_radius = configSP->C_small_radius;
		curr_atomSP->raw_atomS.van_der_Waals_radius =
				configSP->C_van_der_Waals_radius;
		}
	else if (strcmp (curr_atomSP->raw_atomS.chemical_symbolA, " N") == 0)
		{
		curr_atomSP->raw_atomS.radius = configSP->N_radius;
		curr_atomSP->raw_atomS.covalent_radius =
				configSP->N_covalent_radius;
		curr_atomSP->raw_atomS.small_radius = configSP->N_small_radius;
		curr_atomSP->raw_atomS.van_der_Waals_radius =
				configSP->N_van_der_Waals_radius;
		}
	else if (strcmp (curr_atomSP->raw_atomS.chemical_symbolA, " O") == 0)
		{
		curr_atomSP->raw_atomS.radius = configSP->O_radius;
		curr_atomSP->raw_atomS.covalent_radius =
				configSP->O_covalent_radius;
		curr_atomSP->raw_atomS.small_radius = configSP->O_small_radius;
		curr_atomSP->raw_atomS.van_der_Waals_radius =
				configSP->O_van_der_Waals_radius;
		}
	else if (strcmp (curr_atomSP->raw_atomS.chemical_symbolA, " S") == 0)
		{
		curr_atomSP->raw_atomS.radius = configSP->S_radius;
		curr_atomSP->raw_atomS.covalent_radius =
				configSP->S_covalent_radius;
		curr_atomSP->raw_atomS.small_radius = configSP->S_small_radius;
		curr_atomSP->raw_atomS.van_der_Waals_radius =
				configSP->S_van_der_Waals_radius;
		}
	else if (strcmp (curr_atomSP->raw_atomS.chemical_symbolA, " P") == 0)
		{
		curr_atomSP->raw_atomS.radius = configSP->P_radius;
		curr_atomSP->raw_atomS.covalent_radius =
				configSP->P_covalent_radius;
		curr_atomSP->raw_atomS.small_radius = configSP->P_small_radius;
		curr_atomSP->raw_atomS.van_der_Waals_radius =
				configSP->P_van_der_Waals_radius;
		}
	else
		{
		curr_atomSP->raw_atomS.radius = configSP->generic_radius;
		curr_atomSP->raw_atomS.covalent_radius =
				configSP->generic_covalent_radius;
		curr_atomSP->raw_atomS.small_radius =
				configSP->generic_small_radius;
		curr_atomSP->raw_atomS.van_der_Waals_radius =
				configSP->generic_van_der_Waals_radius;
		}
	}

/* Return positive value if this point is reached: */
return 1;
}

/*===========================================================================*/


