/* Copyright (C) 2000, 2001 Damir Zucic */

/*=============================================================================

				flip_vector.c

Purpose:
	Copy vector multiplied by -1.

Input:
	(1) Pointer to VectorS structure, where output vector will be stored.
	(2) Pointer to VectorS structure, with input vector.

Output:
	(1) Output vector (equal to -1 * input_vector) prepared.

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======flip vector (multiply by -1):========================================*/

void FlipVector_ (VectorS *out_vectorS, VectorS *in_vectorS)
{
out_vectorS->x = - in_vectorS->x;
out_vectorS->y = - in_vectorS->y;
out_vectorS->z = - in_vectorS->z;
}

/*===========================================================================*/


