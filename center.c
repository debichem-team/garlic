/* Copyright (C) 2000-2006 Damir Zucic */

/*=============================================================================

				center.c

Purpose:
	Execute center command: calculate the geometric center position
	for currently selected atoms  and  use  this position  as a new
	origin of  the internal coordinate system.  Only one  macromol.
	complex should be caught,  otherwise this function will fail to
	work.

Input:
	(1) Pointer to MolComplexS structure.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to ConfigS structure.
	(5) Pointer to GUIS structure.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.

Output:
	(1) 
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute center command:==============================================*/

int Center_ (MolComplexS *mol_complexSP, int mol_complexesN,
	     RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
	     NearestAtomS *nearest_atomSP, size_t pixelsN,
	     unsigned int *refreshIP)
{
int		mol_complexI;
int		caught_complexesN = 0;
MolComplexS	*curr_mol_complexSP;
size_t		atomsN, atomI;
AtomS		*curr_atomSP;
size_t		selected_atomsN = 0;
double		x = 0.0, y = 0.0, z = 0.0, denom;

/* Count the number of currently caught macromolecular complexes: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	if ((mol_complexSP + mol_complexI)->catchF == 1) caught_complexesN++;
	}

/* Check the number of cought complexes (must be equal to one): */
if (caught_complexesN != 1)
	{
	strcpy (runtimeSP->messageA,
		"One (and only one) complex should be caught!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_CENTER;
	}

/* Find the cought complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
        {
	/** Pointer to the current macromolecular complex: **/
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/** Check is the current macromolecular complex caught: **/
	if (curr_mol_complexSP->catchF == 0) continue;

	/** Number of atoms in a macromolecular complex: **/
	atomsN = curr_mol_complexSP->atomsN;
	if (atomsN == 0)
		{
		strcpy (runtimeSP->messageA,
			"There are no atoms in this macromol. complex!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_CENTER;
		}

	/** Scan all atoms in the current complex: **/
	for (atomI = 0; atomI < atomsN; atomI++)
		{
		/*** Pointer to the current atom: ***/
		curr_atomSP = curr_mol_complexSP->atomSP + atomI;

		/*** Check is this atom selected: ***/
		if (curr_atomSP->selectedF == 0) continue;

		/*** Update the number of selected atoms: ***/
		selected_atomsN++;

		/*** Prepare data required to find geometric center: ***/
		x += curr_atomSP->raw_atomS.x[0];
		y += curr_atomSP->raw_atomS.y;
		z += curr_atomSP->raw_atomS.z[0];
		}

	/** Check how many atoms are selected: **/
	if (selected_atomsN == 0)
		{
		sprintf (runtimeSP->messageA, "There are no selected atoms!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_CENTER;
		}

	/** Calculate the geometric center position: **/
	denom = 1.0 / (double) selected_atomsN;
	x *= denom;
	y *= denom;
	z *= denom;

	/** Update the geometric center position: **/
	curr_mol_complexSP->geometric_center_vectorS.x = x;
	curr_mol_complexSP->geometric_center_vectorS.y = y;
	curr_mol_complexSP->geometric_center_vectorS.z = z;

	/** Update the rotation center position: **/
	curr_mol_complexSP->rotation_center_vectorS.x = x;
	curr_mol_complexSP->rotation_center_vectorS.y = y;
	curr_mol_complexSP->rotation_center_vectorS.z = z;

	/** Update the slab center: **/
	curr_mol_complexSP->slab_center_vectorS.x = x;
	curr_mol_complexSP->slab_center_vectorS.y = y;
	curr_mol_complexSP->slab_center_vectorS.z = z;

	/** Update the color fading center: **/
	curr_mol_complexSP->fading_center_vectorS.x = x;
	curr_mol_complexSP->fading_center_vectorS.y = y;
	curr_mol_complexSP->fading_center_vectorS.z = z;

	/* Update the position_changedF: */
	curr_mol_complexSP->position_changedF = 1;
        }

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return the command code: */
return COMMAND_CENTER;
}

/*===========================================================================*/


