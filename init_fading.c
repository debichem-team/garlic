/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				init_fading.c

Purpose:
	Initialize color fading data: reference point (center) position,
	front surface and back surface distance from the center.
	
Input:
	(1) Pointer to MolComplexS structure.

Output:
	(1) Color fading parameters initialized.

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======initialize color fading parameters:==================================*/

void InitFading_ (MolComplexS *mol_complexSP)
{
double		relative_z0, relative_z1;
double		fading_z0, fading_z1;


/* Initialize the reference point: */
mol_complexSP->fading_center_vectorS.x =
				mol_complexSP->geometric_center_vectorS.x;
mol_complexSP->fading_center_vectorS.y =
				mol_complexSP->geometric_center_vectorS.y;
mol_complexSP->fading_center_vectorS.z =
				mol_complexSP->geometric_center_vectorS.z;

/* Prepare the extreme z values (relative to the fading center): */
relative_z0 = mol_complexSP->left_top_near_vectorS.z -
	      mol_complexSP->fading_center_vectorS.z;
relative_z1 = mol_complexSP->right_bottom_far_vectorS.z -
	      mol_complexSP->fading_center_vectorS.z;

/* Set the initial front and back surface distance */
/* from the center.  This depends on fading style. */
switch (mol_complexSP->fading_modeI)
	{
	/** Fading off (all atoms should be colored as near atoms): **/
	case 0:
		break;

	/** Planar: **/
	case 1:
		fading_z0 = relative_z0;
		fading_z1 = relative_z1;
		mol_complexSP->fading_front_relative_position = fading_z0;
		mol_complexSP->fading_back_relative_position  = fading_z1;
		break;

	/** Sphere, half-sphere, cylinder and half-cylinder: **/
	case 2:
	case 3:
	case 4:
	case 5:
		fading_z0 = -relative_z0;
		fading_z1 = 0.0;
		mol_complexSP->fading_front_relative_position = fading_z0;
		mol_complexSP->fading_back_relative_position  = fading_z1;

	/** Unsupported fading modes (do nothing): **/
	default:
		;
	}
}

/*===========================================================================*/


