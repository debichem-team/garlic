/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				stereo.c

Purpose:
	Execute stereo command.

Input:
	(1) Pointer to MolComplexS structure.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to ConfigS structure.
	(5) Pointer to GUIS structure.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
	(9) Pointer to  the remainder of  the command string.  It is  either
	    empty or contains the keyword OFF.

Output:
	(1) Stereo flag set.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
int		CalculateParameters_ (ConfigS *, GUIS *);
int		PrepareStereoData_ (MolComplexS *, ConfigS *);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute stereo command:==============================================*/

int CommandStereo_ (MolComplexS *mol_complexSP, int mol_complexesN,
		    RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
		    NearestAtomS *nearest_atomSP, size_t pixelsN,
		    unsigned int *refreshIP, char *stringP)
{
char		*remainderP;
char		tokenA[STRINGSIZE];
int		mol_complexI;

/* Initialize the stereo flag: */
configSP->stereoF = 1;

/* Extract the first token: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, stringP, " \t\n");

/* If available, the first token should contain the keyword OFF: */
if (remainderP)
	{
	if (strstr (tokenA, "OFF") == tokenA) configSP->stereoF = 0;
        else
		{
		sprintf (runtimeSP->messageA,
			 "Keyword %s not recognized!",
			 tokenA);
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_STEREO;
		}
	}

/* Refresh calculated parameters: */
if (CalculateParameters_ (configSP, guiSP) < 0) return ERROR_STEREO;

/* Set the position_changedF flag and update stereo data: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/** Set flag: **/
	(mol_complexSP + mol_complexI)->position_changedF = 1;

	/** Update stereo data: **/
	for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
		{
		PrepareStereoData_ (mol_complexSP + mol_complexI, configSP);
		}
	}

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return the command code: */
return COMMAND_STEREO;
}

/*===========================================================================*/


