/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

			    init_colors.c

Purpose:
	Initialize the predefined (hard-coded) color schemes.

Input:
	(1) Pointer to ColorSchemeS structure.
	(2) Pointer to GUIS structure.

Output:
	(1) Predefined color schemes initialized.
	(2) Return value.

Return value:
	(1) Positive always (trivial).

=============================================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		ParseColor_ (RGBS *, unsigned long *, GUIS *, char *, char *);

/*======predefined color schemes:============================================*/

int InitializeColors_ (ColorSchemeS *color_schemeSP, GUIS *guiSP)
{
size_t			rgb_struct_size;
RGBS			rgbS;
unsigned long		dummyID;

/* The size of RGBS structure: */
rgb_struct_size = sizeof (RGBS);

/*------RED (0):-------------------------------------------------------------*/

(color_schemeSP + 0)->surfacesN = 2;

/* Surface 0: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:FFFF/8888/8888", "white");
memcpy ((color_schemeSP + 0)->left_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:FFFF/0000/0000", "white");
memcpy ((color_schemeSP + 0)->middle_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:8888/0000/0000", "white");
memcpy ((color_schemeSP + 0)->right_rgbSA, &rgbS, rgb_struct_size);

/* Surface 1: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:6666/0000/0000", "white");
memcpy ((color_schemeSP + 0)->left_rgbSA   + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:4444/0000/0000", "white");
memcpy ((color_schemeSP + 0)->middle_rgbSA + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:2222/0000/0000", "white");
memcpy ((color_schemeSP + 0)->right_rgbSA  + 1, &rgbS, rgb_struct_size);

/*------GREEN (1):-----------------------------------------------------------*/

(color_schemeSP + 1)->surfacesN = 2;

/* Surface 0: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:8888/FFFF/8888", "white");
memcpy ((color_schemeSP + 1)->left_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/FFFF/0000", "white");
memcpy ((color_schemeSP + 1)->middle_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/8888/0000", "white");
memcpy ((color_schemeSP + 1)->right_rgbSA, &rgbS, rgb_struct_size);

/* Surface 1: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/6666/0000", "white");
memcpy ((color_schemeSP + 1)->left_rgbSA   + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/4444/0000", "white");
memcpy ((color_schemeSP + 1)->middle_rgbSA + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/2222/0000", "white");
memcpy ((color_schemeSP + 1)->right_rgbSA  + 1, &rgbS, rgb_struct_size);

/*------BLUE (2):------------------------------------------------------------*/

(color_schemeSP + 2)->surfacesN = 2;

/* Surface 0: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:8888/8888/FFFF", "white");
memcpy ((color_schemeSP + 2)->left_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/0000/FFFF", "white");
memcpy ((color_schemeSP + 2)->middle_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/0000/8888", "white");
memcpy ((color_schemeSP + 2)->right_rgbSA, &rgbS, rgb_struct_size);

/* Surface 1: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/0000/6666", "white");
memcpy ((color_schemeSP + 2)->left_rgbSA   + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/0000/4444", "white");
memcpy ((color_schemeSP + 2)->middle_rgbSA + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/0000/2222", "white");
memcpy ((color_schemeSP + 2)->right_rgbSA  + 1, &rgbS, rgb_struct_size);

/*------YELLOW (3):----------------------------------------------------------*/

(color_schemeSP + 3)->surfacesN = 2;

/* Surface 0: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:FFFF/FFFF/8888", "white");
memcpy ((color_schemeSP + 3)->left_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:FFFF/FFFF/0000", "white");
memcpy ((color_schemeSP + 3)->middle_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:8888/8888/0000", "white");
memcpy ((color_schemeSP + 3)->right_rgbSA, &rgbS, rgb_struct_size);

/* Surface 1: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:3333/3333/0000", "white");
memcpy ((color_schemeSP + 3)->left_rgbSA   + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:2222/2222/0000", "white");
memcpy ((color_schemeSP + 3)->middle_rgbSA + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:1111/1111/0000", "white");
memcpy ((color_schemeSP + 3)->right_rgbSA  + 1, &rgbS, rgb_struct_size);

/*------CYAN (4):------------------------------------------------------------*/

(color_schemeSP + 4)->surfacesN = 2;

/* Surface 0: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:DDDD/FFFF/FFFF", "white");
memcpy ((color_schemeSP + 4)->left_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/FFFF/FFFF", "white");
memcpy ((color_schemeSP + 4)->middle_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/8888/8888", "white");
memcpy ((color_schemeSP + 4)->right_rgbSA, &rgbS, rgb_struct_size);

/* Surface 1: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/3333/3333", "white");
memcpy ((color_schemeSP + 4)->left_rgbSA   + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/2222/2222", "white");
memcpy ((color_schemeSP + 4)->middle_rgbSA + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/1111/1111", "white");
memcpy ((color_schemeSP + 4)->right_rgbSA  + 1, &rgbS, rgb_struct_size);

/*------MAGENTA (5):---------------------------------------------------------*/

(color_schemeSP + 5)->surfacesN = 2;

/* Surface 0: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:FFFF/9999/FFFF", "white");
memcpy ((color_schemeSP + 5)->left_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:EEEE/4444/EEEE", "white");
memcpy ((color_schemeSP + 5)->middle_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:8888/0000/8888", "white");
memcpy ((color_schemeSP + 5)->right_rgbSA, &rgbS, rgb_struct_size);

/* Surface 1: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:3333/0000/3333", "white");
memcpy ((color_schemeSP + 5)->left_rgbSA   + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:2222/0000/2222", "white");
memcpy ((color_schemeSP + 5)->middle_rgbSA + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:1111/0000/1111", "white");
memcpy ((color_schemeSP + 5)->right_rgbSA  + 1, &rgbS, rgb_struct_size);

/*------WHITE (6):-----------------------------------------------------------*/

(color_schemeSP + 6)->surfacesN = 2;

/* Surface 0: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:DDDD/DDDD/DDDD", "white");
memcpy ((color_schemeSP + 6)->left_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:AAAA/AAAA/AAAA", "white");
memcpy ((color_schemeSP + 6)->middle_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:5555/5555/5555", "white");
memcpy ((color_schemeSP + 6)->right_rgbSA, &rgbS, rgb_struct_size);

/* Surface 1: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:3333/3333/3333", "white");
memcpy ((color_schemeSP + 6)->left_rgbSA   + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:2222/2222/2222", "white");
memcpy ((color_schemeSP + 6)->middle_rgbSA + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:1111/1111/1111", "white");
memcpy ((color_schemeSP + 6)->right_rgbSA  + 1, &rgbS, rgb_struct_size);

/*------YELLOW-GREEN (7):----------------------------------------------------*/

(color_schemeSP + 7)->surfacesN = 2;

/* Surface 0: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:DDDD/FFFF/8888", "white");
memcpy ((color_schemeSP + 7)->left_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:8888/FFFF/0000", "white");
memcpy ((color_schemeSP + 7)->middle_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:4444/8888/0000", "white");
memcpy ((color_schemeSP + 7)->right_rgbSA, &rgbS, rgb_struct_size);

/* Surface 1: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:2222/4444/0000", "white");
memcpy ((color_schemeSP + 7)->left_rgbSA   + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:2222/3333/0000", "white");
memcpy ((color_schemeSP + 7)->middle_rgbSA + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:1111/2222/0000", "white");
memcpy ((color_schemeSP + 7)->right_rgbSA  + 1, &rgbS, rgb_struct_size);

/*-------CYAN-GREEN (8):-----------------------------------------------------*/

(color_schemeSP + 8)->surfacesN = 2;

/* Surface 0: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:8888/FFFF/DDDD", "white");
memcpy ((color_schemeSP + 8)->left_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/FFFF/8888", "white");
memcpy ((color_schemeSP + 8)->middle_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/8888/4444", "white");
memcpy ((color_schemeSP + 8)->right_rgbSA, &rgbS, rgb_struct_size);

/* Surface 1: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/4444/2222", "white");
memcpy ((color_schemeSP + 8)->left_rgbSA   + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/3333/2222", "white");
memcpy ((color_schemeSP + 8)->middle_rgbSA + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/2222/1111", "white");
memcpy ((color_schemeSP + 8)->right_rgbSA  + 1, &rgbS, rgb_struct_size);

/*-------CYAN-BLUE (9):-----------------------------------------------------*/

(color_schemeSP + 9)->surfacesN = 2;

/* Surface 0: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:8888/DDDD/FFFF", "white");
memcpy ((color_schemeSP + 9)->left_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/8888/FFFF", "white");
memcpy ((color_schemeSP + 9)->middle_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/4444/8888", "white");
memcpy ((color_schemeSP + 9)->right_rgbSA, &rgbS, rgb_struct_size);

/* Surface 1: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/2222/4444", "white");
memcpy ((color_schemeSP + 9)->left_rgbSA   + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/2222/3333", "white");
memcpy ((color_schemeSP + 9)->middle_rgbSA + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/1111/2222", "white");
memcpy ((color_schemeSP + 9)->right_rgbSA  + 1, &rgbS, rgb_struct_size);

/*-------MAGENTA-BLUE (10):--------------------------------------------------*/

(color_schemeSP + 10)->surfacesN = 2;

/* Surface 0: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:DDDD/8888/FFFF", "white");
memcpy ((color_schemeSP + 10)->left_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:8888/0000/FFFF", "white");
memcpy ((color_schemeSP + 10)->middle_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:4444/0000/8888", "white");
memcpy ((color_schemeSP + 10)->right_rgbSA, &rgbS, rgb_struct_size);

/* Surface 1: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:2222/0000/4444", "white");
memcpy ((color_schemeSP + 10)->left_rgbSA   + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:2222/0000/3333", "white");
memcpy ((color_schemeSP + 10)->middle_rgbSA + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:1111/0000/2222", "white");
memcpy ((color_schemeSP + 10)->right_rgbSA  + 1, &rgbS, rgb_struct_size);

/*-------MAGENTA-RED (11):---------------------------------------------------*/

(color_schemeSP + 11)->surfacesN = 2;

/* Surface 0: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:FFFF/8888/DDDD", "white");
memcpy ((color_schemeSP + 11)->left_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:FFFF/0000/8888", "white");
memcpy ((color_schemeSP + 11)->middle_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:8888/0000/4444", "white");
memcpy ((color_schemeSP + 11)->right_rgbSA, &rgbS, rgb_struct_size);

/* Surface 1: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:4444/0000/2222", "white");
memcpy ((color_schemeSP + 11)->left_rgbSA   + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:3333/0000/2222", "white");
memcpy ((color_schemeSP + 11)->middle_rgbSA + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:2222/0000/1111", "white");
memcpy ((color_schemeSP + 11)->right_rgbSA  + 1, &rgbS, rgb_struct_size);

/*------ORANGE (12):---------------------------------------------------------*/

(color_schemeSP + 12)->surfacesN = 2;

/* Surface 0: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:FFFF/9999/7777", "white");
memcpy ((color_schemeSP + 12)->left_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:FFFF/8888/0000", "white");
memcpy ((color_schemeSP + 12)->middle_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:8888/4444/0000", "white");
memcpy ((color_schemeSP + 12)->right_rgbSA, &rgbS, rgb_struct_size);

/* Surface 1: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:6666/3333/0000", "white");
memcpy ((color_schemeSP + 12)->left_rgbSA   + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:4444/2222/0000", "white");
memcpy ((color_schemeSP + 12)->middle_rgbSA + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:2222/1111/0000", "white");
memcpy ((color_schemeSP + 12)->right_rgbSA  + 1, &rgbS, rgb_struct_size);

/*------HOT (13):------------------------------------------------------------*/

(color_schemeSP + 13)->surfacesN = 3;

/* Surface 0: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:FFFF/FFFF/4444", "white");
memcpy ((color_schemeSP + 13)->left_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:FFFF/DDDD/0000", "white");
memcpy ((color_schemeSP + 13)->middle_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:8888/5555/0000", "white");
memcpy ((color_schemeSP + 13)->right_rgbSA, &rgbS, rgb_struct_size);

/* Surface 1: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:FFFF/8888/0000", "white");
memcpy ((color_schemeSP + 13)->left_rgbSA   + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:FFFF/0000/0000", "white");
memcpy ((color_schemeSP + 13)->middle_rgbSA + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:8888/0000/0000", "white");
memcpy ((color_schemeSP + 13)->right_rgbSA  + 1, &rgbS, rgb_struct_size);

/* Surface 2: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:8888/0000/0000", "white");
memcpy ((color_schemeSP + 13)->left_rgbSA   + 2, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:4444/0000/0000", "white");
memcpy ((color_schemeSP + 13)->middle_rgbSA + 2, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:2222/0000/0000", "white");
memcpy ((color_schemeSP + 13)->right_rgbSA  + 2, &rgbS, rgb_struct_size);

/*------COLD (14):-----------------------------------------------------------*/

(color_schemeSP + 14)->surfacesN = 3;

/* Surface 0: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:DDDD/FFFF/FFFF", "white");
memcpy ((color_schemeSP + 14)->left_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/FFFF/FFFF", "white");
memcpy ((color_schemeSP + 14)->middle_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/8888/FFFF", "white");
memcpy ((color_schemeSP + 14)->right_rgbSA, &rgbS, rgb_struct_size);

/* Surface 1: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/8888/FFFF", "white");
memcpy ((color_schemeSP + 14)->left_rgbSA   + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/0000/FFFF", "white");
memcpy ((color_schemeSP + 14)->middle_rgbSA + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/0000/8888", "white");
memcpy ((color_schemeSP + 14)->right_rgbSA  + 1, &rgbS, rgb_struct_size);

/* Surface 2: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/0000/8888", "white");
memcpy ((color_schemeSP + 14)->left_rgbSA   + 2, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/0000/4444", "white");
memcpy ((color_schemeSP + 14)->middle_rgbSA + 2, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/0000/2222", "white");
memcpy ((color_schemeSP + 14)->right_rgbSA  + 2, &rgbS, rgb_struct_size);

/*------MONOCHROME (15):-----------------------------------------------------*/

(color_schemeSP + 15)->surfacesN = 1;

/* Surface 0: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/0000/0000", "white");
memcpy ((color_schemeSP + 15)->left_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:FFFF/FFFF/FFFF", "white");
memcpy ((color_schemeSP + 15)->middle_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:0000/0000/0000", "white");
memcpy ((color_schemeSP + 15)->right_rgbSA, &rgbS, rgb_struct_size);

/*------ORANGE-YELLOW (21):--------------------------------------------------*/

(color_schemeSP + 21)->surfacesN = 2;

/* Surface 0: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:FFFF/DDDD/7777", "white");
memcpy ((color_schemeSP + 21)->left_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:FFFF/AAAA/0000", "white");
memcpy ((color_schemeSP + 21)->middle_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:8888/6666/0000", "white");
memcpy ((color_schemeSP + 21)->right_rgbSA, &rgbS, rgb_struct_size);

/* Surface 1: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:5555/4444/0000", "white");
memcpy ((color_schemeSP + 21)->left_rgbSA   + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:3333/2222/0000", "white");
memcpy ((color_schemeSP + 21)->middle_rgbSA + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:2222/1111/0000", "white");
memcpy ((color_schemeSP + 21)->right_rgbSA  + 1, &rgbS, rgb_struct_size);

/*------ORANGE-RED (22):-----------------------------------------------------*/

(color_schemeSP + 22)->surfacesN = 2;

/* Surface 0: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:FFFF/8888/6666", "white");
memcpy ((color_schemeSP + 22)->left_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:FFFF/6666/0000", "white");
memcpy ((color_schemeSP + 22)->middle_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:8888/2222/0000", "white");
memcpy ((color_schemeSP + 22)->right_rgbSA, &rgbS, rgb_struct_size);

/* Surface 1: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:6666/2222/0000", "white");
memcpy ((color_schemeSP + 22)->left_rgbSA   + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:4444/1111/0000", "white");
memcpy ((color_schemeSP + 22)->middle_rgbSA + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:2222/1111/0000", "white");
memcpy ((color_schemeSP + 22)->right_rgbSA  + 1, &rgbS, rgb_struct_size);

/*------SKIN (23):-----------------------------------------------------------*/

(color_schemeSP + 23)->surfacesN = 2;

/* Surface 0: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:EDED/C5C5/9494", "white");
memcpy ((color_schemeSP + 23)->left_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:BCBC/8585/5C5C", "white");
memcpy ((color_schemeSP + 23)->middle_rgbSA, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:8A8A/5757/3A3A", "white");
memcpy ((color_schemeSP + 23)->right_rgbSA, &rgbS, rgb_struct_size);

/* Surface 1: */
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:8585/5555/3535", "white");
memcpy ((color_schemeSP + 23)->left_rgbSA   + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:6565/4040/3030", "white");
memcpy ((color_schemeSP + 23)->middle_rgbSA + 1, &rgbS, rgb_struct_size);
ParseColor_ (&rgbS, &dummyID, guiSP, "RGB:2D2D/1F1F/1C1C", "white");
memcpy ((color_schemeSP + 23)->right_rgbSA  + 1, &rgbS, rgb_struct_size);

/*---------------------------------------------------------------------------*/

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


