/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				add_bond.c

Purpose:
	Add one bond  to the structure associated with a given atom. Add
	atomic pair  ID,  the array  index of  the complex  to which the
	neighboring  atom belongs,  the array index of  neighboring atom
	and bond length (the inter-atomic distance).

Input:
	(1) Pointer to AtomS structure with data about the atom to which
	    the bond belongs.
	(2) Atomic pair ID (see check_dist.c).
	(3) Bond type (0 covalent, 1 hydrogen, 2 disulfide, 11 pseudo).
	(3) Array index of  the macromolecular complex to which neighbor
	    belongs.
	(4) The array index of the neighbor.
	(5) Inter-atomic distance.
	(6) Default bond drawing style.

Output:
	(1) One bond added to  AtomS  structure associated  with a given
	    atom.
	(2) Return value.

Return value:
	(1) The updated number of bonds (positive, non-zero) on success.
	(2) Negative if  the maximal number of bonds was reached before.

Notes:
	(1) Zero is reserved as pair identifier of hydrogen bonds.

	(2) This function checks the total number of bonds.  It will not
	    allow the array overflow error.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======add one bond to the atom:============================================*/

int AddBond_ (AtomS *atomSP,
	      int pairID, int bond_typeI,
	      int neighbor_mol_complexI, size_t neighbor_arrayI,
	      double distance, int default_styleI)
{
int		bondI;

/* Prepare and check the bond index (equal to the previous number of bonds): */
bondI = atomSP->bondsN;
if (bondI >= MAXBONDS) return -1; /* The number of bonds per atom is limited */

/* Update the number of bonds: */
atomSP->bondsN++;

/* Initialize the bond drawing style: */
atomSP->true_bondSA[bondI].bond_styleI = default_styleI;

/* PairID: */
atomSP->true_bondSA[bondI].pairID = pairID;

/* Bond type: */
atomSP->true_bondSA[bondI].bond_typeI = bond_typeI;

/* The array index of the complex to which neighbor belongs: */
atomSP->true_bondSA[bondI].neighbor_mol_complexI = neighbor_mol_complexI;

/* Index number of the neighboring atom: */
atomSP->true_bondSA[bondI].neighbor_arrayI = neighbor_arrayI;

/* Interatomic distance i.e. bond length: */
atomSP->true_bondSA[bondI].bond_length = distance;

/* Return the updated number of bonds: */
return atomSP->bondsN;
}

/*===========================================================================*/


