/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				comm_translate.c

Purpose:
	Execute translate command.  Translate all caught macromolecular
	complexes for a given shift along a given axis.

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure, with some runtime data.
	(4) Pointer to ConfigS structure, with configuration data.
	(5) Pointer to GUIS structure, with GUI data.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
	(9) String  which contains  the translation  axis name and  the
	    translation angle.

Output:
	(1) Atomic coordinates changed.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
void		Translate_ (MolComplexS *, int,
			    RuntimeS *, ConfigS *, double, int);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);
int		DockingRefresh_ (RuntimeS *, GUIS *);

/*======execute translate command:===========================================*/

int CommandTranslate_ (MolComplexS *mol_complexSP, int mol_complexesN,
		       RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
		       NearestAtomS *nearest_atomSP, size_t pixelsN,
		       unsigned int *refreshIP, char *stringP)
{
char		*remainderP;
char		tokenA[STRINGSIZE];
int		axisID;
double		shift;

/* Extract axis name: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, stringP, " \t\n");
if (!remainderP)
	{
	strcpy (runtimeSP->messageA, "Axis specification missing!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_NO_AXIS;
	}

/* Identify the axis: */
if      (strcmp (tokenA, "X") == 0) axisID = 1;
else if (strcmp (tokenA, "Y") == 0) axisID = 2;
else if (strcmp (tokenA, "Z") == 0) axisID = 3;
else
	{
	strcpy (runtimeSP->messageA,
		"Bad axis (only x, y and z are available)!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_BAD_AXIS;
	}

/* Extract the token which contains the shift: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, " \t\n");
if (!remainderP)
	{
	strcpy (runtimeSP->messageA, "Translation shift missing!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_NO_SHIFT;
	}

/* Extract the shift: */
if (sscanf (tokenA, "%lf", &shift) != 1)
	{
	strcpy (runtimeSP->messageA,
		"Translation shift specification is bad!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_NO_SHIFT;
	}

/* Translate all caught macromolecular complexes: */
Translate_ (mol_complexSP, mol_complexesN, runtimeSP, configSP, shift, axisID);

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN, runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Refresh docking window, if docking is switched on: */
if (guiSP->dockingF) DockingRefresh_ (runtimeSP, guiSP);

/* Return positive value on success: */
return COMMAND_TRANSLATE;
}

/*===========================================================================*/


