/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				radius.c

Purpose:
	Execute radius command:  define the radius of the sphere around
	the geometric center calculated for the current selection. This
	radius  may be used to select  all atoms  which are inside this
	sphere. Default (hard-coded) value is 8.0 angstroms.

Input:
	(1) Pointer to RuntimeS structure.
	(2) Pointer to the remainder of the command string.

Output:
	(1) The sphere radius set.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>
#include <ctype.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"


/*======execute radius command:==============================================*/

int Radius_ (RuntimeS *runtimeSP, char *stringP)
{
char		*P;
int		n;
double		value;

/* Replace each non-numeric character (except */
/* minus sign and  decimal point) with space: */
P = stringP;
while ((n = *P++) != '\0')
	{
	if (!isdigit (n) && (n != '-') && (n != '.')) *(P - 1) = ' ';
	}

/* Try to extract the radius: */
if (sscanf (stringP, "%lf", &value) != 1)
	{
	strcpy (runtimeSP->messageA, "Failed to extract the sphere radius!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_RADIUS;
	}

/* Check the radius (must be positive): */
if (value < 0.0)
	{
	strcpy (runtimeSP->messageA, "Positive value expected!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_RADIUS;
	}

/* On success, copy the extracted radius to RuntimeS structure: */
runtimeSP->sphere_radius = value;

/* Return the command code: */
return COMMAND_RADIUS;
}

/*===========================================================================*/


