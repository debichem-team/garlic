/* Copyright (C) 2001-2002 Damir Zucic */

/*=============================================================================

				inform_user.c

Purpose:
	Write some message to the main window.

Input:
	(1) Pointer to GUIS structure, with GUI data.
	(2) Pointer to the string which contains the message.

Output:
	(1) Short message written to the main window.

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======inform user about something:=========================================*/

void InformUser_ (GUIS *guiSP, char *messageP)
{
int		message_length, text_width;
int		frame_width, frame_height;
int		screen_x0, screen_y0;

/* Inform user about something: */
message_length = strlen (messageP);
text_width = XTextWidth (guiSP->main_winS.fontSP, messageP, message_length);
frame_width = text_width + 50;
frame_height = 4 * guiSP->main_winS.text_line_height;
screen_x0 = (guiSP->main_win_free_area_width - frame_width) / 2;
screen_y0 = (guiSP->main_win_free_area_height - frame_height) / 2;
XSetForeground (guiSP->displaySP, guiSP->theGCA[0], guiSP->dark_red_colorID);
XFillRectangle (guiSP->displaySP, guiSP->main_winS.ID, guiSP->theGCA[0],
		screen_x0, screen_y0, frame_width, frame_height);
XSetForeground (guiSP->displaySP, guiSP->theGCA[0], guiSP->yellow_colorID);
screen_x0 = (guiSP->main_win_free_area_width - text_width) / 2;
screen_y0 = (guiSP->main_win_free_area_height +
	     guiSP->main_winS.text_line_height) / 2;
XDrawString (guiSP->displaySP, guiSP->main_winS.ID, guiSP->theGCA[0],
	     screen_x0, screen_y0, messageP, message_length);
XFlush (guiSP->displaySP);

}

/*===========================================================================*/


