/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

				discard.c

Purpose:
	Execute discard command:  discard the specified  macromolecular
	complex.

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) Pointer to the number of macromolecular complexes.
	(3) Pointer to the next macromolecular complex identifier.
	(4) Pointer to RuntimeS structure, with some runtime data.
	(5) Pointer to ConfigS structure, with configuration data.
	(6) Pointer to GUIS structure, with GUI data.
	(7) Pointer to NearestAtomS structure.
	(8) The number of pixels in the main window free area.
	(9) Pointer to refreshI.
       (10) The string which contains macromol. complex identifier.

Output:
	(1) The specified macromolecular complex discarded.
	(2) The number of complexes updated.
	(3) The next macromolecular complex identifier updated.
	(4) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

Notes:
	(1) The value of refreshI  (*refreshIP)  may be changed in this
	    function or in some function called by this function. It is
	    checked in  EventLoop_  (see event_loop.c).  It is  assumed
	    that  refreshI  is updated no more  than once in each event
	    handling function. If this is not the case, the care should
	    be taken to avoid refreshI overflow.

	(2) The number of macromolecular complexes is  not changed.  It
	    should not be used  as the actual number  of macromolecular
	    complexes. It should be used in loops as the value which is
	    defines the highest value of the array index.

	(3) If the currently caught macromol. complex is discarded,  it
	    may happen that  nothing  is caught.  This may be  fixed by
	    catch (take) command.  The ID of some complex which was not
	    discarded should be specified as argument to catch.

	(4) This functions resets  the edit mode index  and some flags.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>
#include <stdlib.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute discard command:=============================================*/

int Discard_ (MolComplexS *mol_complexSP,
	      int *mol_complexesNP, int *next_mol_complexIDP,
	      RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
	      NearestAtomS *nearest_atomSP, size_t pixelsN,
	      unsigned int *refreshIP, char *stringP)
{
char		*remainderP;
char		tokenA[SHORTSTRINGSIZE];
int		complexID;
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;
int		job_doneF = 0;

/* Extract the macromolecular complex identifier: */
remainderP = ExtractToken_ (tokenA, SHORTSTRINGSIZE, stringP, " \t\n");
if (!remainderP)
	{
	strcpy (runtimeSP->messageA, "Complex identifier missing!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_NO_ID;
	}

/* If the second token contains the keyword ALL or *, */
/* discard all  macromolecular complexes  and return: */
if ((strstr (tokenA, "ALL") == tokenA) || (strstr (tokenA, "*")))
	{
	for (mol_complexI = 0; mol_complexI < *mol_complexesNP; mol_complexI++)
		{
		/* Prepare the pointer to the current macromol. complex: */
		curr_mol_complexSP = mol_complexSP + mol_complexI;

		/* Discard the complex and reinitialize associated data: */

		/* Reset counters: */
		curr_mol_complexSP->atomsN = 0;
		curr_mol_complexSP->max_atomsN = 0;

		/* Atomic array: */
		if (curr_mol_complexSP->atomSP)
			{
			free (curr_mol_complexSP->atomSP);
			curr_mol_complexSP->atomSP = NULL;
			}

		/* Header: */
		if (curr_mol_complexSP->headerS.dataP)
			{
			free (curr_mol_complexSP->headerS.dataP);
			curr_mol_complexSP->headerS.dataP = NULL;
			}

		/* Backbone data: */
		if (curr_mol_complexSP->backboneSP)
			{
			free (curr_mol_complexSP->backboneSP);
			curr_mol_complexSP->backboneSP = NULL;
			}
		curr_mol_complexSP->c_alphaN = 0;

		/* Sequence information: */
		if (curr_mol_complexSP->residueSP)
			{
			free (curr_mol_complexSP->residueSP);
			curr_mol_complexSP->residueSP = NULL;
			}
		curr_mol_complexSP->residuesN = 0;

		/* Secondary structure: */
		if (curr_mol_complexSP->secondary_structureP)
			{
			free (curr_mol_complexSP->secondary_structureP);
			curr_mol_complexSP->secondary_structureP = NULL;
			}

		/* Catch flag,  tag flag, plane */
		/* flag and two membrane flags: */
		curr_mol_complexSP->catchF = 0;
		curr_mol_complexSP->tagF = 0;
		curr_mol_complexSP->planeS.hiddenF = 1;
		curr_mol_complexSP->membraneS.definedF = 0;
		curr_mol_complexSP->membraneS.hiddenF = 1;
		}

	/* Reset default_complexI: */
	runtimeSP->default_complexI = 0;

	/* Reset the edit mode index: */
	runtimeSP->edit_modeI = 0;

	/* Reset the edit_single_bondF flag: */
	runtimeSP->edit_single_bondF = 0;

	/* If some structures were grouped, reset the group flag: */
	runtimeSP->groupF = 0;

	/* If some structures were involved in a formation of a */
	/* group, restore the original rotation center vectors: */
	for (mol_complexI = 0; mol_complexI < *mol_complexesNP; mol_complexI++)
		{
		/* The pointer to the current macromolecular complex: */
		curr_mol_complexSP = mol_complexSP + mol_complexI;

		/* Check was this complex a group member: */
		if (curr_mol_complexSP->group_memberF == 0) continue;

		/* If this point is reached, this */
		/* complex  was  a  group member. */

		/* Restore the original rotation center vector: */
		curr_mol_complexSP->rotation_center_vectorS.x =
					curr_mol_complexSP->backup_vectorS.x;
		curr_mol_complexSP->rotation_center_vectorS.y =
					curr_mol_complexSP->backup_vectorS.y;
		curr_mol_complexSP->rotation_center_vectorS.z =
					curr_mol_complexSP->backup_vectorS.z;

		/* Reset the group_memberF: */
		curr_mol_complexSP->group_memberF = 0;
		}

	/* Reinitialize  the number of macromol. complexes */
	/* and the next macromolecular complex identifier: */
	*mol_complexesNP = 0;
	*next_mol_complexIDP = 1;

	/* Refresh the main window: */
	(*refreshIP)++;
	MainRefresh_ (mol_complexSP, *mol_complexesNP,
		      runtimeSP, configSP, guiSP,
		      nearest_atomSP, pixelsN, *refreshIP);

	/* Refresh the control window: */
	ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
			 configSP, guiSP);

	/* Return positive value on success: */
	return COMMAND_DISCARD;
        }

/* If this point is reached, a single macromolecular complex should */
/* be discarded.  Read the complex  identifier  (integer expected): */
if (sscanf (tokenA, "%d", &complexID) != 1)
	{
	strcpy (runtimeSP->messageA, "Bad macromolecular complex identifier!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_BAD_ID;
	}

/* Find and discard the specified macromolecular complex: */
for (mol_complexI = 0; mol_complexI < *mol_complexesNP; mol_complexI++)
	{
	/* Prepare the pointer to the current macromolecular complex: */
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/* If macromolecular complex is recognized */
	/* discard it and set the flag  job_doneF: */
	if (curr_mol_complexSP->mol_complexID == complexID)
		{
		/* Reset counters: */ 
		curr_mol_complexSP->atomsN = 0;
		curr_mol_complexSP->max_atomsN = 0;

		/* Free storage: */
		if (curr_mol_complexSP->atomSP)
			{
			free (curr_mol_complexSP->atomSP);
			curr_mol_complexSP->atomSP = NULL;
			}
		if (curr_mol_complexSP->headerS.dataP)
			{
			free (curr_mol_complexSP->headerS.dataP);
			curr_mol_complexSP->headerS.dataP = NULL;
			}
		if (curr_mol_complexSP->backboneSP)
			{
			free (curr_mol_complexSP->backboneSP);
			curr_mol_complexSP->backboneSP = NULL;
			}
		if (curr_mol_complexSP->residueSP)
			{
			free (curr_mol_complexSP->residueSP);
			curr_mol_complexSP->residueSP = NULL;
			}
		if (curr_mol_complexSP->secondary_structureP)
			{
			free (curr_mol_complexSP->secondary_structureP);
			curr_mol_complexSP->secondary_structureP = NULL;
			}

                /* Catch flag, tag flag, plane flag, two */
                /* membrane flags and group member flag: */
                curr_mol_complexSP->catchF = 0;
                curr_mol_complexSP->tagF = 0;
                curr_mol_complexSP->planeS.hiddenF = 1;
                curr_mol_complexSP->membraneS.definedF = 0;
                curr_mol_complexSP->membraneS.hiddenF = 1;
		curr_mol_complexSP->group_memberF = 0;

		/* Flag which says that job was done: */
		job_doneF = 1;
		}
	}

/* If complex was not found, return the error code: */
if (job_doneF == 0)
	{
	strcpy (runtimeSP->messageA, "Bad complex identifier (out of range)!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_BAD_ID;
	}

/* Reset the edit mode index: */
runtimeSP->edit_modeI = 0;

/* Reset the edit_single_bondF flag: */
runtimeSP->edit_single_bondF = 0;

/* If some structures were grouped, reset the group flag: */
runtimeSP->groupF = 0;

/* If some structures were involved in a formation of a */
/* group, restore the original rotation center vectors: */
for (mol_complexI = 0; mol_complexI < *mol_complexesNP; mol_complexI++)
	{
	/* Prepare the pointer to the current macromolecular complex: */
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/* Check was this complex a group member: */
	if (curr_mol_complexSP->group_memberF == 0) continue;

	/* If this point is reached, the current complex was a group member. */

	/* Restore the original rotation center vector: */
	curr_mol_complexSP->rotation_center_vectorS.x =
				curr_mol_complexSP->backup_vectorS.x;
	curr_mol_complexSP->rotation_center_vectorS.y =
				curr_mol_complexSP->backup_vectorS.y;
	curr_mol_complexSP->rotation_center_vectorS.z =
				curr_mol_complexSP->backup_vectorS.z;
	}


/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, *mol_complexesNP, runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return positive value on success: */
return COMMAND_DISCARD;
}

/*===========================================================================*/


