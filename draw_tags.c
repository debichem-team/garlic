/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

				draw_tags.c

Purpose:
	Draw tags for all caught complexes.

Input:
	(1) Pointer to MolComplexS structure.
	(2) Number of macromolecular complexes.
	(3) Pointer to ConfigS structure.
	(4) Pointer to GUIS structure.

Output:
	(1) Tags drawn to hidden pixmap.
	(2) Return value.

Return value:
	The number of tags drawn.

=============================================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		TagPosition_ (int *, int *, int *, MolComplexS *, int);

/*======draw tags:===========================================================*/

int DrawTags_ (MolComplexS *mol_complexSP, int mol_complexesN,
	       ConfigS *configSP, GUIS *guiSP)
{
int			tags_drawnN = 0;
int			imageI, imagesN;
int			left_edge[2], right_edge[2];
int			mol_complexI;
MolComplexS		*curr_mol_complexSP;
size_t			atomsN;
int			screen_left_x, screen_right_x, screen_x, screen_y;
int			tag_length, text_width;

/* Number of images: */
if (configSP->stereoF) imagesN = 2;
else imagesN = 1;

/* Left and right image edge (in stereo mode there are two images): */
left_edge[0]  = configSP->image_screen_x0[0];
right_edge[0] = configSP->image_screen_x1[0];
left_edge[1]  = configSP->image_screen_x0[1];
right_edge[1] = configSP->image_screen_x1[1];

/* Prepare the text color: */
XSetForeground (guiSP->displaySP, guiSP->theGCA[0],
		guiSP->main_winS.fg_colorID);

/* Check every macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/** Pointer to current macromolecular complex: **/
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/** Prepare and check the number of atoms: **/
	atomsN = curr_mol_complexSP->atomsN;
	if (atomsN == 0) continue;

	/** Check is tag hidden or visible: **/
	if (curr_mol_complexSP->tagF == 0) continue;

	/** Draw one (mono) or two tags (stereo): **/
	for (imageI = 0; imageI < imagesN; imageI++)
		{
		/*** Prepare the screen coordinates: ***/
		TagPosition_ (&screen_left_x, &screen_right_x, &screen_y,
			      curr_mol_complexSP, imagesN);
		if (imageI == 0) screen_x = screen_left_x;
		else screen_x = screen_right_x;

		/** Check is this tag really visible: */
		if (screen_x <  left_edge[imageI])  continue;
		if (screen_x >= right_edge[imageI]) continue;

		/** Do not allow tag to overlap with the structure: **/
		screen_y -= guiSP->main_winS.text_line_height + 1;

		/*** Tag length (the number of characters): ***/
		tag_length = strlen (curr_mol_complexSP->tagA);

		/*** Prepare the tag size in pixels: ***/
		text_width = XTextWidth (guiSP->main_winS.fontSP,
					 curr_mol_complexSP->tagA,
					 tag_length);

		/*** Draw tag: ***/
		XDrawString (guiSP->displaySP,
			     guiSP->main_winS.ID,
			     guiSP->theGCA[0],
			     screen_x - text_width / 2, screen_y,
			     curr_mol_complexSP->tagA,
			     tag_length);

		/*** Update the counter: ***/
		tags_drawnN++;

		}	/* imageI loop */
	}		/* mol_complexI loop */

/* Return the number of tags which were drawn: */
return tags_drawnN;
}

/*===========================================================================*/


