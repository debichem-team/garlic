/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				free_memory.c

Purpose:
	Free storage used for pixmaps and other data.

Input:
	(1) Pointer to GUIS structure.
	(2) Pointer to the array of MolComplexS structures.
	(3) Number of molecular complexes stored in dynamically allocated
	    memory.
	(4) Pointer to the array of NearestAtomS structures. 
	(5) Pointer to the number of pixels.

Output:
	No output (well, some memory may be freed, but this does not count).

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======free data storage:===================================================*/

void FreeMemory_ (GUIS *guiSP,
		  MolComplexS *mol_complexSP, int mol_complexesN,
		  NearestAtomS *nearest_atomSP, size_t *pixelsNP)
{
int		i;

/* Free pixmaps: */
if (guiSP->icon_winS.pixmapF)	 XFreePixmap (guiSP->displaySP,
					      guiSP->icon_winS.pixmapID);
if (guiSP->control_winS.pixmapF) XFreePixmap (guiSP->displaySP,
					      guiSP->control_winS.pixmapID);
if (guiSP->main_hidden_pixmapF)	 XFreePixmap (guiSP->displaySP,
					      guiSP->main_hidden_pixmapID);
if (guiSP->small_arrows_pixmapF) XFreePixmap (guiSP->displaySP,
					      guiSP->small_arrows_pixmapID);
/* Free cursors: */
if (guiSP->main_winS.cursorF)	 XFreeCursor (guiSP->displaySP,
					      guiSP->main_winS.cursorID);
if (guiSP->control_winS.cursorF) XFreeCursor (guiSP->displaySP,
					      guiSP->control_winS.cursorID);
if (guiSP->input_winS.cursorF)	 XFreeCursor (guiSP->displaySP,
					      guiSP->input_winS.cursorID);
if (guiSP->output_winS.cursorF)  XFreeCursor (guiSP->displaySP,
					      guiSP->output_winS.cursorID);

/* Free font: */
if (guiSP->main_winS.fontF)	 XFreeFont (guiSP->displaySP,
					    guiSP->main_winS.fontSP);

/* Free GC's (graphics contexts): */
if (guiSP->gca_createdF)
	{
	XFreeGC (guiSP->displaySP, guiSP->theGCA[0]);
	XFreeGC (guiSP->displaySP, guiSP->theGCA[1]);
	XFreeGC (guiSP->displaySP, guiSP->theGCA[2]);
	XFreeGC (guiSP->displaySP, guiSP->theGCA[3]);
	XFreeGC (guiSP->displaySP, guiSP->theGCA[4]);
	XFreeGC (guiSP->displaySP, guiSP->theGCA[5]);
	XFreeGC (guiSP->displaySP, guiSP->theGCA[6]);
	XFreeGC (guiSP->displaySP, guiSP->theGCA[7]);
	XFreeGC (guiSP->displaySP, guiSP->theGCA[8]);
	XFreeGC (guiSP->displaySP, guiSP->theGCA[9]);
	}

/* Free memory used to store header, atomic data, backbone and sequence: */
for (i = 0; i < mol_complexesN; i++)
	{
	if ((mol_complexSP + i)->headerS.dataP)
		{
		free ((mol_complexSP + i)->headerS.dataP);
		(mol_complexSP + i)->headerS.dataP = NULL;
		}
	if ((mol_complexSP + i)->atomSP)
		{
		free ((mol_complexSP + i)->atomSP);
		(mol_complexSP + i)->atomSP = NULL;
		(mol_complexSP + i)->atomsN = 0;
		(mol_complexSP + i)->max_atomsN = 0;
		}
	if ((mol_complexSP + i)->backboneSP)
		{
		free ((mol_complexSP + i)->backboneSP);
		(mol_complexSP + i)->backboneSP = NULL;
		(mol_complexSP + i)->c_alphaN = 0;
		}
	if ((mol_complexSP + i)->residueSP)
		{
		free ((mol_complexSP + i)->residueSP);
		(mol_complexSP + i)->residueSP = NULL;
		(mol_complexSP + i)->residuesN = 0;
		}
	if ((mol_complexSP + i)->secondary_structureP)
		{
		free ((mol_complexSP + i)->secondary_structureP);
		(mol_complexSP + i)->secondary_structureP = NULL;
		}
	}

/* Free memory used by NearestAtomS array: */
if (nearest_atomSP != NULL)
	{
	free (nearest_atomSP);
	nearest_atomSP = NULL;
	*pixelsNP = 0;
	}

}

/*===========================================================================*/


