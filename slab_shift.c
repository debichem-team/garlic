/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				slab_shift.c

Purpose:
	Prepare the slab shift. By default, the normal slab shift is used
	(slab_shiftA[2] from ConfigS structure).  See note 1 for possible
	combinations of modifier keys.  Modifier keys are used  to select
	the slab shift.

Input:
	(1) Pointer to ConfigS structure, with configuration data.
	(2) Pointer to GUIS structure, with GUI data.
	(3) The sign used to distinguish positive and negative shift.

Output:
	(1) Return value.

Return value:
	(1) Slab shift.

Notes:
	(1) The following  combinations  of modifier keys  may be used to
	    select the slab shift:
	    -------------------------------------------------------------
	    | modifiers           slab shift          ConfigS member    |
	    |-----------------------------------------------------------|
	    | none                normal (default)    slab_stepA[2]     |
	    | shift               large               slab_stepA[3]     |
	    | alt+shift           very large          slab_stepA[4]     |
	    | control             small               slab_stepA[1]     |
	    | alt+control         very small          slab_stepA[0]     |
	    | control+shift       normal              slab_stepA[2]     |
	    | alt                 normal              slab_stepA[2]     |
	    | alt+shift+control   normal              slab_stepA[2]     |
	    -------------------------------------------------------------
	    Note that some modifier combinations are ignored  (alt alone,
	    control+shift and alt+control+shift).

	(2) On some laptops the status of shift key may be changed before
	    the KeyPress event is send for the keys on  "numeric keypad".
 
========includes:============================================================*/

#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======prepare slab shift:==================================================*/

double SlabShift_ (ConfigS *configSP, GUIS *guiSP, double sign)
{
double		slab_shift;
int		largeF, smallF, altF;

/* Copy the flags: */
largeF = guiSP->shift_pressedF;
smallF = guiSP->control_pressedF;
altF   = guiSP->alt_pressedF;

/* Select the requested slab shift: */
if (altF)
	{
	if (largeF && !smallF)
		{
		slab_shift = configSP->slab_stepA[4];
		}
	else if (smallF && !largeF)
		{
		slab_shift = configSP->slab_stepA[0];
		}
	else
		{
		slab_shift = configSP->slab_stepA[2];
		}
	}
else
	{
	if (largeF && !smallF)
		{
		slab_shift = configSP->slab_stepA[3];
		}
	else if (smallF && !largeF)
		{
		slab_shift = configSP->slab_stepA[1];
		}
	else if (smallF && largeF)
		{
		slab_shift = configSP->slab_stepA[0];
		}
	else
		{
		slab_shift = configSP->slab_stepA[2];
		}
	}

/* Take care for the sign: */
slab_shift *= sign;

/* Return the slab shift: */
return slab_shift;
}

/*===========================================================================*/


