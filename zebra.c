/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				    zebra.c

Purpose:
	Zebra color scheme (alternating colors).

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) Number of macromolecular complexes.
	(3) Pointer to GUIS structure, with GUI data.
	(4) Pointer to ColorSchemeS structure.

Output:
	(1) Return value.

Return value:
	(1) Positive always.

=============================================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======zebra color scheme:==================================================*/

int Zebra_ (MolComplexS *mol_complexSP, int mol_complexesN,
	    GUIS *guiSP, ColorSchemeS *color_schemeSP)
{
int			mol_complexI;
MolComplexS		*curr_mol_complexSP;
size_t			atomsN, atomI;
AtomS			*curr_atomSP;
size_t			rgb_struct_size;
ColorSchemeS		*red_schemeSP, *cyan_blue_schemeSP;
int			surfaceI;
int			parity;

/* The size of RGBS structure: */
rgb_struct_size = sizeof (RGBS);

/* Red scheme pointer: */
red_schemeSP = color_schemeSP + 0;

/* Cyan scheme pointer: */
cyan_blue_schemeSP = color_schemeSP + 9;

/* Scan every macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/* Pointer to the current macromolecular complex: */
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/* Check is the current macromolecular complex caught: */
	if (curr_mol_complexSP->catchF == 0) continue;

	/* Number of atoms in a macromolecular complex: */
	atomsN = curr_mol_complexSP->atomsN;

	/* Scan all atoms in the current complex: */
	for (atomI = 0; atomI < atomsN; atomI++)
		{
		/* Pointer to the current atom: */
		curr_atomSP = curr_mol_complexSP->atomSP + atomI;

		/* Check is atom selected: */
		if (curr_atomSP->selectedF == 0) continue;

		/* Set the number of color surfaces: */
		curr_atomSP->surfacesN = 2;

		/* Check the parity (0 = even, 1 = odd): */
		parity = curr_atomSP->raw_atomS.residue_sequenceI % 2;

		/* Even atoms should be red: */
		if (!parity)
			{
			/* Copy the color specifications: */
			for (surfaceI = 0; surfaceI < 2; surfaceI++)
				{
				/* Left color: */
				memcpy (curr_atomSP->left_rgbSA + surfaceI,
					red_schemeSP->left_rgbSA +
					surfaceI,
					rgb_struct_size);

				/* Middle color: */
				memcpy (curr_atomSP->middle_rgbSA + surfaceI,
					red_schemeSP->middle_rgbSA +
					surfaceI,
					rgb_struct_size);

				/* Right color: */
				memcpy (curr_atomSP->right_rgbSA + surfaceI,
					red_schemeSP->right_rgbSA +
					surfaceI,
					rgb_struct_size);
				}
			}

		/* Odd atoms should be cyan: */
		else
			{
			/* Copy the color specifications: */
			for (surfaceI = 0; surfaceI < 2; surfaceI++)
				{
				/* Left color: */
				memcpy (curr_atomSP->left_rgbSA + surfaceI,
					cyan_blue_schemeSP->left_rgbSA +
					surfaceI,
					rgb_struct_size);

				/* Middle color: */
				memcpy (curr_atomSP->middle_rgbSA + surfaceI,
					cyan_blue_schemeSP->middle_rgbSA +
					surfaceI,
					rgb_struct_size);

				/* Right color: */
				memcpy (curr_atomSP->right_rgbSA + surfaceI,
					cyan_blue_schemeSP->right_rgbSA +
					surfaceI,
					rgb_struct_size);
				}
			}
		}

	/* Reset the position_changedF: */
	curr_mol_complexSP->position_changedF = 1;
	}

/* Return positive value: */
return 1;
}

/*===========================================================================*/


