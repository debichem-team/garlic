/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				translate_plane.c

Purpose:
	Translate the plane.

Input:
	(1) Pointer to MolComplexS structure.
	(2) Pointer to VectorS structure, with shift vector.

Output:
	(1) Plane translated.

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======translate plane:=====================================================*/

void TranslatePlane_ (MolComplexS *curr_mol_complexSP, VectorS *shift_vectorSP)
{
PlaneS		*curr_planeSP;

/* Pointer to the current plane: */
curr_planeSP = &curr_mol_complexSP->planeS;

/* Translate the plane center: */
curr_planeSP->center_x[0] += shift_vectorSP->x;
curr_planeSP->center_y    += shift_vectorSP->y;
curr_planeSP->center_z[0] += shift_vectorSP->z;

/* Update stereo data: */
curr_planeSP->center_x[1] += shift_vectorSP->x;
curr_planeSP->center_z[1] += shift_vectorSP->z;

/* Set the position_changedF: */
curr_mol_complexSP->position_changedF = 1;
}

/*===========================================================================*/


