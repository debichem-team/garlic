/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				translate_complex.c

Purpose:
	In default editing mode (0 = no editing), translate the macromol.
	complex and update  the geometric,  slab and color fading center.
	If editing atoms  (editing mode 1),  translate only  the selected
	atoms and leave the geometric parameters unchanged.

Input:
	(1) Pointer to MolComplexS structure, with macromolecular data to
	    be modified.
	(2) Pointer to VectorS structure, with shift vector.
	(3) Pointer to ConfigS structure (stereo flag used).
	(4) Editing mode index  (0 = no editing, 1 = editing atoms etc.).

Output:
	(1) The structure will be translated.

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======translate macromolecular complex:====================================*/

void TranslateComplex_ (MolComplexS *mol_complexSP,
			VectorS *shift_vectorSP, ConfigS *configSP,
			int edit_modeI)
{
size_t		i;
AtomS		*curr_atomSP;

/* Translate all atoms: */
for (i = 0; i < mol_complexSP->atomsN; i++)
	{
	/* Pointer to the current atom: */
	curr_atomSP = mol_complexSP->atomSP + i;

	/* If editing atoms, do not translate */
	/* this atom  if it is  not selected: */
	if (edit_modeI == 1)
		{
		if (curr_atomSP->selectedF == 0) continue;
		}

	/* Translate atom: */
	curr_atomSP->raw_atomS.x[0] += shift_vectorSP->x;
	curr_atomSP->raw_atomS.y    += shift_vectorSP->y;
	curr_atomSP->raw_atomS.z[0] += shift_vectorSP->z;
	}

if (configSP->stereoF)
	{
	for (i = 0; i < mol_complexSP->atomsN; i++)
		{
		/* Pointer to the current atom: */
		curr_atomSP = mol_complexSP->atomSP + i;

		/* If editing atoms, do not translate */
		/* this atom  if it is  not selected: */
		if (edit_modeI == 1)
			{
			if (curr_atomSP->selectedF == 0) continue;
			}

		/* Translate atom: */
		curr_atomSP->raw_atomS.x[1] += shift_vectorSP->x;
		curr_atomSP->raw_atomS.z[1] += shift_vectorSP->z;
		}
	}

/* If not editing atoms, update geometric center and some other data: */
if (edit_modeI != 1)
	{
	/* Update the geometric center: */
	mol_complexSP->geometric_center_vectorS.x += shift_vectorSP->x;
	mol_complexSP->geometric_center_vectorS.y += shift_vectorSP->y;
	mol_complexSP->geometric_center_vectorS.z += shift_vectorSP->z;

	/* Update the extent vectors: */
	mol_complexSP->left_top_near_vectorS.x    += shift_vectorSP->x;
	mol_complexSP->left_top_near_vectorS.y    += shift_vectorSP->y;
	mol_complexSP->left_top_near_vectorS.z    += shift_vectorSP->z;
	mol_complexSP->right_bottom_far_vectorS.x += shift_vectorSP->x;
	mol_complexSP->right_bottom_far_vectorS.y += shift_vectorSP->y;
	mol_complexSP->right_bottom_far_vectorS.z += shift_vectorSP->z;

	/* Update the rotation center: */
	mol_complexSP->rotation_center_vectorS.x  += shift_vectorSP->x;
	mol_complexSP->rotation_center_vectorS.y  += shift_vectorSP->y;
	mol_complexSP->rotation_center_vectorS.z  += shift_vectorSP->z;

	/* Update the slab center: */
	mol_complexSP->slab_center_vectorS.x      += shift_vectorSP->x;
	mol_complexSP->slab_center_vectorS.y      += shift_vectorSP->y;
	mol_complexSP->slab_center_vectorS.z      += shift_vectorSP->z;

	/* Update the color fading center: */
	mol_complexSP->fading_center_vectorS.x    += shift_vectorSP->x;
	mol_complexSP->fading_center_vectorS.y    += shift_vectorSP->y;
	mol_complexSP->fading_center_vectorS.z    += shift_vectorSP->z;
	}

/* Update the position_changedF flag: */
mol_complexSP->position_changedF = 1;
}

/*===========================================================================*/


