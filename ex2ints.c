/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				ex2ints.c

Purpose:
	Extract two integers from a string.

Input:
	(1) Pointer to the first integer.
	(2) Pointer to the second integer.
	(3) Input string pointer.

Output:
	(1) The first integer value.
	(2) The second integer value.
	(3) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>

/*======extract two integers from a string:==================================*/

int ExtractTwoIntegers_ (int *value1P, int *value2P, char *sP)
{
char		*P0, *P1;
int		n;
int		n1, n2;

/* Colon should be separator: */
if ((P0 = strstr (sP, ":")) == NULL) P0 = sP;
else P0++;

/* Replace each non-numeric character except minus sign with space: */
P1 = P0;
while ((n = *P1++) != '\0') if (!isdigit (n) && (n != '-')) *(P1 - 1) = ' ';

/* Try to read two integers: */
if (sscanf (P0, "%d %d", &n1, &n2) != 2) return -1;

/* On success, copy the extracted values: */
*value1P = n1;
*value2P = n2;

/* If everything worked fine, return positive integer: */
return 1;
}

/*===========================================================================*/


