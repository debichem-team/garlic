/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				warnmsg.c

Purpose:
	Print warning message.

Input:
	(1) Program name.
	(2) Name of the function where something strange happened.
	(3) Name of the file which caused trouble, if file caused trouble.
	(4) \
	(5)  \   ... Warning message, split into four strings.
	(6)  /       Don't forget to put newline somewhere!
	(7) /

Output:
	(1) Warning message written to stdout.

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>
#include <string.h>

/*======print warning message:===============================================*/

/* Note: there is no newline in the last four fprintf! */

void WarningMessage_ (char *module, char *function, char *filename,
		      char *s1, char *s2, char *s3, char *s4)
{
fprintf (stdout, "WARNING>");
fprintf (stdout, " module: %s, function: %s", module, function);
if (strlen (filename) != 0) fprintf (stdout, ", file: %s", filename);
fprintf (stdout, "\n");
if (strlen (s1) != 0) fprintf (stdout, "%s", s1);
if (strlen (s2) != 0) fprintf (stdout, "%s", s2);
if (strlen (s3) != 0) fprintf (stdout, "%s", s3);
if (strlen (s4) != 0) fprintf (stdout, "%s", s4);
}

/*===========================================================================*/


