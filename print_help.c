/* Copyright (C) 2000-2006 Damir Zucic */

/*=============================================================================

				print_help.c

Purpose:
	Print brief help: short introduction and list of options. This is not
	the true (on-line) help for garlic!

Input:
	No input.

Output:
	Introductory information written to stdout.

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>

/*======print brief help:====================================================*/

void PrintHelp_ (void)
{
fprintf (stdout, "\n");
fprintf (stdout,
"=========================================================================\n");
fprintf (stdout,
"               Garlic - free molecular viewer and editor\n");
fprintf (stdout,
"=========================================================================\n");
fprintf (stdout, "\n");
fprintf (stdout,
"Garlic  was written by  Damir Zucic  (zucic@mefos.hr)  as a complement to\n");
fprintf (stdout,
"other molecular  visualization  programs,  like rasmol  (by Roger Sayle).\n");
fprintf (stdout,
"It was written to simplify  the analysis of membrane proteins, but it can\n");
fprintf (stdout,
"be used as general purpose molecular visualization program, provided that\n");
fprintf (stdout,
"molecular coordinates are available in  PDB format, version 2.1 or older.\n");
fprintf (stdout,
"Garlic may also be used to analyze protein sequences.\n");
fprintf (stdout, "\n");
fprintf (stdout,
"Some of the  features, which  distinguish this program from  others, are:\n");
fprintf (stdout,
"(1) Coordinates, labels and  other data are  automatically displayed  for\n");
fprintf (stdout,
"    the atom nearest to the current pointer (mouse) position.\n");
fprintf (stdout,
"(2) Additional numeric data,  like  hydrophobicity, may be associated  to\n");
fprintf (stdout,
"    each amino acid. The  whole  structure  may be  colored  according to\n");
fprintf (stdout,
"    these values.\n");
fprintf (stdout,
"(3) Selection mechanism is different.\n");
fprintf (stdout,
"(4) Two bonds at different distance from  observer have different colors.\n");
fprintf (stdout,
"-------------------------------------------------------------------------\n");
fprintf (stdout,
"Usage:              garlic [true_garlic_options] [X11_options] [filename]\n");
fprintf (stdout,
"-------------------------------------------------------------------------\n");
fprintf (stdout,
"Command line options may be divided in two groups: true (specific) garlic\n");
fprintf (stdout,
"options and X11 options.  Options may be given in any order.  All options\n");
fprintf (stdout,
"and hard-coded  default values are listed  in the table  below.  For each\n");
fprintf (stdout,
"parameter, the value defined through command line is used,  if available.\n");
fprintf (stdout,
"If not, the value defined in .garlicrc file is used. If .garlicrc file is\n");
fprintf (stdout,
"not  available,  or there is  no corresponding  entry in  this file,  the\n");
fprintf (stdout,
"hard-coded default value is used.  The sample  .garlicrc file included in\n");
fprintf (stdout,
"the original package contains  the values equal to  hard-coded  defaults.\n");
fprintf (stdout,
"-------------------------------------------------------------------------\n");
fprintf (stdout,
"garlic option:        Description:\n");
fprintf (stdout,
"-------------------------------------------------------------------------\n");
fprintf (stdout,
"-h, --help            Print help and exit.\n");
fprintf (stdout,
"-v, --version         Print version and exit.\n");
fprintf (stdout,
"-r, --register        Register garlic:  inform author that  your site has\n");
fprintf (stdout,
"                        garlic installed and that someone is going to use\n");
fprintf (stdout,
"                        this program. By registering, you are encouraging\n"); 
fprintf (stdout,
"                        author to continue development of garlic. You can\n");
fprintf (stdout,
"                        also help  author to  ensure support  for further\n");
fprintf (stdout,
"                        development. This option sends an e-mail message,\n");
fprintf (stdout,
"                        containing only the e-mail message header and the\n");
fprintf (stdout,
"                        word 'Hi!', to zucic@garlic.mefos.hr. Please type\n");
fprintf (stdout,
"                        garlic -r   if you are going to use this program.\n");
fprintf (stdout,
"                        If your system is  not connected to the Internet,\n");
fprintf (stdout,
"                        you can send short e-mail from some other system.\n");
fprintf (stdout,
"-nosys                Hide the coordinate system icon  (top left corner).\n");
fprintf (stdout,
"-no-control           Hide the control window (upper right corner).\n");
fprintf (stdout,
"-stereo               Display stereo image.\n");
fprintf (stdout,
"-slab mode            Default slab mode.  Available slab modes are:  off,\n");
fprintf (stdout,
"                        planar (default),  sphere, half-sphere,  cylinder\n");
fprintf (stdout,
"                        and half cylinder.\n");
fprintf (stdout,
"-fading mode          Default color fading mode.  Available  modes:  off,\n");
fprintf (stdout,
"                        planar (default),  sphere, half-sphere,  cylinder\n");
fprintf (stdout,
"                        and half cylinder.\n");
fprintf (stdout,
"-as number            Default atom drawing style (hard-coded default: 2).\n");
fprintf (stdout,
"-bs number            Default bond drawing style (hard-coded default: 3).\n");
fprintf (stdout,
"-fs number            Default number of  color fading surfaces.  Parallel\n");
fprintf (stdout,
"                        planes, concentric spheres or conc. cylinders may\n");
fprintf (stdout,
"                        be used as  fading surfaces.  Three  RGB triplets\n");
fprintf (stdout,
"                        are assigned to each surface. Surfaces are evenly\n");
fprintf (stdout,
"                        spaced;  at least one and at most eight should be\n");
fprintf (stdout,
"                        defined. At least three colors  (left, middle and\n");
fprintf (stdout,
"                        right) are assigned to  each visible atom.  These\n");
fprintf (stdout,
"                        three colors are based on the position of a given\n");
fprintf (stdout,
"                        atom  relative to  the nearest  fading surface(s)\n");
fprintf (stdout,
"                        and on colors assigned to these surfaces.  Colors\n");
fprintf (stdout,
"                        are combined using linear weighting.\n");
fprintf (stdout,
"-lc<i> color          Left color at the  i-th surface;  i is between zero\n");
fprintf (stdout,
"                        and seven.  The color string should be  compliant\n");
fprintf (stdout,
"                        with X11R5.  Blue, for example,  may be specified\n");
fprintf (stdout,
"                        as blue or as RGB:0000/0000/FFFF etc. For example\n");
fprintf (stdout,
"                        -lc4 RGB:BBBB/8888/4444  defines  the left  color\n");
fprintf (stdout,
"                        at the surface whose index is equal to four.\n");
fprintf (stdout,
"-mc<i> color          Middle color at the i-th surface.\n");
fprintf (stdout,
"-rc<i> color          Right color at the i-th surface.\n");
fprintf (stdout,
"-pc, --print-config   Print configuration data and exit.\n");
fprintf (stdout,
"-pcn                  Print cursor names to stdout and exit.\n");
fprintf (stdout,
"-log logfile          Write commands and messages to  log file.  The file\n");
fprintf (stdout,
"                        will be created in the current working directory.\n");
fprintf (stdout,
"                        If this is not possible, log file will be created\n");
fprintf (stdout,
"                        in users home directory.\n");
fprintf (stdout,
"-------------------------------------------------------------------------\n");
fprintf (stdout,
"X11 option:           Description:\n");
fprintf (stdout,
"-------------------------------------------------------------------------\n");
fprintf (stdout,
"-display displayname  X server to contact.\n");
fprintf (stdout,
"-geometry geom        Window geometry (default: almost the whole screen).\n");
fprintf (stdout,
"-bg color             Main window background color (default is black).\n");
fprintf (stdout,
"-fg color             Main window foreground color (default: white).\n");
fprintf (stdout,
"-fn fontname          Text font; (default is 10x20).\n");
fprintf (stdout,
"-tbg color            Text background color (default is black).\n");
fprintf (stdout,
"-tfg color            Text color (default is white).\n");
fprintf (stdout,
"-cursor cursorname    Cursor name; see  /usr/include/X11/cursorfont.h for\n");
fprintf (stdout,
"                         names; remove the XC_ prefix.\n");
fprintf (stdout,
"-------------------------------------------------------------------------\n");
fprintf (stdout,
"filename:             The input file name.  Full pathname may be used. If\n");
fprintf (stdout,
"                      filename does not contain the path, garlic will try\n");
fprintf (stdout,
"                      to find  the specified file in  the current working\n");
fprintf (stdout,
"                      directory.  If the first attempt  to open  the file\n");
fprintf (stdout,
"                      fails,  the filename  is appended to the content of\n");
fprintf (stdout,
"                      the environment  variable  MOL_DIR  for  the second\n");
fprintf (stdout,
"                      attempt.  If this fails too, garlic will search all\n");
fprintf (stdout,
"                      directories  specified in  the environment variable\n");
fprintf (stdout,
"                      MOL_PATH.\n");
fprintf (stdout,
"-------------------------------------------------------------------------\n");
fprintf (stdout,
"Personal initializations may be written to .garlicrc file. Normally, this\n");
fprintf (stdout,
"file should be kept in users home directory.  Note that this is  not  the\n");
fprintf (stdout,
"first directory to be searched for  .garlicrc file! Up to seven pathnames\n");
fprintf (stdout,
"are tryed in the following order:\n");
fprintf (stdout, "\n");
fprintf (stdout,
".garlicrc                       .garlicrc  in current  working directory.\n");
fprintf (stdout,
"$HOME/.garlicrc                 .garlicrc in home directory.  This is the\n");
fprintf (stdout,
"                                   recommended location.\n");
fprintf (stdout,
"$HOME/garlic/.garlicrc          ./garlicrc,  subdirectory garlic of users\n");
fprintf (stdout,
"                                   home directory.\n");
fprintf (stdout,
"/usr/share/garlic/.garlicrc     ./garlicrc in  /usr/share/garlic (Linux).\n");
fprintf (stdout,
"/etc/garlicrc                   garlicrc in /etc, Debian Linux; note that\n");
fprintf (stdout,
"                                   file name is  garlicrc  in  this case!\n");
fprintf (stdout,
"/usr/local/lib/garlic/.garlicrc .garlicrc  in  /usr/local/lib/garlic dir.\n");
fprintf (stdout,
"/usr/lib/garlic/.garlicrc       .garlicrc in /usr/lib/garlic; this is the\n");
fprintf (stdout,
"                                   last attempt; if it failes, hard-coded\n");
fprintf (stdout,
"                                   default values will be used.\n");
fprintf (stdout,
"-------------------------------------------------------------------------\n");
fprintf (stdout,
"Environment variables:\n");
fprintf (stdout,
"-------------------------------------------------------------------------\n");
fprintf (stdout,
"MOL_DIR               Defines where to look for the specified file if not\n");
fprintf (stdout,
"                         found in the current working directory.\n");
fprintf (stdout,
"MOL_PATH              The list of directories to be searched.  It is used\n");
fprintf (stdout,
"                         if attempt with MOL_DIR failes  or if MOL_DIR is\n");
fprintf (stdout,
"                         not defined. Use space  or colon (:) to separate\n");
fprintf (stdout,
"                         directory names. MOL_PATH is quite useful; it is\n");
fprintf (stdout,
"                         recommended to define  this environment variable\n");
fprintf (stdout,
"                         through  .login,  .bashrc or  some other script.\n");
fprintf (stdout,
"                         MOL_PATH  is more useful  than  MOL_DIR so it is\n");
fprintf (stdout,
"                         recommended  to define only  MOL_PATH  variable.\n");
fprintf (stdout,
"DISPLAY               X server to which  the program should be connected.\n");
fprintf (stdout,
"-------------------------------------------------------------------------\n");
fprintf (stdout,
"garlic home page:                           http://garlic.mefos.hr/garlic\n");
fprintf (stdout,
"Damir Zucic home page:                      http://garlic.mefos.hr/zucic\n");
fprintf (stdout,
"E-mail:                                     zucic@mefos.hr\n");
fprintf (stdout,
"=========================================================================\n");
fprintf (stdout, "\n");
}

/*===========================================================================*/


