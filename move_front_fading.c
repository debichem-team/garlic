/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				move_front_fading.c

Purpose:
	Change the position of the front fading surface for each caught
	macromolecular complex.

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) Number of macromolecular complexes.
	(3) Pointer to ConfigS structure.
	(4) Translation shift.

Output:
	(1) The front fading surface moved.

Return value:
	No return value.

Notes:
	(1) Indentation is exceptionally 4 spaces.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======move the front fading surface:=======================================*/

void MoveFrontFading_ (MolComplexS *mol_complexSP, int mol_complexesN,
		       ConfigS *configSP, double shift)
{
int			mol_complexI;
MolComplexS		*curr_mol_complexSP;

for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
    {
    /* Pointer to the current macromolecular complex: */
    curr_mol_complexSP = mol_complexSP + mol_complexI;

    /* Check is the current macromolecular complex caught: */
    if (curr_mol_complexSP->catchF == 0) continue;

    /* Move front fading surface: */
    switch (curr_mol_complexSP->fading_modeI)
	{
	/* Planar fading: */
	case 1:
	    /* Prepare the new position: */
	    curr_mol_complexSP->fading_front_relative_position += shift;

	    /* Check it: */
            if (curr_mol_complexSP->fading_front_relative_position >
		curr_mol_complexSP->fading_back_relative_position -
		MINFADINGSPACING)
		{
		curr_mol_complexSP->fading_front_relative_position =
			curr_mol_complexSP->fading_back_relative_position -
			MINFADINGSPACING;
		}
	    break;

	/* Spherical, semi-spher., cylindrical and semi-cyl. fading: */
	case 2:
	case 3:
	case 4:
	case 5:
	    /* Prepare the new position: */
	    curr_mol_complexSP->fading_front_relative_position -= shift;

	    /* Check it: */
	    if (curr_mol_complexSP->fading_front_relative_position <
		curr_mol_complexSP->fading_back_relative_position +
		MINFADINGSPACING)
		{
		curr_mol_complexSP->fading_front_relative_position =
			curr_mol_complexSP->fading_back_relative_position +
			MINFADINGSPACING;
		}
	    if (curr_mol_complexSP->fading_front_relative_position < 0)
		{
		curr_mol_complexSP->fading_front_relative_position = 0.0;
		}
	    break;

	/* Ignore all other cases: */
	default:
	    ;
	}

    /* Set the position_changedF: */
    curr_mol_complexSP->position_changedF = 1;
    }
}

/*===========================================================================*/


