/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				labels.c

Purpose:
	Execute labels command.

Input:
	(1) Pointer to MolComplexS structure.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to ConfigS structure.
	(5) Pointer to GUIS structure.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
	(9) Pointer to  the remainder of the command string.  It is either
	    empty or contains the keyword OFF.

Output:
	(1) Label flag set for each atom in each caught macromol. complex.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute labels command:==============================================*/

int Labels_ (MolComplexS *mol_complexSP, int mol_complexesN,
	     RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
	     NearestAtomS *nearest_atomSP, size_t pixelsN,
	     unsigned int *refreshIP, char *stringP)
{
char		*remainderP;
char		tokenA[STRINGSIZE];
int		labelF;
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;
size_t		atomsN, atomI;
AtomS		*curr_atomSP;

/* Initialize the labelF: */
labelF = 1;

/* Extract the first token: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, stringP, " \t\n");

/* If available, the first token should contain the keyword OFF: */
if (remainderP)
	{
	if (strstr (tokenA, "OFF") == tokenA) labelF = 0;
	else
		{
		sprintf (runtimeSP->messageA,
			 "Keyword %s not recognized!",
			 tokenA);
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_LABELS;
		}
	}

/* Check every macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/** Pointer to the current macromolecular complex: **/
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/** Check is the current macromolecular complex caught: **/
	if (curr_mol_complexSP->catchF == 0) continue;

	/** Number of atoms in a macromolecular complex: **/
	atomsN = curr_mol_complexSP->atomsN;
	if (atomsN == 0) continue;

	/** Scan all atoms in the current complex: **/
	for (atomI = 0; atomI < atomsN; atomI++)
		{
		/** Pointer to the current atom: **/
		curr_atomSP = curr_mol_complexSP->atomSP + atomI;

		/** Check the selection flag; set the labelF: **/
		if (curr_atomSP->selectedF) curr_atomSP->labelF = labelF;
		}
	}

/* Set the position_changedF flag: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/** Set flag: **/
	(mol_complexSP + mol_complexI)->position_changedF = 1;
	}

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return the command code: */
return COMMAND_LABELS;
}

/*===========================================================================*/


