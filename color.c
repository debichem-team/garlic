/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				color.c

Purpose:
	Execute color command: assign the requested color scheme to all
	currently selected atoms in each  caught complex.  This command
	may not be used  to change  the surface colors.  The only value
	of the mode index  (the last argument to ColorSchemes_) allowed
	in this function is zero.

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure, with some runtime data.
	(4) Pointer to ConfigS structure, with configuration data.
	(5) Pointer to GUIS structure, with GUI data.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
	(9) String which contains the color scheme name.

Output:
	(1) Atomic colors changed.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		FullColors_ (MolComplexS *, int, GUIS *, char *);
char		*ExtractToken_ (char *, int, char *, char *);
int		ColorSchemes_ (MolComplexS *, int, ConfigS *,
			       GUIS *, char *, int);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute color command:===============================================*/

int Color_ (MolComplexS *mol_complexSP, int mol_complexesN,
	    RuntimeS *runtimeSP,
	    ConfigS *configSP, GUIS *guiSP,
	    NearestAtomS *nearest_atomSP, size_t pixelsN,
	    unsigned int *refreshIP, char *stringP)
{
char		*commandP;
char		*remainderP;
char		tokenA[STRINGSIZE];
int		n;

/* Try to interpret  the string as  the full color */
/* specification. Use the original command string: */
commandP = runtimeSP->curr_commandA;
if (FullColors_ (mol_complexSP, mol_complexesN, guiSP, commandP) > 0)
	{
	(*refreshIP)++;
	MainRefresh_ (mol_complexSP, mol_complexesN,
		      runtimeSP, configSP, guiSP,
		      nearest_atomSP, pixelsN, *refreshIP);
	ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
			 configSP, guiSP);
	return COMMAND_COLOR;
	}

/* Extract the color scheme name: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, stringP, " \t\n");
if (!remainderP)
	{
	strcpy (runtimeSP->messageA, "Color scheme specification missing!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_NO_SCHEME;
	}

/* Assign the requested predefined colors to  all selected atoms. */
/* The mode index (the last ColorSchemes_ argument) should be set */
/* to zero, to define the atomic colors,  not the surface colors. */

/** If some simple color scheme is requested: **/
n = ColorSchemes_ (mol_complexSP, mol_complexesN, configSP, guiSP, tokenA, 0);

/** If scheme is not recognized: **/
if (n <= 0)
	{
	sprintf (runtimeSP->messageA,
		 "Color scheme %s is not available!", tokenA);
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_BAD_SCHEME;
	}

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN, runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return positive value on success: */
return COMMAND_COLOR;
}

/*===========================================================================*/


