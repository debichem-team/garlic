/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				selectev.c

Purpose:
	Select events.

Input:
	Pointer to GUIS structure.

Output:
	X server informed about events which should be reported for
	given windows.

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======select events:=======================================================*/

void SelectEvents_ (GUIS *guiSP)
{
long		event_mask;

event_mask = ExposureMask |
	     KeyPressMask | KeyReleaseMask |
	     ButtonPressMask | ButtonReleaseMask |
	     PointerMotionMask |
	     StructureNotifyMask;	/* Selects ConfigureNotify event */

XSelectInput (guiSP->displaySP, guiSP->main_winS.ID, event_mask);
}

/*===========================================================================*/


