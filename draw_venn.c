/* Copyright (C) 2000-2004 Damir Zucic */

/*=============================================================================

				draw_venn.c

Purpose:
	Draw Venn diagram.  This function is used if the main win. drawing
	mode is equal to three. The sequence stored to the sequence buffer
	is used to prepare the Venn diagram.

Input:
	(1) Pointer to RuntimeS structure, with some runtime data.
	(2) Pointer to GUIS structure, with GUI data.
	(3) Pointer to NearestAtomS structure,  with information about the
	    atom occupying the given pixel.
	(4) The number of pixels in the main window free area.
	(5) The refreshI,  used to check the  NearestAtomS associated with
	    a given pixel. It is currently unused in this function.

Output:
	(1) Venn diagram drawn to the main window.
	(2) Return value.

Return value:
	(1) The number of residues used to prepare the Venn diagram.

Notes:
	(1) The initial drawing destination is the hidden pixmap,  and the
	    content of this pixmap is copied later to the main window.

	(2) Cysteine is treated  in a special way:  cysteine which  is not
	    involved in a disulfide bond should be treated as polar, while
	    cysteine which takes part  in disulfide bond  formation is not
	    polar.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		RefreshPixmap_ (Display *, Pixmap, GC,
				unsigned int, unsigned int, unsigned long);

/*======draw Venn diagram:===================================================*/

size_t DrawVenn_ (RuntimeS *runtimeSP, GUIS *guiSP,
		  NearestAtomS *nearest_atomSP, size_t pixelsN,
		  unsigned int refreshI)
{
int		max_length;
size_t		residuesN, residueI;
char		*residue_nameP;
char		residue_nameA[RESNAMESIZE];
int		range_startI, range_endI, residues_in_vennN = 0;
size_t		vennI = 0;
int		alaN = 0, argN = 0, asnN = 0, aspN = 0, cysN = 0;
int		glnN = 0, gluN = 0, glyN = 0, hisN = 0, ileN = 0;
int		leuN = 0, lysN = 0, metN = 0, pheN = 0, proN = 0;
int		serN = 0, thrN = 0, trpN = 0, tyrN = 0, valN = 0;
int		unkN = 0;
int		S_S_cysN = 0, S_H_cysN = 0;
int		hydrophobicN, polarN, smallN;
int		aromaticN, aliphaticN, tinyN, chargedN;
int		positiveN, negativeN;
int		screen_margin_x, screen_margin_y;
int		screen_delta_x, screen_delta_y;
int		screen_x0, screen_y0, screen_x;
unsigned int	screen_width, screen_height;
double		d;
int		text_width, text_height;
char		stringA[SHORTSTRINGSIZE];

/* The number of residues in sequence buffer: */
residuesN = runtimeSP->residuesN;
if (residuesN == 0)
	{
	strcpy (runtimeSP->messageA, "The sequence buffer is empty!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return 0;
	}

/* The maximal residue name length: */
max_length = RESNAMESIZE - 1;

/* Residue array index range: */
range_startI = runtimeSP->range_startI;
range_endI   = runtimeSP->range_endI;

/* Count the number of residues in the specified range: */
for (residueI = 0; residueI < residuesN; residueI++)
	{
	/* Check is the residue array index inside the range: */
	if (((int) residueI < range_startI) || ((int) residueI > range_endI))
		{
		continue;
		}

	/* Increase the count of residues if current */
	/* residue  belongs to  the specified range: */
	residues_in_vennN++;
	}

/* Check the number of residues in Venn diagram: */
if (residues_in_vennN == 0)
	{
	strcpy (runtimeSP->messageA, "The specified range is empty!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return 0;
	}

/* Refresh the hidden pixmap, where drawing will be done: */
RefreshPixmap_ (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[0],
		guiSP->control_winS.x0, guiSP->input_winS.y0,
		guiSP->main_winS.bg_colorID);

/*------auxilliary parameters:-----------------------------------------------*/

/* Text line height: */
text_height = guiSP->main_winS.font_height;

/* Prepare margins and useful area width and height: */
screen_margin_x = guiSP->main_win_free_area_width / 20;
if (screen_margin_x < 2) screen_margin_x = 2;
screen_margin_y = guiSP->main_win_free_area_height / 20;
if (screen_margin_y < 2) screen_margin_y = 2;
screen_delta_x = guiSP->main_win_free_area_width  - 2 * screen_margin_x;
if (screen_delta_x < 0) screen_delta_x = 0;
screen_delta_y = guiSP->main_win_free_area_height - 2 * screen_margin_y;
if (screen_delta_y < 0) screen_delta_y = 0;

/*------scan the sequence and count residues by type:------------------------*/

/* Count residues by type: */
for (residueI = 0; residueI < residuesN; residueI++)
	{
	/* Check is the residue array index inside the range: */
	if (((int) residueI < range_startI) || ((int) residueI > range_endI))
		{
		continue;
		}

	/* Pointer to the current residue name: */
	residue_nameP = runtimeSP->sequenceP + residueI * max_length;

	/* Copy the residue name: */
	strncpy (residue_nameA, residue_nameP, max_length);
	residue_nameA[max_length] = '\0';

	/* Check the residue name: */
	if      (strcmp (residue_nameA, "ALA") == 0) alaN++;
	else if (strcmp (residue_nameA, "ARG") == 0) argN++;
	else if (strcmp (residue_nameA, "ASN") == 0) asnN++;
	else if (strcmp (residue_nameA, "ASP") == 0) aspN++;
	else if (strcmp (residue_nameA, "CYS") == 0)
		{
		/* Update the total number of cysteines: */
		cysN++;

		/* Check disulfide flag: */
		if (*(runtimeSP->disulfideFP + residueI)) S_S_cysN++;
		else S_H_cysN++;
		}
	else if (strcmp (residue_nameA, "GLN") == 0) glnN++;
	else if (strcmp (residue_nameA, "GLU") == 0) gluN++;
	else if (strcmp (residue_nameA, "GLY") == 0) glyN++;
	else if (strcmp (residue_nameA, "HIS") == 0) hisN++;
	else if (strcmp (residue_nameA, "ILE") == 0) ileN++;
	else if (strcmp (residue_nameA, "LEU") == 0) leuN++;
	else if (strcmp (residue_nameA, "LYS") == 0) lysN++;
	else if (strcmp (residue_nameA, "MET") == 0) metN++;
	else if (strcmp (residue_nameA, "PHE") == 0) pheN++;
	else if (strcmp (residue_nameA, "PRO") == 0) proN++;
	else if (strcmp (residue_nameA, "SER") == 0) serN++;
	else if (strcmp (residue_nameA, "THR") == 0) thrN++;
	else if (strcmp (residue_nameA, "TRP") == 0) trpN++;
	else if (strcmp (residue_nameA, "TYR") == 0) tyrN++;
	else if (strcmp (residue_nameA, "VAL") == 0) valN++;
	else unkN++;

	/* Update the Venn diagram index: */
	vennI++;
	}

/* Prepare the combined counts: */
hydrophobicN = pheN + tyrN + trpN + hisN + metN +
	       ileN + leuN + valN + alaN + glyN +
	       cysN;
polarN = tyrN + trpN + hisN + lysN + argN + gluN +
	 glnN + aspN + asnN + thrN + serN + S_H_cysN;
smallN = glyN + alaN + serN + proN + valN + thrN + aspN + asnN + cysN;
chargedN = argN + lysN + hisN + gluN + aspN;
aromaticN = trpN + tyrN + pheN + hisN;
aliphaticN = valN + leuN + ileN;
tinyN = glyN + alaN + serN + S_H_cysN;
positiveN = argN + lysN + hisN;
negativeN = gluN + aspN;

/*------draw polar, hydrophobic and small rectangle:-------------------------*/

/* Polar rectangle: */
screen_x0 = screen_margin_x;
d = 0.075 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (unsigned int) d;
d = 0.666 * (double) screen_delta_x;
if (d < 1.0) d = 1.0;
screen_width = (unsigned int) d;
d = 0.765 * (double) screen_delta_y;
if (d < 1.0) d = 1.0;
screen_height = (unsigned int) d;
XSetForeground (guiSP->displaySP, guiSP->theGCA[0], guiSP->blue_colorID);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[0],
		screen_x0, screen_y0,
		screen_width, screen_height);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[0],
		screen_x0 - 1, screen_y0 - 1,
		screen_width + 2, screen_height + 2);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[0],
		screen_x0 - 2, screen_y0 - 2,
		screen_width + 4, screen_height + 4);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[0],
		screen_x0 - 3, screen_y0 - 3,
		screen_width + 6, screen_height + 6);

/* Hydrophobic rectangle: */
d = 0.333 * (double) screen_delta_x;
screen_x0 = screen_margin_x + (unsigned int) d;
screen_y0 = screen_margin_y;
d = 0.666 * (double) screen_delta_x;
if (d < 1.0) d = 1.0;
screen_width = (unsigned int) d;
d = 0.824 * (double) screen_delta_y;
if (d < 1.0) d = 1.0;
screen_height = (unsigned int) d;
XSetForeground (guiSP->displaySP, guiSP->theGCA[1], guiSP->red_colorID);
XDrawRectangle (guiSP->displaySP,
                guiSP->main_hidden_pixmapID,
                guiSP->theGCA[1],
                screen_x0, screen_y0,
                screen_width, screen_height);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[1],
		screen_x0 - 1, screen_y0 - 1,
		screen_width + 2, screen_height + 2);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[1],
		screen_x0 - 2, screen_y0 - 2,
		screen_width + 4, screen_height + 4);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[1],
		screen_x0 - 3, screen_y0 - 3,
		screen_width + 6, screen_height + 6);

/* Small rectangle (small refers to small residues, not to the rectangle): */
d = 0.012 * (double) screen_delta_x;
screen_x0 = screen_margin_x + (unsigned int) d;
d = 0.534 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (unsigned int) d;
d = 0.967 * (double) screen_delta_x;
if (d < 1.0) d = 1.0;
screen_width = (unsigned int) d;
d = 0.470 * (double) screen_delta_y;
if (d < 1.0) d = 1.0;
screen_height = (unsigned int) d;
XSetForeground (guiSP->displaySP, guiSP->theGCA[2], guiSP->green_colorID);
XDrawRectangle (guiSP->displaySP,
                guiSP->main_hidden_pixmapID,
                guiSP->theGCA[2],
                screen_x0, screen_y0,
                screen_width, screen_height);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[2],
		screen_x0 - 1, screen_y0 - 1,
		screen_width + 2, screen_height + 2);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[2],
		screen_x0 - 2, screen_y0 - 2,
		screen_width + 4, screen_height + 4);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[2],
		screen_x0 - 3, screen_y0 - 3,
		screen_width + 6, screen_height + 6);

/*------draw charged, aromatic, aliphatic and tiny rectangle:----------------*/

/* Charged rectangle: */
d = 0.023 * (double) screen_delta_x;
screen_x0 = screen_margin_x + (unsigned int) d;
d = 0.200 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (unsigned int) d;
d = 0.635 * (double) screen_delta_x;
if (d < 1.0) d = 1.0;
screen_width = (unsigned int) d;
d = 0.430 * (double) screen_delta_y;
if (d < 1.0) d = 1.0;
screen_height = (unsigned int) d;
XSetForeground (guiSP->displaySP, guiSP->theGCA[0], guiSP->blue_colorID);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[0],
		screen_x0, screen_y0,
		screen_width, screen_height);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[0],
		screen_x0 - 1, screen_y0 - 1,
		screen_width + 2, screen_height + 2);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[0],
		screen_x0 - 2, screen_y0 - 2,
		screen_width + 4, screen_height + 4);

/* Aromatic rectangle: */
d = 0.342 * (double) screen_delta_x;
screen_x0 = screen_margin_x + (unsigned int) d;
d = 0.088 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (unsigned int) d;
d = 0.648 * (double) screen_delta_x;
if (d < 1.0) d = 1.0;
screen_width = (unsigned int) d;
d = 0.255 * (double) screen_delta_y;
if (d < 1.0) d = 1.0;
screen_height = (unsigned int) d;
XSetForeground (guiSP->displaySP, guiSP->theGCA[1], guiSP->red_colorID);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[1],
		screen_x0, screen_y0,
		screen_width, screen_height);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[1],
		screen_x0 - 1, screen_y0 - 1,
		screen_width + 2, screen_height + 2);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[1],
		screen_x0 - 2, screen_y0 - 2,
		screen_width + 4, screen_height + 4);

/* Aliphatic rectangle: */
d = 0.679 * (double) screen_delta_x;
screen_x0 = screen_margin_x + (unsigned int) d;
d = 0.372 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (unsigned int) d;
d = 0.311 * (double) screen_delta_x;
if (d < 1.0) d = 1.0;
screen_width = (unsigned int) d;
d = 0.257 * (double) screen_delta_y;
if (d < 1.0) d = 1.0;
screen_height = (unsigned int) d;
XSetForeground (guiSP->displaySP, guiSP->theGCA[1], guiSP->red_colorID);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[1],
		screen_x0, screen_y0,
		screen_width, screen_height);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[1],
		screen_x0 - 1, screen_y0 - 1,
		screen_width + 2, screen_height + 2);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[1],
		screen_x0 - 2, screen_y0 - 2,
		screen_width + 4, screen_height + 4);

/* Tiny rectangle: */
d = 0.023 * (double) screen_delta_x;
screen_x0 = screen_margin_x + (unsigned int) d;
d = 0.730 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (unsigned int) d;
d = 0.948 * (double) screen_delta_x;
if (d < 1.0) d = 1.0;
screen_width = (unsigned int) d;
d = 0.190 * (double) screen_delta_y;
if (d < 1.0) d = 1.0;
screen_height = (unsigned int) d;
XSetForeground (guiSP->displaySP, guiSP->theGCA[2], guiSP->green_colorID);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[2],
		screen_x0, screen_y0,
		screen_width, screen_height);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[2],
		screen_x0 - 1, screen_y0 - 1,
		screen_width + 2, screen_height + 2);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[2],
		screen_x0 - 2, screen_y0 - 2,
		screen_width + 4, screen_height + 4);

/*------positive and negative rectangle:-------------------------------------*/

/* Positive rectangle: */
d = 0.030 * (double) screen_delta_x;
screen_x0 = screen_margin_x + (unsigned int) d;
d = 0.270 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (unsigned int) d;
d = 0.618 * (double) screen_delta_x;
if (d < 1.0) d = 1.0;
screen_width = (unsigned int) d;
d = 0.150 * (double) screen_delta_y;
if (d < 1.0) d = 1.0;
screen_height = (unsigned int) d;
XSetForeground (guiSP->displaySP, guiSP->theGCA[0], guiSP->blue_colorID);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[0],
		screen_x0, screen_y0,
		screen_width, screen_height);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[0],
		screen_x0 - 1, screen_y0 - 1,
		screen_width + 2, screen_height + 2);

/* Negative rectangle: */
d = 0.030 * (double) screen_delta_x;
screen_x0 = screen_margin_x + (unsigned int) d;
d = 0.430 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (unsigned int) d;
d = 0.292 * (double) screen_delta_x;
if (d < 1.0) d = 1.0;
screen_width = (unsigned int) d;
d = 0.190 * (double) screen_delta_y;
if (d < 1.0) d = 1.0;
screen_height = (unsigned int) d;
XSetForeground (guiSP->displaySP, guiSP->theGCA[0], guiSP->blue_colorID);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[0],
		screen_x0, screen_y0,
		screen_width, screen_height);
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[0],
		screen_x0 - 1, screen_y0 - 1,
		screen_width + 2, screen_height + 2);

/*------draw text:-----------------------------------------------------------*/

XSetForeground (guiSP->displaySP, guiSP->theGCA[0],
		guiSP->main_winS.fg_colorID);

/* TOTAL NUMBER OF RESIDUES: */
screen_x0 = screen_margin_x;
screen_y0 = screen_margin_y + text_height - 1;
sprintf (stringA, "TOTAL: %d", residuesN);
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* POLAR: */
screen_x0 = screen_margin_x + 5;
d = 0.075 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (int) d + text_height - 1;
sprintf (stringA, "POLAR: %d", polarN);
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
             screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* HYDROPHOBIC: */
d = 0.333 * (double) screen_delta_x;
screen_x0 = screen_margin_x + (int) d + 5;
screen_y0 = screen_margin_y + text_height - 1;
sprintf (stringA, "HYDROPHOBIC: %d", hydrophobicN);
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* SMALL: */
d = 0.012 * (double) screen_delta_x;
screen_x0 = screen_margin_x + (int) d + 5;
d = 0.985 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (int) d + guiSP->main_winS.fontSP->descent + 1;
sprintf (stringA, "SMALL: %d", smallN);
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* CHARGED: */
d = 0.023 * (double) screen_delta_x;
screen_x0 = screen_margin_x + (int) d + 5;
d = 0.200 * (double) screen_delta_y + text_height;
screen_y0 = screen_margin_y + (int) d - 1;
sprintf (stringA, "CHARGED: %d", chargedN);
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* AROMATIC: */
d = 0.670 * (double) screen_delta_x;
screen_x0 = screen_margin_x + (int) d + 5;
d = 0.088 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (int) d + text_height - 1;
sprintf (stringA, "AROMATIC: %d", aromaticN);
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* ALIPHATIC: */
d = 0.679 * (double) screen_delta_x;
screen_x0 = screen_margin_x + (int) d + 5;
d = 0.372 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (int) d + text_height - 1;
sprintf (stringA, "ALIPHATIC: %d", aliphaticN);
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* TINY: */
d = 0.023 * (double) screen_delta_x;
screen_x0 = screen_margin_x + (int) d + 5;
d = 0.905 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (unsigned int) d +
	    guiSP->main_winS.fontSP->descent + 1;
sprintf (stringA, "TINY: %d", tinyN);
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* POSITIVE: */
d = 0.030 * (double) screen_delta_x;
screen_x0 = screen_margin_x + (int) d + 5;
d = 0.270 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (int) d + text_height - 1;
sprintf (stringA, "POSITIVE: %d", positiveN);
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* NEGATIVE: */
d = 0.030 * (double) screen_delta_x;
screen_x0 = screen_margin_x + (int) d + 5;
d = 0.428 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (int) d + text_height - 1;
sprintf (stringA, "NEGATIVE: %d", negativeN);
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* The first five strings will be placed at the same screen_x: */
d = 0.167 * screen_delta_x;
screen_x = screen_margin_x + (int) d;

/* GLN: */
sprintf (stringA, "GLN: %d", glnN);
text_width = XTextWidth (guiSP->main_winS.fontSP, stringA, strlen (stringA));
screen_x0 = screen_x - text_width / 2;
d = 0.138 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (int) d + text_height / 2;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* ARG: */
sprintf (stringA, "ARG: %d", argN);
text_width = XTextWidth (guiSP->main_winS.fontSP, stringA, strlen (stringA));
screen_x0 = screen_x - text_width / 2;
d = 0.340 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (int) d + text_height / 2;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* LYS: */
sprintf (stringA, "LYS: %d", lysN);
text_width = XTextWidth (guiSP->main_winS.fontSP, stringA, strlen (stringA));
screen_x0 = screen_x - text_width / 2;
d = 0.380 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (int) d + text_height / 2;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* GLU: */
sprintf (stringA, "GLU: %d", gluN);
text_width = XTextWidth (guiSP->main_winS.fontSP, stringA, strlen (stringA));
screen_x0 = screen_x - text_width / 2;
d = 0.515 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (int) d;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* ASP: */
sprintf (stringA, "ASP: %d", aspN);
text_width = XTextWidth (guiSP->main_winS.fontSP, stringA, strlen (stringA));
screen_x0 = screen_x - text_width / 2;
d = 0.570 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (int) d + text_height / 2;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* SER: */
sprintf (stringA, "SER: %d", serN);
text_width = XTextWidth (guiSP->main_winS.fontSP, stringA, strlen (stringA));
screen_x0 = screen_x - text_width / 2;
d = 0.780 * screen_delta_y;
screen_y0 = screen_margin_y + (int) d + text_height / 2;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* The next two strings are not aligned vertically with the previous five: */

/* ASN: */
sprintf (stringA, "ASN: %d", asnN);
text_width = XTextWidth (guiSP->main_winS.fontSP, stringA, strlen (stringA));
d = 0.092 * screen_delta_x;
screen_x0 = screen_margin_x + (int) d - text_width / 2;
d = 0.675 * screen_delta_y;
screen_y0 = screen_margin_y + (int) d + text_height / 2;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* THR: */
sprintf (stringA, "THR: %d", thrN);
text_width = XTextWidth (guiSP->main_winS.fontSP, stringA, strlen (stringA));
d = 0.253 * screen_delta_x;
screen_x0 = screen_margin_x + (int) d - text_width / 2;
d = 0.675 * screen_delta_y;
screen_y0 = screen_margin_y + (int) d + text_height / 2;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* The next five strings are placed at the same screen_x: */
d = 0.500 * screen_delta_x;
screen_x = screen_margin_x + (int) d;

/* HIS: */
sprintf (stringA, "HIS: %d", hisN);
text_width = XTextWidth (guiSP->main_winS.fontSP, stringA, strlen (stringA));
screen_x0 = screen_x - text_width / 2;
d = 0.307 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (int) d + text_height / 2;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* CYS (S-S): */
sprintf (stringA, "CYS (S-S): %d", S_S_cysN);
text_width = XTextWidth (guiSP->main_winS.fontSP, stringA, strlen (stringA));
screen_x0 = screen_x - text_width / 2;
d = 0.675 * screen_delta_y;
screen_y0 = screen_margin_y + (int) d + text_height / 2;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* CYS (S-H): */
sprintf (stringA, "CYS (S-H): %d", S_H_cysN);
text_width = XTextWidth (guiSP->main_winS.fontSP, stringA, strlen (stringA));
screen_x0 = screen_x - text_width / 2;
d = 0.780 * screen_delta_y;
screen_y0 = screen_margin_y + (int) d + text_height / 2;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* PRO: */
sprintf (stringA, "PRO: %d", proN);
text_width = XTextWidth (guiSP->main_winS.fontSP, stringA, strlen (stringA));
screen_x0 = screen_x - text_width / 2;
d = 0.960 * screen_delta_y;
screen_y0 = screen_margin_y + (int) d + text_height / 2;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* The next two strings are not aligned vertically with the previous five: */

/* TYR: */
sprintf (stringA, "TYR: %d", tyrN);
text_width = XTextWidth (guiSP->main_winS.fontSP, stringA, strlen (stringA));
d = 0.423 * screen_delta_x;
screen_x0 = screen_margin_x + (int) d - text_width / 2;
d = 0.138 * screen_delta_y;
screen_y0 = screen_margin_y + (int) d + text_height / 2;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* TRP: */
sprintf (stringA, "TRP: %d", trpN);
text_width = XTextWidth (guiSP->main_winS.fontSP, stringA, strlen (stringA));
d = 0.585 * screen_delta_x;
screen_x0 = screen_margin_x + (int) d - text_width / 2;
d = 0.138 * screen_delta_y;
screen_y0 = screen_margin_y + (int) d + text_height / 2;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* The next three strings are placed at the same screen_x: */
d = 0.825 * screen_delta_x;
screen_x = screen_margin_x + (int) d;

/* MET: */
sprintf (stringA, "MET: %d", metN);
text_width = XTextWidth (guiSP->main_winS.fontSP, stringA, strlen (stringA));
screen_x0 = screen_x - text_width / 2;
d = 0.044 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (int) d + text_height / 2;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* PHE: */
sprintf (stringA, "PHE: %d", pheN);
text_width = XTextWidth (guiSP->main_winS.fontSP, stringA, strlen (stringA));
screen_x0 = screen_x - text_width / 2;
d = 0.216 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (int) d + text_height / 2;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* VAL: */
sprintf (stringA, "VAL: %d", valN);
text_width = XTextWidth (guiSP->main_winS.fontSP, stringA, strlen (stringA));
screen_x = screen_x0 - text_width / 2;
d = 0.570 * (double) screen_delta_y;
screen_y0 = screen_margin_y + (int) d + text_height / 2;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* The next four strings are not aligned with the previous three: */

/* LEU: */
sprintf (stringA, "LEU: %d", leuN);
text_width = XTextWidth (guiSP->main_winS.fontSP, stringA, strlen (stringA));
d = 0.757 * screen_delta_x;
screen_x0 = screen_margin_x + (int) d - text_width / 2;
d = 0.470 * screen_delta_y;
screen_y0 = screen_margin_y + (int) d;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* ILE: */
sprintf (stringA, "ILE: %d", ileN);
text_width = XTextWidth (guiSP->main_winS.fontSP, stringA, strlen (stringA));
d = 0.912 * screen_delta_x;
screen_x0 = screen_margin_x + (int) d - text_width / 2;
d = 0.470 * screen_delta_y;
screen_y0 = screen_margin_y + (int) d;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* ALA: */
sprintf (stringA, "ALA: %d", alaN);
text_width = XTextWidth (guiSP->main_winS.fontSP, stringA, strlen (stringA));
d = 0.745 * screen_delta_x;
screen_x0 = screen_margin_x + (int) d - text_width / 2;
d = 0.780 * screen_delta_y;
screen_y0 = screen_margin_y + (int) d;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/* GLY: */
sprintf (stringA, "GLY: %d", glyN);
text_width = XTextWidth (guiSP->main_winS.fontSP, stringA, strlen (stringA));
d = 0.890 * screen_delta_x;
screen_x0 = screen_margin_x + (int) d - text_width / 2;
d = 0.780 * screen_delta_y;
screen_y0 = screen_margin_y + (int) d;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0,
	     stringA, strlen (stringA));

/*------copy the prepared image to the main window:--------------------------*/

/* Copy hidden pixmap to the main window: */
XCopyArea (guiSP->displaySP,
	   guiSP->main_hidden_pixmapID, guiSP->main_winS.ID,
	   guiSP->theGCA[0],
	   0, 0,
	   guiSP->main_win_free_area_width, guiSP->main_win_free_area_height,
	   0, 0);

return residuesN;
}

/*===========================================================================*/


