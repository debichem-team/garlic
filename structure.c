/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

				structure.c

Purpose:
	Execute structure command. This command may be used to define or
	copy  the secondary structure.  The structure  is stored  to the
	structure buffer. The command is used with at least one keyword.
	Keywords: RESET, =, FROM, LOAD (READ) and SAVE.

Input:
	(1) Pointer to MolComplexS structure,  with macromol. complexes.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to the string with command parameters.

Output:
	(1) Secondary structure stored to the structure buffer.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

Notes:
	(1) Use one letter codes to define the secondary structure:
	    C = coil,
	    E = strand,
	    G = 3-10 helix,
	    H = alpha helix,
	    T = turn.

	(2) Space, comma and tab may be used as separators.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>
#include <ctype.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		ParseSecondaryStructure_ (RuntimeS *, char *);
char		*ExtractToken_ (char *, int, char *, char *);
int		SecondaryStructureFrom_ (MolComplexS *, int,
					 RuntimeS *, char *);
int             CopySecondaryStructure_ (RuntimeS *);
int		LoadSecondaryStructure_ (RuntimeS *);
int		SaveSecondaryStructure_ (RuntimeS *);

/*======execute structure command:===========================================*/

int Structure_ (MolComplexS *mol_complexSP, int mol_complexesN,
		RuntimeS *runtimeSP, char *stringP)
{
char		string_copyA[STRINGSIZE];
char		*P;
char		*sec_structureP;
int		n;
char		*remainderP;
char		tokenA[SHORTSTRINGSIZE];
int		i;

/* Copy the original command string: */
strncpy (string_copyA, runtimeSP->curr_commandA, STRINGSIZE - 1);
string_copyA[STRINGSIZE - 1] = '\0';

/* Check is the equality sign present in the original command string: */
if ((P = strstr (string_copyA, "=")) != NULL)
	{
	/* Prepare the pointer to the string with the secondary structure: */
	sec_structureP = P + 1;

	/* Convert the secondary structure to uppercase: */
	P = sec_structureP;
	while ((n = *P++) != '\0') *(P - 1) = toupper (n);

	/* Parse and store the secondary structure: */
	n = ParseSecondaryStructure_ (runtimeSP, sec_structureP);
	if (n > 0) return COMMAND_STRUCTURE;
	else return ERROR_STRUCTURE;
	}

/* If this point is reached, the equality sign was not found */
/* in the original command string. Check for other keywords. */

/* Take the first token; it should be present: */
remainderP = ExtractToken_ (tokenA, SHORTSTRINGSIZE, stringP, " \t\n");
if (!remainderP)
	{
	strcpy (runtimeSP->messageA, "Command parameter(s) missing!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_STRUCTURE;
	}

/* Reset the secondary structure, if requested by keyword RES (RESET). */
/* The entire buffer will be filled with codes for extended structure. */
if (strstr (tokenA, "RES") == tokenA)
	{
	runtimeSP->sec_structure_length = MAXRESIDUES;
	for (i = 0; i < MAXRESIDUES; i++)
		{
		*(runtimeSP->sec_structureP + i) = 'E';
		}
	return COMMAND_STRUCTURE;
	}

/* Copy the secondary structure  from macromol. */
/* complex, if requested by keyword FRO (FROM): */
else if (strstr (tokenA, "FRO") == tokenA)
	{
	n = SecondaryStructureFrom_ (mol_complexSP, mol_complexesN,
				     runtimeSP, remainderP);
	if (n > 0) return COMMAND_STRUCTURE;
	else return ERROR_STRUCTURE;
	}

/* Copy the secondary structure from the main */
/* structure buffer  to the reference buffer: */
else if (strstr (tokenA, "COP") == tokenA)
	{
	CopySecondaryStructure_ (runtimeSP);
	return COMMAND_STRUCTURE;
	}

/* Load  the secondary  structure  from file,  if */
/* requested by keyword LOA (LOAD) or REA (READ): */
else if ((strstr (tokenA, "LOA") == tokenA) ||
	 (strstr (tokenA, "REA") == tokenA))
	{
	n = LoadSecondaryStructure_ (runtimeSP);
	if (n > 0) return COMMAND_STRUCTURE;
	else return ERROR_STRUCTURE;
	}

/* Save the secondary structure, if requested by keyword SAV: */
else if (strstr (tokenA, "SAV") == tokenA)
	{
	n = SaveSecondaryStructure_ (runtimeSP);
	if (n > 0) return COMMAND_STRUCTURE;
	else return ERROR_STRUCTURE;
	}

/* If keyword recognition failed: */
else
	{
	strcpy (runtimeSP->messageA, "Keyword not recognized!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_STRUCTURE;
	}

/* Return the command code: */
return COMMAND_STRUCTURE;
}

/*===========================================================================*/


