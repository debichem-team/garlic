/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				rotation_angle.c

Purpose:
	Prepare the rotation angle. By default, the normal rotation angle
	is copied (rotation_stepA[2] from ConfigS structure).  See note 1
	for available combinations of modifier keys. Modifier keys may be
	used to select the rotation angle.

Input:
	(1) Pointer to ConfigS structure, with configuration data.
	(2) Pointer to GUIS structure, with GUI data.
	(3) The sign used to distinguish  positive and negative rotation.
	    Value +1.0 is used with positive rotation, while -1.0 defines
	    negative rotation.

Output:
	(1) Return value.

Return value:
	(1) Rotation angle.

Notes:
	(1) The following combinations of modifier keys may be used to
	    select the rotation angle:
	    -------------------------------------------------------------
	    |  modifiers          rotation step      ConfigS member     |
	    |-----------------------------------------------------------|
	    | none               normal (default)   rotation_stepA[2]   |
	    | shift              large              rotation_stepA[3]   |
	    | alt+shift          very large         rotation_stepA[4]   |
	    | control            small              rotation_stepA[1]   |
	    | alt+control        very small         rotation_stepA[0]   |
	    | shift+control      very small         rotation_stepA[2]   |
	    | alt                normal             rotation_stepA[2]   |
	    | alt+shift+control  normal             rotation_stepA[2]   |
	    -------------------------------------------------------------
	    Note that some modifier combinations  are ignored  (alt alone
	    and alt+control+shift).

	(2) On some laptops the status of shift key may be changed before
	    the KeyPress event is send for the keys on  "numeric keypad".
 
========includes:============================================================*/

#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======prepare rotation angle:==============================================*/

double RotationAngle_ (ConfigS *configSP, GUIS *guiSP, double sign)
{
double		rotation_angle;
int		largeF, smallF, altF;

/* Copy the flags: */
largeF = guiSP->shift_pressedF;
smallF = guiSP->control_pressedF;
altF   = guiSP->alt_pressedF;

/* Select the requested rotation step: */
if (altF)
	{
	if (largeF && !smallF)
		{
		rotation_angle = configSP->rotation_stepA[4];
		}
	else if (smallF && !largeF)
		{
		rotation_angle = configSP->rotation_stepA[0];
		}
	else
		{
		rotation_angle = configSP->rotation_stepA[2];
		}
	}
else
	{
	if (largeF && !smallF)
		{
		rotation_angle = configSP->rotation_stepA[3];
		}
	else if (smallF && !largeF)
		{
		rotation_angle = configSP->rotation_stepA[1];
		}
	else if (smallF && largeF)
		{
		rotation_angle = configSP->rotation_stepA[0];
		}
	else
		{
		rotation_angle = configSP->rotation_stepA[2];
		}
	}

/* Take care for the sign: */
rotation_angle *= sign;

/* Return the rotation angle: */
return rotation_angle;
}

/*===========================================================================*/


