/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

			     exclude_distant.c

Purpose:
	Exclude exposed polar residues which are below the plane.

Input:
	(1) Pointer to RuntimeS structure.
	(2) The complex index (1 = bottom, 2 = top).

Output:
	(1) Some exposed polar residues excluded from the list.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

void		ErrorMessage_ (char *, char *, char *,
			       char *, char *, char *, char *);
double		ScalarProduct_ (VectorS *, VectorS *);

/*======exclude distant residues from the list:==============================*/

int ExcludeDistant_ (RuntimeS *runtimeSP, int docking_complexI)
{
int			exposed_polarN, exposed_polarI;
MolComplexS		*curr_mol_complexSP;
int			*exposed_atomIP;
ExposedResidueS		*exposed_polarSP;
double			plane_center_x, plane_center_y, plane_center_z;
VectorS			normal_vectorS;
ExposedResidueS		*curr_exposedSP;
AtomS			*curr_atomSP;
double			x, y, z;
VectorS			curr_vectorS;

/* Check the complex index and prepare the pointers: */
if (docking_complexI == 1)
	{
	exposed_polarN = runtimeSP->exposed_polar1N;
	curr_mol_complexSP = runtimeSP->mol_complex1SP;
	exposed_atomIP = runtimeSP->exposed_atom1IP;
	exposed_polarSP = runtimeSP->exposed_polar1SP;
	}
else if (docking_complexI == 2)
	{
	exposed_polarN = runtimeSP->exposed_polar2N;
	curr_mol_complexSP = runtimeSP->mol_complex2SP;
	exposed_atomIP = runtimeSP->exposed_atom2IP;
	exposed_polarSP = runtimeSP->exposed_polar2SP;
	}
else
	{
	ErrorMessage_ ("garlic", "RepresentativeAtoms_", "",
		       "Bad macromolecular complex index!\n",
		       "", "", "");
	return -1;
	}

/* Copy the plane center position (plane belongs to bottom complex): */
plane_center_x = curr_mol_complexSP->planeS.center_x[0];
plane_center_y = curr_mol_complexSP->planeS.center_y;
plane_center_z = curr_mol_complexSP->planeS.center_z[0];

/* Copy the normal vector (plane belongs to bottom complex): */
normal_vectorS.x = curr_mol_complexSP->planeS.normal_x[0];
normal_vectorS.y = curr_mol_complexSP->planeS.normal_y;
normal_vectorS.z = curr_mol_complexSP->planeS.normal_z[0];

/* Scan the list: */
for (exposed_polarI = 0; exposed_polarI < exposed_polarN; exposed_polarI++)
	{
	/* Pointer to the current exposed polar residue: */
	curr_exposedSP = exposed_polarSP + exposed_polarI;

	/* Pointer to the representative atom: */
	curr_atomSP = curr_mol_complexSP->atomSP +
		      curr_exposedSP->representative_atomI;

	/* Copy the atomic coordinates: */
	x = curr_atomSP->raw_atomS.x[0];
	y = curr_atomSP->raw_atomS.y;
	z = curr_atomSP->raw_atomS.z[0];

	/* Position of the atom relative to the plane center: */
	curr_vectorS.x = x - plane_center_x;
	curr_vectorS.y = y - plane_center_y;
	curr_vectorS.z = z - plane_center_z;

	/* Check the sign of the scalar product: */
	if (ScalarProduct_ (&curr_vectorS, &normal_vectorS) < 0)
		{
		curr_exposedSP->excludedF = 1;
		}
	}

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


