/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				fileformat.c

Purpose:
	Try to recognize input file format.

Input:
	(1) File name.

Output:
	(1) Return value.

Return value:
	(1) File format identifier on success (see defines.h for values).
	(3) Negative on failure.

========includes:============================================================*/

#include <stdio.h>

#include "defines.h"

/*======function prototypes:=================================================*/

void		ErrorMessage_ (char *, char *, char *,
			       char *, char *, char *, char *);
int		IsPDB_ (char *);

/*======recognize file format:===============================================*/

int RecogFileFormat_ (char *file_nameP)
{
int		n;

/* Check is it PDB file: */
n = IsPDB_ (file_nameP);
if (n >= 0) return n;

/* If this point is reached, file format recognition failed: */
return -1;
}

/*===========================================================================*/


