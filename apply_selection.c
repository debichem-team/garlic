/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				apply_selection.c

Purpose:
	Apply the selection criteria.  Cancel the previous  selection and
	apply the new one.

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) Number of macromolecular complexes.
	(3) Pointer to SelectS structure, with inclusion criteria.
	(4) Pointer to SelectS structure, with exclusion criteria.
	(5) Selection mode index (0 = overwrite, 1 = restrict, 2 = expand
	    previous selection).

Output:
	(1) The flag  selectedF set for all atoms in all currently caught
	    macromolecular complexes.
	(2) Return value.

Return value:
	(1) The number of selected atoms (zero or positive value).

Notes:
	(1) Indentation is exceptionally four spaces.

	(2) The position_changedF is updated,  because some atoms may not
	    have the proper color.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======apply selection criteria:============================================*/

long ApplySelection_ (MolComplexS *mol_complexSP, int mol_complexesN,
		      SelectS *include_selectSP, SelectS *exclude_selectSP,
		      int selection_modeI)
{
long		selected_atomsN = 0;
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;
size_t		atomsN, atomI;
AtomS		*curr_atomSP;
int		i, serialI, startI, endI;
int		chainF, residue_serialF, residue_nameF, atom_nameF, combinedF;

/* Check every macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
    {
    /** Pointer to the current macromolecular complex: **/
    curr_mol_complexSP = mol_complexSP + mol_complexI;

    /** Check is the current macromolecular complex caught: **/
    if (curr_mol_complexSP->catchF == 0) continue;

    /** Number of atoms in a macromolecular complex: **/
    atomsN = curr_mol_complexSP->atomsN;

    /** Scan all atoms in the current complex: **/
    for (atomI = 0; atomI < atomsN; atomI++)
	{
	/** Pointer to the current atom: **/
	curr_atomSP = curr_mol_complexSP->atomSP + atomI;

/*---------------------------------------------------------------------------*/

	/** Apply the inclusion criteria: **/

	/*** Check chain ID: ***/
	chainF = 0;
	if (include_selectSP->all_chainsF) chainF = 1;
	else
	    {
	    for (i = 0; i < include_selectSP->chainsN; i++)
		{
		if (curr_atomSP->raw_atomS.chainID ==
		    include_selectSP->chainIDA[i])
		    {
		    chainF = 1;
		    break;
		    }
		}
	    }

	/*** Check the residue serial number: ***/
	residue_serialF = 0;
	if (include_selectSP->all_residue_serialF) residue_serialF = 1;
	else
	    {
	    for (i = 0; i < include_selectSP->residue_serial_rangesN; i++)
		{
		serialI = curr_atomSP->raw_atomS.residue_sequenceI;
		startI  = include_selectSP->residue_serial_start[i];
		endI    = include_selectSP->residue_serial_end[i];
		if ((serialI >= startI) && (serialI <= endI))
		    {
		    residue_serialF = 1;
		    break;
		    }
		}
	    }

	/*** Check residue name: ***/
	residue_nameF = 0;
	if (include_selectSP->all_residue_namesF) residue_nameF = 1;
	else
	    {
	    for (i = 0; i < include_selectSP->residue_namesN; i++)
		{
		if (strcmp (curr_atomSP->raw_atomS.pure_residue_nameA,
			    include_selectSP->residue_nameAA[i]) == 0)
		    {
		    residue_nameF = 1;
		    break;
		    }
		}
	    }

	/*** Check atom name: ***/
	atom_nameF = 0;
	if (include_selectSP->all_atom_namesF) atom_nameF = 1;
	else
	    {
	    for (i = 0; i < include_selectSP->atom_namesN; i++)
		{
		if (strcmp (curr_atomSP->raw_atomS.pure_atom_nameA,
			    include_selectSP->atom_nameAA[i]) == 0)
		    {
		    atom_nameF = 1;
		    break;
		    }
		}
	    }

/*---------------------------------------------------------------------------*/

	/** Apply the exclusion criteria: **/

	/*** Check chain ID: ***/
	if (exclude_selectSP->all_chainsF) chainF = 0;
	else
	    {
	    for (i = 0; i < exclude_selectSP->chainsN; i++)
		{
		if (curr_atomSP->raw_atomS.chainID ==
		    exclude_selectSP->chainIDA[i])
		    {
		    chainF = 0;
		    break;
		    }
		}
	    }

	/*** Check the residue serial number: ***/
	if (exclude_selectSP->all_residue_serialF) residue_serialF = 0;
	else
	    {
	    for (i = 0; i < exclude_selectSP->residue_serial_rangesN; i++)
		{
		serialI = curr_atomSP->raw_atomS.residue_sequenceI;
		startI  = exclude_selectSP->residue_serial_start[i];
		endI    = exclude_selectSP->residue_serial_end[i];
		if ((serialI >= startI) && (serialI <= endI))
		    {
		    residue_serialF = 0;
		    break;
		    }
		}
	    }

	/*** Check residue name: ***/
	if (exclude_selectSP->all_residue_namesF) residue_nameF = 0;
	else
	    {
	    for (i = 0; i < exclude_selectSP->residue_namesN; i++)
		{
		if (strcmp (curr_atomSP->raw_atomS.pure_residue_nameA,
			    exclude_selectSP->residue_nameAA[i]) == 0)
		    {
		    residue_nameF = 0;
		    break;
		    }
		}
	    }

	/*** Check atom name: ***/
	if (exclude_selectSP->all_atom_namesF) atom_nameF = 0;
	else
	    {
	    for (i = 0; i < exclude_selectSP->atom_namesN; i++)
		{
		if (strcmp (curr_atomSP->raw_atomS.pure_atom_nameA,
			    exclude_selectSP->atom_nameAA[i]) == 0)
		    {
		    atom_nameF = 0;
		    break;
		    }
		}
	    }

/*---------------------------------------------------------------------------*/

	/** Combine all flags to calculate the selection flag: **/
	combinedF = chainF * residue_serialF * residue_nameF * atom_nameF;

	/** Set the selection flag for the current atom: **/
	switch (selection_modeI)
	    {
	    /*** Overwrite the previous selection: ***/
	    case 0:
		curr_atomSP->selectedF = combinedF;
		break;

	    /*** Restrict the previous selection: ***/
	    case 1:
		curr_atomSP->selectedF &= combinedF;
		break;

	    /*** Expand the previous selection: ***/
	    case 2:
		curr_atomSP->selectedF |= combinedF;
		break;

	    default:
		;
	    }

	/** Check the selection flag; increase **/
	/** the count if flag is equal to one: **/
	if (curr_atomSP->selectedF) selected_atomsN++;
	}

    /** Update the position_changedF (some atoms may have bad color): **/
    curr_mol_complexSP->position_changedF = 1;
    }

/* Return the number of selected atoms: */
return selected_atomsN;
}

/*===========================================================================*/


