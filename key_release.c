/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				key_release.c

Purpose:
	Handle KeyRelease events (just reset some flags).

Input:
	(1) Pointer to GUIS structure, with GUI data.
	(2) Pointer to XKeyEvent structure.

Output:
	(1) One flag changed in GUIS structure.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include <X11/keysym.h>

#include "defines.h"
#include "typedefs.h"

/*======handle KeyRelease events:============================================*/

int KeyRelease_ (GUIS *guiSP, XKeyEvent *key_eventSP)
{
char			stringA[STRINGSIZE];
KeySym			key_symID;
XComposeStatus		compose_statusS;

/* Get the KeySym: */
XLookupString (key_eventSP, stringA, STRINGSIZE, &key_symID, &compose_statusS);

/* Select the proper action for a given KeySym: */
switch (key_symID)
	{
	/* Shift key released: */
	case XK_Shift_L:
	case XK_Shift_R:
		guiSP->shift_pressedF = 0;
		break;

	/* Control key released: */
	case XK_Control_L:
	case XK_Control_R:
		guiSP->control_pressedF = 0;
		break;

	/* Alt or meta key released: */
	case XK_Alt_L:
	case XK_Alt_R:
	case XK_Meta_L:
	case XK_Meta_R:
		guiSP->alt_pressedF = 0;
		break;

	/* All other cases are ignored: */
	default:
		;
	}

/* Return positive value: */
return 1;
}

/*===========================================================================*/


