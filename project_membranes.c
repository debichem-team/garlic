/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

				project_membranes.c

Purpose:
	For each macromolecular complex, project membrane to the screen.
	The membrane is represented by two circles in space. Each circle
	is represented by  an ellipse on the screen.  This function will
	project each membrane always,  even if it is not visible  on the
	screen.  The parameters prepared in  this function are stored in
	two PlaneS structures, which are members of MembraneS structure.
	Some  other parameters  will be prepared  in the function  which
	draw the membrane to the screen.

Input:
	(1) Pointer to MolComplexS structure,  with macromol. complexes.
	(2) The number of macromolecular complexes.
	(3) Pointer to ConfigS structure, with configuration data.
	    
Output:
	(1) Parameters which define the membrane projection  (i.e.,  the
	    projections of two constituent planes) to the screen will be
	    calculated  for each  macromolecular complex  containing  at
	    least one atom. 

Return value:
	No return value.

Notes:
	(1) If definedF  is zero for a given membrane,  the position and
	    orientation of that membrane are not defined properly.

========includes:============================================================*/

#include <stdio.h>

#include <limits.h>
#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======project membranes:===================================================*/

void ProjectMembranes_ (MolComplexS *mol_complexSP, int mol_complexesN,
			ConfigS *configSP)
{
int			imagesN, imageI;
double			nominator_x, nominator_y, user_atomic_position;
int			center_screen_x[2], center_screen_y;
double			cos_stereo_angle, sin_stereo_angle;
int			mol_complexI;
MolComplexS		*curr_mol_complexSP;
MembraneS		*membraneSP;
double			half_thickness;
PlaneS			*plane1SP, *plane2SP;
double			x0, z0, x, z, x_new, z_new;
double			denominator, reciprocal_denominator;
double			normal_x, normal_y, normal_z;
double			rationalized_x, rationalized_y;
double			scalar_product, abs_value_squared, abs_value;
double			cos_theta, cos_phi, angle;
double			value1, value2;

/* The number of images (1 for mono, 2 for stereo): */
if (configSP->stereoF == 1) imagesN = 2;
else imagesN = 1;

/* Prepare the factors which are used to */
/* reduce the number of multiplications: */
nominator_x = configSP->user_screen_atomic_distance *
	      configSP->atomic_to_screen_scale_x;
nominator_y = configSP->user_screen_atomic_distance *
	      configSP->atomic_to_screen_scale_y;

/* Prepare the user position in atomic coordinates: */
user_atomic_position = configSP->user_atomic_position;

/* Position of the window free area center (stereo data): */
center_screen_x[0] = configSP->center_screen_x[0];
center_screen_x[1] = configSP->center_screen_x[1];
center_screen_y    = configSP->center_screen_y;

/* Calculate cosine and sine of the stereo angle: */
cos_stereo_angle = cos (configSP->stereo_angle);
sin_stereo_angle = sin (configSP->stereo_angle);

/* Project the membrane (two planes) for each macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/* Pointer to the current macromolecular complex: */
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/* If this complex contains no atoms, ignore it: */
	if (curr_mol_complexSP->atomsN == 0) continue;

	/* If the membrane associated with the current */
	/* complex is not defined,  skip this complex: */
	if (curr_mol_complexSP->membraneS.definedF == 0) continue;

	/* Prepare the pointer to the membrane: */
	membraneSP = &curr_mol_complexSP->membraneS;

	/* Half of the membrane thickness: */
	half_thickness = membraneSP->thickness / 2.0;

	/*------refresh the members of the first PlaneS structure:-----------*/

	/* Prepare the pointer to the first plane: */
	plane1SP = &membraneSP->plane1S;

	/* The center of the first plane: */
	plane1SP->center_x[0] = membraneSP->center_x +
				half_thickness * plane1SP->normal_x[0];
	plane1SP->center_y    = membraneSP->center_y +
				half_thickness * plane1SP->normal_y;
	plane1SP->center_z[0] = membraneSP->center_z +
				half_thickness * plane1SP->normal_z[0];

	/* The stereo data associated with the center of the first plane: */
	x0 = curr_mol_complexSP->geometric_center_vectorS.x;
	z0 = curr_mol_complexSP->geometric_center_vectorS.z;
	x = plane1SP->center_x[0] - x0;
	z = plane1SP->center_z[0] - z0;
	x_new =  x * cos_stereo_angle + z * sin_stereo_angle;
	z_new = -x * sin_stereo_angle + z * cos_stereo_angle;
	plane1SP->center_x[1] = x_new + x0;
	plane1SP->center_z[1] = z_new + z0;

	/*------refresh the members of the second PlaneS structure:----------*/

	/* Prepare the pointer to the second plane: */
	plane2SP = &membraneSP->plane2S;

	/* The center of the second plane: */
	plane2SP->center_x[0] = membraneSP->center_x +
				half_thickness * plane2SP->normal_x[0];
	plane2SP->center_y    = membraneSP->center_y +
				half_thickness * plane2SP->normal_y;
	plane2SP->center_z[0] = membraneSP->center_z +
				half_thickness * plane2SP->normal_z[0];

	/* The stereo data associated with the center of the second plane: */
	x0 = curr_mol_complexSP->geometric_center_vectorS.x;
	z0 = curr_mol_complexSP->geometric_center_vectorS.z;
	x = plane2SP->center_x[0] - x0;
	z = plane2SP->center_z[0] - z0;
	x_new =  x * cos_stereo_angle + z * sin_stereo_angle;
	z_new = -x * sin_stereo_angle + z * cos_stereo_angle;
	plane2SP->center_x[1] = x_new + x0;
	plane2SP->center_z[1] = z_new + z0;

	/*------project the first plane:-------------------------------------*/

	/* Project the first plane: */

	/* Stereo data should be prepared: */
	for (imageI = 0; imageI < imagesN; imageI++)
		{
		/*------prepare auxilliary parameters:-----------------------*/

		denominator = plane1SP->center_z[imageI] -
			      user_atomic_position;
		if (denominator == 0.0) reciprocal_denominator = 0.0;
		else reciprocal_denominator = 1.0 / denominator;
		normal_x = plane1SP->normal_x[imageI];
		normal_y = plane1SP->normal_y;
		normal_z = plane1SP->normal_z[imageI];

		/*------project plane center to the screen:------------------*/

		/* Prepare rationalized coordinates: */
		rationalized_x = plane1SP->center_x[imageI] *
				 nominator_x * reciprocal_denominator;
		rationalized_y = plane1SP->center_y *
				 nominator_y * reciprocal_denominator;

		/* Keep them in a reasonable range: */
		if      (rationalized_x > (double) INT_MAX / 4)
			 rationalized_x = (double) INT_MAX / 4;
		else if (rationalized_x < (double) INT_MIN / 4)
			 rationalized_x = (double) INT_MIN / 4;
		if      (rationalized_y > (double) INT_MAX / 4)
			 rationalized_y = (double) INT_MAX / 4;
		else if (rationalized_y < (double) INT_MIN / 4)
			 rationalized_y = (double) INT_MIN / 4;

		/* Prepare screen coordinates of the plane center: */
		plane1SP->center_screen_x[imageI] =
				(int) rationalized_x + center_screen_x[imageI];
		plane1SP->center_screen_y =
				(int) rationalized_y + center_screen_y;

		/*------angle between normal vector and z axis:--------------*/

		scalar_product = normal_z;
		abs_value_squared = normal_x * normal_x +
				    normal_y * normal_y +
				    normal_z * normal_z;
		abs_value = sqrt (abs_value_squared);
		if (abs_value == 0.0)
			{
			cos_theta = 0.0;
			plane1SP->normal_theta[imageI] = 1.5707963;
			}
		else
			{
			cos_theta = scalar_product / abs_value;
			if (cos_theta <= -1.0) angle = 3.1415927;
			else if (cos_theta >= 1.0) angle = 0.0;
			else angle = acos (cos_theta);
			plane1SP->normal_theta[imageI] = angle;
			}

		/*------visible side index (0 = top, 1 = bottom):------------*/

		if (cos_theta <= 0.0) plane1SP->visible_sideI[imageI] = 0;
		else plane1SP->visible_sideI[imageI] = 1;

		/*------angle between x and project. of n. v. to xy plane:---*/

		/* Prepare and check the cosine of normal_phi: */
		abs_value = sqrt (normal_x * normal_x + normal_y * normal_y);
		cos_phi = normal_x / abs_value;
		if (cos_phi >  1.0) cos_phi =  1.0;
		if (cos_phi < -1.0) cos_phi = -1.0;

		/* The special case (undefined phi): */
		if ((normal_x == 0.0) && (normal_y == 0.0))
			{
			plane1SP->normal_phi[imageI] = 0.0;
			}

		/* If y component of the normal vector is positive: */
		else if (normal_y >= 0.0)
			{
			if (cos_phi <= -1.0) angle = 3.1415927;
			else if (cos_phi >= 1.0) angle = 0.0;
			else angle = acos (cos_phi);
			plane1SP->normal_phi[imageI] = angle;
			}

		/* If y component of the normal vector is negative: */
		else
			{
			if (cos_phi <= -1.0) angle = 3.1415927;
			else if (cos_phi >= 1.0) angle = 0.0;
			else angle = acos (cos_phi);
			plane1SP->normal_phi[imageI] = 6.2831853 - angle;
			}

		/*------large half axis of the ellipse in screen units:------*/

		value1 = plane1SP->circle_radius *
			 nominator_x *
			 reciprocal_denominator;
		value2 = plane1SP->circle_radius *
			 nominator_y *
			 reciprocal_denominator;
		if (value1 > value2) plane1SP->screen_a = value1;
		else plane1SP->screen_a = value2;

		/*------small half axis of the ellipse (stereo data):--------*/

		plane1SP->screen_b[imageI] = plane1SP->screen_a *
						 fabs (cos_theta);

		/* The minimal value should be one: */
		if (plane1SP->screen_b[imageI] < 1.0)
			plane1SP->screen_b[imageI] = 1.0;
		}

	/*------project the second plane:------------------------------------*/

	/* Stereo data should be prepared: */
	for (imageI = 0; imageI < imagesN; imageI++)
		{
		/*------prepare auxilliary parameters:-----------------------*/

		denominator = plane2SP->center_z[imageI] -
			      user_atomic_position;
		if (denominator == 0.0) reciprocal_denominator = 0.0;
		else reciprocal_denominator = 1.0 / denominator;
		normal_x = plane2SP->normal_x[imageI];
		normal_y = plane2SP->normal_y;
		normal_z = plane2SP->normal_z[imageI];

		/*------project plane center to the screen:------------------*/

		/* Prepare rationalized coordinates: */
		rationalized_x = plane2SP->center_x[imageI] *
				 nominator_x * reciprocal_denominator;
		rationalized_y = plane2SP->center_y *
				 nominator_y * reciprocal_denominator;

		/* Keep them in a reasonable range: */
		if      (rationalized_x > (double) INT_MAX / 4)
			 rationalized_x = (double) INT_MAX / 4;
		else if (rationalized_x < (double) INT_MIN / 4)
			 rationalized_x = (double) INT_MIN / 4;
		if      (rationalized_y > (double) INT_MAX / 4)
			 rationalized_y = (double) INT_MAX / 4;
		else if (rationalized_y < (double) INT_MIN / 4)
			 rationalized_y = (double) INT_MIN / 4;

		/* Prepare screen coordinates of the plane center: */
		plane2SP->center_screen_x[imageI] =
				(int) rationalized_x + center_screen_x[imageI];
		plane2SP->center_screen_y =
				(int) rationalized_y + center_screen_y;

		/*------angle between normal vector and z axis:--------------*/

		scalar_product = normal_z;
		abs_value_squared = normal_x * normal_x +
				    normal_y * normal_y +
				    normal_z * normal_z;
		abs_value = sqrt (abs_value_squared);
		if (abs_value == 0.0)
			{
			cos_theta = 0.0;
			plane2SP->normal_theta[imageI] = 1.5707963;
			}
		else
			{
			cos_theta = scalar_product / abs_value;
			if (cos_theta <= -1.0) angle = 3.1415927;
			else if (cos_theta >= 1.0) angle = 0.0;
			else angle = acos (cos_theta);
			plane2SP->normal_theta[imageI] = angle;
			}

		/*------visible side index (0 = top, 1 = bottom):------------*/

		if (cos_theta <= 0.0) plane2SP->visible_sideI[imageI] = 0;
		else plane2SP->visible_sideI[imageI] = 1;

		/*------angle between x and project. of n. v. to xy plane:---*/

		/* Prepare and check the cosine of normal_phi: */
		abs_value = sqrt (normal_x * normal_x + normal_y * normal_y);
		cos_phi = normal_x / abs_value;
		if (cos_phi >  1.0) cos_phi =  1.0;
		if (cos_phi < -1.0) cos_phi = -1.0;

		/* The special case (undefined phi): */
		if ((normal_x == 0.0) && (normal_y == 0.0))
			{
			plane2SP->normal_phi[imageI] = 0.0;
			}

		/* If y component of the normal vector is positive: */
		else if (normal_y >= 0.0)
			{
			if (cos_phi <= -1.0) angle = 3.1415927;
			else if (cos_phi >= 1.0) angle = 0.0;
			else angle = acos (cos_phi);
			plane2SP->normal_phi[imageI] = angle;
			}

		/* If y component of the normal vector is negative: */
		else
			{
			if (cos_phi <= -1.0) angle = 3.1415927;
			else if (cos_phi >= 1.0) angle = 0.0;
			else angle = acos (cos_phi);
			plane2SP->normal_phi[imageI] = 6.2831853 - angle;
			}

		/*------large half axis of the ellipse in screen units:------*/

		value1 = plane2SP->circle_radius *
			 nominator_x *
			 reciprocal_denominator;
		value2 = plane2SP->circle_radius *
			 nominator_y *
			 reciprocal_denominator;
		if (value1 > value2) plane2SP->screen_a = value1;
		else plane2SP->screen_a = value2;

		/*------small half axis of the ellipse (stereo data):--------*/

		plane2SP->screen_b[imageI] = plane2SP->screen_a *
						 fabs (cos_theta);

		/* The minimal value should be one: */
		if (plane2SP->screen_b[imageI] < 1.0)
			plane2SP->screen_b[imageI] = 1.0;
		}
	}
}

/*===========================================================================*/


