/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				extract_double.c

Purpose:
	Extract double value from a string.

Input:
	Input string pointer.

Output:
	Return value.

Return value:
	(1) Double value read from input string, on success.
	(2) Zero on failure (separator not found or no digits available).

========includes:============================================================*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>

/*======extract double from a string:========================================*/

double ExtractDouble_ (char *sP)
{
char		*P0, *P1;
int		n;
double		value;

/* Colon should be separator: */
if ((P0 = strstr (sP, ":")) == NULL) P0 = sP;

/* Replace each non-numeric character (except */
/* minus sign and  decimal point) with space: */
P1 = P0;
while ((n = *P1++) != '\0')
	{
	if (!isdigit (n) && (n != '-') && (n != '.')) *(P1 - 1) = ' ';
	}

/* Try to read double value: */
if (sscanf (P0, "%lf", &value) != 1) return 0.0;

/* If everything worked fine, return the extracted double value: */
return value;
}

/*===========================================================================*/


