/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

				extract_cac.c

Purpose:
	Extract CA and C coordinates for a given residue.

Input:
	(1) Pointer to VectorS structure, where CA coord. will be stored.
	(2) Pointer to VectorS structure, where C  coord. will be stored.
	(3) Pointer to AtomS structure,  pointing to the first element of
	    the atomic array.
	(4) Index of the first atom of a given residue.
	(5) Index of the last atom of a given residue.

Output:
	(1) VectorS structures filled with data.
	(2) Return value.

Return value:
	(1) The number of successfully extracted vectors  (at least zero,
	    at most two).

Notes:
	(1) Some files contain more than one entry for some atoms. Garlic
	    uses only the first entry and  discards other entries for the
	    same atom.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======extract CA and C coordinates:========================================*/

int ExtractCAC_ (VectorS  *CA_vectorSP, VectorS *C_vectorSP,
		 AtomS *atomSP, size_t atom_startI, size_t atom_endI)
{
int		vectors_extractedN = 0;
size_t		atomI;
AtomS		*curr_atomSP;
int		CA_foundF = 0, C_foundF = 0;

/* Scan the given residue: */
for (atomI = atom_startI; atomI <= atom_endI; atomI++)
	{
	/* Pointer to the current atom: */
	curr_atomSP = atomSP + atomI;

	/* CA: */
	if (strcmp (curr_atomSP->raw_atomS.pure_atom_nameA, "CA") == 0)
		{
		if (CA_foundF) continue;
		CA_vectorSP->x = curr_atomSP->raw_atomS.x[0];
		CA_vectorSP->y = curr_atomSP->raw_atomS.y;
		CA_vectorSP->z = curr_atomSP->raw_atomS.z[0];
		vectors_extractedN++;
		CA_foundF = 1;
		}

	/* C: */
	else if (strcmp (curr_atomSP->raw_atomS.pure_atom_nameA, "C") == 0)
		{
		if (C_foundF) continue;
		C_vectorSP->x = curr_atomSP->raw_atomS.x[0];
		C_vectorSP->y = curr_atomSP->raw_atomS.y;
		C_vectorSP->z = curr_atomSP->raw_atomS.z[0];
		vectors_extractedN++;
		C_foundF = 1;
		}
	}

/* Return the number of extracted vectors: */
return vectors_extractedN;
}

/*===========================================================================*/


