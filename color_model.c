/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				    color_model.c

Purpose:
	Model color scheme: assign different colors to different models.
	This should be useful with NMR structures.

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) Number of macromolecular complexes.
	(3) Pointer to GUIS structure, with GUI data.
	(4) Pointer to ColorSchemeS structure.

Output:
	(1) Return value.

Return value:
	(1) Positive always.

=============================================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======model color scheme:==================================================*/

int ColorModel_ (MolComplexS *mol_complexSP, int mol_complexesN,
		 GUIS *guiSP, ColorSchemeS *color_schemeSP)
{
int			mol_complexI;
MolComplexS		*curr_mol_complexSP;
size_t			atomsN, atomI;
AtomS			*curr_atomSP;
size_t			rgb_struct_size;
ColorSchemeS		*schemeSP;
int			surfaceI;
int			old_modelI = 0;
int			color_schemeI = -1;

/* The size of RGBS structure: */
rgb_struct_size = sizeof (RGBS);

/* Initialize the pointer to color scheme: */
schemeSP = color_schemeSP;

/* Scan every macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/* Pointer to the current macromolecular complex: */
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/* Check is the current macromolecular complex caught: */
	if (curr_mol_complexSP->catchF == 0) continue;

	/* Number of atoms in a macromolecular complex: */
	atomsN = curr_mol_complexSP->atomsN;

	/* Scan all atoms in the current complex: */
	for (atomI = 0; atomI < atomsN; atomI++)
		{
		/* Pointer to the current atom: */
		curr_atomSP = curr_mol_complexSP->atomSP + atomI;

		/* Check is atom selected: */
		if (curr_atomSP->selectedF == 0) continue;

		/* Set the number of color surfaces: */
		curr_atomSP->surfacesN = 2;

		/* Check the model serial number: */
		if (curr_atomSP->raw_atomS.model_serialI != old_modelI)
			{
			/* Store the model serial number: */
			old_modelI = curr_atomSP->raw_atomS.model_serialI;

			/* Change color: */
			color_schemeI++;

			/* Thirteen color schemes are used for models: */
			if (color_schemeI >= 13) color_schemeI = 0;

			/* Prepare the color scheme pointer: */
			schemeSP = color_schemeSP + color_schemeI;
			}

		/* Copy the color specifications: */
		for (surfaceI = 0; surfaceI < 2; surfaceI++)
			{
			/* Left color: */
			memcpy (curr_atomSP->left_rgbSA + surfaceI,
				schemeSP->left_rgbSA + surfaceI,
				rgb_struct_size);

			/* Middle color: */
			memcpy (curr_atomSP->middle_rgbSA + surfaceI,
				schemeSP->middle_rgbSA + surfaceI,
				rgb_struct_size);

			/* Right color: */
			memcpy (curr_atomSP->right_rgbSA + surfaceI,
				schemeSP->right_rgbSA + surfaceI,
				rgb_struct_size);
			}
		}

	/* Reset the position_changedF: */
	curr_mol_complexSP->position_changedF = 1;
	}

/* Return positive value: */
return 1;
}

/*===========================================================================*/


