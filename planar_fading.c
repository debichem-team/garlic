/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				planar_fading.c

Purpose:
	Prepare the left, middle and right color for each atom in a complex.
	The color fading is  planar (linear) in  this function.  Two planes,
	perpendicular to z axis,  define the area of color fading. All atoms
	closer to the observer than the front plane will have  the near atom
	color.  All atoms  behind  the back surface  will have  the far atom
	color.  RGB values are calculated from basic colors for each atom in
	the area between two planes. XAllocColor is not used, because it may
	work to slow.

Input:
	(1) Pointer to MolComplexS structure.
	(2) Pointer to GUIS structure.

Output:
	(1) left_colorID, middle_colorID and right_colorID members of  AtomS
	    structure initialized for each atom in macromolecular complex.
	(2) Return value.

Return value:
	(1) On success, the number of atoms which have colors different from
	    near and far color.  These atoms are located in the area between
	    the front and back surface.
	(2) Zero on failure.

Notes:
	(1) Do not skip hidden atoms,  the color of these atoms  may be used
	    for backbone drawing!

=============================================================================*/

#include <stdio.h>
#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

unsigned long	PixelFromRGBS_ (RGBS *, GUIS *);
unsigned long	PrepareColor_ (RGBS *, RGBS *, double, GUIS *);

/*======planar fading:=======================================================*/

size_t PlanarFading_ (MolComplexS *curr_mol_complexSP, GUIS *guiSP)
{
size_t			atoms_between_surfacesN = 0;
size_t			atomsN, atomI;
AtomS			*curr_atomSP;
int			surfacesN, surfaceI, near_surfaceI, far_surfaceI;
double			z0, z1, z;
double			overall_range, internal_range;
double			inverse_internal_range;
double			delta_z, scale_factor;

/* The number of atoms in a complex: */
atomsN = curr_mol_complexSP->atomsN;

/* The z coordinate of the front surface (plane): */
z0 = curr_mol_complexSP->fading_front_relative_position +
     curr_mol_complexSP->fading_center_vectorS.z;

/* The z coordinate of the back surface (plane): */
z1 = curr_mol_complexSP->fading_back_relative_position +
     curr_mol_complexSP->fading_center_vectorS.z;

/* Check: */
if (z0 >= z1) return 0;

/* The overall range: */
overall_range = z1 - z0;

/* Assign three colors to each atom: */
for (atomI = 0; atomI < atomsN; atomI++)
	{
	/** Pointer to the current atom: **/
	curr_atomSP = curr_mol_complexSP->atomSP + atomI;

	/** Is atom out of slab? **/
	if (!curr_atomSP->inside_slabF) continue;

	/** Check the number of color fading surfaces: **/
	surfacesN = curr_atomSP->surfacesN;
	/*** If there is only one, there is no fading: ***/
	if (surfacesN == 1)
		{
		curr_atomSP->left_colorID   =
			PixelFromRGBS_ (curr_atomSP->left_rgbSA, guiSP);
		curr_atomSP->middle_colorID =
			PixelFromRGBS_ (curr_atomSP->middle_rgbSA, guiSP);
		curr_atomSP->right_colorID  =
			PixelFromRGBS_ (curr_atomSP->right_rgbSA, guiSP);
		continue;
		}

	/** The z coordinate of the current atom: **/
	z = curr_atomSP->raw_atomS.z[0];

	/** If current atom is closer to the observer than **/
	/** the front surface, near colors should be used: **/
	if (z <= z0)
		{
		curr_atomSP->left_colorID   =
			PixelFromRGBS_ (curr_atomSP->left_rgbSA, guiSP);
		curr_atomSP->middle_colorID =
			PixelFromRGBS_ (curr_atomSP->middle_rgbSA, guiSP);
		curr_atomSP->right_colorID  =
			PixelFromRGBS_ (curr_atomSP->right_rgbSA, guiSP);
		continue;
		}

	/** If current atom is  behind the back **/
	/** surface, far colors should be used: **/
	else if (z >= z1)
		{
		surfaceI = surfacesN - 1;
		curr_atomSP->left_colorID   =
			PixelFromRGBS_ (curr_atomSP->left_rgbSA + surfaceI,
					guiSP);
		curr_atomSP->middle_colorID =
			PixelFromRGBS_ (curr_atomSP->middle_rgbSA + surfaceI,
					guiSP);
		curr_atomSP->right_colorID  =
			PixelFromRGBS_ (curr_atomSP->right_rgbSA + surfaceI,
					guiSP);
		continue;
		}

	/** If this point is reached, current atom is in **/
	/** the area between two surfaces; colors should **/
	/** be calculated weighting near and far colors: **/

	/** Prepare auxiliary variables: **/
	if (surfacesN > 1)
		{
		internal_range = overall_range / (double) (surfacesN - 1);
		inverse_internal_range = 1.0 / internal_range;
		near_surfaceI = (int) floor ((z - z0) *
				inverse_internal_range);
		far_surfaceI = near_surfaceI + 1;
		delta_z = z - near_surfaceI * internal_range - z0;
		scale_factor = delta_z * inverse_internal_range;
		}
	else
		{
		scale_factor = 0.0;
		far_surfaceI = 0;
		near_surfaceI = 0;
		}

	/** Left color: **/
	curr_atomSP->left_colorID =
		PrepareColor_ (curr_atomSP->left_rgbSA + near_surfaceI,
			       curr_atomSP->left_rgbSA + far_surfaceI,
			       scale_factor, guiSP);

	/** Middle color: **/
	curr_atomSP->middle_colorID =
		PrepareColor_ (curr_atomSP->middle_rgbSA + near_surfaceI,
			       curr_atomSP->middle_rgbSA + far_surfaceI,
			       scale_factor, guiSP);

        /** Right color: **/
        curr_atomSP->right_colorID =
		PrepareColor_ (curr_atomSP->right_rgbSA + near_surfaceI,
			       curr_atomSP->right_rgbSA + far_surfaceI,
			       scale_factor, guiSP);

	/** Update the number of atoms in the area between two surfaces: **/
	atoms_between_surfacesN++;
	}

/* Return  the  number  of atoms  which  have */
/* colors different from near and far colors: */
return atoms_between_surfacesN;
}

/*===========================================================================*/


