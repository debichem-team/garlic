/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				absolute_value.c

Purpose:
	Calculate the absolute value of a given vector.

Input:
	(1) Pointer to VectorS structure.

Output:
	Return value.

Return value:
	Absolute value (positive double value).

========includes:============================================================*/

#include <stdio.h>

#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======calculate absolute value of a vector:================================*/

double AbsoluteValue_ (VectorS *vectorSP)
{

return sqrt (vectorSP->x * vectorSP->x +
	     vectorSP->y * vectorSP->y +
	     vectorSP->z * vectorSP->z);

}

/*===========================================================================*/


