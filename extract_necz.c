/* Copyright (C) 2001, 2002 Damir Zucic */

/*=============================================================================

				extract_necz.c

Purpose:
	Extract NE and CZ coordinates for a given residue.

Input:
	(1) Pointer to VectorS structure, for NE coordinates.
	(2) Pointer to VectorS structure, for CZ coordinates.
	(3) Pointer to AtomS structure,  pointing to the first element of
	    the atomic array.
	(4) Index of the first atom of a given residue.
	(5) Index of the last atom of a given residue.

Output:
	(1) VectorS structures filled with data.
	(2) Return value.

Return value:
	(1) The number of successfully extracted vectors  (at least zero,
	    at most two).

Notes:
	(1) Some files contain more than one entry for some atoms. Garlic
	    uses only the first entry and  discards other entries for the
	    same atom.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======extract NE and CZ coordinates:=======================================*/

int ExtractNECZ_ (VectorS  *NE_vectorSP, VectorS *CZ_vectorSP,
		  AtomS *atomSP, size_t atom_startI, size_t atom_endI)
{
int		vectors_extractedN = 0;
size_t		atomI;
AtomS		*curr_atomSP;
char		*atom_nameP;
int		NE_foundF = 0, CZ_foundF = 0;

/* Scan the given residue: */
for (atomI = atom_startI; atomI <= atom_endI; atomI++)
	{
	/* Pointer to the current atom: */
	curr_atomSP = atomSP + atomI;

	/* Pointer to the purified atom name: */
	atom_nameP = curr_atomSP->raw_atomS.pure_atom_nameA;

	/* NE: */
	if (strcmp (atom_nameP, "NE") == 0)
		{
		if (NE_foundF) continue;
		NE_vectorSP->x = curr_atomSP->raw_atomS.x[0];
		NE_vectorSP->y = curr_atomSP->raw_atomS.y;
		NE_vectorSP->z = curr_atomSP->raw_atomS.z[0];
		vectors_extractedN++;
		NE_foundF = 1;
		}

	/* CZ: */
	else if (strcmp (atom_nameP, "CZ") == 0)
		{
		if (CZ_foundF) continue;
		CZ_vectorSP->x = curr_atomSP->raw_atomS.x[0];
		CZ_vectorSP->y = curr_atomSP->raw_atomS.y;
		CZ_vectorSP->z = curr_atomSP->raw_atomS.z[0];
		vectors_extractedN++;
		CZ_foundF = 1;
		}
	}

/* Return the number of extracted vectors: */
return vectors_extractedN;
}

/*===========================================================================*/


