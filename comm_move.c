/* Copyright (C) 2000, 2001 Damir Zucic */

/*=============================================================================

				comm_move.c

Purpose:
	Execute move command: set bits for objects which should be moved.
	More keywords may be specified simultaneously.  The keywords are:
	ALL, STR (STRUCTURE), PLA (PLANE) and MEM (MEMBRANE). 

Input:
	(1) Pointer to MolComplexS structure.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to the remainder of the command string.  This command
	    may be given with a list of keywords.

Output:
	(1) Bits set for specified objects.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======execute move command:================================================*/

int CommandMove_ (MolComplexS *mol_complexSP, int mol_complexesN,
		  RuntimeS *runtimeSP, char *stringP)
{
int		move_bits = 0;
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;

/* If keyword ALL is present, set all bits: */
if (strstr (stringP, "ALL"))
	{
	move_bits = STRUCTURE_MASK | PLANE_MASK | MEMBRANE_MASK;
	}

/* If keyword STR (STRUCTURE) is present: */
if (strstr (stringP, "STR")) move_bits = STRUCTURE_MASK;

/* If keyword PLA (PLANE) is present: */
if (strstr (stringP, "PLA")) move_bits = PLANE_MASK;

/* If keyword MEM (MEMBRANE) is present: */
if (strstr (stringP, "MEM")) move_bits = MEMBRANE_MASK;

/* If move_bits are unchanged, keyword recognition failed: */
if (move_bits == 0)
	{
	strcpy (runtimeSP->messageA, "Failed to recognize keyword(s)!");
        runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_MOVE;
	}

/* Sets bits for every caught macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/* Pointer to the current macromolecular complex: */
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/* Check is the current macromolecular complex caught: */
	if (curr_mol_complexSP->catchF == 0) continue;

	/* Copy movement bits: */
	curr_mol_complexSP->move_bits = move_bits;
	}

/* Return the command code on success: */
return COMMAND_MOVE;
}

/*===========================================================================*/


