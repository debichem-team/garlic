/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				chains.c

Purpose:
	Extract chain identifiers from the list.

Input:
	(1) Pointer to SelectS structure, where data will be stored.
	(2) Pointer to the string with list of chain identifiers.

Output:
	(1) Some data added to SelectS structure.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======extract chain ID's:==================================================*/

int ExtractChains_ (SelectS *selectSP, char *stringP)
{
char		*P, *tokenP;
int		chainI;

/* Check the string length: */
if (strlen (stringP) == 0) return -1;

/* Initialize all_chainsF (1 = select all chains): */
selectSP->all_chainsF = 0;

/* Initialize the number of chain identifiers: */
selectSP->chainsN = 0; 

/* If wildcard (asterisk) is present, set all_chainsF to one and return: */
if (strstr (stringP, "*"))
	{
	selectSP->all_chainsF = 1;
	return 1;
	}

/* Parse the list of chain identifiers: */
P = stringP;
while ((tokenP = strtok (P, " \t,;")) != NULL)
	{
	/** Ensure the proper operation of strtok: **/
	P = NULL;

	/** Check the token length - it should be equal to one: **/
	if (strlen (tokenP) != 1) return -1;

	/** Copy the chain identifier and update the count: **/
	chainI = selectSP->chainsN;
	selectSP->chainIDA[chainI] = *tokenP;

	/** Update and check the count: **/
	selectSP->chainsN++;
	if (selectSP->chainsN >= MAXFIELDS) break;
	}

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


