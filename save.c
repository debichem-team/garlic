/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

				save.c

Purpose:
	Execute save command: save the atomic coordinates to a specified
	file. PDB format is used.

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.

Output:
	(1) Atomic coordinates written to file, on success.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>
#include <ctype.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
int		WriteLine_ (FILE *, RawAtomS *);

/*======execute save command:================================================*/

int Save_ (MolComplexS *mol_complexSP, int mol_complexesN, RuntimeS *runtimeSP)
{
char		*remainderP;
char		tokenA[STRINGSIZE];
FILE		*fileP;
char		token_copyA[STRINGSIZE];
char		*P;
int		n;
int		selectionF = 0;
MolComplexS	*curr_mol_complexSP;
size_t		atomsN, atomI;
AtomS		*curr_atomSP;

/* Skip the first token: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE,
			    runtimeSP->curr_commandA, " \t\n");
if (!remainderP) return ERROR_SAVE;

/* The second token should contain the file name: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, " \t\n");
if (!remainderP)
	{
	strcpy (runtimeSP->messageA, "Missing the output file name!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_NO_FILE_NAME;
	}

/* Try to open the output file: */
if ((fileP = fopen (tokenA, "w")) == NULL)
	{
	strcpy (runtimeSP->messageA, "Failed to open output file!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_OPEN_FAILURE;
	}

/* If third token is present, it should contain the keyword SEL: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, " \t\n");
if (remainderP)
	{
	/** Convert token to uppercase: **/
	strncpy (token_copyA, tokenA, STRINGSIZE - 1);
	P = token_copyA;
	while ((n = *P++) != '\0') *(P - 1) = toupper (n);

	/** If this token contains the keyword SEL, set selectionF: **/
	if (strstr (token_copyA, "SEL") == token_copyA) selectionF = 1;
	}

/* Prepare the pointer to default caught complex: */
curr_mol_complexSP = mol_complexSP + runtimeSP->default_complexI;

/** Number of atoms in a macromolecular complex: **/
atomsN = curr_mol_complexSP->atomsN;
if (atomsN == 0)
	{
	strcpy (runtimeSP->messageA, "Catch the complex which is not empty!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	fclose (fileP);
	return ERROR_SAVE;
	}

/* Scan all atoms in the current complex: */
for (atomI = 0; atomI < atomsN; atomI++)
	{
	/** Pointer to the current atom: **/
	curr_atomSP = curr_mol_complexSP->atomSP + atomI;

	/** If only  selected  atoms  should be **/
	/** saved, check is this atom selected: **/
	if (selectionF)
		{
		if (curr_atomSP->selectedF == 0) continue;
		}

	/** Write data to output file: **/
	WriteLine_ (fileP, &curr_atomSP->raw_atomS);
	}

/* Close the output file: */
fclose (fileP);

/* Return the command code: */
return COMMAND_SAVE;
}

/*===========================================================================*/


