/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				extract_phrase.c

Purpose:
	Extract phrase: copy the string, removing leading and trailing spaces,
	tabs and newline. Maximal phrase length is SHORTSTRINGSIZE - 1.

Input:
	(1) Output string pointer.
	(2) Input string pointer.

Output:
	(1) Substring stored at location pointed at  by output string pointer.
	(2) Return value.

Return value:
	(1) Output string length, on success.
	(2) Negative on failure.

Notes:
	(1) Color specification in .garlicrc may contain a colon. Be careful!

========includes:============================================================*/

#include <stdio.h>
#include <string.h>

#include "defines.h"

/*======extract phrase:======================================================*/

int ExtractPhrase_ (char *outP, char *inP)
{
char		*currP;
int		i, n;

/* Colon must be present (the leftmost one is a separator): */
if ((currP = strstr (inP, ":")) == NULL)
	{
	*outP = '\0';
	return 0;
	}

/* Skip the leading spaces and tabs (and newline, if line is bad): */
currP++;  				    /* The character following colon */
while ((n = *currP) != '\0')
	{
	if ((n != ' ') && (n != '\t') && (n != '\n')) break;
	currP++;
	}

/* Copy the rest, but no more than SHORTSTRINGSIZE - 1 character: */
i = 0;
while ((n = *currP++) != '\0')
	{
	*(outP + i) = n;
	if (i >= SHORTSTRINGSIZE - 1) break;
	i++;
	}
*(outP + i) = '\0';

/* Remove the trailing spaces, tabs and newline: */
n = strlen (outP);
currP = outP + n - 1;
while (currP != outP)
	{
	n = *currP;
	if ((n == ' ') || (n == '\t') || (n == '\n')) *currP = '\0';
	else break;
	currP--;
	}

return strlen (outP);
}

/*===========================================================================*/


