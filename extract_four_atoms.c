/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

				extract_four_atoms.c

Purpose:
	Extract the coordinates of the four atoms. The atoms are defined
	given by the purified atomic names  (like "N", "CA", "CB" etc.).

Input:
	(1) Pointer to VectorS structure for the first atom.
	(2) Pointer to VectorS structure for the second atom.
	(3) Pointer to VectorS structure for the third atom.
	(4) Pointer to VectorS structure for the fourth atom.
	(5) Pointer to the name of the first atom.
	(6) Pointer to the name of the second atom.
	(7) Pointer to the name of the third atom.
	(8) Pointer to the name of the fourth atom.
	(9) Pointer to AtomS structure, pointing to the first element of
	    the atomic array.
       (10) Index of the first atom of a given residue.
       (11) Index of the last atom of a given residue.

Output:
	(1) VectorS structures filled with data.
	(2) Return value.

Return value:
	(1) The number of successfully extracted vectors (at least zero,
	    at most four).

Notes:
	(1) Some files contain more than one entry for some atoms.  Only
	    the first entry is used here.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======extract four atoms:==================================================*/

int ExtractFourAtoms_ (VectorS *vector1SP, VectorS *vector2SP,
		       VectorS *vector3SP, VectorS *vector4SP,
		       char *name1P, char *name2P, char *name3P, char *name4P,
		       AtomS *atomSP, size_t atom_startI, size_t atom_endI)
{
int		vectors_extractedN = 0;
size_t		atomI;
AtomS		*curr_atomSP;
int		found1F = 0, found2F = 0, found3F = 0, found4F = 0;

/* Scan the given residue: */
for (atomI = atom_startI; atomI <= atom_endI; atomI++)
	{
	/* Pointer to the current atom: */
	curr_atomSP = atomSP + atomI;

	/* The first atom: */
	if (strcmp (curr_atomSP->raw_atomS.pure_atom_nameA, name1P) == 0)
		{
		if (found1F) continue;
                vector1SP->x = curr_atomSP->raw_atomS.x[0];
                vector1SP->y = curr_atomSP->raw_atomS.y;
                vector1SP->z = curr_atomSP->raw_atomS.z[0];
		vectors_extractedN++;
		found1F = 1;
		}

	/* The second atom: */
	else if (strcmp (curr_atomSP->raw_atomS.pure_atom_nameA, name2P) == 0)
		{
		if (found2F) continue;
		vector2SP->x = curr_atomSP->raw_atomS.x[0];
		vector2SP->y = curr_atomSP->raw_atomS.y;
		vector2SP->z = curr_atomSP->raw_atomS.z[0];
		vectors_extractedN++;
		found2F = 1;
		}

	/* The third atom: */
	else if (strcmp (curr_atomSP->raw_atomS.pure_atom_nameA, name3P) == 0)
		{
		if (found3F) continue;
		vector3SP->x = curr_atomSP->raw_atomS.x[0];
		vector3SP->y = curr_atomSP->raw_atomS.y;
		vector3SP->z = curr_atomSP->raw_atomS.z[0];
		vectors_extractedN++;
		found3F = 1;
		}

	/* The fourth atom: */
	else if (strcmp (curr_atomSP->raw_atomS.pure_atom_nameA, name4P) == 0)
		{
		if (found4F) continue;
		vector4SP->x = curr_atomSP->raw_atomS.x[0];
		vector4SP->y = curr_atomSP->raw_atomS.y;
		vector4SP->z = curr_atomSP->raw_atomS.z[0];
		vectors_extractedN++;
		found4F = 1;
		}
	}

/* Return the number of extracted vectors: */
return vectors_extractedN;
}

/*===========================================================================*/


