/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				bonds_style3.c

Purpose:
	Draw bonds using style 3 (five lines per bond). Half of each bond
	is drawn and another half is assigned to the partner. The z value
	is calculated  for  each pixel.  A bond  is visible only  if both
	atoms forming this bond are visible.

Input:
	(1) Pointer to MolComplexS structure.
	(2) Number of macromolecular complexes.
	(3) Pointer to ConfigS structure, with configuration data.
	(4) Pointer to GUIS structure.
	(5) Pointer to NearestAtomS structure, with information about the
	    atom occupying the given pixel.
	(6) The number of pixels in the main window free area.
	(7) The refreshI, used to check the  NearestAtomS associated with
	    a given pixel.

Output:
	(1) Atoms drawn to the hidden pixmap.
	(2) Return value.

Return value:
	(1) The number of bonds drawn.

Notes:
	(1) Indentation is exceptionally 4 spaces.

	(2) Deep indentation is allowed in this function.  This is not my
	    favorite coding style but it may help to improve performance.

	(3) The vector from  the current atom to the bond partner belongs
	    to one of five regions.  If atomic positions  are exactly the
	    same, the vector belongs to the region "zero".  Otherwise, it
	    belongs to  one of four quadrants.  Quadrants are arranged as
	    shown here:

             ***    4    ***       o----->
               ***     ***         |       X
                 *** ***           |
             3     ***    1        V
                 *** ***              Y
               ***     ***
             ***    2    ***

	(4) Nothing is drawn for bonds perpendicular to the screen.

	(5) Two additional colors are prepared.

=============================================================================*/

#include <stdio.h>
#include <stdlib.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

unsigned long	InterpolateColor_ (unsigned long, unsigned long, GUIS *);
int		BondStyle3Quadrant1_ (Aux1S *, int);
int		BondStyle3Quadrant2_ (Aux1S *, int);
int		BondStyle3Quadrant3_ (Aux1S *, int);
int		BondStyle3Quadrant4_ (Aux1S *, int);

/*======draw bonds using style 3 (five lines per bond):======================*/

size_t DrawBondsStyle3_ (MolComplexS *mol_complexSP, int mol_complexesN,
			 ConfigS *configSP, GUIS *guiSP,
			 NearestAtomS *nearest_atomSP, size_t pixelsN,
			 unsigned int refreshI)
{
size_t			bonds_drawnN = 0;
int			imageI, imagesN;
Aux1S			aux1S;
int			mol_complexI;
MolComplexS		*curr_mol_complexSP, *partner_mol_complexSP;
size_t			atomsN, atomI;
unsigned int		color1ID, color2ID;
AtomS			*curr_atomSP, *partner_atomSP;
int			bondsN, bondI;
TrueBondS		*curr_bondSP;
int			screen_abs_delta_x, screen_abs_delta_y;
int			regionI;
double			z_shift, delta_z_sign;
int			hybonds_hiddenF;

/* Number of images: */
if (configSP->stereoF) imagesN = 2;
else imagesN = 1;

/* Prepare the auxiliary  Aux1S  structure,  used to */
/* reduce the number of arguments in function calls: */
aux1S.configSP = configSP;
aux1S.guiSP = guiSP;
aux1S.nearest_atomSP = nearest_atomSP;
aux1S.pixelsN = pixelsN;
aux1S.refreshI = refreshI;

/* Draw each macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
    {
    /* Pointer to current macromolecular complex: */
    curr_mol_complexSP = mol_complexSP + mol_complexI;

    /* Prepare and check the number of atoms: */
    atomsN = curr_mol_complexSP->atomsN;
    if (atomsN == 0) continue;

    /* Copy the macromol. complex index into aux1S: */
    aux1S.mol_complexI = mol_complexI;

    /* Copy the flag which says are hydrogen bonds hidden or not: */
    hybonds_hiddenF = curr_mol_complexSP->hydrogen_bonds_hiddenF;

    /* Draw bonds which have the given style: */
    for (atomI = 0; atomI < atomsN; atomI++)
	{
	/* Pointer to the current atom: */
	curr_atomSP = curr_mol_complexSP->atomSP + atomI;

	/* Check is atom hidden: */
	if (curr_atomSP->hiddenF) continue;

	/* Check is atom inside slab: */
	if (!curr_atomSP->inside_slabF) continue;

	/* Check is atom inside window: */
	if (!curr_atomSP->inside_windowF) continue;

	/* Prepare two additional colors: */
	color1ID = InterpolateColor_ (curr_atomSP->left_colorID,
				      curr_atomSP->middle_colorID, guiSP);
	color2ID = InterpolateColor_ (curr_atomSP->middle_colorID,
				      curr_atomSP->right_colorID, guiSP);

	/* Prepare GC's: */
	XSetForeground (guiSP->displaySP, guiSP->theGCA[0],
			curr_atomSP->left_colorID);
	XSetForeground (guiSP->displaySP, guiSP->theGCA[1],
			color1ID);
        XSetForeground (guiSP->displaySP, guiSP->theGCA[2],
			curr_atomSP->middle_colorID);
	XSetForeground (guiSP->displaySP, guiSP->theGCA[3],
			color2ID);
        XSetForeground (guiSP->displaySP, guiSP->theGCA[4],
			curr_atomSP->right_colorID);

	/* Copy color values to aux1S: */
	aux1S.colorIDA[0] = curr_atomSP->left_colorID;
	aux1S.colorIDA[1] = color1ID;
	aux1S.colorIDA[2] = curr_atomSP->middle_colorID;
	aux1S.colorIDA[3] = color2ID;
	aux1S.colorIDA[4] = curr_atomSP->right_colorID;

	/* Copy the atomic index into aux1S: */
	aux1S.atomI = atomI;

	/* Number of bonds: */
	bondsN = curr_atomSP->bondsN;

	/* Bond loop: */
	for (bondI = 0; bondI < bondsN; bondI++)
	    {
	    /* Pointer to the structure with data about current bond: */
	    curr_bondSP = curr_atomSP->true_bondSA + bondI;
	
	    /* Check bond drawing style: */
	    if (curr_bondSP->bond_styleI != 3) continue;

	    /* If the current bond is hydrogen bond, it may be hidden: */
	    if (curr_bondSP->bond_typeI == 0)
		{
		if (hybonds_hiddenF) continue;
		}

	    /* The complex which contains the atom forming the bond: */
	    partner_mol_complexSP = mol_complexSP +
				    curr_bondSP->neighbor_mol_complexI;

	    /* Pointer to the bond partner: */
	    partner_atomSP = partner_mol_complexSP->atomSP +
			     curr_bondSP->neighbor_arrayI;

	    /* If partner is not visible, do not draw this bond: */
	    if (partner_atomSP->hiddenF) continue;

	    /* Draw one (mono) or two bonds (stereo): */
	    for (imageI = 0; imageI < imagesN; imageI++)
		{
		/* Copy imageI to aux1S: */
		aux1S.imageI = imageI;

		/* Prepare screen coordinates of */
		/* both atoms  involved in bond: */
		aux1S.screen_x0 = curr_atomSP->raw_atomS.screen_x[imageI];
		aux1S.screen_y0 = curr_atomSP->raw_atomS.screen_y;
		aux1S.screen_x1 = partner_atomSP->raw_atomS.screen_x[imageI];
		aux1S.screen_y1 = partner_atomSP->raw_atomS.screen_y;
		aux1S.atomic_z0 = curr_atomSP->raw_atomS.z[imageI];
		aux1S.atomic_z1 = partner_atomSP->raw_atomS.z[imageI];

		/* Prepare differences: */
		aux1S.screen_delta_x = aux1S.screen_x1 - aux1S.screen_x0;
		aux1S.screen_delta_y = aux1S.screen_y1 - aux1S.screen_y0;
		aux1S.atomic_delta_z = aux1S.atomic_z1 - aux1S.atomic_z0;

		/* Shift and tilt bond to ensure */
		/* the proper stacking of bonds: */
		delta_z_sign = 1.0;
		if (aux1S.atomic_delta_z < 0) delta_z_sign = -1.0;
		z_shift = delta_z_sign * Z_SHIFT;
		aux1S.atomic_z0 += z_shift;
		aux1S.atomic_z1 -= z_shift;
		aux1S.atomic_delta_z -= 3 * z_shift;

		/* Some absolute differences: */
		screen_abs_delta_x = abs (aux1S.screen_delta_x);
		screen_abs_delta_y = abs (aux1S.screen_delta_y);

		/* Find the region: */
		if (screen_abs_delta_x >= screen_abs_delta_y)
		    {
		    if (aux1S.screen_delta_x >= 0) regionI = 1;
		    else regionI = 3;
		    }
		else
		    {
		    if (aux1S.screen_delta_y >= 0) regionI = 2;
		    else regionI = 4;
		    }
		if ((aux1S.screen_delta_x == 0) && (aux1S.screen_delta_y == 0))
		    {
		    regionI = 0;
		    }

		/* Each region has its own drawing procedure: */
		switch (regionI)
		    {
		    /* Both atoms are  projected to  the same  point, */
		    /* i.e. this bond is perpendicular to the screen: */
		    case 0:
			/* Do nothing! */
			break;

		    /* Right quadrant: */
		    case 1:
			BondStyle3Quadrant1_ (&aux1S, bondI);
			break;

		    /* Bottom quadrant (check Note 3!): */
		    case 2:
			BondStyle3Quadrant2_ (&aux1S, bondI);
			break;

		    /* Left quadrant: */
		    case 3:
			BondStyle3Quadrant3_ (&aux1S, bondI);
			break;

		    /* Top quadrant (check Note 3!): */
		    case 4:
			BondStyle3Quadrant4_ (&aux1S, bondI);
			break;

		    /* This should be the impossible case: */
		    default:
			;
		    }

		/* Update the counter: */
		bonds_drawnN++;
		}	/* imageI loop */
	    }		/* bondI  loop */
	}		/* atomI  loop */
    }			/* mol_complexI loop */

/* Return the number of bonds which were drawn: */
return bonds_drawnN;
}

/*===========================================================================*/


