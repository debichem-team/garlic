/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				docking_refresh.c

Purpose:
	Refresh the content of docking window.

Input:
	(1) Pointer to RuntimeS structure.
	(2) Pointer to GUIS structure.

Output:
	(1) The content of docking window refreshed.
	(2) Return value.

Return value:
	(1) Positive always (trivial).

Notes:
	(1) The index of exposed atom is of the type int, though size_t
	    is used elsewhere. The reason is that int may have negative
	    value,  while size_t is unsigned on many systems.  Negative
	    values are used  to signal that data stored  to a given box
	    are obsolete.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		DockingProject_ (RuntimeS *, int);
int		ListExposedPolarResidues_ (RuntimeS *, int);
int		RepresentativeAtoms_ (RuntimeS *, int);
int		ExcludeDistant_ (RuntimeS *, int);
int		DrawBottom_ (GUIS *, RuntimeS *);
int		DrawTop_ (GUIS *, RuntimeS *);

/*======refresh docking window:==============================================*/

int DockingRefresh_ (RuntimeS *runtimeSP, GUIS *guiSP)
{

/* Prepare orthogonal projections: */
DockingProject_ (runtimeSP, 1);
DockingProject_ (runtimeSP, 2);

/* Prepare  two lists of  exposed  polar  residues.  Each */
/* residue may be found more than once in docking matrix: */
ListExposedPolarResidues_ (runtimeSP, 1);
ListExposedPolarResidues_ (runtimeSP, 2);

/* Find representative atoms: */
RepresentativeAtoms_ (runtimeSP, 1);
RepresentativeAtoms_ (runtimeSP, 2);

/* Exclude residues which are below the plane: */
ExcludeDistant_ (runtimeSP, 1);
ExcludeDistant_ (runtimeSP, 2);

/* Draw symbols: */
DrawBottom_ (guiSP, runtimeSP);
DrawTop_ (guiSP, runtimeSP);

/* Return positive value: */
return 1;
}

/*===========================================================================*/


