/* Copyright (C) 2002 Damir Zucic */

/*=============================================================================

				balls.c

Purpose:
	Execute balls command: change the ball radius. This radius is used
	used to draw atoms as balls.

Input:
	(1) Pointer to ConfigS structure.
	(2) Pointer to the remainder of the command string.

Output:
	(1) The ball radius.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>
#include <ctype.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute balls command:===============================================*/

int Balls_ (MolComplexS *mol_complexSP, int mol_complexesN,
	    RuntimeS *runtimeSP,
	    ConfigS *configSP, GUIS *guiSP,
	    NearestAtomS *nearest_atomSP, size_t pixelsN,
	    unsigned int *refreshIP, char *stringP)
{
char		*P;
int		n;
double		value;
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;
size_t		atomI;
AtomS		*curr_atomSP;

/* Replace each non-numeric character (except */
/* minus sign and  decimal point) with space: */
P = stringP;
while ((n = *P++) != '\0')
	{
	if (!isdigit (n) && (n != '-') && (n != '.')) *(P - 1) = ' ';
	}

/* Try to extract the ball radius: */
if (sscanf (stringP, "%lf", &value) != 1)
	{
	strcpy (runtimeSP->messageA,
		"Failed to extract the ball radius!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_BALLS;
	}

/* Check the ball radius (must be positive): */
if (value < 0.0)
	{
	strcpy (runtimeSP->messageA, "Positive value expected!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_BALLS;
	}

/* On success, set the ball radius for each */
/* selected atom  in each  caught  complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/* Pointer to the current complex: */
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/* If this complex is not caught, skip it: */
	if (curr_mol_complexSP->catchF == 0) continue;

	/* Scan all atoms: */
	for (atomI = 0; atomI < curr_mol_complexSP->atomsN; atomI++)
		{
		/* Pointer to the current atom: */
		curr_atomSP = curr_mol_complexSP->atomSP + atomI;

		/* If this atom is not selected, skip it: */
		if (curr_atomSP->selectedF == 0) continue;

		/* Change the ball radius: */
		curr_atomSP->raw_atomS.ball_radius = value;
		}
	}

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN, runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return the command code: */
return COMMAND_BALLS;
}

/*===========================================================================*/


