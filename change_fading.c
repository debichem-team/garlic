/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				change_fading.c

Purpose:
	Change fading mode for each caught macromolecular complex.

Input:
	(1) Pointer to MolComplexS structure.
	(2) Number of macromolecular complexes.
	(3) Fading mode index.

Output:
	(1) Fading mode changed for each caught macromolecular complex.
	(2) Return value.

Return value:
	(1) The number of complexes for which the fading mode was changed.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

void		InitFading_ (MolComplexS *);

/*======change fading mode:====================================================*/

int ChangeFading_ (MolComplexS *mol_complexSP, int mol_complexesN,
		   int fading_modeI)
{
int		changesN = 0;
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;

/* Check every macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/** Pointer to the current macromolecular complex: **/
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/** Do not apply fading if position has not changed: **/
	if (curr_mol_complexSP->catchF== 0) continue;

	/** Change the fading mode: **/
	curr_mol_complexSP->fading_modeI = fading_modeI;

	/** Initialize fading: **/
	InitFading_ (curr_mol_complexSP);

	/** Change the position_changedF: **/
	curr_mol_complexSP->position_changedF = 1;

	/** Increase the count: **/
	changesN++;
	}

/* Return the number of  m. complexes for */
/* for which the fading mode was changed: */
return changesN;
}

/*===========================================================================*/

