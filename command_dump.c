/* Copyright (C) 2004-2006 Damir Zucic */

/*=============================================================================

				command_dump.c

Purpose:
	Execute dump command: dump some numeric data to the specified file
	or to the standard output stream.

Input:
	(1) Pointer to MolComplexS structure.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to the remainder of the command string.  The remainder
	    should contain only one keyword and optionally the output file
	    name.  The keywords are:  AVE (AVERAGE),  WEI (WEIGHTED),  HYD
	    (HYDROPHOBICITY), F1, F2, F3, F4, F5, F6 and F7.

Output:
	(1) Numeric data written to the output file or to stdout stream.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);

/*======execute dump command:================================================*/

int Dump_ (MolComplexS *mol_complexSP, int mol_complexesN,
	   RuntimeS *runtimeSP, char *stringP)
{
char		*remainderP;
char		tokenA[STRINGSIZE];
char		keywordA[SHORTSTRINGSIZE];
int		output_fileF = 0;
char		string_copyA[STRINGSIZE];
FILE		*outfileP = NULL;
size_t		residueI;
double		d;

/* Extract the first token, if present: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, stringP, " \t\n");

/* If there are no tokens in  the remainder of */
/* the command string, the keyword is missing: */
if (!remainderP)
	{
	strcpy (runtimeSP->messageA, "At least one keyword is expected!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_DUMP;
	}

/* Copy up to three letters of the first token for future use: */
strncpy (keywordA, tokenA, 3);
keywordA[3] = '\0';

/* If the second token is present, it should contain the output file name: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, " \t\n");
if (!remainderP) output_fileF = 0;	/* 0 = send output to stdout */
else             output_fileF = 1;	/* 1 = send output to a file */

/* Extract the output file name (if requested) and open file: */
if (output_fileF)
	{
	/* Copy the original command string: */
	strncpy (string_copyA, runtimeSP->curr_commandA, STRINGSIZE - 1);
	string_copyA[STRINGSIZE - 1] = '\0';

	/* Skip two tokens; the third token should contain output file name: */
	remainderP = ExtractToken_ (tokenA, STRINGSIZE, string_copyA, " \t\n");
	if (!remainderP)
		{
		strcpy (runtimeSP->messageA, "Nasty error #1 in Dump_!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_DUMP;
		}
	remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, " \t\n");
	if (!remainderP)
		{
		strcpy (runtimeSP->messageA, "Nasty error #2 in Dump_!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_DUMP;
		}
	remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, " \t\n");
	if (!remainderP) 
		{
		strcpy (runtimeSP->messageA, "Nasty error #3 in Dump_!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_DUMP;
		}

	/* Try to open output file: */
	if (!(outfileP = fopen (tokenA, "w")))
		{
		strcpy (runtimeSP->messageA, "Failed to open output file!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_DUMP;
		}
	}

/* A paranoid check: */
if (!outfileP) output_fileF = 0;

/* Dump the raw hydrophobicity, the average hydrophobicity or some */
/* other  hydrophobicity function  (F1, F2, F3, F4, F5, F6 or F7): */
if ((strstr (keywordA, "AVE") == keywordA) ||
    (strstr (keywordA, "WEI") == keywordA) ||
    (strstr (keywordA, "HYD") == keywordA) ||
    (strstr (keywordA, "F1")  == keywordA) ||
    (strstr (keywordA, "F2")  == keywordA) ||
    (strstr (keywordA, "F3")  == keywordA) ||
    (strstr (keywordA, "F4")  == keywordA) ||
    (strstr (keywordA, "F5")  == keywordA) ||
    (strstr (keywordA, "F6")  == keywordA) ||
    (strstr (keywordA, "F7")  == keywordA))
	{
	/* Write the title: */
	if (strstr (keywordA, "AVE") == keywordA)
		{
		if (output_fileF)
			fprintf (outfileP, "# Average hydrophobicity\n");
		else fprintf (stdout, "# Average hydrophobicity\n");
		}
	else if (strstr (keywordA, "WEI") == keywordA)
		{
		if (output_fileF)
			fprintf (outfileP, "# Weighted hydrophobicity\n");
		else fprintf (stdout, "# Weighted hydrophobicity\n");
		}
	else if (strstr (keywordA, "HYD") == keywordA)
		{
		if (output_fileF) fprintf (outfileP, "# Raw hydrophobicity\n");
		else fprintf (stdout, "# Raw hydrophobicity\n");
		}
	else if (strstr (keywordA, "F1") == keywordA)
		{
		if (output_fileF) fprintf (outfileP, "# Function F1\n");
		else fprintf (stdout, "# Function F1\n"); 
		}
	else if (strstr (keywordA, "F2") == keywordA)
		{
		if (output_fileF) fprintf (outfileP, "# Function F2\n");
		else fprintf (stdout, "# Function F2\n"); 
		}
	else if (strstr (keywordA, "F3") == keywordA)
		{
		if (output_fileF) fprintf (outfileP, "# Function F3\n");
		else fprintf (stdout, "# Function F3\n"); 
		}
	else if (strstr (keywordA, "F4") == keywordA)
		{
		if (output_fileF) fprintf (outfileP, "# Function F4\n");
		else fprintf (stdout, "# Function F4\n"); 
		}
	else if (strstr (keywordA, "F5") == keywordA)
		{
		if (output_fileF) fprintf (outfileP, "# Function F5\n");
		else fprintf (stdout, "# Function F5\n"); 
		}
	else if (strstr (keywordA, "F6") == keywordA)
		{
		if (output_fileF) fprintf (outfileP, "# Function F6\n");
		else fprintf (stdout, "# Function F6\n"); 
		}
	else if (strstr (keywordA, "F7") == keywordA)
		{
		if (output_fileF) fprintf (outfileP, "# Function F7\n");
		else fprintf (stdout, "# Function F7\n"); 
		}

	/* Write the values: */
	for (residueI = 0; residueI < runtimeSP->residuesN; residueI++)
		{
		/* Write the serial number, without newline: */
		if (output_fileF) fprintf (outfileP, "%8d", residueI);
		else fprintf (stdout, "%8d", residueI);

		/* Write the requested hydrophobicity function: */
		if (strstr (keywordA, "AVE") == keywordA)
			{
			d = *(runtimeSP->average_hydrophobicityP + residueI);
			if (output_fileF) fprintf (outfileP, " %8.2f\n", d);
			else fprintf (stdout, " %8.2f\n", d);
			}
		else if (strstr (keywordA, "WEI") == keywordA)
			{
			d = *(runtimeSP->weighted_hydrophobicityP + residueI);
			if (output_fileF) fprintf (outfileP, " %8.2f\n", d);
			else fprintf (stdout, " %8.2f\n", d);
			}
		else if (strstr (keywordA, "HYD") == keywordA)
			{
			d = *(runtimeSP->hydrophobicityP + residueI);
			if (output_fileF) fprintf (outfileP, " %8.2f\n", d);
			else fprintf (stdout, " %8.2f\n", d);
			}
		else if (strstr (keywordA, "F1") == keywordA)
			{
			d = *(runtimeSP->function1P + residueI);
			if (output_fileF) fprintf (outfileP, " %8.2f\n", d);
			else fprintf (stdout, " %8.2f\n", d); 
			}
		else if (strstr (keywordA, "F2") == keywordA)
			{
			d = *(runtimeSP->function2P + residueI);
			if (output_fileF) fprintf (outfileP, " %8.2f\n", d);
			else fprintf (stdout, " %8.2f\n", d); 
			}
		else if (strstr (keywordA, "F3") == keywordA)
			{
			d = *(runtimeSP->function3P + residueI);
			if (output_fileF) fprintf (outfileP, " %8.2f\n", d);
			else fprintf (stdout, " %8.2f\n", d); 
			}
		else if (strstr (keywordA, "F4") == keywordA)
			{
			d = *(runtimeSP->function4P + residueI);
			if (output_fileF) fprintf (outfileP, " %8.2f\n", d);
			else fprintf (stdout, " %8.2f\n", d); 
			}
		else if (strstr (keywordA, "F5") == keywordA)
			{
			d = *(runtimeSP->function5P + residueI);
			if (output_fileF) fprintf (outfileP, " %8.2f\n", d);
			else fprintf (stdout, " %8.2f\n", d); 
			}
		else if (strstr (keywordA, "F6") == keywordA)
			{
			d = *(runtimeSP->function6P + residueI);
			if (output_fileF) fprintf (outfileP, " %8.2f\n", d);
			else fprintf (stdout, " %8.2f\n", d); 
			}
		else if (strstr (keywordA, "F7") == keywordA)
			{
			d = *(runtimeSP->function7P + residueI);
			if (output_fileF) fprintf (outfileP, " %8.2f\n", d);
			else fprintf (stdout, " %8.2f\n", d); 
			}
		}
	}

/* Close the output file, if opened before: */
if (output_fileF) fclose (outfileP);

/* Return the command code: */
return COMMAND_DUMP;
}

/*===========================================================================*/


