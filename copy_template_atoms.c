/* Copyright (C) 2001-2003 Damir Zucic */

/*=============================================================================

			    copy_template_atoms.c

Purpose:
	Copy all atoms from the specified template residue to the new
	structure. Update the atomic serial numbers, residue sequence
	numbers and other data specific for the new structure. Update
	the number of atoms in the new structure.

Input:
	(1) Pointer to MolComplexS structure.
	(2) Pointer to RuntimeS structure.
	(3) Pointer to the current template residue.
	(4) Residue serial index.

Output:
	(1) Return value.

Return value:
	(1) The number of atoms in the specified template residue.

Notes:
	(1) This function  does not check  the amount of storage left
	    for atomic data.

	(2) The number of atoms is updated in this function.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>
#include <stdlib.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======copy atoms from template residue to new structure:===================*/

int CopyTemplateAtoms_ (MolComplexS *mol_complexSP, RuntimeS *runtimeSP,
			ResidueS *template_residueSP, int residueI)
{
int		atoms_copiedN = 0;
int		atom_struct_size;
int		first_template_atomI, last_template_atomI, template_atomI;
int		atomI;
AtomS		*curr_template_atomSP;
AtomS		*curr_atomSP;

/* Size of AtomS structure: */
atom_struct_size = sizeof (AtomS);

/* Prepare the indices of the first and the last template atom: */
first_template_atomI = template_residueSP->residue_startI;
last_template_atomI  = template_residueSP->residue_endI;

/* Prepare the index of the next atom in the new structure: */
atomI = mol_complexSP->atomsN;

/* Scan the template residue: */
for (template_atomI = first_template_atomI;
     template_atomI <= last_template_atomI;
     template_atomI++)
	{
	/* Pointer to the current template atom: */
	curr_template_atomSP = runtimeSP->template_atomSP + template_atomI;

	/* Pointer to the current atom: */
	curr_atomSP = mol_complexSP->atomSP + atomI;

	/* Copy  the complete  atomic data  from the */
	/* current template atom to the current atom: */
	memcpy (curr_atomSP,
		curr_template_atomSP, atom_struct_size);

	/* Fix atom serial number: */
	curr_atomSP->raw_atomS.serialI = atomI + 1;

	/* Fix residue sequence number: */
	curr_atomSP->raw_atomS.residue_sequenceI = residueI + 1;

	/* Fix residue insertion code: */
	curr_atomSP->raw_atomS.residue_insertion_code = ' ';

	/* Fix residue array index: */
	curr_atomSP->residue_arrayI = residueI;

	/* Update the atomic index: */
	atomI++;
	}

/* Update the number of atoms in the new structure: */
mol_complexSP->atomsN = atomI;

/* Return the number of atoms which were copied: */
return atoms_copiedN;
}

/*===========================================================================*/


