/* Copyright (C) 2000-2006 Damir Zucic */

/*=============================================================================

				plot.c

Purpose:
	Execute plot command: plot requested functions.  The command plot
	may be followed by a list of keywords. If keyword OFF is present,
	the main window  drawing mode  will be  changed  to default mode.
	Otherwise, for all recognized keywords the corresponding flags in
	RuntimeS structure will be set to one. Other keywords:
	HYD (HYDROPHOBICITY) - average hydrophobicity;
	WEI (WEIGHTED)       - weigthed hydrophobicity;
	MOM (MOMENT)	     - hydrophobic moment;
	SID (SIDED)          - sided hydrophobicity (porins).
	F1                   - hydrophobicity function F1;
	F2                   - hydrophobicity function F2.
	F3                   - hydrophobicity function F3.
	F4                   - hydrophobicity function F4.
	F5                   - hydrophobicity function F5.
	F6                   - hydrophobicity function F6.
	F7                   - hydrophobicity function F7.

Input:
	(1) Pointer to MolComplexS structure.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to ConfigS structure.
	(5) Pointer to GUIS structure.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
	(9) Pointer to the remainder of the command string.  This command
	    may be given with a list of keywords or with keyword OFF.

Output:
	(1) The main window mode changed to 4 (default is zero).
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

Notes:
	(1) This command reinitializes the NearestAtomS array,  except if
	    at least one of additional keywords is not recognized.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

void		InitNearest_ (NearestAtomS *, size_t);
int		ExtractTwoIntegers_ (int *, int *, char *);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute plot command:================================================*/

int Plot_ (MolComplexS *mol_complexSP, int mol_complexesN,
	   RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
	   NearestAtomS *nearest_atomSP, size_t pixelsN,
	   unsigned int *refreshIP, char *stringP)
{
char		*P;
int		n;
int		residue1I, residue2I;

/* Reset flags: */
runtimeSP->average_hydrophobicityF  = 0;
runtimeSP->weighted_hydrophobicityF = 0;
runtimeSP->hydrophobic_momentF      = 0;
runtimeSP->sided_hydrophobicityF    = 0;
runtimeSP->function1F               = 0;
runtimeSP->function2F               = 0;
runtimeSP->function3F               = 0;
runtimeSP->function4F               = 0;
runtimeSP->function5F               = 0;
runtimeSP->function6F               = 0;
runtimeSP->function7F               = 0;

/* If keyword OFF is present, switch to default drawing mode: */
if (strstr (stringP, "OFF"))
	{
	/* Reset drawing mode index: */
	guiSP->main_window_modeI = 0;

	/* Reinitialize the NearestAtomS array: */
	InitNearest_ (nearest_atomSP, pixelsN);
	*refreshIP = 1;

	/* Refresh the main window: */
	(*refreshIP)++;
	MainRefresh_ (mol_complexSP, mol_complexesN,
		      runtimeSP, configSP, guiSP,
		      nearest_atomSP, pixelsN, *refreshIP);

	/* Refresh the control window: */
	ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
			 configSP, guiSP);

	/* Return the command code: */
	return COMMAND_PLOT;
	}

/* The sequence buffer should be filled with something: */
if ((int) runtimeSP->residuesN == 0)
	{
	strcpy (runtimeSP->messageA, "The sequence buffer is empty!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_PLOT;
	}

/* The sliding window should not be wider than the sequence: */
if (runtimeSP->sliding_window_width > (int) runtimeSP->residuesN)
	{
	strcpy (runtimeSP->messageA,
		"The sliding window is wider than the sequence!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_PLOT;
	}

/* If keyword  HYD (short for HYDROPHOBICITY) is present,  set the */
/* flag which signals that average hydrophobicity should be drawn: */
if (strstr (stringP, "HYD"))
	{
	runtimeSP->average_hydrophobicityF = 1;
	}

/* If keyword WEI (short for WEIGHTED) is present, set the flag */
/* which signals that  weighted hydrophobicity should be drawn: */
if (strstr (stringP, "WEI"))
	{
	runtimeSP->weighted_hydrophobicityF = 1;
	}

/* If keyword MOM (short for MOMENT) is present, set the flag */
/* which signals  that  hydrophobic moment  should be  drawn: */
if (strstr (stringP, "MOM"))
	{
	runtimeSP->hydrophobic_momentF = 1;
	}

/* If keyword SID (short for SIDED_HYDROPHOBICITY) is present, set the flag */
/* which says that  sided hydrophobicity should be calculated  (two lines): */
if (strstr (stringP, "SID"))
	{
	runtimeSP->sided_hydrophobicityF = 1;
	}

/* If keyword F1 is present, draw the function F1. Do not forget */
/* to remove the substring "F1" from the string,  because it may */
/* confuse the parser which retrieves the residue array indices! */
if (strstr (stringP, "F1"))
	{
	/* Set flag: */
	runtimeSP->function1F = 1;

	/* Replace the substring "F1": */
	P = strstr (stringP, "F1");
	if (P)
		{
		*P = ' ';
		*(P + 1) = ' ';
		}
	}

/* If keyword F2 is present, draw the function F2. Do not forget */
/* to remove the substring "F2" from the string,  because it may */
/* confuse the parser which retrieves the residue array indices! */
if (strstr (stringP, "F2"))
	{
	/* Set flag: */
	runtimeSP->function2F = 1;

	/* Replace the substring "F2": */
	P = strstr (stringP, "F2");
	if (P)
		{
		*P = ' ';
		*(P + 1) = ' ';
		}
	}

/* If keyword F3 is present, draw the function F3. Do not forget */
/* to remove the substring "F3" from the string,  because it may */
/* confuse the parser which retrieves the residue array indices! */
if (strstr (stringP, "F3"))
	{
	/* Set flag: */
	runtimeSP->function3F = 1;

	/* Replace the substring "F3": */
	P = strstr (stringP, "F3");
	if (P)
		{
		*P = ' ';
		*(P + 1) = ' ';
		}
	}

/* If keyword F4 is present, draw the function F4. Do not forget */
/* to remove the substring "F4" from the string,  because it may */
/* confuse the parser which retrieves the residue array indices! */
if (strstr (stringP, "F4"))
	{
	/* Set flag: */
	runtimeSP->function4F = 1;

	/* Replace the substring "F4": */
	P = strstr (stringP, "F4");
	if (P)
		{
		*P = ' ';
		*(P + 1) = ' ';
		}
	}

/* If keyword F5 is present, draw the function F5. Do not forget */
/* to remove the substring "F5" from the string,  because it may */
/* confuse the parser which retrieves the residue array indices! */
if (strstr (stringP, "F5"))
	{
	/* Set flag: */
	runtimeSP->function5F = 1;

	/* Replace the substring "F5": */
	P = strstr (stringP, "F5");
	if (P)
		{
		*P = ' ';
		*(P + 1) = ' ';
		}
	}

/* If keyword F6 is present, draw the function F6. Do not forget */
/* to remove the substring "F6" from the string,  because it may */
/* confuse the parser which retrieves the residue array indices! */
if (strstr (stringP, "F6"))
	{
	/* Set flag: */
	runtimeSP->function6F = 1;

	/* Replace the substring "F6": */
	P = strstr (stringP, "F6");
	if (P)
		{
		*P = ' ';
		*(P + 1) = ' ';
		}
	}

/* If keyword F7 is present, draw the function F7. Do not forget */
/* to remove the substring "F7" from the string,  because it may */
/* confuse the parser which retrieves the residue array indices! */
if (strstr (stringP, "F7"))
	{
	/* Set flag: */
	runtimeSP->function7F = 1;

	/* Replace the substring "F7": */
	P = strstr (stringP, "F7");
	if (P)
		{
		*P = ' ';
		*(P + 1) = ' ';
		}
	}

/* At least one flag should be set to one: */
if ((runtimeSP->average_hydrophobicityF  == 0) &&
    (runtimeSP->weighted_hydrophobicityF == 0) &&
    (runtimeSP->hydrophobic_momentF      == 0) &&
    (runtimeSP->sided_hydrophobicityF    == 0) &&
    (runtimeSP->function1F               == 0) &&
    (runtimeSP->function2F               == 0) &&
    (runtimeSP->function3F               == 0) &&
    (runtimeSP->function4F               == 0) &&
    (runtimeSP->function5F               == 0) &&
    (runtimeSP->function6F               == 0) &&
    (runtimeSP->function7F               == 0))
	{
	strcpy (runtimeSP->messageA,
		"Failed to recognize what should be drawn!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_PLOT;
	}

/* If you want sided hydrophobicity, the sliding */
/* window should contain  at least two residues: */
if ((runtimeSP->sided_hydrophobicityF == 1) &&
    (runtimeSP->sliding_window_width < 2))
	{
	strcpy (runtimeSP->messageA,
		"For sided hyd. plot, the sliding window");
	strcat (runtimeSP->messageA,
		" should contain at least two residues!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_PLOT;
	}

/* Replace each minus and colon in the input string with space: */
P = stringP;
while ((n = *P++) != '\0')
	{
	if      (n == '-') *(P - 1) = ' ';
	else if (n == ':') *(P - 1) = ' ';
	}

/* Try to extract two indices: */
residue1I = 0;
residue2I = 0;
if (ExtractTwoIntegers_ (&residue1I, &residue2I, stringP) > 0)
	{
	/* Check indices: */
	if ((residue1I < 1) || (residue2I < residue1I))
		{
		strcpy (runtimeSP->messageA, "Bad range (check indices)!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_PLOT;
		}
	if (residue1I <= 0) residue1I = 1;
	if (residue2I <= 0) residue2I = 1;

	/* Store the extracted indices: */
	runtimeSP->range_startI = residue1I - 1;
	runtimeSP->range_endI   = residue2I - 1;
	}

/* If failed to extract indices, use default values: */
else
	{
	runtimeSP->range_startI = 0;
	if (runtimeSP->residuesN >= 1)
		{
		runtimeSP->range_endI = (int) runtimeSP->residuesN - 1;
		}
	else
		{
		runtimeSP->range_endI = 0;
		}
	}

/* Set the main window drawing mode index: */
guiSP->main_window_modeI = 4;

/* Reinitialize the NearestAtomS array and refresh index: */
InitNearest_ (nearest_atomSP, pixelsN);
*refreshIP = 1;

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return the command code: */
return COMMAND_PLOT;
}

/*===========================================================================*/


