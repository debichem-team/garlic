/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				angle.c

Purpose:
	Execute  angle  command:  define the angle separating neighboring
	residues along the axis of an alpha helix. The input angle should
	be specified in degrees.  Default hard-coded value is 100 degrees
	(see init_runtime.c).

Input:
	(1) Pointer to RuntimeS structure.
	(2) Pointer to the remainder of the command string.

Output:
	(1) The helix step angle value changed.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>
#include <ctype.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"


/*======execute angle command:===============================================*/

int Angle_ (RuntimeS *runtimeSP, char *stringP)
{
char		*P;
int		n;
double		value;

/* Replace each non-numeric character (except */
/* minus sign and  decimal point) with space: */
P = stringP;
while ((n = *P++) != '\0')
	{
	if (!isdigit (n) && (n != '-') && (n != '.')) *(P - 1) = ' ';
	}

/* Try to extract the angle: */
if (sscanf (stringP, "%lf", &value) != 1) return ERROR_ANGLE;

/* On success, copy the extracted angle to RuntimeS structure: */
runtimeSP->helix_step_angle = value;

/* Return the command code: */
return COMMAND_ANGLE;
}

/*===========================================================================*/


