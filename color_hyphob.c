/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

				color_hyphob.c

Purpose:
	Hyphob color scheme: colors are assigned according to hydrophobicity. 
	Residues with hydrophobicity close  to threshold value will be white,
	hydrophobic residues  will be  red and  hydrophilic residues  will be
	blue.

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) Number of macromolecular complexes.
	(3) Pointer to GUIS structure, with GUI data.
	(4) Pointer to ColorSchemeS structure.

Output:
	(1) Return value.

Return value:
	(1) Positive always.

=============================================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		MixColors_ (AtomS *, ColorSchemeS *, ColorSchemeS *,
			    double, GUIS *);

/*======hyphob color scheme:=================================================*/

int ColorHyphob_ (MolComplexS *mol_complexSP, int mol_complexesN,
		  GUIS *guiSP, ColorSchemeS *color_schemeSP)
{
size_t			rgb_struct_size;
ColorSchemeS		*blue_schemeSP, *white_schemeSP, *red_schemeSP;
int			mol_complexI;
MolComplexS		*curr_mol_complexSP;
size_t			atomsN, atomI;
AtomS			*curr_atomSP;
double			min_hyphob, threshold_hyphob, max_hyphob;
double			delta_hyphob1, delta_hyphob2;
double			inverse_range1, inverse_range2;
double			hyphob;
double			scale_factor;

/* The size of RGBS structure: */
rgb_struct_size = sizeof (RGBS);

/* Red scheme pointer: */
red_schemeSP = color_schemeSP + 0;

/* White scheme pointer: */
white_schemeSP = color_schemeSP + 6;

/* Blue scheme pointer: */
blue_schemeSP = color_schemeSP + 2;

/* Scan every macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/* Pointer to the current macromolecular complex: */
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/* Check is the current macromolecular complex caught: */
	if (curr_mol_complexSP->catchF == 0) continue;

	/* Minimal, maximal and threshold hydrophob. for current complex: */
	min_hyphob = (double) curr_mol_complexSP->min_hydrophobicity;
	max_hyphob = (double) curr_mol_complexSP->max_hydrophobicity;
	threshold_hyphob =
		     (double) curr_mol_complexSP->threshold_hydrophobicity;

	/* Inverse hydrophobicity ranges: */
	delta_hyphob1 = max_hyphob - threshold_hyphob;
	if (delta_hyphob1 != 0.0) inverse_range1 = 1.0 / delta_hyphob1;
	else inverse_range1 = 0.0;
	delta_hyphob2 = threshold_hyphob - min_hyphob;
	if (delta_hyphob2 != 0.0) inverse_range2 = 1.0 / delta_hyphob2;
	else inverse_range2 = 0.0;

	/* Number of atoms in a macromolecular complex: */
	atomsN = curr_mol_complexSP->atomsN;

	/* Scan all atoms in the current complex: */
	for (atomI = 0; atomI < atomsN; atomI++)
		{
		/* Pointer to the current atom: */
		curr_atomSP = curr_mol_complexSP->atomSP + atomI;

		/* Check is atom selected: */
		if (curr_atomSP->selectedF == 0) continue;

		/* Set the number of color surfaces: */
		curr_atomSP->surfacesN = 2;

		/* Hydrophobicity of the current atom: */
		hyphob = (double) curr_atomSP->raw_atomS.hydrophobicity;

		/* If hydrophobicity is below threshold value: */
		if (hyphob <= threshold_hyphob)
			{
			scale_factor = (threshold_hyphob - hyphob) *
					inverse_range2;
			MixColors_ (curr_atomSP,
				    white_schemeSP, blue_schemeSP,
				    scale_factor, guiSP);
			}

		/* If hydrophobicity is above threshold value: */
		else
			{
			scale_factor = (hyphob - threshold_hyphob) *
					inverse_range1;
			MixColors_ (curr_atomSP,
				    white_schemeSP, red_schemeSP,
				    scale_factor, guiSP);
			}
		}

	/* Reset the position_changedF: */
	curr_mol_complexSP->position_changedF = 1;
	}

/* Return positive value: */
return 1;
}

/*===========================================================================*/


