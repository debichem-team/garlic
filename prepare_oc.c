/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				prepare_oc.c

Purpose:
        Prepare O-C vector, used to check the hydrogen bond angle.  This
	function assumes that the atom you specify is an oxygen atom. No
	check is done!

Input:
	(1) Pointer to VectorS structure.
	(2) Pointer to AtomS structure.
	(3) Pointer to MolComplexS structure, pointing to the first such
	    structure.
	(3) Macromolecular complex index.

Output:
	(1) O-C vector may be prepared.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======prepare O-C vector:==================================================*/

int PrepareOCVector_ (VectorS *O_C_vectorSP, AtomS *atom1SP,
		      MolComplexS *mol_complexSP, int mol_complexI)
{
int		bondsN, bondI;
int		carbon_foundF;
TrueBondS	*curr_bondSP;
int		neighbor_mol_complexI;
AtomS		*atom2SP;

/* Prepare and check the number of bonds associated with a given atom: */
bondsN = atom1SP->bondsN;
if (bondsN <= 0) return -1;

/* Try to find the carbon atom: */
carbon_foundF = 0;
for (bondI = 0; bondI < bondsN; bondI++)
	{
	/** Pointer to the current bond: **/
	curr_bondSP = atom1SP->true_bondSA + bondI;

	/** The neighbor should belong to the same macromol. complex: **/
	neighbor_mol_complexI = curr_bondSP->neighbor_mol_complexI;
	if (neighbor_mol_complexI != mol_complexI) continue;

	/** Pointer to the neighboring atom: **/
	atom2SP = (mol_complexSP + neighbor_mol_complexI)->atomSP +
		  curr_bondSP->neighbor_arrayI;

	/** Check is neighboring atom carbon: **/
	if (strcmp (atom2SP->raw_atomS.chemical_symbolA, " C") != 0) continue;

	/** If this point is reached, the neighboring **/
	/** atom is carbon; prepare the  O-C  vector: **/
	O_C_vectorSP->x = atom2SP->raw_atomS.x[0] - atom1SP->raw_atomS.x[0];
	O_C_vectorSP->y = atom2SP->raw_atomS.y    - atom1SP->raw_atomS.y;
	O_C_vectorSP->z = atom2SP->raw_atomS.z[0] - atom1SP->raw_atomS.z[0];

	/** Set the flag which says that carbon atom was found: **/
	carbon_foundF = 1;

	/** Break from the loop, job is done: **/
	break;
	}

/* Return negative value on failure: */
if (!carbon_foundF) return -2;

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


