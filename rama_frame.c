/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				rama_frame.c

Purpose:
	Draw frame for Ramachandran plot, including labels.

Input:
	(1) Pointer to GUIS structure, with GUI data.

Output:
	(1) A rectangular frame drawn to the main window.

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		ParseColor_ (RGBS *, unsigned long *, GUIS *, char *, char *);

/*======draw frame for Ramachandran plot:====================================*/

void RamaFrame_ (GUIS *guiSP)
{
int		free_width, free_height;
int		frame_screen_x0, frame_screen_y0;
int		frame_screen_x1, frame_screen_y1;
int		frame_screen_width, frame_screen_height;
RGBS		rgbS;
unsigned long	frame_color1ID, frame_color2ID, frame_color3ID;
int		i;
int		screen_x0, screen_y0;
char		stringA[SHORTSTRINGSIZE];
int		string_length;

/* Prepare geometric parameters: */
free_width  = (int) guiSP->main_win_free_area_width;
free_height = (int) guiSP->main_win_free_area_height;
frame_screen_x0 = guiSP->main_winS.text_line_height + 2;
frame_screen_y0 = frame_screen_x0;
frame_screen_x1 = free_width - frame_screen_x0;
if (frame_screen_x1 < frame_screen_x0) frame_screen_x1 = frame_screen_x0;
frame_screen_y1 = free_height - frame_screen_y0;
if (frame_screen_y1 < frame_screen_y0) frame_screen_y1 = frame_screen_y0;
frame_screen_width  = frame_screen_x1 - frame_screen_x0 + 1;
frame_screen_height = frame_screen_y1 - frame_screen_y0 + 1;

/* Prepare frame colors: */
ParseColor_ (&rgbS, &frame_color1ID, guiSP, "RGB:EEEE/EEEE/EEEE", "white");
ParseColor_ (&rgbS, &frame_color2ID, guiSP, "RGB:9999/9999/9999", "white");
ParseColor_ (&rgbS, &frame_color3ID, guiSP, "RGB:4444/4444/4444", "black");
XSetForeground (guiSP->displaySP, guiSP->theGCA[1], frame_color1ID);
XSetForeground (guiSP->displaySP, guiSP->theGCA[2], frame_color2ID);
XSetForeground (guiSP->displaySP, guiSP->theGCA[3], frame_color3ID);

/* Thin lines: */
for (i = 1; i < 12; i++)
	{
	if (i == 6) continue;
	screen_x0 = frame_screen_x0 + i * frame_screen_width / 12;
	if ((i % 3) == 0)
		{
		XDrawLine (guiSP->displaySP,
			   guiSP->main_hidden_pixmapID,
			   guiSP->theGCA[1],
			   screen_x0 - 1, frame_screen_y0,
			   screen_x0 - 1, frame_screen_y1);
		XDrawLine (guiSP->displaySP,
			   guiSP->main_hidden_pixmapID,
			   guiSP->theGCA[2],
			   screen_x0, frame_screen_y0,
			   screen_x0, frame_screen_y1);
		XDrawLine (guiSP->displaySP,
			   guiSP->main_hidden_pixmapID,
			   guiSP->theGCA[3],
			   screen_x0 + 1, frame_screen_y0,
			   screen_x0 + 1, frame_screen_y1);
		}
	else
		{
		XDrawLine (guiSP->displaySP,
			   guiSP->main_hidden_pixmapID,
			   guiSP->theGCA[2],
			   screen_x0, frame_screen_y0,
			   screen_x0, frame_screen_y1);
		}
	}
for (i = 1; i < 12; i++)
	{
	if (i == 6) continue;
	screen_y0 = frame_screen_y0 + i * frame_screen_height / 12;
	if ((i % 3) == 0)
		{
		XDrawLine (guiSP->displaySP,
			   guiSP->main_hidden_pixmapID,
			   guiSP->theGCA[1],
			   frame_screen_x0, screen_y0 - 1,
			   frame_screen_x1, screen_y0 - 1);
		XDrawLine (guiSP->displaySP,
			   guiSP->main_hidden_pixmapID,
			   guiSP->theGCA[2],
			   frame_screen_x0, screen_y0,
			   frame_screen_x1, screen_y0);
		XDrawLine (guiSP->displaySP,
			   guiSP->main_hidden_pixmapID,
			   guiSP->theGCA[3],
			   frame_screen_x0, screen_y0 + 1,
			   frame_screen_x1, screen_y0 + 1);
		}
	else
		{
		XDrawLine (guiSP->displaySP,
			   guiSP->main_hidden_pixmapID,
			   guiSP->theGCA[2],
			   frame_screen_x0, screen_y0,
			   frame_screen_x1, screen_y0);
		}
	}

/* Thick outer frame: */
XDrawRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[2],
		frame_screen_x0, frame_screen_y0,
		(unsigned int) (frame_screen_width - 1),
		(unsigned int) (frame_screen_height - 1));
XDrawLine (guiSP->displaySP,
	   guiSP->main_hidden_pixmapID,
	   guiSP->theGCA[1],
	   frame_screen_x0 - 1, frame_screen_y0 - 1,
	   frame_screen_x0 - 1, frame_screen_y1 + 1);
XDrawLine (guiSP->displaySP,
	   guiSP->main_hidden_pixmapID,
	   guiSP->theGCA[1],
	   frame_screen_x0 - 1, frame_screen_y0 - 1,
	   frame_screen_x1 + 1, frame_screen_y0 - 1);
XDrawLine (guiSP->displaySP,
           guiSP->main_hidden_pixmapID,
           guiSP->theGCA[1],
           frame_screen_x1 - 1, frame_screen_y0 + 1,
           frame_screen_x1 - 1, frame_screen_y1 - 1);
XDrawLine (guiSP->displaySP,
           guiSP->main_hidden_pixmapID,
           guiSP->theGCA[1],
           frame_screen_x0 + 1, frame_screen_y1 - 1,
           frame_screen_x1 - 1, frame_screen_y1 - 1);
XDrawLine (guiSP->displaySP,
	   guiSP->main_hidden_pixmapID,
	   guiSP->theGCA[3],
	   frame_screen_x0 + 1, frame_screen_y0 + 1,
	   frame_screen_x0 + 1, frame_screen_y1 - 1);
XDrawLine (guiSP->displaySP,
	   guiSP->main_hidden_pixmapID,
	   guiSP->theGCA[3],
	   frame_screen_x0 + 1, frame_screen_y0 + 1,
	   frame_screen_x1 - 1, frame_screen_y0 + 1);
XDrawLine (guiSP->displaySP,
	   guiSP->main_hidden_pixmapID,
	   guiSP->theGCA[3],
	   frame_screen_x1 + 1, frame_screen_y0 - 1,
	   frame_screen_x1 + 1, frame_screen_y1 + 1);
XDrawLine (guiSP->displaySP,
	   guiSP->main_hidden_pixmapID,
	   guiSP->theGCA[3],
	   frame_screen_x0 - 1, frame_screen_y1 + 1,
	   frame_screen_x1 + 1, frame_screen_y1 + 1);

/* Thick inner frame: */
screen_x0 = (frame_screen_x0 + frame_screen_x1) / 2;
XDrawLine (guiSP->displaySP,
	   guiSP->main_hidden_pixmapID,
	   guiSP->theGCA[2],
	   screen_x0, frame_screen_y0,
	   screen_x0, frame_screen_y1);
XDrawLine (guiSP->displaySP,
           guiSP->main_hidden_pixmapID,
           guiSP->theGCA[1],
           screen_x0 - 1, frame_screen_y0 + 1,
           screen_x0 - 1, frame_screen_y1 - 1);
XDrawLine (guiSP->displaySP,
           guiSP->main_hidden_pixmapID,
           guiSP->theGCA[3],
           screen_x0 + 1, frame_screen_y0 + 1,
           screen_x0 + 1, frame_screen_y1 - 1);
screen_y0 = (frame_screen_y0 + frame_screen_y1) / 2;
XDrawLine (guiSP->displaySP,
	   guiSP->main_hidden_pixmapID,
	   guiSP->theGCA[2],
	   frame_screen_x0, screen_y0,
	   frame_screen_x1, screen_y0);
XDrawLine (guiSP->displaySP,
	   guiSP->main_hidden_pixmapID,
	   guiSP->theGCA[1],
	   frame_screen_x0 + 1, screen_y0 - 1,
	   frame_screen_x1 - 1, screen_y0 - 1);
XDrawLine (guiSP->displaySP,
	   guiSP->main_hidden_pixmapID,
	   guiSP->theGCA[3],
	   frame_screen_x0 + 1, screen_y0 + 1,
	   frame_screen_x1 - 1, screen_y0 + 1);
XDrawLine (guiSP->displaySP,
           guiSP->main_hidden_pixmapID,
           guiSP->theGCA[2],
           screen_x0, screen_y0 - 1,
           screen_x0, screen_y0 + 1);

/* Draw labels: */
XSetForeground (guiSP->displaySP, guiSP->theGCA[0],
		guiSP->main_winS.fg_colorID);
strcpy (stringA, "abscisa: phi     ordinate: psi");
string_length = strlen (stringA);
screen_x0 = (frame_screen_x0 + frame_screen_x1) / 2 -
	     XTextWidth (guiSP->main_winS.fontSP, stringA, string_length) / 2;
screen_y0 = frame_screen_y0 - guiSP->main_winS.fontSP->descent - 1;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0, stringA, string_length);

screen_y0 = frame_screen_y1 + guiSP->main_winS.text_line_height -
	    guiSP->main_winS.text_line_height / 5;
screen_x0 = frame_screen_x0 + 10;
strcpy (stringA, "trans");
string_length = strlen (stringA);
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0, stringA, string_length);
screen_x0 = frame_screen_x0 + (frame_screen_x1 - frame_screen_x0) / 4 + 10;
strcpy (stringA, "cis");
string_length = strlen (stringA);
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0, stringA, string_length);
screen_x0 = frame_screen_x0 + 2 * (frame_screen_x1 - frame_screen_x0) / 4 + 10;
strcpy (stringA, "bad / amino terminus");
string_length = strlen (stringA);
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0, stringA, string_length);

strcpy (stringA, "0");
string_length = strlen (stringA);
screen_x0 = (frame_screen_x0 + frame_screen_x1) / 2 + 3;
screen_y0 = (frame_screen_y0 + frame_screen_y1) / 2 - 3;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0, stringA, string_length);

strcpy (stringA, "-180");
string_length = strlen (stringA);
screen_x0 = frame_screen_x0 + 3;
screen_y0 = (frame_screen_y0 + frame_screen_y1) / 2 - 3;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0, stringA, string_length);
screen_x0 = (frame_screen_x0 + frame_screen_x1) / 2 + 3;
screen_y0 = frame_screen_y1 - 3;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0, stringA, string_length);

strcpy (stringA, "-90");
string_length = strlen (stringA);
screen_x0 = frame_screen_x0 + (frame_screen_x1 - frame_screen_x0) / 4 + 3;
screen_y0 = (frame_screen_y0 + frame_screen_y1) / 2 - 3;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0, stringA, string_length);
screen_x0 = (frame_screen_x0 + frame_screen_x1) / 2 + 3;
screen_y0 = frame_screen_y0 + 3 * (frame_screen_y1 - frame_screen_y0) / 4 - 3;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0, stringA, string_length);

strcpy (stringA, "90");
string_length = strlen (stringA);
screen_x0 = frame_screen_x0 + 3 * (frame_screen_x1 - frame_screen_x0) / 4 + 3;
screen_y0 = (frame_screen_y0 + frame_screen_y1) / 2 - 3;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0, stringA, string_length);
screen_x0 = (frame_screen_x0 + frame_screen_x1) / 2 + 3;
screen_y0 = frame_screen_y0 + (frame_screen_y1 - frame_screen_y0) / 4 - 3;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0, stringA, string_length);

strcpy (stringA, "180");
string_length = strlen (stringA);
screen_x0 = frame_screen_x1 -
	    XTextWidth (guiSP->main_winS.fontSP, stringA, string_length) - 3;
screen_y0 = (frame_screen_y0 + frame_screen_y1) / 2 - 3;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0, stringA, string_length);
screen_x0 = (frame_screen_x0 + frame_screen_x1) / 2 + 3;
screen_y0 = frame_screen_y0 + guiSP->main_winS.text_line_height -
	    guiSP->main_winS.text_line_height / 5;
XDrawString (guiSP->displaySP,
	     guiSP->main_hidden_pixmapID,
	     guiSP->theGCA[0],
	     screen_x0, screen_y0, stringA, string_length);

}

/*===========================================================================*/


