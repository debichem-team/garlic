/* Copyright (C) 2001-2003 Damir Zucic */

/*=============================================================================

				init_membrane.c

Purpose:
	Initialize the membrane thickness and  some other data required
	to draw two planes which are used to represent the membrane. It
	is done for all proteins,  though makes sense only for membrane
	proteins.

Input:
	(1) Pointer to MolComplexS structure, with macromolecular data.
	(2) Pointer to ConfigS structure, with configuration data.
	(3) Pointer to GUIS structure, with GUI data.

Output:
	(1) Plane colors and  some other data set for  two planes which
	    are used to represent the membrane.
	(2) Return value.

Return value:
	(1) Positive always (trivial).

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		ParseColor_ (RGBS *, unsigned long *, GUIS *, char *, char *);

/*======initialize some membrane data:=======================================*/

int InitializeMembrane_ (MolComplexS *mol_complexSP,
			 ConfigS *configSP, GUIS *guiSP)
{

/* Initialize the membrane thickness: */
mol_complexSP->membraneS.thickness = MEMBRANE_THICKNESS;

/* Initialize the membrane radius: */
mol_complexSP->membraneS.plane1S.circle_radius = 1.5 * MEMBRANE_THICKNESS;
mol_complexSP->membraneS.plane2S.circle_radius = 1.5 * MEMBRANE_THICKNESS;

/* Initialize the membrane transparency: */
mol_complexSP->membraneS.plane1S.transparency = DEFAULT_TRANSP;
mol_complexSP->membraneS.plane2S.transparency = DEFAULT_TRANSP;

/* There is a special flag used to hide or show the membrane, so */
/* the flags associated with individual planes are not required. */
mol_complexSP->membraneS.plane1S.hiddenF = 0;
mol_complexSP->membraneS.plane2S.hiddenF = 0;

/* Colors for the first (top) plane: */

/* Outer side near color for the first plane: */
ParseColor_ (&mol_complexSP->membraneS.plane1S.top_near_rgbS,
	     &mol_complexSP->membraneS.plane1S.top_near_colorID,
	     guiSP, "RGB:0000/8888/FFFF", "white");

/* Outer side far color for the first plane: */
ParseColor_ (&mol_complexSP->membraneS.plane1S.top_far_rgbS,
	     &mol_complexSP->membraneS.plane1S.top_far_colorID,
	     guiSP, "RGB:0000/2222/4444", "black");

/* Inner side near color for the first plane: */
ParseColor_ (&mol_complexSP->membraneS.plane1S.bottom_near_rgbS,
	     &mol_complexSP->membraneS.plane1S.bottom_near_colorID,
	     guiSP, "RGB:DDDD/8888/8888", "white");

/* Inner side far color for the first plane: */
ParseColor_ (&mol_complexSP->membraneS.plane1S.bottom_far_rgbS,
	     &mol_complexSP->membraneS.plane1S.bottom_far_colorID,
	     guiSP, "RGB:3333/2222/2222", "black");

/* Colors for the second (bottom) plane: */

/* Inner side near color for the second plane: */
ParseColor_ (&mol_complexSP->membraneS.plane2S.top_near_rgbS,
	     &mol_complexSP->membraneS.plane2S.top_near_colorID,
	     guiSP, "RGB:0000/8888/FFFF", "white");

/* Inner side far color for the second plane: */
ParseColor_ (&mol_complexSP->membraneS.plane2S.top_far_rgbS,
	     &mol_complexSP->membraneS.plane2S.top_far_colorID,
	     guiSP, "RGB:0000/2222/4444", "black");

/* Outer side near color for the second plane: */
ParseColor_ (&mol_complexSP->membraneS.plane2S.bottom_near_rgbS,
	     &mol_complexSP->membraneS.plane2S.bottom_near_colorID,
	     guiSP, "RGB:DDDD/8888/8888", "white");

/* Outer side far color for the second plane: */
ParseColor_ (&mol_complexSP->membraneS.plane2S.bottom_far_rgbS,
	     &mol_complexSP->membraneS.plane2S.bottom_far_colorID,
	     guiSP, "RGB:3333/2222/2222", "black");

/* Initialize transparencies: */
mol_complexSP->membraneS.plane1S.transparency = DEFAULT_TRANSP;
mol_complexSP->membraneS.plane2S.transparency = DEFAULT_TRANSP;

/* If this point is reached, return positive value (success indicator): */
return 1;
}

/*===========================================================================*/


