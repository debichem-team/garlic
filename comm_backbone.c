/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				comm_backbone.c

Purpose:
	Execute backbone command.  Hide the selected atoms and make backbone
	visible.

Input:
	(1) Pointer to MolComplexS structure,  with macromol. complexes.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure, with some runtime data.
	(4) Pointer to ConfigS structure, with configuration data.
	(5) Pointer to GUIS structure, with GUI data.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
	(9) Pointer to  the remainder of  the command string.  It is  either
	    empty or contains the backbone thickness specification.

Output:
	(1) Backbone drawn.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

Notes:
	(1) It is important to initialize the value of  backbone_styleI with
	    a value different from zero!

	(2) The backbone_styleI may be used in two roles:  as the real style
	    index and as the signal that  backbone should be  hidden and all
	    selected atoms shown. In the later case, the value is set to -1.

	(3) The term  "backbone element"  refers to a pair of bonds assigned
	    to the given CA atom.  The entire backbone is  a set of backbone
	    elements.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
int		ShowNoRefresh_ (MolComplexS *, int);
int		HideBackbone_ (MolComplexS *, int);
int		HideNoRefresh_ (MolComplexS *, int);
int		ShowBackbone_ (MolComplexS *, int);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute backbone command:============================================*/

int CommandBackbone_ (MolComplexS *mol_complexSP, int mol_complexesN,
		      RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
		      NearestAtomS *nearest_atomSP, size_t pixelsN,
		      unsigned int *refreshIP, char *stringP)
{
char		*remainderP;
char		tokenA[STRINGSIZE];
int		backbone_styleI;
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;
size_t		c_alphaI, c_alphaN;
BackboneS	*curr_backboneSP;
AtomS		*curr_atomSP;

/* Initialize the backbone_styleI: */
backbone_styleI = configSP->default_backbone_styleI;

/* Extract the first token: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, stringP, " \t\n");

/* If available, the first token should contain the backbone drawing style: */
if (remainderP)
	{
	/** Extract the backbone drawing style: **/
	if 	(strstr (tokenA, "OFF") == tokenA) backbone_styleI = -1;
	else if (strcmp (tokenA,   "0") == 0)      backbone_styleI =  0;
	else if (strcmp (tokenA,   "1") == 0)      backbone_styleI =  1;
	else if (strcmp (tokenA,   "2") == 0)      backbone_styleI =  2;
	else if (strcmp (tokenA,   "3") == 0)      backbone_styleI =  3;
	else if (strcmp (tokenA,   "4") == 0)      backbone_styleI =  4;
	else if (strcmp (tokenA,   "5") == 0)      backbone_styleI =  5;
	else
		{
		sprintf (runtimeSP->messageA,
			 "Backbone drawing style \"%s\" not available!",
			 tokenA);
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_BACKBONE;
		}
	}

/* Set the backbone  drawing style for each */
/* selected CA atom in each cought complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/** Pointer to the current macromolecular complex: **/
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/** Check is the current macromol. complex caught: **/
	if (curr_mol_complexSP->catchF == 0) continue;

	/** Number of CA atoms in the current macromolecular complex: **/
	c_alphaN = curr_mol_complexSP->c_alphaN;

	/** Set backbone drawing style: **/
	for (c_alphaI = 0; c_alphaI < c_alphaN; c_alphaI++)
		{
		/*** Pointer to the current BackboneS structure: ***/
		curr_backboneSP = curr_mol_complexSP->backboneSP + c_alphaI;

		/*** Pointer to the current CA atom: ***/
		curr_atomSP = curr_mol_complexSP->atomSP +
			      curr_backboneSP->c_alphaI;

		/*** If this CA atom is not selected, skip it: ***/
		if (curr_atomSP->selectedF == 0) continue;

		/*** Copy style: ***/
		curr_backboneSP->backbone_styleI = backbone_styleI;
		}
	}

/* Hide backbone (if requested): */
if (backbone_styleI == -1)
	{
	/** Show all selected atoms but do not refresh graphics: **/
	ShowNoRefresh_ (mol_complexSP, mol_complexesN);

	/** Hide backbone (update hiddenF for each backbone element): **/
	HideBackbone_ (mol_complexSP, mol_complexesN);
	}

/* Show backbone or change backbone style (if requested): */
else
	{
	/** Hide all selected atoms but do not refresh graphics: **/
	HideNoRefresh_ (mol_complexSP, mol_complexesN);

	/** Show backbone (update hiddenF for each backbone element): **/
	ShowBackbone_ (mol_complexSP, mol_complexesN);
	}

/*---------------------------------------------------------------------------*/

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN, runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return positive value on success: */
return COMMAND_BACKBONE;
}

/*===========================================================================*/


