/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

				event_loop.c

Purpose:
	Take and process events.

Input:
	(1) Pointer to the array of MolComplexS structures.
	(2) Pointer to the number of macromolecular complexes.
	(3) Pointer to the next macromolecular complex identifier.
	(4) Pointer to pointer to the array of NearestAtomS structures.
	(5) Pointer to the number of pixels in the main window free area.
	(6) Pointer to ConfigS structure, with configuration data.
	(7) Pointer to GUIS structure,  with GUI data and the rest of command
	    line options.
	(8) Events (from event queue).
	(9) Molecular coordinates read from one or more files, on request.

Output:
	(1) Graphics, text and other useful things.
	(2) Array pointed at by  MolComplexS  structure pointer may be filled
	    with data.
	(3) Number of  macromolecular  complexes  updated,  if  at least  one
	    complex is loaded at runtime.
	(4) Return value.

Return value:
	(1) Positive if everything worked fine.
	(2) Negative if something failed.

Notes:
	(1) When a new macromolecular complex is loaded, *next_mol_complexIDP
	    should be used as complex identifier. The m. complex index should
	    not be used,  because  one of  the previously initialized complex
	    identifiers may  have the  value equal to  current_complexI. This
	    may happen if one or more objects are discarded from memory.

	(2) The refreshI index is updated  before refreshing the main window.
	    The lowest value of refreshI should be one.  Never reset to zero!
	    The value zero is reserved as the initial value for last_refreshI
	    member of NearestAtomS structure. Pixels which have last_refreshI
	    value set to zero are free for drawing; there is no need to check
	    the z coordinate associated with  this pixel.  Otherwise,  if the
	    last_refreshI is equal to the current refreshI,  the z coordinate
	    of a given atom  should be compared  with a z value stored in the
	    NearestAtomS  structure  associated  with  a given  pixel.  It is
	    assumed that refreshI is updated no more than  once in each event
	    handling function.  If this is  not the case,  the care should be
	    taken to avoid refreshI overflow.

	(3) A single action may result in several  Expose events.  The server
	    guarantees that such  Expose  events are  sent contiguously.  The
	    last event has the  xexpose.count  equal to zero. In this program
	    only the last Expose event is processed.

	(4) Note the unusual indentation of while (1) loop.

	(5) The array of NearestAtomS structures may be reallocated, so it is
	    neccesary to use  pointer to pointer.  The array is  freed later.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		InitRuntime_ (RuntimeS *, ConfigS *);
void		InitNearest_ (NearestAtomS *, size_t);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);
int		InputRefresh_ (GUIS *, RuntimeS *);
int		DockingRefresh_ (RuntimeS *, GUIS *);
int		KeyPress_ (MolComplexS *, int *, int *,
			   RuntimeS *, ConfigS *, GUIS *,
			   NearestAtomS **, size_t *, unsigned int *,
			   XKeyEvent *);
int		KeyRelease_ (GUIS *, XKeyEvent *);
int		MotionNotify_ (MolComplexS *, int,
			       RuntimeS *, ConfigS *, GUIS *,
			       NearestAtomS *, size_t, unsigned int,
			       XMotionEvent *);
int		DockingResize_ (RuntimeS *, GUIS *, XConfigureEvent *);
int		MainResize_ (NearestAtomS **, size_t *,
			     MolComplexS *, int,
			     ConfigS *, GUIS *,
			     XConfigureEvent *);
int		FreeRuntime_ (RuntimeS *);
int		ButtonPress_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int *,
			      XButtonEvent *);

/*======do the remaining initializations; take and process events:===========*/

int EventLoop_ (MolComplexS *mol_complexSP, int *mol_complexesNP,
		int *next_mol_complexIDP,
		NearestAtomS **nearest_atomSPP, size_t *pixelsNP,
		ConfigS *configSP, GUIS *guiSP)
{
static XEvent			eventS;
static unsigned int		refreshI = 1;
static XClientMessageEvent	*client_eventSP;
static RuntimeS			runtimeS;
static int			command_code;

/* Initialize memory for some runtime data and do some initializations: */
if (InitRuntime_ (&runtimeS, configSP) < 0) return -1;

/* The event loop: */
while (1)
{

/* Take the next event: */
XNextEvent (guiSP->displaySP, &eventS);

/* Check the current refreshI value; reset it and initialize the */
/* NearestAtomS array  if maximal value  is reached or exceeded: */
if (refreshI >= MAXREFRESHI)
	{
	InitNearest_ (*nearest_atomSPP, *pixelsNP);
	refreshI = 1;
	}

/* Handle the event: */
switch (eventS.type)
	{

/*---------------------------------------------------------------------------*/

	case Expose:

		/* A single action may result in several  Expose */
		/* events. Only the last one is taken seriously: */
		if (eventS.xexpose.count != 0) continue;

		/* Update refresh index before use: */
		refreshI++;

		/* Refreshing the main window: */
		MainRefresh_ (mol_complexSP, *mol_complexesNP,
			      &runtimeS, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, refreshI);

		/* Refresh the control window: */
		ControlRefresh_ (mol_complexSP + runtimeS.default_complexI,
				 configSP, guiSP);

		/* Refresh the input window: */
		InputRefresh_ (guiSP, &runtimeS);

		/* Refresh docking window, if docking is switched on: */
		if (guiSP->dockingF) DockingRefresh_ (&runtimeS, guiSP);

		/* Flush the output buffer: */
		XFlush (guiSP->displaySP);

		/* Reset modifier flags: */
		guiSP->shift_pressedF = 0;
		guiSP->control_pressedF = 0;
		guiSP->alt_pressedF = 0;

		/* End of case Expose: */
		break;

/*---------------------------------------------------------------------------*/

	case KeyPress:

		/* Handle KeyPress_ event: */
		command_code = KeyPress_ (mol_complexSP, mol_complexesNP,
					  next_mol_complexIDP,
					  &runtimeS,
					  configSP, guiSP,
					  nearest_atomSPP, pixelsNP,
					  &refreshI,
					  &eventS.xkey);

		/* Flush the output buffer: */
		XFlush (guiSP->displaySP);

		/* Break from event loop if quit command issued: */
		if (command_code == COMMAND_QUIT)
			{
			/** Free memory used for runtime data: **/
			FreeRuntime_ (&runtimeS);

			/** Return to caller: **/
			return 1;
			}

		/* End of case KeyPress: */
		break;

/*---------------------------------------------------------------------------*/

	case KeyRelease:

		KeyRelease_ (guiSP, &eventS.xkey);

		/* End of case KeyRelease: */
		break;

/*---------------------------------------------------------------------------*/

	case MotionNotify:

		MotionNotify_ (mol_complexSP, *mol_complexesNP,
			       &runtimeS, configSP, guiSP,
			       *nearest_atomSPP, *pixelsNP, refreshI,
			       &eventS.xmotion);

		/* End of case MotionNotify: */
		break;

/*---------------------------------------------------------------------------*/

	case ButtonPress:

		ButtonPress_ (mol_complexSP, *mol_complexesNP,
			      &runtimeS, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, &refreshI,
			      &eventS.xbutton);

		/* End of case ButtonPress: */
		break;

/*---------------------------------------------------------------------------*/

	case ButtonRelease:

		break;

/*---------------------------------------------------------------------------*/

	case ConfigureNotify:

		/* If event belongs to docking window: */
		if (eventS.xconfigure.window == guiSP->docking_winS.ID)
			{
			DockingResize_ (&runtimeS, guiSP, &eventS.xconfigure);
			}

		/* If event belongs to the main window: */
		else
			{
			MainResize_ (nearest_atomSPP, pixelsNP,
				     mol_complexSP, *mol_complexesNP, 
				     configSP, guiSP,
				     &eventS.xconfigure);
			}

		break;

/*---------------------------------------------------------------------------*/

	case ClientMessage:

		client_eventSP = (XClientMessageEvent *) &eventS;
		if ((client_eventSP->message_type == guiSP->protocols_atom) &&
	   	 (client_eventSP->data.l[0] ==
					(long) guiSP->delete_window_atom))
			{
			return 1;
			}
		break;

/*---------------------------------------------------------------------------*/

	default:

		/* Do nothing */
		;

/*---------------------------------------------------------------------------*/

	} /* End of switch (eventS.type) */

} /* End of while (1) loop */

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


