/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				hide_control.c

Purpose:
	Hide control and resize output window.

Input:
	(1) Pointer to GUIS structure.

Output:
	(1) Control window hidden.

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======hide control and resize output window:===============================*/

void HideControlWindow_ (GUIS *guiSP)
{
int		border_width = 1;

/* Hide the control window: */
XUnmapWindow (guiSP->displaySP, guiSP->control_winS.ID);

/* Move the output window to the new position: */
guiSP->output_winS.y0 = guiSP->control_winS.y0;
XMoveWindow (guiSP->displaySP, guiSP->output_winS.ID,
	     guiSP->output_winS.x0, guiSP->output_winS.y0);

/* Resize the output window: */
guiSP->output_winS.height = guiSP->main_winS.height - 2 * border_width - 2;
XResizeWindow (guiSP->displaySP, guiSP->output_winS.ID,
	       guiSP->output_winS.width, guiSP->output_winS.height);

}

/*===========================================================================*/


