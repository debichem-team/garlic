/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

				select_sequence.c

Purpose:
	Select  residue ranges  which are matching  the sequence from the
	sequence buffer.

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) Number of macromolecular complexes.
	(3) Pointer to RuntimeS structure (sequence buffer is there).
	(4) Selection mode index (0 = overwrite, 1 = restrict, 2 = expand
	    previous selection).

Output:
	(1) The flag  selectedF  set to one  for selected atoms  in every
	    caught macromolecular complex.
	(2) Return value.

Return value:
	(1) The number of selected atoms (zero or positive value).

Notes:
	(1) The sequence associated with the current macromol. complex is
	    treated as the sequence number 1, while the sequence from the
	    sequence buffer is treated as the sequence number 2.
	
	(2) Most users are likely to store just a short sequence fragment
	    into the sequence buffer  before using  the command  SEL SEQ.

	(3) The position_changedF is updated.

	(4) Use strncmp to compare residue names because residue names in
	    the sequence buffer are not zero terminated!

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		CompareSequences_ (MolComplexS *, size_t, size_t,
				   char *, size_t);

/*======select sequence:=====================================================*/

long SelectSequence_ (MolComplexS *mol_complexSP, int mol_complexesN,
		      RuntimeS *runtimeSP, int selection_modeI)
{
long		selected_atomsN = 0;
size_t		residues2N;
int		max_length;
char		*first_name2P;
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;
size_t		atomsN, atomI;
AtomS		*curr_atomSP;
size_t		residues1N, residue1I;
ResidueS	*first_residue1SP;
char		*first_name1P;
int		n;
ResidueS	*last_residue1SP;
size_t		atom_startI, atom_endI;
int		selectedF = 1;

/* Check is there anything in the sequence buffer: */
residues2N = runtimeSP->residuesN;
if (residues2N == 0) return (long) 0;

/* The maximal residue name length: */
max_length = RESNAMESIZE - 1;

/* Prepare the pointer to the name of the first residue from the sequence */
/* buffer.  Warning: this string is not zero terminated! Be very careful! */
first_name2P = runtimeSP->sequenceP;

/* Check every macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/* Pointer to the current macromolecular complex: */
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/* Check is the current macromolecular complex caught: */
	if (curr_mol_complexSP->catchF == 0) continue;

	/* Prepare and check the number of atoms in the current complex: */
	atomsN = curr_mol_complexSP->atomsN;
	if (atomsN == 0) continue;

	/* Compare  the number  of residues  in the current */
	/* macromolecular  complex  with the length  of the */
	/* sequence which is stored to the sequence buffer: */
	if (curr_mol_complexSP->residuesN < runtimeSP->residuesN) continue;

	/* Backup the current selection if selection mode is restrict: */
	else if (selection_modeI == 1)
		{
		for (atomI = 0; atomI < atomsN; atomI++)
			{
			/* Pointer to the current atom: */
			curr_atomSP = curr_mol_complexSP->atomSP + atomI;

			/* Copy the selection flag: */
			curr_atomSP->previous_selectedF =
						curr_atomSP->selectedF;
			}
		}

	/* Unselect everything if selection mode is overwrite or restrict: */
	if ((selection_modeI == 0) || (selection_modeI == 1))
		{
		for (atomI = 0; atomI < atomsN; atomI++)
			{
			/* Pointer to the current atom: */
			curr_atomSP = curr_mol_complexSP->atomSP + atomI;

			/* Unselect the current atom: */
			curr_atomSP->selectedF = 0;
			}
		}

	/* Scan the sequence associated with the current macromol. complex: */
	residues1N = curr_mol_complexSP->residuesN;
	for (residue1I = 0; residue1I < residues1N; residue1I++)
		{
		/* Pointer to the leading residue: */
		first_residue1SP = curr_mol_complexSP->residueSP + residue1I;

		/* Pointer to the first atom of the current residue: */
		curr_atomSP = curr_mol_complexSP->atomSP +
			      first_residue1SP->residue_startI;

		/* Pointer to the name of the leading residue: */
		first_name1P = curr_atomSP->raw_atomS.pure_residue_nameA;

		/* Compare the residue names: */
		if (strncmp (first_name1P, first_name2P, max_length) != 0)
			{
			continue;
			}

		/* If this point is reached, the residue names */
		/* from two sequences match; compare the rest: */
		n = CompareSequences_ (curr_mol_complexSP,
				       residue1I, residues1N,
				       first_name2P, residues2N);
		if (n < 0) continue;

		/* If this point is reached, the current seq. fragment */
		/* is equal to the sequence from  the sequence buffer. */

		/* Prepare the pointer to the last residue in the fragment: */
		last_residue1SP = curr_mol_complexSP->residueSP +
				  residue1I + residues2N;
		last_residue1SP--;

		/* Prepare the atomic start and end index: */
		atom_startI = first_residue1SP->residue_startI;
		atom_endI   = last_residue1SP->residue_endI;

		/* Apply selection to the atoms in */
		/* the current  sequence fragment: */
		for (atomI = atom_startI; atomI <= atom_endI; atomI++)
			{
			/* Check the atomic index: */
			if (atomI >= atomsN) break;

			/* Pointer to the current atom: */
			curr_atomSP = curr_mol_complexSP->atomSP + atomI;

			/* Set the selection flag for the current atom: */
			curr_atomSP->selectedF = selectedF;

			/* Check the selection flag; increase the count of */
			/* selected residues if this flag is equal to one: */
			if (curr_atomSP->selectedF) selected_atomsN++;
			}
		}

	/* Combine  the current selection  with the */
	/* previous, if selection mode is restrict: */
	if (selection_modeI == 1)
		{
		for (atomI = 0; atomI < atomsN; atomI++)
			{
			/* Pointer to the current atom: */
			curr_atomSP = curr_mol_complexSP->atomSP + atomI;

			/* Combine the old and the new selection flag: */
			curr_atomSP->selectedF *=
					 curr_atomSP->previous_selectedF;
			}
		}

	/* Update the position_changedF (some atoms may have bad color): */
	curr_mol_complexSP->position_changedF = 1;
	}

/* Return the number of selected atoms: */
return selected_atomsN;
}

/*===========================================================================*/


