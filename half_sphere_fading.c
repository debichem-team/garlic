/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				half_sphere_fading.c

Purpose:
	Prepare the left, middle and right color for each atom in a complex.
	The color fading is  semi-spherical:  two spherical surfaces and one
	plane are used.  The plane is parallel to  x-y plane;  it is passing
	through the fading center.
	

Input:
	(1) Pointer to MolComplexS structure.
	(2) Pointer to GUIS structure.

Output:
	(1) left_colorID, middle_colorID and right_colorID members of  AtomS
	    structure initialized for each atom in macromolecular complex.
	(2) Return value.

Return value:
	(1) On success, the number of atoms which have colors different from
	    near and far color.  These atoms are located in the area between
	    the front and back surface.
	(2) Zero on failure.

Notes:
	(1) Do not skip hidden atoms,  the color of these atoms  may be used
	    for backbone drawing!

=============================================================================*/

#include <stdio.h>
#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

unsigned long	PixelFromRGBS_ (RGBS *, GUIS *);
unsigned long	PrepareColor_ (RGBS *, RGBS *, double, GUIS *);

/*======semi-spherical fading:===============================================*/

size_t HalfSphereFading_ (MolComplexS *curr_mol_complexSP, GUIS *guiSP)
{
size_t			atoms_between_surfacesN = 0;
size_t			atomsN, atomI;
AtomS			*curr_atomSP;
int			surfacesN, surfaceI, inner_surfaceI, outer_surfaceI;
double			r_out, r_out_squared, r_in, r_in_squared;
double			x0, y0, z0;
double			overall_range, internal_range;
double			inverse_internal_range;
double			x, y, z, r_squared, r, delta_r, scale_factor;
int			near_surfaceI, far_surfaceI;

/* The radius of the front (outer) surface: */
r_out = curr_mol_complexSP->fading_front_relative_position;
r_out_squared = r_out * r_out;

/* The radius of the back (inner) surface: */
r_in = curr_mol_complexSP->fading_back_relative_position;
r_in_squared = r_in * r_in;

/* Check: */
if (r_in >= r_out) return 0;

/* The overall range: */
overall_range = r_out - r_in;

/* Fading center coordinates: */
x0 = curr_mol_complexSP->fading_center_vectorS.x;
y0 = curr_mol_complexSP->fading_center_vectorS.y;
z0 = curr_mol_complexSP->fading_center_vectorS.z;

/* The number of atoms in a complex: */
atomsN = curr_mol_complexSP->atomsN;

/* Assign three colors to each atom: */
for (atomI = 0; atomI < atomsN; atomI++)
	{
	/** Pointer to the current atom: **/
	curr_atomSP = curr_mol_complexSP->atomSP + atomI;

	/** Is atom out of slab? **/
	if (!curr_atomSP->inside_slabF) continue;

	/** Check the number of color fading surfaces: **/
	surfacesN = curr_atomSP->surfacesN;
	/*** If there is only one, there is no fading: ***/
	if (surfacesN == 1)
		{
		curr_atomSP->left_colorID   =
			PixelFromRGBS_ (curr_atomSP->left_rgbSA, guiSP);
		curr_atomSP->middle_colorID =
			PixelFromRGBS_ (curr_atomSP->middle_rgbSA, guiSP);
		curr_atomSP->right_colorID  =
			PixelFromRGBS_ (curr_atomSP->right_rgbSA, guiSP);
		continue;
		}

	/** Atomic coordinates (relative to fading center): **/
	x = curr_atomSP->raw_atomS.x[0] - x0;
	y = curr_atomSP->raw_atomS.y    - y0;
	z = curr_atomSP->raw_atomS.z[0] - z0;

	/** Check is atom behind the plane; if it is, treat it as far atom: **/
	if (z > 0.0)
		{
		surfaceI = surfacesN - 1;
		curr_atomSP->left_colorID   =
			PixelFromRGBS_ (curr_atomSP->left_rgbSA + surfaceI,
					guiSP);
		curr_atomSP->middle_colorID =
			PixelFromRGBS_ (curr_atomSP->middle_rgbSA + surfaceI,
					guiSP);
		curr_atomSP->right_colorID  =
			PixelFromRGBS_ (curr_atomSP->right_rgbSA + surfaceI,
					guiSP);
		continue;
		}

	/** Distance between the current atom and the fading center: **/
	r_squared = x * x + y * y + z * z;

	/** Atoms which are too far from the fading **/
	/** center should be treated as near atoms: **/
	if (r_squared >= r_out_squared)
		{
		curr_atomSP->left_colorID   =
			PixelFromRGBS_ (curr_atomSP->left_rgbSA, guiSP);
		curr_atomSP->middle_colorID =
			PixelFromRGBS_ (curr_atomSP->middle_rgbSA, guiSP);
		curr_atomSP->right_colorID  =
			PixelFromRGBS_ (curr_atomSP->right_rgbSA, guiSP);
		continue;
		}

	/** Atoms which are to close to the fading **/
	/** center should be treated as far atoms: **/
	if (r_squared <= r_in_squared)
		{
		surfaceI = surfacesN - 1;
		curr_atomSP->left_colorID   =
			PixelFromRGBS_ (curr_atomSP->left_rgbSA + surfaceI,
					guiSP);
		curr_atomSP->middle_colorID =
			PixelFromRGBS_ (curr_atomSP->middle_rgbSA + surfaceI,
					guiSP);
		curr_atomSP->right_colorID  =
			PixelFromRGBS_ (curr_atomSP->right_rgbSA + surfaceI,
					guiSP);
		continue;
		}

	/** If this point is reached, current atom is in **/
	/** the area between two surfaces; colors should **/
	/** be calculated weighting near and far colors: **/

	/** Prepare auxiliary variables: **/
	if (surfacesN > 1)
		{
		r = sqrt (r_squared);
		internal_range = overall_range / (double) (surfacesN - 1);
		inverse_internal_range = 1.0 / internal_range;
		inner_surfaceI = (int) floor ((r - r_in) *
				 inverse_internal_range);
		outer_surfaceI = inner_surfaceI + 1;
		delta_r = outer_surfaceI * internal_range + r_in - r;
		scale_factor = delta_r * inverse_internal_range;
		far_surfaceI = surfacesN - inner_surfaceI - 1;
		near_surfaceI = far_surfaceI - 1;
		}
	else
		{
		scale_factor = 0.0;
		far_surfaceI = 0;
		near_surfaceI = 0;
		}

	/** Left color: **/
	curr_atomSP->left_colorID =
		PrepareColor_ (curr_atomSP->left_rgbSA + near_surfaceI,
			       curr_atomSP->left_rgbSA + far_surfaceI,
			       scale_factor, guiSP);

	/** Middle color: **/
	curr_atomSP->middle_colorID =
		PrepareColor_ (curr_atomSP->middle_rgbSA + near_surfaceI,
			       curr_atomSP->middle_rgbSA + far_surfaceI,
			       scale_factor, guiSP);

	/** Right color: **/
	curr_atomSP->right_colorID =
		PrepareColor_ (curr_atomSP->right_rgbSA + near_surfaceI,
			       curr_atomSP->right_rgbSA + far_surfaceI,
			       scale_factor, guiSP);

	/** Update the number of atoms in the area between two surfaces: **/
	atoms_between_surfacesN++;
	}

/* Return  the  number  of atoms  which  have */
/* colors different from near and far colors: */
return atoms_between_surfacesN;
}

/*===========================================================================*/


