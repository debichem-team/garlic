/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

				is_polar.c

Purpose:
	Check is the specified residue polar. Check also is it hydrogen
	bond donor or acceptor.

Input:
	(1) Residue name (purified, i.e. without spaces).

Output:
	(1) Return value.

Return value:
	(1) Negative, if residue is apolar.
	(2) Zero, if residue is polar and hydrogen bond acceptor.
	(3) One, if residue is polar and hydrogen bond donor.
	(4) Two, if residue is both donor and acceptor or ASX or GLX.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

/*======check is the specified residue polar:================================*/

int IsPolar_ (char *pure_residue_nameP)
{
static char	polar_nameAA[13][4]  = {"GLU", "GLN", "HIS", "LYS", "TYR",
					"TRP", "THR", "SER", "ASP", "ASN",	
					"ARG", "ASX", "GLX"};
static int	donorI[13] =           {  0,     2,     1,     1,     1,
					  1,     0,     0,     0,     2,
					  1,     2,     2};
int		i;

/* Compare only the first three characters: */
for (i = 0; i < 13; i++)
	{
	if (!strcmp (polar_nameAA[i], pure_residue_nameP)) return donorI[i];
	}

/* If this point is reached, the specified residue is apolar: */
return -1;
}

/*===========================================================================*/


