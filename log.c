/* Copyright (C) 2000-2004 Damir Zucic */

/*=============================================================================

				log.c

Purpose:
	Execute log command:  open the specified  log file  for writing.
	If some  log file  was opened  before,  close it  and create new
	file. First try to create file in the current working directory;
	if the first attempt fails, try to create log file in users home
	directory.

Input:
	(1) Pointer to ConfigS structure, with configuration data.
	(2) Pointer to RuntimeS structure, with some runtime data.

Output:
	(1) Log file created.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);

/*======execute log command:=================================================*/

int Log_ (ConfigS *configSP, RuntimeS *runtimeSP)
{
char		*remainderP;
char		tokenA[STRINGSIZE];
char		token_copyA[STRINGSIZE];
char		*P;
int		n;
static char	*env_valueP;
char		path_nameA[STRINGSIZE];

/* Skip the first token: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE,
			    runtimeSP->curr_commandA, " \t\n");
if (!remainderP) return ERROR_LOG;

/* The second token should contain the file name: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, " \t\n");
if (!remainderP)
	{
	strcpy (runtimeSP->messageA, "Log file not specified!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_NO_FILE_NAME;
	}

/* If the second token is keyword OFF,  close the */
/* previous log file if opened at all and return: */
strncpy (token_copyA, tokenA, STRINGSIZE - 1);
token_copyA[STRINGSIZE - 1] = '\0';
P = token_copyA;
while ((n = *P++) != '\0') *(P - 1) = toupper (n);
if (strstr (token_copyA, "OFF") == token_copyA)
	{
	fclose (configSP->log_fileP);
	configSP->log_fileF = 0;
	configSP->log_fileP = NULL;
	configSP->log_file_nameA[0] = '\0';
	return COMMAND_LOG;
	}

/* Some log file may be in use already; close it and reset associated data: */
if (configSP->log_fileF)
	{
	fclose (configSP->log_fileP);
	configSP->log_fileF = 0;
	configSP->log_fileP = NULL;
	configSP->log_file_nameA[0] = '\0';
	}

/* Open the fresh log file: */
configSP->log_fileP = fopen (tokenA, "w");

/* If the first attempt failed, try with users home directory: */
if (configSP->log_fileP == NULL)
	{
	/** Prepare the file name pointer: **/
	P = tokenA;
	if (*P == '/') P++;

	/** The second attempt (using environment variable HOME): **/
	if ((env_valueP = getenv ("HOME")) != NULL)
		{
		/*** Copy the value of the environment variable HOME: ***/
		strncpy (path_nameA, env_valueP, STRINGSIZE - 1);
		path_nameA[STRINGSIZE - 1] = '\0';

		/*** The last character should be slash: ***/
		n = (int) strlen (path_nameA) - 1;
		if (path_nameA[n] != '/') strcat (path_nameA, "/");

		/*** Concatename the file name to directory name: ***/
		n = STRINGSIZE - (int) strlen (path_nameA) - 1;
		strncat (path_nameA, P, n);
		path_nameA[STRINGSIZE - 1] = '\0';

		/*** The second attempt to open file: ***/
		configSP->log_fileP = fopen (path_nameA, "w");
		}
	}

if (configSP->log_fileP == NULL)
	{
	sprintf (runtimeSP->messageA, "Unable to open log file %s!", tokenA);
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_OPEN_FAILURE;
	}

/* If this point is reached, log file is ready for use: */
strncpy (configSP->log_file_nameA, tokenA, STRINGSIZE - 1);
configSP->log_file_nameA[STRINGSIZE - 1] = '\0';
configSP->log_fileF = 1;

/* Return positive value on success: */
return COMMAND_LOG;
}

/*===========================================================================*/


