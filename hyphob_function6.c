/* Copyright (C) 2006 Damir Zucic */

/*=============================================================================

			    hyphob_function6.c

Purpose:
	Draw the hydrophobicity function F6.  The sequence stored to the
	main sequence buffer  is used  to calculate  the function value.

Input:
	(1) Pointer to the storage where the minimal function value will
	    be stored.
	(2) Pointer to the storage where the maximal function value will
	    be stored.
	(3) Pointer to RuntimeS structure.

Output:
	(1) Function F6 calculated and stored.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

Notes:
	(1) The function  F6 may be modified and used for many purposes.
	    Originally, it was introduced while searching for the method
	    which will be suitable for prediction of the porin secondary
	    structure.

========includes:============================================================*/

#include <stdio.h>

#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		HyphobFunction1_ (double *, double *, RuntimeS *);
int		HyphobFunction2_ (double *, double *, RuntimeS *);
int		HyphobFunction3_ (double *, double *, RuntimeS *);
int		HyphobFunction4_ (double *, double *, RuntimeS *);
void		InitHyphob_ (RuntimeS *);

/*======calculate the hydrophobicity function F6:============================*/

int HyphobFunction6_ (double *f6minP, double *f6maxP, RuntimeS *runtimeSP)
{
int		residuesN, residueI;   /* Do not use size_t instead of int ! */
int		max_length;
char		*residue_nameP;
char		residue_nameA[RESNAMESIZE];
double		f1, f1min, f1max, f1after;
double		f2, f2min, f2max, f2after;
double		f3, f3min, f3max, f3after;
double		f4, f4min, f4max;
double		f6;
int		windowI, combinedI;
double		left_value, central_value, right_value;
double		weightA[5] = {0.20, 0.40, 0.60, 0.80, 1.00};
double		average_value;

/*------prepare some parameters:---------------------------------------------*/

/* The number of residues in the sequence buffer: */
residuesN = (int) runtimeSP->residuesN;
if (residuesN == 0) return -1;

/* The maximal residue name length: */
max_length = RESNAMESIZE - 1;

/*------initialize (reset) F6:-----------------------------------------------*/

/* Initialize F6: */
for (residueI = 0; residueI < residuesN; residueI++)
        {
        /* Reset the function F6, it might be initialized before: */
        *(runtimeSP->function6P + residueI) = 0.0;
        }

/*------calculate the function F1:-------------------------------------------*/

/* Calculate the function F1: */
HyphobFunction1_ (&f1min, &f1max, runtimeSP);

/*------calculate the function F2:-------------------------------------------*/

/* Calculate the function F2: */
HyphobFunction2_ (&f2min, &f2max, runtimeSP);

/*------calculate the function F3:-------------------------------------------*/

/* Calculate the function F3: */
HyphobFunction3_ (&f3min, &f3max, runtimeSP);

/*------calculate the function F4:-------------------------------------------*/

/* Calculate the function F4: */
HyphobFunction4_ (&f4min, &f4max, runtimeSP);

/*------calculate the function F6:-------------------------------------------*/

/* Initialize the extreme values: */
*f6minP = +999999.0;
*f6maxP = -999999.0;

/* Scan the sequence and calculate F6: */
for (residueI = 0; residueI < residuesN; residueI++)
	{
	/* Prepare the pointer to the residue name: */
	residue_nameP = runtimeSP->sequenceP + residueI * max_length;

	/* Copy the residue name: */
	strncpy (residue_nameA, residue_nameP, max_length);
	residue_nameA[max_length] = '\0';

	/* Initialize the function value: */
	*(runtimeSP->function6P + residueI) = 0.0;

	/* Skip 7 residues (a single transmembrane strand) at both edges: */
	if ((residueI < 7) || (residueI > residuesN - 7)) continue;

	/* Reset default F6 value: */
	f6 = 0.0;

	/* 20060607.1309: */
	/* Copy and use the values of  F1,  F2, */
	/* F3 and F4. Note the shifted indices! */
	f1      = *(runtimeSP->function1P + residueI - 1);
	f1after = *(runtimeSP->function1P + residueI + 5);
	f2      = *(runtimeSP->function2P + residueI - 1);
	f2after = *(runtimeSP->function2P + residueI + 5);
	f3after = *(runtimeSP->function3P + residueI + 7);
	f3      = *(runtimeSP->function3P + residueI);
	f4      = *(runtimeSP->function4P + residueI);

	/* Apply the cryteria for nonzero peaks: */
	if (f1 - 3.0 >= -0.001)
		{
		f6 = -1.0 * (f3after + f2 - f2after);
		if (f6 > 0.0) f6 = 0.0;
		}

	/* Scale and store the function value: */
	
	*(runtimeSP->function6P + residueI) = 0.2 * f6;
	}

/*------fill single-residue gaps in F6:--------------------------------------*/

/* Scan the sequence and check are there any positions where the value of */
/* F6 is zero, while it is different from zero at two adjacent positions. */
/* Fill such gaps, using the average value for two neighboring positions. */
for (residueI = 1; residueI < residuesN - 1; residueI++)
	{
	/* Prepare three values of F6: */
	left_value    = *(runtimeSP->function6P + residueI - 1);
	central_value = *(runtimeSP->function6P + residueI);
	right_value   = *(runtimeSP->function6P + residueI + 1);

	/* Fill the gap in F6: */
	if ((fabs (central_value) < 0.001) &&
	    (fabs (left_value) > 0.001) && (fabs (right_value) > 0.001))
		{
		central_value = 0.5 * (left_value + right_value);
		*(runtimeSP->function6P + residueI) = central_value;
		}
	}

/*------calculate the weighted average F6 value, over 5 residues:------------*/

/* Copy the old values into auxiliary buffer: */
for (residueI = 0; residueI < residuesN; residueI++)
	{
	*(runtimeSP->aux_doubleP + residueI) =
				*(runtimeSP->function6P + residueI);
	}

/* Scan the sequence, omitting leading four residues: */
for (residueI = 4; residueI < residuesN; residueI++)
	{
	/* Reset the average value: */
	average_value = 0.0;

	/* Scan the sliding window: */
	for (windowI = 0; windowI < 5; windowI++)
		{
		/* Prepare and check the combined index: */
		combinedI = residueI + windowI - 4;
		if (combinedI < 0) continue;
		if (combinedI >= residuesN) continue;

		/* Copy the function value: */
		f6 = *(runtimeSP->aux_doubleP + combinedI);

		/* Add the weighted function value to the average value: */
		average_value += weightA[windowI] * f6;
		}

	/* Calculate and store the average value of F6: */
	average_value /= 3.0;		 /* Divide by 3 */
	*(runtimeSP->function6P + residueI) = average_value;
	}

/*------find the extreme values of F6:---------------------------------------*/

/* Find the extreme values of F6: */
for (residueI = 0; residueI < residuesN; residueI++)
	{
	/* Find extreme values for F6: */
	f6 = *(runtimeSP->function6P + residueI);
	if (f6 < *f6minP) *f6minP = f6;
	if (f6 > *f6maxP) *f6maxP = f6;
	}

/*---------------------------------------------------------------------------*/

return 1;
}

/*===========================================================================*/


