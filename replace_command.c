/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				replace_command.c

Purpose:
	Replace the current command with the one from the history buffer.
	If there is nothing in the history buffer, return negative value.
	The history buffer is empty if the  highest_commandI  is equal to
	minus one.

Input:
	(1) Pointer to RuntimeS structure.
	(2) The relative index (index difference).
	(3) Pointer to GUIS structure.

Output:
	(1) The current command may be replaced with the old one.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

Notes:
	(1) After the highest index value is reached, the current command
	    will be replaced by the first one. 	After the lowest index is
	    reached (zero),  the current command will be  replaced by the
	    the command which is specified by the highest_commandI index.
	    The current command string  is replaced immediately after the
	    arrow key "Up" or "Down" is pressed.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======replace command:=====================================================*/

int ReplaceCommand_ (RuntimeS *runtimeSP, int deltaI, GUIS *guiSP)
{
char			*oldP, *newP;
int			text_width, max_width;

/* Check is  there anything in  the history buffer. */
/* The initial value of the highest_commandI is -1. */
if (runtimeSP->highest_commandI < 0) return -1;

/* If  this point  is reached,  there is at */
/* least one command in the history buffer. */

/* Prepare and check the index of the old (previously used) command: */
runtimeSP->old_commandI += deltaI;
if (runtimeSP->old_commandI < 0)    /* Read note 2! */
	runtimeSP->old_commandI = runtimeSP->highest_commandI;
if (runtimeSP->old_commandI > runtimeSP->highest_commandI)
	runtimeSP->old_commandI = 0;

/* Copy the old command: */
oldP = runtimeSP->commandsP + runtimeSP->old_commandI * COMMSTRINGSIZE;
strncpy (runtimeSP->curr_commandA, oldP, COMMSTRINGSIZE - 1);
runtimeSP->curr_commandA[COMMSTRINGSIZE - 1] = '\0';
newP = runtimeSP->commandsP + runtimeSP->next_commandI * COMMSTRINGSIZE;
strncpy (newP, oldP, COMMSTRINGSIZE - 1);
*(newP + COMMSTRINGSIZE - 1) = '\0';

/* Update the current command length and carriage position: */
runtimeSP->command_length = strlen (runtimeSP->curr_commandA);
runtimeSP->carriage_position = runtimeSP->command_length;

/* Prepare, check and store the width (in pixels) of the command string: */

/** Prepare the width: **/
text_width = XTextWidth (guiSP->main_winS.fontSP,
			 runtimeSP->curr_commandA,
			 runtimeSP->carriage_position);

/** Check is it larger than output window: **/
max_width = (int) guiSP->input_winS.width - 3;
if (text_width > max_width)
	{
	text_width = 0;
	runtimeSP->carriage_position = 0;
	text_width = 0;
	}

/** Store the width: **/
runtimeSP->left_part_widthA[runtimeSP->carriage_position] = text_width;

/* Return positive value: */
return 1;
}

/*===========================================================================*/


