/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				init_buttons.c

Purpose:
	Initialize dummy buttons in control window, i.e. define edge
	coordinates for all windows.

Input:
	(1) Pointer to ButtonS structure.

Output:
	(1) ButtonS array of structures initialized.
	(2) Return value.

Return value:
	(1) Positive always (trivial).

Notes:
	(1) Indices are assigned as follows:
	     0 = KP_0
	     1 = KP_1
	     2 = KP_2
	     3 = KP_3
	     4 = KP_4
	     5 = KP_5
	     6 = KP_6
	     7 = KP_7
	     8 = KP_8
	     9 = KP_9
	    10 = KP_Decimal
	    11 = KP_Enter
	    12 = KP_Add
	    13 = KP_Subtract
	    14 = KP_Multiply
	    15 = KP_Divide
	    16 = F1
	    17 = F2
	    18 = F3
	    19 = F4

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======initialize dummy buttons:============================================*/

int InitializeButtons_ (ButtonS *buttonSP)
{
ButtonS		*curr_buttonSP;

/* [0] KP_Zero: */
curr_buttonSP = buttonSP;
curr_buttonSP->left_edge = 3;
curr_buttonSP->top_edge = 162;
curr_buttonSP->right_edge = 65;
curr_buttonSP->bottom_edge = 192;

/* [1] KP_1: */
curr_buttonSP = buttonSP + 1;
curr_buttonSP->left_edge = 3;
curr_buttonSP->top_edge = 130;
curr_buttonSP->right_edge = 33;
curr_buttonSP->bottom_edge = 160;

/* [2] KP_2: */
curr_buttonSP = buttonSP + 2;
curr_buttonSP->left_edge = 35;
curr_buttonSP->top_edge = 130;
curr_buttonSP->right_edge = 65;
curr_buttonSP->bottom_edge = 160;

/* [3] KP_3: */
curr_buttonSP = buttonSP + 3;
curr_buttonSP->left_edge = 67;
curr_buttonSP->top_edge = 130;
curr_buttonSP->right_edge = 97;
curr_buttonSP->bottom_edge = 160;

/* [4] KP_4: */
curr_buttonSP = buttonSP + 4;
curr_buttonSP->left_edge = 3;
curr_buttonSP->top_edge = 98;
curr_buttonSP->right_edge = 33;
curr_buttonSP->bottom_edge = 128;

/* [5] KP_5: */
curr_buttonSP = buttonSP + 5;
curr_buttonSP->left_edge = 35;
curr_buttonSP->top_edge = 98;
curr_buttonSP->right_edge = 65;
curr_buttonSP->bottom_edge = 128;

/* [6] KP_6: */
curr_buttonSP = buttonSP + 6;
curr_buttonSP->left_edge = 67;
curr_buttonSP->top_edge = 98;
curr_buttonSP->right_edge = 97;
curr_buttonSP->bottom_edge = 128;

/* [7] KP_7: */
curr_buttonSP = buttonSP + 7;
curr_buttonSP->left_edge = 3;
curr_buttonSP->top_edge = 66;
curr_buttonSP->right_edge = 33;
curr_buttonSP->bottom_edge = 96;

/* [8] KP_8: */
curr_buttonSP = buttonSP + 8;
curr_buttonSP->left_edge = 35;
curr_buttonSP->top_edge = 66;
curr_buttonSP->right_edge = 65;
curr_buttonSP->bottom_edge = 96;

/* [9] KP_9: */
curr_buttonSP = buttonSP + 9;
curr_buttonSP->left_edge = 67;
curr_buttonSP->top_edge = 66;
curr_buttonSP->right_edge = 97;
curr_buttonSP->bottom_edge = 96;

/* [10] KP_Decimal: */
curr_buttonSP = buttonSP + 10;
curr_buttonSP->left_edge = 67;
curr_buttonSP->top_edge = 162;
curr_buttonSP->right_edge = 97;
curr_buttonSP->bottom_edge = 192;

/* [11] KP_Enter: */
curr_buttonSP = buttonSP + 11;
curr_buttonSP->left_edge = 99;
curr_buttonSP->top_edge = 130;
curr_buttonSP->right_edge = 129;
curr_buttonSP->bottom_edge = 192;

/* [12] KP_Add: */
curr_buttonSP = buttonSP + 12;
curr_buttonSP->left_edge = 99;
curr_buttonSP->top_edge = 66;
curr_buttonSP->right_edge = 129;
curr_buttonSP->bottom_edge = 128;

/* [13] KP_Subtract: */
curr_buttonSP = buttonSP + 13;
curr_buttonSP->left_edge = 99;
curr_buttonSP->top_edge = 34;
curr_buttonSP->right_edge = 129;
curr_buttonSP->bottom_edge = 64;

/* [14] KP_Multiply: */
curr_buttonSP = buttonSP + 14;
curr_buttonSP->left_edge = 67;
curr_buttonSP->top_edge = 34;
curr_buttonSP->right_edge = 97;
curr_buttonSP->bottom_edge = 64;

/* [15] KP_Divide: */
curr_buttonSP = buttonSP + 15;
curr_buttonSP->left_edge = 35;
curr_buttonSP->top_edge = 34;
curr_buttonSP->right_edge = 65;
curr_buttonSP->bottom_edge = 64;

/* [16] F1: */
curr_buttonSP = buttonSP + 16;
curr_buttonSP->left_edge = 3;
curr_buttonSP->top_edge = 288;
curr_buttonSP->right_edge = 33;
curr_buttonSP->bottom_edge = 326;

/* [17] F2: */
curr_buttonSP = buttonSP + 17;
curr_buttonSP->left_edge = 35;
curr_buttonSP->top_edge = 288;
curr_buttonSP->right_edge = 65;
curr_buttonSP->bottom_edge = 326;

/* [18] F3: */
curr_buttonSP = buttonSP + 18;
curr_buttonSP->left_edge = 67;
curr_buttonSP->top_edge = 288;
curr_buttonSP->right_edge = 97;
curr_buttonSP->bottom_edge = 326;

/* [19] F4: */
curr_buttonSP = buttonSP + 19;
curr_buttonSP->left_edge = 99;
curr_buttonSP->top_edge = 288;
curr_buttonSP->right_edge = 129;
curr_buttonSP->bottom_edge = 326;

/* Return positive value (trivial): */
return 1;
}

/*===========================================================================*/


