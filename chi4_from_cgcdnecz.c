/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

				chi4_from_cgcdnecz.c

Purpose:
	Calculate dihedral angle chi4, using CG, CD, NE and CZ coordinates.

Input:
	(1) Pointer to AtomS structure, pointing to the first atom of the
	    current macromolecular complex.
	(2) Index of the first atom of the current residue.
        (3) Index of the last atom of the currrent residue.

Output:
	Return value.

Return value:
	(1) Dihedral angle chi4, on success.
	(2) BADDIHEDANGLE on failure.

========includes:============================================================*/

#include <stdio.h>

#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		ExtractFourAtoms_ (VectorS *, VectorS *, VectorS *, VectorS *,
				   char *, char *, char *, char *,
				   AtomS *, size_t, size_t);
void		VectorProduct_ (VectorS *, VectorS *, VectorS *);
double		AbsoluteValue_ (VectorS *);
double		ScalarProduct_ (VectorS *, VectorS *);

/*======calculate chi4 from CG, CD, NE and CZ:===============================*/

double Chi4FromCGCDNECZ_ (AtomS *atomSP, size_t atom_startI, size_t atom_endI)
{
static VectorS		CG_vectorS, CD_vectorS, NE_vectorS, CZ_vectorS;
int			n;
VectorS			CD_CG_vectorS, CD_NE_vectorS;
VectorS			NE_CD_vectorS, NE_CZ_vectorS;
VectorS			u1S, u2S;
VectorS			v1S, v2S;
double			denom, ratio, alpha;
double			chi4;

/* Extract CG, CD, NE and CZ coordinates: */
n = ExtractFourAtoms_ (&CG_vectorS, &CD_vectorS, &NE_vectorS, &CZ_vectorS,
		       "CG", "CD", "NE", "CZ",
		       atomSP, atom_startI, atom_endI);

/* All four atoms are required to calculate the angle chi4: */
if (n < 4) return BADDIHEDANGLE;

/* The first pair of auxiliary vectors: */
CD_CG_vectorS.x = CG_vectorS.x - CD_vectorS.x;
CD_CG_vectorS.y = CG_vectorS.y - CD_vectorS.y;
CD_CG_vectorS.z = CG_vectorS.z - CD_vectorS.z;
CD_NE_vectorS.x = NE_vectorS.x - CD_vectorS.x;
CD_NE_vectorS.y = NE_vectorS.y - CD_vectorS.y;
CD_NE_vectorS.z = NE_vectorS.z - CD_vectorS.z;

/* The second pair of auxiliary vectors: */
NE_CD_vectorS.x = CD_vectorS.x - NE_vectorS.x;
NE_CD_vectorS.y = CD_vectorS.y - NE_vectorS.y;
NE_CD_vectorS.z = CD_vectorS.z - NE_vectorS.z;
NE_CZ_vectorS.x = CZ_vectorS.x - NE_vectorS.x;
NE_CZ_vectorS.y = CZ_vectorS.y - NE_vectorS.y;
NE_CZ_vectorS.z = CZ_vectorS.z - NE_vectorS.z;

/* Two vectors  perpendicular to  CD_NE_vectorS,  mutually orthogonal, */
/* the second in the plane defined by CD_CG_vectorS and CD_NE_vectorS: */
VectorProduct_ (&u1S, &CD_CG_vectorS, &CD_NE_vectorS);
VectorProduct_ (&u2S, &u1S, &CD_NE_vectorS);

/* Two vectors  perpendicular to  NE_CD_vectorS,  mutually orthogonal, */
/* the second in the plane defined by NE_CD_vectorS and NE_CZ_vectorS: */
VectorProduct_ (&v1S, &NE_CD_vectorS, &NE_CZ_vectorS);
VectorProduct_ (&v2S, &NE_CD_vectorS, &v1S);

/* Calculate the angle alpha, which will be used to calculate chi4: */

/* Avoid division by zero: */
denom = AbsoluteValue_ (&u1S) * AbsoluteValue_ (&v1S);
if (denom == 0.0) return BADDIHEDANGLE;

/* Use the scalar product to calculate the cosine of the angle: */
ratio = ScalarProduct_ (&u1S, &v1S) / denom;

/* Arc cosine is very sensitive to floating point errors: */
if (ratio <= -1.0) alpha = 3.1415927;
else if (ratio >= 1.0) alpha = 0.0;
else alpha = acos (ratio);

/* There are two possible solutions; the right one is resolved here: */
if (ScalarProduct_ (&v2S, &u1S) >= 0) chi4 = alpha;
else chi4 = -alpha;

/* Return the angle (in radians): */
return chi4;
}

/*===========================================================================*/


