/* Copyright (C) 2000, 2001 Damir Zucic */

/*=============================================================================

			     representative_atoms.c

Purpose:
	Find representative atoms for a set of exposed polar residues.
	For side chains  which have only  one polar atom  this atom is
	taken as representative.  For side chains which have two polar
	atoms, the atom which is bound to both of these atoms is used,
	if it exists.

Input:
	(1) Pointer to RuntimeS structure.
	(2) The complex index (1 = bottom, 2 = top).

Output:
	(1) The information about representative atom  will be asigned
	    to each exposed polar residue.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

Notes:
	(1) Note that atomic index  (atomI) is of the type  int.  Some
	    related indices are also of the type int.  The size_t type
	    is not  safe,  because  it is  (at least on  some systems)
	    unsigned.  As such,  it is  not suitable  for subtraction.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

void		ErrorMessage_ (char *, char *, char *,
			       char *, char *, char *, char *);

/*======find representative atoms:===========================================*/

int RepresentativeAtoms_ (RuntimeS *runtimeSP, int docking_complexI)
{
int			exposed_polarN, exposed_polarI;
MolComplexS		*curr_mol_complexSP;
int			*exposed_atomIP;
ExposedResidueS		*exposed_polarSP;
int			scan_half_width;
static char		polar_nameAA[13][4] =
					{"GLU", "GLN", "HIS", "LYS", "TYR",
					 "TRP", "THR", "SER", "ASP", "ASN",
					 "ARG", "ASX", "GLX"};
static char		representative_atomAA[13][ATOMNAMESIZE] =
					{"CD",  "CD",  "ND1", "NZ",  "OH",
					 "NE1", "OG1", "OG",  "CG",  "CG",
					 "CZ",  "CG",  "CD"};
ExposedResidueS		*curr_exposedSP;
int			name_foundF;
int			listI;
int			atomsN, atom0I, atom1I, atomI; /* Do not use size_t! */
int			atom_foundF;
AtomS			*curr_atomSP;

/* Check the complex index and prepare the pointers: */
if (docking_complexI == 1)
	{
	exposed_polarN = runtimeSP->exposed_polar1N;
	curr_mol_complexSP = runtimeSP->mol_complex1SP;
	exposed_atomIP = runtimeSP->exposed_atom1IP;
	exposed_polarSP = runtimeSP->exposed_polar1SP;
	}
else if (docking_complexI == 2)
	{
	exposed_polarN = runtimeSP->exposed_polar2N;
	curr_mol_complexSP = runtimeSP->mol_complex2SP;
	exposed_atomIP = runtimeSP->exposed_atom2IP;
	exposed_polarSP = runtimeSP->exposed_polar2SP;
	}
else
	{
	ErrorMessage_ ("garlic", "RepresentativeAtoms_", "",
		       "Bad macromolecular complex index!\n",
		       "", "", "");
	return -1;
	}

/* Prepare docking half width: */
scan_half_width = DOCKING_SCAN_WIDTH / 2;

/* Scan the list of exposed polar residues: */
for (exposed_polarI = 0; exposed_polarI < exposed_polarN; exposed_polarI++)
	{
	/* Prepare the pointer to the current exposed polar residue: */
	curr_exposedSP = exposed_polarSP + exposed_polarI;

	/* Identify the residue: */
	name_foundF = 0;
	for (listI = 0; listI < 13; listI++)
		{
		if (strcmp (curr_exposedSP->pure_residue_nameA,
			    polar_nameAA[listI]) == 0)
			{
			name_foundF = 1;
			break;
			}
		}

	/* If residue identification failes, the residue should be excluded: */
	if (!name_foundF)
		{
		curr_exposedSP->excludedF = 1;
		continue;
		}

	/* If the residue was successfully identifyed, */
	/* find the index of  the representative atom. */

	/* Prepare the scan range: */
	atomsN = curr_mol_complexSP->atomsN;
	atom0I = curr_exposedSP->representative_atomI - scan_half_width;
	if (atom0I < 0) atom0I = 0;
	atom1I = curr_exposedSP->representative_atomI + scan_half_width;
	if (atom1I >= atomsN) atom1I = atomsN - 1;

	/* Scan the neighbourhood of the temporary representative atom: */
	atom_foundF = 0;
	for (atomI = atom0I; atomI <= atom1I; atomI++)
		{
		/* Pointer to the current atom: */
		curr_atomSP = curr_mol_complexSP->atomSP + atomI;

		/* If  the purified atom name  corresponds  to the */
		/* representative name, check residue information: */
		if (strcmp (curr_atomSP->raw_atomS.pure_atom_nameA,
			    representative_atomAA[listI]) == 0)
			{
			/* Check residue sequence index: */
			if (curr_atomSP->raw_atomS.residue_sequenceI !=
			    curr_exposedSP->residue_sequenceI) continue;

			/* Check the residue insertion code: */
			if (curr_atomSP->raw_atomS.residue_insertion_code !=
			    curr_exposedSP->residue_insertion_code) continue;

			/* Check the chain identifier: */
			if (curr_atomSP->raw_atomS.chainID !=
			    curr_exposedSP->chainID) continue;

			/* If this point is reached, this is the right atom: */
			atom_foundF = 1;
			curr_exposedSP->representative_atomI = (size_t) atomI;
			break;
			}
		}

	/* If the proper representative atom */
	/* is missing, exclude this residue: */
	if (!atom_foundF) curr_exposedSP->excludedF = 1;
	}

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


