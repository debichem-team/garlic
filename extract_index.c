/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				extract_index.c

Purpose:
	Extract index from string.

Input:
	Input string pointer.

Output:
	Return value.

Return value:
	(1) Integer read from string, on success (positive or zero).
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "defines.h"

/*======extract index from a string:=========================================*/

int ExtractIndex_ (char *input_stringP)
{
int		index;
char		copyA[SHORTSTRINGSIZE];
char		*P;
int		n;

/* Copy the input string: */
strncpy (copyA, input_stringP, SHORTSTRINGSIZE - 1);
copyA[SHORTSTRINGSIZE - 1] = '\0';

/* Replace each non-numeric character except minus sign with space: */
P = copyA;
while ((n = *P++) != '\0') if (!isdigit (n) && (n != '-')) *(P - 1) = ' ';

/* Try to read one integer: */
if (sscanf (copyA, "%d", &index) != 1) return -1;

/* Check the sign: */
if (index < 0) return -2;

/* If everything worked fine, return the extracted index: */
return index;
}

/*===========================================================================*/


