/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				disp_color_cap.c

Purpose:
	Find display depth. Check is the TrueColor visual class supported.
	Prepare the colormap. Prepare the parameters for color allocation.
	TrueColor is  the only visual class suitable for garlic.  A server
	which  is capable  to support  DirectColor  should  be capable  to
	support TrueColor also. Some X servers do not support this visual
	at 8 bits per pixel: increase the color depth and try again!

Input:
	(1) Pointer to GUIS structure (with GUI data).

Output:
	(1) Data stored to GUIS structure.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

=============================================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

void		ErrorMessage_ (char *, char *, char *,
			       char *, char *, char *, char *);
int		PrepareColorShifts_ (GUIS *);

/*======retrieve display color capabilities:=================================*/

int DisplayColorCapabilities_ (GUIS *guiSP)
{
Status		status;
Window		root_windowID;
int		n;

/*------depth:---------------------------------------------------------------*/

/* Default depth for default screen (maximal number of bits per pixel). */
/* Note:  it is not possible to distinguish gray-scale and color screen */
/* by the number of bits per pixel; XVisualInfo structure must be used. */
guiSP->depth = DefaultDepth (guiSP->displaySP, guiSP->screenID);
if (guiSP->depth <= 0)
	{
	ErrorMessage_ ("garlic", "DisplayColorCapabilities_", "",
		       "Default depth <= 0\n", "", "", "");
	return -1;
	}

/*------visual:--------------------------------------------------------------*/

/* Check is  TrueColor visual class supported */
/* by X server; if it is, prepare the visual: */
status = XMatchVisualInfo (guiSP->displaySP, guiSP->screenID,
			   guiSP->depth, TrueColor, &guiSP->visual_infoS);
if (status == 0)
	{
	fprintf (stdout,
		 "Sorry, garlic refuses to work if TrueColor");
	fprintf (stdout,
		 " visual is not supported!\n");
	fprintf (stdout,
		 "The current color depth is %d bits per pixel.\n",
		 guiSP->depth);
	fprintf (stdout,
		 "Your X server does not support the TrueColor visual");
	fprintf (stdout,
		 " at this color depth.\n");
	if (guiSP->depth <= 8)
		{
		fprintf (stdout, "Switch to at least 12 bits per pixel");
		fprintf (stdout, " (or more -> 24 is recommended)!\n");
		}
	return -2;
	}
else guiSP->visualSP = guiSP->visual_infoS.visual;

/*------colormap:------------------------------------------------------------*/

/* Prepare the colormap: */
root_windowID = RootWindow (guiSP->displaySP, guiSP->screenID);
guiSP->colormapID = XCreateColormap (guiSP->displaySP, root_windowID,
				     guiSP->visualSP, AllocNone);

/*------parameters for fast color allocation:--------------------------------*/

n = PrepareColorShifts_ (guiSP);

/*---------------------------------------------------------------------------*/

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


