/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				pixel_from_rgbs.c

Purpose:
	Prepare pixel value (color) from RGB components. This function is
	used instead of XAllocColor,  which is too slow. Garlic uses only
	the TrueColor visual, so that the pixel value is created from raw
	RGB values by simple bit shifting.

Input:
	(1) Pointer to RGBS structure, with raw RGB values.
	(2) Pointer to GUIS structure.

Output:
	(1) Return value.

Return value:
	(1) Pixel value (unsigned long).

=============================================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======prepare pixel value from RGB components:=============================*/

unsigned long PixelFromRGBS_ (RGBS *rgbSP, GUIS *guiSP)
{
unsigned long		pixel;

/* Prepare the pixel value (color): */
pixel = (((unsigned long) rgbSP->red >>
	   guiSP->red_right_shift) <<
	   guiSP->red_left_shift) |
	(((unsigned long) rgbSP->green >>
	   guiSP->green_right_shift) <<
	   guiSP->green_left_shift) |
	(((unsigned long) rgbSP->blue >>
	   guiSP->blue_right_shift) <<
	   guiSP->blue_left_shift);

/* Return the pixel value: */
return pixel;
}

/*===========================================================================*/


