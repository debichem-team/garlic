/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				title.c

Purpose:
	Execute title command:  write the title to the specified position
	at the screen. The same command may be used to hide the title.

Input:
	(1) Pointer to MolComplexS structure.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to ConfigS structure.
	(5) Pointer to GUIS structure.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.

Output:
	(1) The specified title written or hidden.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

Notes:
	(1) This function  uses the original command string,  to preserve
	    case. The first token is the keyword title (tit). The rest of
	    the command string  may contain  three numeric values and the
	    title string,  three numeric values, or one numeric value and
	    keyword OFF. Here are some examples:

            -------------------------------------------------------------
            | Example                                Purpose            |
            -------------------------------------------------------------
            | TITLE 1 100 50 "PORIN" | Add the title number 1 to the    |
            |                        | screen at the position (100, 50) |
            -------------------------------------------------------------
            | TITLE 2 200 80         | Move the title number 2 to the   |
            |                        | position (200, 80)               |
            -------------------------------------------------------------
            | TITLE 4 OFF            | Hide the title number 4          |
            -------------------------------------------------------------

	(2) The lowest title index is zero. The highest is MAXTITLES - 1.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>
#include <ctype.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute title command:===============================================*/

int Title_ (MolComplexS *mol_complexSP, int mol_complexesN,
	    RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
	    NearestAtomS *nearest_atomSP, size_t pixelsN,
	    unsigned int *refreshIP)
{
char			*stringP;
char			*remainderP;
char			tokenA[STRINGSIZE];
int			tokensN = 0;
int			titleI, screen_x, screen_y;
char			*P;
int			n;

/* Skip the first token: */
stringP = ExtractToken_ (tokenA, STRINGSIZE,
			 runtimeSP->curr_commandA, " \t\n");
if (!stringP) return ERROR_TITLE;

/* Count tokens: */
remainderP = stringP;
while ((remainderP = ExtractToken_ (tokenA, STRINGSIZE,
				    remainderP, " \t,;")) != NULL) tokensN++;

/* If there are too few tokens: */
if (tokensN < 2)
	{
	strcpy (runtimeSP->messageA, "Command incomplete!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_TITLE;
	}

/* In all three cases, the first token should contain the title index: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, stringP, " \t,;");
if ((sscanf (tokenA, "%d", &titleI) != 1) ||
    (titleI < 0) ||
    (titleI > MAXTITLES - 1))
	{
	strcpy (runtimeSP->messageA, "Bad token index!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_TITLE;
	}

/* Add new title: */
if (tokensN >= 4)
	{
	/** At least one (single or double) quote should be present: */
	if ((strstr (remainderP, "\"") == NULL) &&
	    (strstr (remainderP, "'")  == NULL))
		{
		strcpy (runtimeSP->messageA,
			"The title string should be enclosed in qoutes!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_TITLE;
		}

	/** The second token contains the x coordinate in screen units: **/
	remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, " \t,;");
	if (sscanf (tokenA, "%d", &screen_x) != 1)
		{
		strcpy (runtimeSP->messageA, "Bad x coordinate!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_TITLE;
		}

	/** The third token contains the y coordinate in screen units: **/
	remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, " \t,;");
	if (sscanf (tokenA, "%d", &screen_y) != 1)
		{
		strcpy (runtimeSP->messageA, "Bad y coordinate!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_TITLE;
		}

	/** Copy the coordinates: **/
	runtimeSP->title_screen_x[titleI] = screen_x;
	runtimeSP->title_screen_y[titleI] = screen_y;

	/** The title string should be enclosed in single or double quotes: **/
	remainderP = strstr (remainderP, "\"") + 1;
	ExtractToken_ (tokenA, STRINGSIZE, remainderP, "\"");

	/* Copy the title: */
	P = runtimeSP->titlesP + titleI * TITLESTRINGSIZE;
	strncpy (P, tokenA, TITLESTRINGSIZE - 1);
	*(P + TITLESTRINGSIZE - 1) = '\0';

	/** Unhide the specified title: **/
	runtimeSP->title_hiddenF[titleI] = 0;
	}

/* Move title to a new position: */
else if (tokensN == 3)
	{
	/** The second token contains the x coordinate in screen units: **/
	remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, " \t,;");
	if (sscanf (tokenA, "%d", &screen_x) != 1)
		{
		strcpy (runtimeSP->messageA, "Bad x coordinate!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_TITLE;
		}

	/** The third token cotains the y coordinate in screen units: **/
	ExtractToken_ (tokenA, STRINGSIZE, remainderP, " \t,;");
	if (sscanf (tokenA, "%d", &screen_y) != 1)
		{
		strcpy (runtimeSP->messageA, "Bad y coordinate!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_TITLE;
		}

	/** Copy new coordinates: **/
	runtimeSP->title_screen_x[titleI] = screen_x;
	runtimeSP->title_screen_y[titleI] = screen_y;
	}

/* Hide title: */
else if (tokensN == 2)
	{
	/** The second token should be the keyword OFF: **/
	ExtractToken_ (tokenA, STRINGSIZE, remainderP, " \t,;");
	P = tokenA;
	while ((n = *P++) != '\0') *(P - 1) = toupper (n);
	if (strstr (tokenA, "OFF") != tokenA)
		{
		n = STRINGSIZE - 100;
		if (n < 0) n = 0;
		tokenA[n] = '\0';
		sprintf (runtimeSP->messageA,
			 "Keyword %s not recognized!", tokenA);
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_TITLE;
		}

	/** Hide the specified title: **/
	runtimeSP->title_hiddenF[titleI] = 1;
	}

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return the command code: */
return COMMAND_TITLE;
}

/*===========================================================================*/


