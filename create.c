/* Copyright (C) 2001, 2002 Damir Zucic */

/*=============================================================================

				create.c

Purpose:
	Execute  create  command:  create  the atomic structure  using the
	sequence from the main sequence buffer and the secondary structure
	from the main  secondary structure buffer. This function does some
	checks and preparations.  The actual creation  of the structure is
	done by the function CreateStructure_ (). This function works only
	for protein sequences.

Input:
	(1) Pointer to MolComplexS structure.
	(2) Pointer to the number of macromolecular complexes.
	(3) Pointer to the next macromolecular complex identifier.
	(4) Pointer to RuntimeS structure.
	(5) Pointer to ConfigS structure.
	(6) Pointer to GUIS structure.
	(7) Pointer to NearestAtomS structure.
	(8) The number of pixels in the main window free area.
	(9) Pointer to refreshI.

Output:
	(1) A new macromolecular complex (structure) created.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

Notes:
	(1) The template residues, which consist of the template atoms and
	    are used to generate  the new structure,  should be written to
	    the file called residues.pdb and stored to at least one of the
	    following five directories:

	    (1) The current working directory.
	    (2) Home directory.
	    (3) $HOME/garlic .
	    (4) /usr/local/lib/garlic (recommended location).
	    (5) /usr/lib/garlic .

	    These directories will be searched in the specified order.  It
	    is recommended to mantain  a single template file in directory
	    /usr/local/lib/garlic .  In that case, the users will not need
	    to have their own template files.

	(2) CREATE is not the only command using  the template atoms.  The
	    storage  for  template residues  (atoms)  is allocated  at the 
	    beginning of garlic session,  in the function InitRuntime_ ().
	    The template file is read in the same function. If the reading
	    fails,  the number of template atoms (template_atomsN) will be
	    equal to zero.  Do not forget to check this number!

	(3) This function resets the edit mode index.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		CreateStructure_ (MolComplexS *, int *, int *,
				  RuntimeS *, int,
				  ConfigS *, GUIS *);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute command create:==============================================*/

int Create_ (MolComplexS *mol_complexSP,
	     int *mol_complexesNP, int *next_mol_complexIDP,
	     RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
	     NearestAtomS *nearest_atomSP, size_t pixelsN,
	     unsigned int *refreshIP)
{
int		max_length;
int		residuesN, residueI;
int		residue_recognizedF;
char		*curr_residue_nameP;
int		template_residuesN, template_residueI;
ResidueS	*template_residueSP;
AtomS		*first_atomSP;
char		*curr_template_nameP;
char		bad_residue_nameA[10];
int		n;

/* The maximal residue name length: */
max_length = RESNAMESIZE - 1;

/* Check the number of residues in the main sequence buffer: */
if (runtimeSP->residuesN == 0)
	{
	strcpy (runtimeSP->messageA, "The sequence buffer is empty!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_CREATE;
	}

/* Check the number of codes in the main secondary structure buffer: */
if (runtimeSP->sec_structure_length == 0)
	{
	strcpy (runtimeSP->messageA,
		"The secondary structure buffer is empty!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_CREATE;
	}

/* Prepare the number of residues for the new atomic structure: */
residuesN = runtimeSP->residuesN;
if (runtimeSP->sec_structure_length < residuesN)
	{
	residuesN = runtimeSP->sec_structure_length;
	}

/* Copy the number of template residues: */
template_residuesN = runtimeSP->template_residuesN;

/* Check the sequence: */
for (residueI = 0; residueI < residuesN; residueI++)
	{
	/* Prepare the pointer to the current residue name: */
	curr_residue_nameP = runtimeSP->sequenceP + residueI * max_length;

	/* If the current residue is recognized, this flag will be set to */
	/* one;  if recognition failes,  the value will remain unchanged: */
	residue_recognizedF = 0;

	/* Scan the list of template residues: */
	for (template_residueI = 0;
	     template_residueI < template_residuesN;
	     template_residueI++)
		{
		/* Pointer to the current template residue: */
		template_residueSP = runtimeSP->template_residueSP +
				     template_residueI;

		/* Pointer to  the first atom of */
		/* the current template residue: */
		first_atomSP = runtimeSP->template_atomSP +
			       template_residueSP->residue_startI;

		/* Pointer to the current template residue name: */
		curr_template_nameP =
				first_atomSP->raw_atomS.pure_residue_nameA;

		/* Compare the current residue name */
		/* with  the template residue name: */
		if (strncmp (curr_residue_nameP,
			     curr_template_nameP, max_length) == 0)
			{
			residue_recognizedF = 1;
			break;
			}
		}

	/* If residue recognition failed,  the */
	/* atomic structure cannot be created: */
	if (!residue_recognizedF)
		{
		strncpy (bad_residue_nameA, curr_residue_nameP, 3);
		bad_residue_nameA[3] = '\0';
		sprintf (runtimeSP->messageA, "Residue not recognized: %s",
			 bad_residue_nameA);
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_CREATE;
		}
	}

/* Check is there any space left in the array of MolComplexS structures: */
if (*mol_complexesNP >= MAXCOMPLEXES)
	{
	strcpy (runtimeSP->messageA, "Too many structures in use!\n");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_CREATE;
	}

/* Create the macromolecular complex: */
n = CreateStructure_ (mol_complexSP, mol_complexesNP, next_mol_complexIDP,
		      runtimeSP, residuesN, configSP, guiSP);
if (n < 0)
	{
	sprintf (runtimeSP->messageA,
		 "Unable to create structure! Error code: %d!", n);
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_CREATE;
	}

/* The new complex will be a new default caught complex: */
if (*mol_complexesNP > 0)
	{
	runtimeSP->default_complexI = *mol_complexesNP - 1;
	}

/* Reset the editing mode index: */
runtimeSP->edit_modeI = 0;

/* Reset the edit_single_bondF flag: */
runtimeSP->edit_single_bondF = 0;

/* Reset the main window mode index: */
guiSP->main_window_modeI = 0;

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, *mol_complexesNP,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return the command code: */
return COMMAND_CREATE;
}

/*===========================================================================*/


