CC = gcc

#CCOPT = -O
CCOPT = -O3
#CCOPT = -O3 -g3
#CCOPT = -ansi -pedantic -g3
#CCOPT = -ansi -pedantic -O -Wall -Wstrict-prototypes -g3

HEADERS = defines.h typedefs.h

LIBPATH = -L/usr/X11R6/lib

LINKOPT = -lX11 -lm

OBJECTS =	garlic.o print_usage.o print_help.o print_version.o \
		register.o alloc_gui.o alloc_config.o alloc_mol_complex.o \
		free_memory.o display_init.o disp_color_cap.o \
		color_shifts.o calc_params.o errmsg.o warnmsg.o \
		openfile_r.o read_config.o open_config_file.o extract_int.o \
		ex2ints.o extract_double.o ex2doubles.o extract_phrase.o \
		extract_index.o ex_rot_steps.o ex_trans_steps.o \
		ex_slab_steps.o ex_fading_steps.o extract_args.o \
		parse_options.o parse_color.o load_font.o prepare_cursor.o \
		print_config.o create_windows.o make_pixmap.o make_pixmap2.o \
		rgbs_from_xcolor.o pixel_from_rgbs.o size_hints.o wmhints.o \
		select_events.o hide_control.o load_complex.o read_file.o \
		fileformat.o is_pdb.o read_pdb.o init_header.o pdb_atom.o \
		extract_field.o copypur.o realloc_pdb.o headerline.o \
		geomcenter.o init_slab.o init_fading.o flip_vector.o \
		translate_complex.o extent.o stereo_data.o strong_bonds.o \
		pseudo_bonds.o check_dist.o add_bond.o event_loop.o \
		alloc_nearest.o init_nearest.o main_refresh.o key_press.o \
		rotation_angle.o rotate.o translation_shift.o translate.o \
		slab_shift.o move_back_slab.o move_front_slab.o \
		fading_shift.o move_back_fading.o move_front_fading.o \
		key_release.o refresh_pixmap.o slab.o no_slab.o \
		planar_slab.o sphere_slab.o half_sphere_slab.o cylin_slab.o \
		half_cylin_slab.o project_atoms.o fading.o prepare_color.o \
		no_fading.o planar_fading.o sphere_fading.o \
		half_sphere_fading.o cylin_fading.o half_cylin_fading.o \
		reset_pos_flag.o atoms_style1.o atoms_style2.o \
		atoms_style3.o atoms_style4.o atoms_style5.o atoms_style6.o \
		atoms_style7.o bonds_style1.o bond_style1_quad1.o \
		bond_style1_quad2.o bond_style1_quad3.o bond_style1_quad4.o \
		bonds_style2.o bond_style2_quad1.o bond_style2_quad2.o \
		bond_style2_quad3.o bond_style2_quad4.o bonds_style3.o \
		interpolate_color.o bond_style3_quad1.o bond_style3_quad2.o \
		bond_style3_quad3.o bond_style3_quad4.o bonds_style4.o \
		add_two_colors.o bond_style4_quad1.o bond_style4_quad2.o \
		bond_style4_quad3.o bond_style4_quad4.o bonds_style5.o \
		add_three_colors.o bond_style5_quad1.o bond_style5_quad2.o \
		bond_style5_quad3.o bond_style5_quad4.o draw_system.o \
		motion_notify.o assign_hyphob.o control_refresh.o \
		print_cursor_names.o main_resize.o init_runtime.o \
		add_char.o input_refresh.o eat_left_char.o eat_right_char.o \
		execute_command1.o execute_command2.o truncate_comm.o \
		create_log_file.o load.o catch.o discard.o select.o \
		full_select.o include.o chains.o residue_ranges.o \
		extract_token.o extract_serials.o residue_names.o \
		atom_names.o exclude.o skip_keyword.o apply_selection.o \
		show.o hide.o color_schemes.o init_colors.o color.o \
		special_select.o select_hetero.o select_element.o \
		select_atoms.o quick_select.o replace_command.o atoms.o \
		bonds.o execute_script.o comm_rotate.o comm_translate.o \
		comm_slab.o comm_fading.o log.o prepare_backbone.o \
		background.o text_background.o text_foreground.o \
		change_slab.o change_fading.o full_colors.o is_old.o \
		font.o cpk.o zebra.o chain.o color_hyphob.o mix_colors.o \
		comm_backbone.o hide_no_refresh.o show_no_refresh.o \
		backbone.o hybonds.o generate_hybonds.o hybond_style.o \
		show_backbone.o hide_backbone.o pdb_id.o generate_plane.o \
		stereo.o mono.o position.o copy_doubles.o title.o \
		free_runtime.o draw_titles.o foreground.o prepare_oc.o \
		select_complement.o labels.o draw_labels.o \
		assign_radii.o spacefill.o spacefill_color.o draw_balls.o \
		center.o save.o write_line.o big_spheres.o tag.o \
		draw_tags.o tag_position.o button_press.o control_click.o \
		identify_button.o init_buttons.o write_hints.o \
		extract_sequence.o count_residues.o sequence.o \
		parse_sequence.o sequence_from.o load_sequence.o \
		select_sequence.o compare_sequences.o dihedral_angles.o \
		extract_ncaco.o phi_from_cncac.o psi_from_ncaco.o \
		vector_product.o absolute_value.o scalar_product.o \
		omega_from_cacnca.o select_cis_trans.o \
		ramachandran.o draw_ramachandran.o rama_frame.o \
		rama_mouse.o read_fasta.o save_sequence.o wheel.o \
		wheel_top_view.o wheel_bottom_view.o hydrophobicity.o \
		weight_colors.o angle.o radius.o select_sphere.o \
		disulfide_bonds.o venn.o draw_venn.o spacefill2.o \
		sp2_color.o save_seq_w_numbers.o ssb.o select_alternate.o \
		select_model.o color_model.o window.o plot.o draw_plots.o \
		weighted_hydrophobicity.o average_hydrophobicity.o \
		init_hyphob.o plot_mouse.o project_planes.o draw_planes.o \
		plane_extent.o rotate_plane.o translate_plane.o \
		comm_move.o plane.o select_above.o select_below.o big2.o \
		scale.o pattern.o dock.o align_normal.o align_complexes.o \
		docking_window.o docking_resize.o docking_refresh.o \
		docking_project.o list_exposed_polar.o is_polar.o \
		representative_atoms.o exclude_distant.o draw_bottom.o \
		draw_top.o copy_sequence.o compare.o draw_comparison.o \
		neighborhood.o draw_balls2.o comparison_mouse.o structure.o \
		parse_sec_structure.o copy_sec_structure.o \
		load_sec_structure.o save_sec_structure.o \
		alloc_sec_structure.o sec_structure_from.o create.o \
		open_template_file.o read_template_file.o \
		ex_template_residues.o count_template_res.o \
		create_structure.o required_atoms.o copy_template_atoms.o \
		fix_chain.o reset_phi.o extract_hncac.o phi_from_hncac.o \
		rotate_atom.o parallel_part.o reset_psi.o build_chain.o \
		extract_ncac.o translate_range.o rotate_range.o \
		apply_structure.o generate_phi_psi.o extract_nca.o \
		extract_c.o replace.o extract_n.o refresh_backbone.o \
		edit.o draw_edit_symbol.o draw_squid.o draw_seahorse.o \
		are_atoms_bound.o bond_partners.o edit_phi.o edit_psi.o \
		extract_cac.o small_ramachandran.o edit_omega.o \
		small_omega.o extract_h.o extract_o.o psi_from_ncacn.o \
		is_standard.o extract_four_atoms.o chi1_from_ncacbcg.o \
		chi1_from_ncacbsg.o chi1_from_ncacbcg1.o \
		chi1_from_ncacbog.o chi1_from_ncacbog1.o \
		chi2_from_cacbcgcd.o chi2_from_cacbcgod1.o \
		chi2_from_cacbcgnd1.o chi2_from_cacbcg1cd.o \
		chi2_from_cacbcgcd1.o chi2_from_cacbcgsd.o \
		chi3_from_cbcgcdne.o chi3_from_cbcgcdoe1.o \
		chi3_from_cbcgcdce.o chi3_from_cbcgsdce.o \
		chi4_from_cgcdnecz.o chi4_from_cgcdcenz.o \
		chi5_from_cdnecznh1.o is_part_of_ring.o edit_bond.o \
		edit_chi2.o extract_cacb.o edit_chi1.o extract_cbxg.o \
		resize_complex.o membrane.o inform_user.o \
		alpha_membrane.o extract_ca.o extract_vector.o \
		alpha_membrane_center.o init_membrane.o alpha_normal.o \
		project_membranes.o draw_membranes.o \
		rotate_membrane.o translate_membrane.o select_triplet.o \
		set.o set_phi.o set_psi.o set_omega.o set_chi1.o set_chi2.o \
		set_chi3.o extract_cgxd.o set_chi4.o extract_cdxe.o \
		set_chi5.o extract_necz.o pick_bond.o nice_bonds.o \
		covalent.o covalent2.o half_nice_bond.o probe.o \
		small_atoms.o small_atoms2.o draw_sticks.o half_stick.o \
		command_refresh.o acceptor_dipole.o donor_dipole.o sticks.o \
		balls.o blur.o blur_main_window.o group.o beta_membrane.o \
		extract_cg.o select_tm.o parse_pattern.o select_pattern.o \
		hyphob_function1.o hyphob_function2.o hyphob_function3.o \
		hyphob_function4.o hyphob_function5.o hyphob_function6.o \
		hyphob_function7.o color_size.o command_dump.o is_aromatic.o \
		select_2c3.o select_2cz3.o select_2nz3.o \
		select_3c4.o select_3cz4.o select_3nz4.o \
		select_3c5.o select_3cz5.o select_3nz5.o select_3p5.o \
		select_4c5.o select_4cz5.o select_4nz5.o \
		select_3c6.o select_3cz6.o select_3nz6.o \
		select_4c6.o select_4cz6.o select_4nz6.o select_4p6.o \
		select_5c6.o select_5cz6.o select_5nz6.o \
		select_4c7.o select_4cz7.o select_4nz7.o \
		select_5c7.o select_5cz7.o select_5nz7.o select_5p7.o \
		select_6p7.o \
		select_4c9.o select_4cz9.o select_4nz9.o \
		select_5c9.o select_5cz9.o select_5nz9.o \
		color_weighted.o

garlic: $(OBJECTS)
	$(CC) -o garlic $(LIBPATH) $(OBJECTS) $(LINKOPT)

print_usage.o: print_usage.c $(HEADERS)
	$(CC) -c $(CCOPT) print_usage.c

print_help.o: print_help.c $(HEADERS)
	$(CC) -c $(CCOPT) print_help.c

print_version.o: print_version.c $(HEADERS)
	$(CC) -c $(CCOPT) print_version.c

register.o: register.c $(HEADERS)
	$(CC) -c $(CCOPT) register.c

garlic.o: garlic.c $(HEADERS)
	$(CC) -c $(CCOPT) garlic.c

alloc_gui.o: alloc_gui.c $(HEADERS)
	$(CC) -c $(CCOPT) alloc_gui.c

alloc_config.o: alloc_config.c $(HEADERS)
	$(CC) -c $(CCOPT) alloc_config.c

alloc_mol_complex.o: alloc_mol_complex.c $(HEADERS)
	$(CC) -c $(CCOPT) alloc_mol_complex.c

free_memory.o: free_memory.c $(HEADERS)
	$(CC) -c $(CCOPT) free_memory.c

display_init.o: display_init.c $(HEADERS)
	$(CC) -c $(CCOPT) display_init.c

disp_color_cap.o: disp_color_cap.c $(HEADERS)
	$(CC) -c $(CCOPT) disp_color_cap.c

color_shifts.o: color_shifts.c $(HEADERS)
	$(CC) -c $(CCOPT) color_shifts.c

calc_params.o: calc_params.c $(HEADERS)
	$(CC) -c $(CCOPT) calc_params.c

errmsg.o: errmsg.c $(HEADERS)
	$(CC) -c $(CCOPT) errmsg.c

warnmsg.o: warnmsg.c $(HEADERS)
	$(CC) -c $(CCOPT) warnmsg.c

openfile_r.o: openfile_r.c $(HEADERS)
	$(CC) -c $(CCOPT) openfile_r.c

read_config.o: read_config.c $(HEADERS)
	$(CC) -c $(CCOPT) read_config.c

open_config_file.o: open_config_file.c $(HEADERS)
	$(CC) -c $(CCOPT) open_config_file.c

extract_int.o: extract_int.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_int.c

ex2ints.o: ex2ints.c $(HEADERS)
	$(CC) -c $(CCOPT) ex2ints.c

extract_double.o: extract_double.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_double.c

ex2doubles.o: ex2doubles.c $(HEADERS)
	$(CC) -c $(CCOPT) ex2doubles.c

extract_phrase.o: extract_phrase.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_phrase.c

extract_index.o: extract_index.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_index.c

ex_rot_steps.o: ex_rot_steps.c $(HEADERS)
	$(CC) -c $(CCOPT) ex_rot_steps.c

ex_trans_steps.o: ex_trans_steps.c $(HEADERS)
	$(CC) -c $(CCOPT) ex_trans_steps.c

ex_slab_steps.o: ex_slab_steps.c $(HEADERS)
	$(CC) -c $(CCOPT) ex_slab_steps.c

ex_fading_steps.o: ex_fading_steps.c $(HEADERS)
	$(CC) -c $(CCOPT) ex_fading_steps.c

extract_args.o: extract_args.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_args.c

parse_options.o: parse_options.c $(HEADERS)
	$(CC) -c $(CCOPT) parse_options.c

parse_color.o: parse_color.c $(HEADERS)
	$(CC) -c $(CCOPT) parse_color.c

load_font.o: load_font.c $(HEADERS)
	$(CC) -c $(CCOPT) load_font.c

prepare_cursor.o: prepare_cursor.c $(HEADERS)
	$(CC) -c $(CCOPT) prepare_cursor.c

print_config.o: print_config.c $(HEADERS)
	$(CC) -c $(CCOPT) print_config.c

create_windows.o: create_windows.c $(HEADERS) garlic.xpm control.xpm
	$(CC) -c $(CCOPT) create_windows.c

make_pixmap.o: make_pixmap.c $(HEADERS)
	$(CC) -c $(CCOPT) make_pixmap.c

make_pixmap2.o: make_pixmap2.c $(HEADERS)
	$(CC) -c $(CCOPT) make_pixmap2.c

rgbs_from_xcolor.o: rgbs_from_xcolor.c $(HEADERS)
	$(CC) -c $(CCOPT) rgbs_from_xcolor.c

pixel_from_rgbs.o: pixel_from_rgbs.c $(HEADERS)
	$(CC) -c $(CCOPT) pixel_from_rgbs.c

size_hints.o: size_hints.c $(HEADERS)
	$(CC) -c $(CCOPT) size_hints.c

wmhints.o: wmhints.c $(HEADERS)
	$(CC) -c $(CCOPT) wmhints.c

select_events.o: select_events.c $(HEADERS)
	$(CC) -c $(CCOPT) select_events.c

hide_control.o: hide_control.c $(HEADERS)
	$(CC) -c $(CCOPT) hide_control.c

load_complex.o: load_complex.c $(HEADERS)
	$(CC) -c $(CCOPT) load_complex.c

read_file.o: read_file.c $(HEADERS)
	$(CC) -c $(CCOPT) read_file.c

fileformat.o: fileformat.c $(HEADERS)
	$(CC) -c $(CCOPT) fileformat.c

is_pdb.o: is_pdb.c $(HEADERS)
	$(CC) -c $(CCOPT) is_pdb.c

read_pdb.o: read_pdb.c $(HEADERS)
	$(CC) -c $(CCOPT) read_pdb.c

init_header.o: init_header.c $(HEADERS)
	$(CC) -c $(CCOPT) init_header.c

pdb_atom.o: pdb_atom.c $(HEADERS)
	$(CC) -c $(CCOPT) pdb_atom.c

extract_field.o: extract_field.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_field.c

copypur.o: copypur.c $(HEADERS)
	$(CC) -c $(CCOPT) copypur.c

realloc_pdb.o: realloc_pdb.c $(HEADERS)
	$(CC) -c $(CCOPT) realloc_pdb.c

headerline.o: headerline.c $(HEADERS)
	$(CC) -c $(CCOPT) headerline.c

geomcenter.o: geomcenter.c $(HEADERS)
	$(CC) -c $(CCOPT) geomcenter.c

init_slab.o: init_slab.c $(HEADERS)
	$(CC) -c $(CCOPT) init_slab.c

init_fading.o: init_fading.c $(HEADERS)
	$(CC) -c $(CCOPT) init_fading.c

flip_vector.o: flip_vector.c $(HEADERS)
	$(CC) -c $(CCOPT) flip_vector.c

translate_complex.o: translate_complex.c $(HEADERS)
	$(CC) -c $(CCOPT) translate_complex.c

extent.o: extent.c $(HEADERS)
	$(CC) -c $(CCOPT) extent.c

stereo_data.o: stereo_data.c $(HEADERS)
	$(CC) -c $(CCOPT) stereo_data.c

strong_bonds.o: strong_bonds.c $(HEADERS)
	$(CC) -c $(CCOPT) strong_bonds.c

pseudo_bonds.o: pseudo_bonds.c $(HEADERS)
	$(CC) -c $(CCOPT) pseudo_bonds.c

check_dist.o: check_dist.c $(HEADERS)
	$(CC) -c $(CCOPT) check_dist.c

add_bond.o: add_bond.c $(HEADERS)
	$(CC) -c $(CCOPT) add_bond.c

event_loop.o: event_loop.c $(HEADERS)
	$(CC) -c $(CCOPT) event_loop.c

alloc_nearest.o: alloc_nearest.c $(HEADERS)
	$(CC) -c $(CCOPT) alloc_nearest.c

init_nearest.o: init_nearest.c $(HEADERS)
	$(CC) -c $(CCOPT) init_nearest.c

main_refresh.o: main_refresh.c $(HEADERS)
	$(CC) -c $(CCOPT) main_refresh.c

key_press.o: key_press.c $(HEADERS)
	$(CC) -c $(CCOPT) key_press.c

rotation_angle.o: rotation_angle.c $(HEADERS)
	$(CC) -c $(CCOPT) rotation_angle.c

rotate.o: rotate.c $(HEADERS)
	$(CC) -c $(CCOPT) rotate.c

translation_shift.o: translation_shift.c $(HEADERS)
	$(CC) -c $(CCOPT) translation_shift.c

translate.o: translate.c $(HEADERS)
	$(CC) -c $(CCOPT) translate.c

slab_shift.o: slab_shift.c $(HEADERS)
	$(CC) -c $(CCOPT) slab_shift.c

move_back_slab.o: move_back_slab.c $(HEADERS)
	$(CC) -c $(CCOPT) move_back_slab.c

move_front_slab.o: move_front_slab.c $(HEADERS)
	$(CC) -c $(CCOPT) move_front_slab.c

fading_shift.o: fading_shift.c $(HEADERS)
	$(CC) -c $(CCOPT) fading_shift.c

move_back_fading.o: move_back_fading.c $(HEADERS)
	$(CC) -c $(CCOPT) move_back_fading.c

move_front_fading.o: move_front_fading.c $(HEADERS)
	$(CC) -c $(CCOPT) move_front_fading.c

key_release.o: key_release.c $(HEADERS)
	$(CC) -c $(CCOPT) key_release.c

refresh_pixmap.o: refresh_pixmap.c $(HEADERS)
	$(CC) -c $(CCOPT) refresh_pixmap.c

slab.o: slab.c $(HEADERS)
	$(CC) -c $(CCOPT) slab.c

no_slab.o: no_slab.c $(HEADERS)
	$(CC) -c $(CCOPT) no_slab.c

planar_slab.o: planar_slab.c $(HEADERS)
	$(CC) -c $(CCOPT) planar_slab.c

sphere_slab.o: sphere_slab.c $(HEADERS)
	$(CC) -c $(CCOPT) sphere_slab.c

half_sphere_slab.o: half_sphere_slab.c $(HEADERS)
	$(CC) -c $(CCOPT) half_sphere_slab.c

cylin_slab.o: cylin_slab.c $(HEADERS)
	$(CC) -c $(CCOPT) cylin_slab.c

half_cylin_slab.o: half_cylin_slab.c $(HEADERS)
	$(CC) -c $(CCOPT) half_cylin_slab.c

project_atoms.o: project_atoms.c $(HEADERS)
	$(CC) -c $(CCOPT) project_atoms.c

fading.o: fading.c $(HEADERS)
	$(CC) -c $(CCOPT) fading.c

prepare_color.o: prepare_color.c $(HEADERS)
	$(CC) -c $(CCOPT) prepare_color.c

no_fading.o: no_fading.c $(HEADERS)
	$(CC) -c $(CCOPT) no_fading.c

planar_fading.o: planar_fading.c $(HEADERS)
	$(CC) -c $(CCOPT) planar_fading.c

sphere_fading.o: sphere_fading.c $(HEADERS)
	$(CC) -c $(CCOPT) sphere_fading.c

half_sphere_fading.o: half_sphere_fading.c $(HEADERS)
	$(CC) -c $(CCOPT) half_sphere_fading.c

cylin_fading.o: cylin_fading.c $(HEADERS)
	$(CC) -c $(CCOPT) cylin_fading.c

half_cylin_fading.o: half_cylin_fading.c $(HEADERS)
	$(CC) -c $(CCOPT) half_cylin_fading.c

reset_pos_flag.o: reset_pos_flag.c $(HEADERS)
	$(CC) -c $(CCOPT) reset_pos_flag.c

atoms_style1.o: atoms_style1.c $(HEADERS)
	$(CC) -c $(CCOPT) atoms_style1.c

atoms_style2.o: atoms_style2.c $(HEADERS)
	$(CC) -c $(CCOPT) atoms_style2.c

atoms_style3.o: atoms_style3.c $(HEADERS)
	$(CC) -c $(CCOPT) atoms_style3.c

atoms_style4.o: atoms_style4.c $(HEADERS)
	$(CC) -c $(CCOPT) atoms_style4.c

atoms_style5.o: atoms_style5.c $(HEADERS)
	$(CC) -c $(CCOPT) atoms_style5.c

atoms_style6.o: atoms_style6.c $(HEADERS)
	$(CC) -c $(CCOPT) atoms_style6.c

atoms_style7.o: atoms_style7.c $(HEADERS)
	$(CC) -c $(CCOPT) atoms_style7.c

bonds_style1.o: bonds_style1.c $(HEADERS)
	$(CC) -c $(CCOPT) bonds_style1.c

bond_style1_quad1.o: bond_style1_quad1.c $(HEADERS)
	$(CC) -c $(CCOPT) bond_style1_quad1.c

bond_style1_quad2.o: bond_style1_quad2.c $(HEADERS)
	$(CC) -c $(CCOPT) bond_style1_quad2.c

bond_style1_quad3.o: bond_style1_quad3.c $(HEADERS)
	$(CC) -c $(CCOPT) bond_style1_quad3.c

bond_style1_quad4.o: bond_style1_quad4.c $(HEADERS)
	$(CC) -c $(CCOPT) bond_style1_quad4.c

bonds_style2.o: bonds_style2.c $(HEADERS)
	$(CC) -c $(CCOPT) bonds_style2.c

bond_style2_quad1.o: bond_style2_quad1.c $(HEADERS)
	$(CC) -c $(CCOPT) bond_style2_quad1.c

bond_style2_quad2.o: bond_style2_quad2.c $(HEADERS)
	$(CC) -c $(CCOPT) bond_style2_quad2.c

bond_style2_quad3.o: bond_style2_quad3.c $(HEADERS)
	$(CC) -c $(CCOPT) bond_style2_quad3.c

bond_style2_quad4.o: bond_style2_quad4.c $(HEADERS)
	$(CC) -c $(CCOPT) bond_style2_quad4.c

bonds_style3.o: bonds_style3.c $(HEADERS)
	$(CC) -c $(CCOPT) bonds_style3.c

interpolate_color.o: interpolate_color.c $(HEADERS)
	$(CC) -c $(CCOPT) interpolate_color.c

bond_style3_quad1.o: bond_style3_quad1.c $(HEADERS)
	$(CC) -c $(CCOPT) bond_style3_quad1.c

bond_style3_quad2.o: bond_style3_quad2.c $(HEADERS)
	$(CC) -c $(CCOPT) bond_style3_quad2.c

bond_style3_quad3.o: bond_style3_quad3.c $(HEADERS)
	$(CC) -c $(CCOPT) bond_style3_quad3.c

bond_style3_quad4.o: bond_style3_quad4.c $(HEADERS)
	$(CC) -c $(CCOPT) bond_style3_quad4.c

bonds_style4.o: bonds_style4.c $(HEADERS)
	$(CC) -c $(CCOPT) bonds_style4.c

add_two_colors.o: add_two_colors.c $(HEADERS)
	$(CC) -c $(CCOPT) add_two_colors.c

bond_style4_quad1.o: bond_style4_quad1.c $(HEADERS)
	$(CC) -c $(CCOPT) bond_style4_quad1.c

bond_style4_quad2.o: bond_style4_quad2.c $(HEADERS)
	$(CC) -c $(CCOPT) bond_style4_quad2.c

bond_style4_quad3.o: bond_style4_quad3.c $(HEADERS)
	$(CC) -c $(CCOPT) bond_style4_quad3.c

bond_style4_quad4.o: bond_style4_quad4.c $(HEADERS)
	$(CC) -c $(CCOPT) bond_style4_quad4.c

bonds_style5.o: bonds_style5.c $(HEADERS)
	$(CC) -c $(CCOPT) bonds_style5.c

add_three_colors.o: add_three_colors.c $(HEADERS)
	$(CC) -c $(CCOPT) add_three_colors.c

bond_style5_quad1.o: bond_style5_quad1.c $(HEADERS)
	$(CC) -c $(CCOPT) bond_style5_quad1.c

bond_style5_quad2.o: bond_style5_quad2.c $(HEADERS)
	$(CC) -c $(CCOPT) bond_style5_quad2.c

bond_style5_quad3.o: bond_style5_quad3.c $(HEADERS)
	$(CC) -c $(CCOPT) bond_style5_quad3.c

bond_style5_quad4.o: bond_style5_quad4.c $(HEADERS)
	$(CC) -c $(CCOPT) bond_style5_quad4.c

draw_system.o: draw_system.c $(HEADERS) system.xpm
	$(CC) -c $(CCOPT) draw_system.c

motion_notify.o: motion_notify.c $(HEADERS)
	$(CC) -c $(CCOPT) motion_notify.c

assign_hyphob.o: assign_hyphob.c $(HEADERS)
	$(CC) -c $(CCOPT) assign_hyphob.c

control_refresh.o: control_refresh.c $(HEADERS)
	$(CC) -c $(CCOPT) control_refresh.c

print_cursor_names.o: print_cursor_names.c $(HEADERS)
	$(CC) -c $(CCOPT) print_cursor_names.c

main_resize.o: main_resize.c $(HEADERS)
	$(CC) -c $(CCOPT) main_resize.c

init_runtime.o: init_runtime.c $(HEADERS)
	$(CC) -c $(CCOPT) init_runtime.c

add_char.o: add_char.c $(HEADERS)
	$(CC) -c $(CCOPT) add_char.c

input_refresh.o: input_refresh.c $(HEADERS)
	$(CC) -c $(CCOPT) input_refresh.c

eat_left_char.o: eat_left_char.c $(HEADERS)
	$(CC) -c $(CCOPT) eat_left_char.c

eat_right_char.o: eat_right_char.c $(HEADERS)
	$(CC) -c $(CCOPT) eat_right_char.c

execute_command1.o: execute_command1.c $(HEADERS)
	$(CC) -c $(CCOPT) execute_command1.c

execute_command2.o: execute_command2.c $(HEADERS)
	$(CC) -c $(CCOPT) execute_command2.c

truncate_comm.o: truncate_comm.c $(HEADERS)
	$(CC) -c $(CCOPT) truncate_comm.c

create_log_file.o: create_log_file.c $(HEADERS)
	$(CC) -c $(CCOPT) create_log_file.c

load.o: load.c $(HEADERS)
	$(CC) -c $(CCOPT) load.c

catch.o: catch.c $(HEADERS)
	$(CC) -c $(CCOPT) catch.c

discard.o: discard.c $(HEADERS)
	$(CC) -c $(CCOPT) discard.c

select.o: select.c $(HEADERS)
	$(CC) -c $(CCOPT) select.c

full_select.o: full_select.c $(HEADERS)
	$(CC) -c $(CCOPT) full_select.c

include.o: include.c $(HEADERS)
	$(CC) -c $(CCOPT) include.c

chains.o: chains.c $(HEADERS)
	$(CC) -c $(CCOPT) chains.c

residue_ranges.o: residue_ranges.c $(HEADERS)
	$(CC) -c $(CCOPT) residue_ranges.c

extract_token.o: extract_token.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_token.c

extract_serials.o: extract_serials.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_serials.c

residue_names.o: residue_names.c $(HEADERS)
	$(CC) -c $(CCOPT) residue_names.c

atom_names.o: atom_names.c $(HEADERS)
	$(CC) -c $(CCOPT) atom_names.c

exclude.o: exclude.c $(HEADERS)
	$(CC) -c $(CCOPT) exclude.c

skip_keyword.o: skip_keyword.c $(HEADERS)
	$(CC) -c $(CCOPT) skip_keyword.c

apply_selection.o: apply_selection.c $(HEADERS)
	$(CC) -c $(CCOPT) apply_selection.c

show.o: show.c $(HEADERS)
	$(CC) -c $(CCOPT) show.c

hide.o: hide.c $(HEADERS)
	$(CC) -c $(CCOPT) hide.c

color_schemes.o: color_schemes.c $(HEADERS)
	$(CC) -c $(CCOPT) color_schemes.c

init_colors.o: init_colors.c $(HEADERS)
	$(CC) -c $(CCOPT) init_colors.c

color.o: color.c $(HEADERS)
	$(CC) -c $(CCOPT) color.c

special_select.o: special_select.c $(HEADERS)
	$(CC) -c $(CCOPT) special_select.c

select_hetero.o: select_hetero.c $(HEADERS)
	$(CC) -c $(CCOPT) select_hetero.c

select_element.o: select_element.c $(HEADERS)
	$(CC) -c $(CCOPT) select_element.c

select_atoms.o: select_atoms.c $(HEADERS)
	$(CC) -c $(CCOPT) select_atoms.c

quick_select.o: quick_select.c $(HEADERS)
	$(CC) -c $(CCOPT) quick_select.c

replace_command.o: replace_command.c $(HEADERS)
	$(CC) -c $(CCOPT) replace_command.c

atoms.o: atoms.c $(HEADERS)
	$(CC) -c $(CCOPT) atoms.c

bonds.o: bonds.c $(HEADERS)
	$(CC) -c $(CCOPT) bonds.c

execute_script.o: execute_script.c $(HEADERS)
	$(CC) -c $(CCOPT) execute_script.c

comm_rotate.o: comm_rotate.c $(HEADERS)
	$(CC) -c $(CCOPT) comm_rotate.c

comm_translate.o: comm_translate.c $(HEADERS)
	$(CC) -c $(CCOPT) comm_translate.c

comm_slab.o: comm_slab.c $(HEADERS)
	$(CC) -c $(CCOPT) comm_slab.c

comm_fading.o: comm_fading.c $(HEADERS)
	$(CC) -c $(CCOPT) comm_fading.c

log.o: log.c $(HEADERS)
	$(CC) -c $(CCOPT) log.c

prepare_backbone.o: prepare_backbone.c $(HEADERS)
	$(CC) -c $(CCOPT) prepare_backbone.c

background.o: background.c $(HEADERS)
	$(CC) -c $(CCOPT) background.c

text_background.o: text_background.c $(HEADERS)
	$(CC) -c $(CCOPT) text_background.c

text_foreground.o: text_foreground.c $(HEADERS)
	$(CC) -c $(CCOPT) text_foreground.c

change_slab.o: change_slab.c $(HEADERS)
	$(CC) -c $(CCOPT) change_slab.c

change_fading.o: change_fading.c $(HEADERS)
	$(CC) -c $(CCOPT) change_fading.c

full_colors.o: full_colors.c $(HEADERS)
	$(CC) -c $(CCOPT) full_colors.c

is_old.o: is_old.c $(HEADERS)
	$(CC) -c $(CCOPT) is_old.c

font.o: font.c $(HEADERS)
	$(CC) -c $(CCOPT) font.c

cpk.o: cpk.c $(HEADERS)
	$(CC) -c $(CCOPT) cpk.c

zebra.o: zebra.c $(HEADERS)
	$(CC) -c $(CCOPT) zebra.c

chain.o: chain.c $(HEADERS)
	$(CC) -c $(CCOPT) chain.c

color_hyphob.o: color_hyphob.c $(HEADERS)
	$(CC) -c $(CCOPT) color_hyphob.c

mix_colors.o: mix_colors.c $(HEADERS)
	$(CC) -c $(CCOPT) mix_colors.c

comm_backbone.o: comm_backbone.c $(HEADERS)
	$(CC) -c $(CCOPT) comm_backbone.c

hide_no_refresh.o: hide_no_refresh.c $(HEADERS)
	$(CC) -c $(CCOPT) hide_no_refresh.c

show_no_refresh.o: show_no_refresh.c $(HEADERS)
	$(CC) -c $(CCOPT) show_no_refresh.c

backbone.o: backbone.c $(HEADERS)
	$(CC) -c $(CCOPT) backbone.c

hybonds.o: hybonds.c $(HEADERS)
	$(CC) -c $(CCOPT) hybonds.c

generate_hybonds.o: generate_hybonds.c $(HEADERS)
	$(CC) -c $(CCOPT) generate_hybonds.c

hybond_style.o: hybond_style.c $(HEADERS)
	$(CC) -c $(CCOPT) hybond_style.c

show_backbone.o: show_backbone.c $(HEADERS)
	$(CC) -c $(CCOPT) show_backbone.c

hide_backbone.o: hide_backbone.c $(HEADERS)
	$(CC) -c $(CCOPT) hide_backbone.c

pdb_id.o: pdb_id.c $(HEADERS)
	$(CC) -c $(CCOPT) pdb_id.c

generate_plane.o: generate_plane.c $(HEADERS)
	$(CC) -c $(CCOPT) generate_plane.c

stereo.o: stereo.c $(HEADERS)
	$(CC) -c $(CCOPT) stereo.c

mono.o: mono.c $(HEADERS)
	$(CC) -c $(CCOPT) mono.c

position.o: position.c $(HEADERS)
	$(CC) -c $(CCOPT) position.c

copy_doubles.o: copy_doubles.c $(HEADERS)
	$(CC) -c $(CCOPT) copy_doubles.c

title.o: title.c $(HEADERS)
	$(CC) -c $(CCOPT) title.c

free_runtime.o: free_runtime.c $(HEADERS)
	$(CC) -c $(CCOPT) free_runtime.c

draw_titles.o: draw_titles.c $(HEADERS)
	$(CC) -c $(CCOPT) draw_titles.c

foreground.o: foreground.c $(HEADERS)
	$(CC) -c $(CCOPT) foreground.c

prepare_oc.o: prepare_oc.c $(HEADERS)
	$(CC) -c $(CCOPT) prepare_oc.c

select_complement.o: select_complement.c $(HEADERS)
	$(CC) -c $(CCOPT) select_complement.c

labels.o: labels.c $(HEADERS)
	$(CC) -c $(CCOPT) labels.c

draw_labels.o: draw_labels.c $(HEADERS)
	$(CC) -c $(CCOPT) draw_labels.c

assign_radii.o: assign_radii.c $(HEADERS)
	$(CC) -c $(CCOPT) assign_radii.c

spacefill.o: spacefill.c $(HEADERS)
	$(CC) -c $(CCOPT) spacefill.c

spacefill_color.o: spacefill_color.c $(HEADERS)
	$(CC) -c $(CCOPT) spacefill_color.c

draw_balls.o: draw_balls.c $(HEADERS)
	$(CC) -c $(CCOPT) draw_balls.c

center.o: center.c $(HEADERS)
	$(CC) -c $(CCOPT) center.c

save.o: save.c $(HEADERS)
	$(CC) -c $(CCOPT) save.c

write_line.o: write_line.c $(HEADERS)
	$(CC) -c $(CCOPT) write_line.c

big_spheres.o: big_spheres.c $(HEADERS)
	$(CC) -c $(CCOPT) big_spheres.c

tag.o: tag.c $(HEADERS)
	$(CC) -c $(CCOPT) tag.c

draw_tags.o: draw_tags.c $(HEADERS)
	$(CC) -c $(CCOPT) draw_tags.c

tag_position.o: tag_position.c $(HEADERS)
	$(CC) -c $(CCOPT) tag_position.c

button_press.o: button_press.c $(HEADERS)
	$(CC) -c $(CCOPT) button_press.c

control_click.o: control_click.c $(HEADERS)
	$(CC) -c $(CCOPT) control_click.c

identify_button.o: identify_button.c $(HEADERS)
	$(CC) -c $(CCOPT) identify_button.c

init_buttons.o: init_buttons.c $(HEADERS)
	$(CC) -c $(CCOPT) init_buttons.c

write_hints.o: write_hints.c $(HEADERS)
	$(CC) -c $(CCOPT) write_hints.c

extract_sequence.o: extract_sequence.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_sequence.c

count_residues.o: count_residues.c $(HEADERS)
	$(CC) -c $(CCOPT) count_residues.c

sequence.o: sequence.c $(HEADERS)
	$(CC) -c $(CCOPT) sequence.c

parse_sequence.o: parse_sequence.c $(HEADERS)
	$(CC) -c $(CCOPT) parse_sequence.c

sequence_from.o: sequence_from.c $(HEADERS)
	$(CC) -c $(CCOPT) sequence_from.c

load_sequence.o: load_sequence.c $(HEADERS)
	$(CC) -c $(CCOPT) load_sequence.c

select_sequence.o: select_sequence.c $(HEADERS)
	$(CC) -c $(CCOPT) select_sequence.c

compare_sequences.o: compare_sequences.c $(HEADERS)
	$(CC) -c $(CCOPT) compare_sequences.c

dihedral_angles.o: dihedral_angles.c $(HEADERS)
	$(CC) -c $(CCOPT) dihedral_angles.c

extract_ncaco.o: extract_ncaco.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_ncaco.c

phi_from_cncac.o: phi_from_cncac.c $(HEADERS)
	$(CC) -c $(CCOPT) phi_from_cncac.c

psi_from_ncaco.o: psi_from_ncaco.c $(HEADERS)
	$(CC) -c $(CCOPT) psi_from_ncaco.c

vector_product.o: vector_product.c $(HEADERS)
	$(CC) -c $(CCOPT) vector_product.c

absolute_value.o: absolute_value.c $(HEADERS)
	$(CC) -c $(CCOPT) absolute_value.c

scalar_product.o: scalar_product.c $(HEADERS)
	$(CC) -c $(CCOPT) scalar_product.c

omega_from_cacnca.o: omega_from_cacnca.c $(HEADERS)
	$(CC) -c $(CCOPT) omega_from_cacnca.c

select_cis_trans.o: select_cis_trans.c $(HEADERS)
	$(CC) -c $(CCOPT) select_cis_trans.c

ramachandran.o: ramachandran.c $(HEADERS)
	$(CC) -c $(CCOPT) ramachandran.c

draw_ramachandran.o: draw_ramachandran.c $(HEADERS)
	$(CC) -c $(CCOPT) draw_ramachandran.c

rama_frame.o: rama_frame.c $(HEADERS)
	$(CC) -c $(CCOPT) rama_frame.c

rama_mouse.o: rama_mouse.c $(HEADERS)
	$(CC) -c $(CCOPT) rama_mouse.c

read_fasta.o: read_fasta.c $(HEADERS)
	$(CC) -c $(CCOPT) read_fasta.c

save_sequence.o: save_sequence.c $(HEADERS)
	$(CC) -c $(CCOPT) save_sequence.c

wheel.o: wheel.c $(HEADERS)
	$(CC) -c $(CCOPT) wheel.c

wheel_top_view.o: wheel_top_view.c $(HEADERS)
	$(CC) -c $(CCOPT) wheel_top_view.c

wheel_bottom_view.o: wheel_bottom_view.c $(HEADERS)
	$(CC) -c $(CCOPT) wheel_bottom_view.c

hydrophobicity.o: hydrophobicity.c $(HEADERS)
	$(CC) -c $(CCOPT) hydrophobicity.c

weight_colors.o: weight_colors.c $(HEADERS)
	$(CC) -c $(CCOPT) weight_colors.c

angle.o: angle.c $(HEADERS)
	$(CC) -c $(CCOPT) angle.c

radius.o: radius.c $(HEADERS)
	$(CC) -c $(CCOPT) radius.c

select_sphere.o: select_sphere.c $(HEADERS)
	$(CC) -c $(CCOPT) select_sphere.c

disulfide_bonds.o: disulfide_bonds.c $(HEADERS)
	$(CC) -c $(CCOPT) disulfide_bonds.c

venn.o: venn.c $(HEADERS)
	$(CC) -c $(CCOPT) venn.c

draw_venn.o: draw_venn.c $(HEADERS)
	$(CC) -c $(CCOPT) draw_venn.c

spacefill2.o: spacefill2.c $(HEADERS)
	$(CC) -c $(CCOPT) spacefill2.c

sp2_color.o: sp2_color.c $(HEADERS)
	$(CC) -c $(CCOPT) sp2_color.c

save_seq_w_numbers.o: save_seq_w_numbers.c $(HEADERS)
	$(CC) -c $(CCOPT) save_seq_w_numbers.c

ssb.o: ssb.c $(HEADERS)
	$(CC) -c $(CCOPT) ssb.c

select_alternate.o: select_alternate.c $(HEADERS)
	$(CC) -c $(CCOPT) select_alternate.c

select_model.o: select_model.c $(HEADERS)
	$(CC) -c $(CCOPT) select_model.c

color_model.o: color_model.c $(HEADERS)
	$(CC) -c $(CCOPT) color_model.c

window.o: window.c $(HEADERS)
	$(CC) -c $(CCOPT) window.c

plot.o: plot.c $(HEADERS)
	$(CC) -c $(CCOPT) plot.c

draw_plots.o: draw_plots.c $(HEADERS)
	$(CC) -c $(CCOPT) draw_plots.c

weighted_hydrophobicity.o: weighted_hydrophobicity.c $(HEADERS)
	$(CC) -c $(CCOPT) weighted_hydrophobicity.c

average_hydrophobicity.o: average_hydrophobicity.c $(HEADERS)
	$(CC) -c $(CCOPT) average_hydrophobicity.c

init_hyphob.o: init_hyphob.c $(HEADERS)
	$(CC) -c $(CCOPT) init_hyphob.c

plot_mouse.o: plot_mouse.c $(HEADERS)
	$(CC) -c $(CCOPT) plot_mouse.c

project_planes.o: project_planes.c $(HEADERS)
	$(CC) -c $(CCOPT) project_planes.c

draw_planes.o: draw_planes.c $(HEADERS)
	$(CC) -c $(CCOPT) draw_planes.c

plane_extent.o: plane_extent.c $(HEADERS)
	$(CC) -c $(CCOPT) plane_extent.c

rotate_plane.o: rotate_plane.c $(HEADERS)
	$(CC) -c $(CCOPT) rotate_plane.c

translate_plane.o: translate_plane.c $(HEADERS)
	$(CC) -c $(CCOPT) translate_plane.c

comm_move.o: comm_move.c $(HEADERS)
	$(CC) -c $(CCOPT) comm_move.c

plane.o: plane.c $(HEADERS)
	$(CC) -c $(CCOPT) plane.c

select_above.o: select_above.c $(HEADERS)
	$(CC) -c $(CCOPT) select_above.c

select_below.o: select_below.c $(HEADERS)
	$(CC) -c $(CCOPT) select_below.c

big2.o: big2.c $(HEADERS)
	$(CC) -c $(CCOPT) big2.c

scale.o: scale.c $(HEADERS)
	$(CC) -c $(CCOPT) scale.c

pattern.o: pattern.c $(HEADERS)
	$(CC) -c $(CCOPT) pattern.c

dock.o: dock.c $(HEADERS)
	$(CC) -c $(CCOPT) dock.c

align_normal.o: align_normal.c $(HEADERS)
	$(CC) -c $(CCOPT) align_normal.c

align_complexes.o: align_complexes.c $(HEADERS)
	$(CC) -c $(CCOPT) align_complexes.c

docking_window.o: docking_window.c $(HEADERS) docking.xpm
	$(CC) -c $(CCOPT) docking_window.c

docking_resize.o: docking_resize.c $(HEADERS)
	$(CC) -c $(CCOPT) docking_resize.c

docking_refresh.o: docking_refresh.c $(HEADERS)
	$(CC) -c $(CCOPT) docking_refresh.c

docking_project.o: docking_project.c $(HEADERS)
	$(CC) -c $(CCOPT) docking_project.c

list_exposed_polar.o: list_exposed_polar.c $(HEADERS)
	$(CC) -c $(CCOPT) list_exposed_polar.c

is_polar.o: is_polar.c $(HEADERS)
	$(CC) -c $(CCOPT) is_polar.c

representative_atoms.o: representative_atoms.c $(HEADERS)
	$(CC) -c $(CCOPT) representative_atoms.c

exclude_distant.o: exclude_distant.c $(HEADERS)
	$(CC) -c $(CCOPT) exclude_distant.c

draw_bottom.o: draw_bottom.c $(HEADERS)
	$(CC) -c $(CCOPT) draw_bottom.c

draw_top.o: draw_top.c $(HEADERS)
	$(CC) -c $(CCOPT) draw_top.c

copy_sequence.o: copy_sequence.c $(HEADERS)
	$(CC) -c $(CCOPT) copy_sequence.c

compare.o: compare.c $(HEADERS)
	$(CC) -c $(CCOPT) compare.c

draw_comparison.o: draw_comparison.c $(HEADERS)
	$(CC) -c $(CCOPT) draw_comparison.c

neighborhood.o: neighborhood.c $(HEADERS)
	$(CC) -c $(CCOPT) neighborhood.c

draw_balls2.o: draw_balls2.c $(HEADERS)
	$(CC) -c $(CCOPT) draw_balls2.c

comparison_mouse.o: comparison_mouse.c $(HEADERS)
	$(CC) -c $(CCOPT) comparison_mouse.c

structure.o: structure.c $(HEADERS)
	$(CC) -c $(CCOPT) structure.c

parse_sec_structure.o: parse_sec_structure.c $(HEADERS)
	$(CC) -c $(CCOPT) parse_sec_structure.c

copy_sec_structure.o: copy_sec_structure.c $(HEADERS)
	$(CC) -c $(CCOPT) copy_sec_structure.c

load_sec_structure.o: load_sec_structure.c $(HEADERS)
	$(CC) -c $(CCOPT) load_sec_structure.c

save_sec_structure.o: save_sec_structure.c $(HEADERS)
	$(CC) -c $(CCOPT) save_sec_structure.c

alloc_sec_structure.o: alloc_sec_structure.c $(HEADERS)
	$(CC) -c $(CCOPT) alloc_sec_structure.c

sec_structure_from.o: sec_structure_from.c $(HEADERS)
	$(CC) -c $(CCOPT) sec_structure_from.c

create.o: create.c $(HEADERS)
	$(CC) -c $(CCOPT) create.c

open_template_file.o: open_template_file.c $(HEADERS)
	$(CC) -c $(CCOPT) open_template_file.c

read_template_file.o: read_template_file.c $(HEADERS)
	$(CC) -c $(CCOPT) read_template_file.c

ex_template_residues.o: ex_template_residues.c $(HEADERS)
	$(CC) -c $(CCOPT) ex_template_residues.c

count_template_res.o: count_template_res.c $(HEADERS)
	$(CC) -c $(CCOPT) count_template_res.c

create_structure.o: create_structure.c $(HEADERS)
	$(CC) -c $(CCOPT) create_structure.c

required_atoms.o: required_atoms.c $(HEADERS)
	$(CC) -c $(CCOPT) required_atoms.c

copy_template_atoms.o: copy_template_atoms.c $(HEADERS)
	$(CC) -c $(CCOPT) copy_template_atoms.c

fix_chain.o: fix_chain.c $(HEADERS)
	$(CC) -c $(CCOPT) fix_chain.c

reset_phi.o: reset_phi.c $(HEADERS)
	$(CC) -c $(CCOPT) reset_phi.c

extract_hncac.o: extract_hncac.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_hncac.c

phi_from_hncac.o: phi_from_hncac.c $(HEADERS)
	$(CC) -c $(CCOPT) phi_from_hncac.c

rotate_atom.o: rotate_atom.c $(HEADERS)
	$(CC) -c $(CCOPT) rotate_atom.c

parallel_part.o: parallel_part.c $(HEADERS)
	$(CC) -c $(CCOPT) parallel_part.c

reset_psi.o: reset_psi.c $(HEADERS)
	$(CC) -c $(CCOPT) reset_psi.c

build_chain.o: build_chain.c $(HEADERS)
	$(CC) -c $(CCOPT) build_chain.c

extract_ncac.o: extract_ncac.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_ncac.c

translate_range.o: translate_range.c $(HEADERS)
	$(CC) -c $(CCOPT) translate_range.c

rotate_range.o: rotate_range.c $(HEADERS)
	$(CC) -c $(CCOPT) rotate_range.c

apply_structure.o: apply_structure.c $(HEADERS)
	$(CC) -c $(CCOPT) apply_structure.c

generate_phi_psi.o: generate_phi_psi.c $(HEADERS)
	$(CC) -c $(CCOPT) generate_phi_psi.c

extract_nca.o: extract_nca.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_nca.c

extract_c.o: extract_c.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_c.c

replace.o: replace.c $(HEADERS)
	$(CC) -c $(CCOPT) replace.c

extract_n.o: extract_n.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_n.c

refresh_backbone.o: refresh_backbone.c $(HEADERS)
	$(CC) -c $(CCOPT) refresh_backbone.c

edit.o: edit.c $(HEADERS)
	$(CC) -c $(CCOPT) edit.c

draw_edit_symbol.o: draw_edit_symbol.c $(HEADERS)
	$(CC) -c $(CCOPT) draw_edit_symbol.c

draw_squid.o: draw_squid.c $(HEADERS) squid.xpm
	$(CC) -c $(CCOPT) draw_squid.c

draw_seahorse.o: draw_seahorse.c $(HEADERS) seahorse.xpm
	$(CC) -c $(CCOPT) draw_seahorse.c

are_atoms_bound.o: are_atoms_bound.c $(HEADERS)
	$(CC) -c $(CCOPT) are_atoms_bound.c

bond_partners.o: bond_partners.c $(HEADERS)
	$(CC) -c $(CCOPT) bond_partners.c

edit_phi.o: edit_phi.c $(HEADERS)
	$(CC) -c $(CCOPT) edit_phi.c

edit_psi.o: edit_psi.c $(HEADERS)
	$(CC) -c $(CCOPT) edit_psi.c

extract_cac.o: extract_cac.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_cac.c

small_ramachandran.o: small_ramachandran.c $(HEADERS)
	$(CC) -c $(CCOPT) small_ramachandran.c

edit_omega.o: edit_omega.c $(HEADERS)
	$(CC) -c $(CCOPT) edit_omega.c

small_omega.o: small_omega.c $(HEADERS)
	$(CC) -c $(CCOPT) small_omega.c

extract_h.o: extract_h.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_h.c

extract_o.o: extract_o.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_o.c

psi_from_ncacn.o: psi_from_ncacn.c $(HEADERS)
	$(CC) -c $(CCOPT) psi_from_ncacn.c

is_standard.o: is_standard.c $(HEADERS)
	$(CC) -c $(CCOPT) is_standard.c

extract_four_atoms.o: extract_four_atoms.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_four_atoms.c

chi1_from_ncacbcg.o: chi1_from_ncacbcg.c $(HEADERS)
	$(CC) -c $(CCOPT) chi1_from_ncacbcg.c

chi1_from_ncacbsg.o: chi1_from_ncacbsg.c $(HEADERS)
	$(CC) -c $(CCOPT) chi1_from_ncacbsg.c

chi1_from_ncacbcg1.o: chi1_from_ncacbcg1.c $(HEADERS)
	$(CC) -c $(CCOPT) chi1_from_ncacbcg1.c

chi1_from_ncacbog.o: chi1_from_ncacbog.c $(HEADERS)
	$(CC) -c $(CCOPT) chi1_from_ncacbog.c

chi1_from_ncacbog1.o: chi1_from_ncacbog1.c $(HEADERS)
	$(CC) -c $(CCOPT) chi1_from_ncacbog1.c

chi2_from_cacbcgcd.o: chi2_from_cacbcgcd.c $(HEADERS)
	$(CC) -c $(CCOPT) chi2_from_cacbcgcd.c

chi2_from_cacbcgod1.o: chi2_from_cacbcgod1.c $(HEADERS)
	$(CC) -c $(CCOPT) chi2_from_cacbcgod1.c

chi2_from_cacbcgnd1.o: chi2_from_cacbcgnd1.c $(HEADERS)
	$(CC) -c $(CCOPT) chi2_from_cacbcgnd1.c

chi2_from_cacbcg1cd.o: chi2_from_cacbcg1cd.c $(HEADERS)
	$(CC) -c $(CCOPT) chi2_from_cacbcg1cd.c

chi2_from_cacbcgcd1.o: chi2_from_cacbcgcd1.c $(HEADERS)
	$(CC) -c $(CCOPT) chi2_from_cacbcgcd1.c

chi2_from_cacbcgsd.o: chi2_from_cacbcgsd.c $(HEADERS)
	$(CC) -c $(CCOPT) chi2_from_cacbcgsd.c

chi3_from_cbcgcdne.o: chi3_from_cbcgcdne.c $(HEADERS)
	$(CC) -c $(CCOPT) chi3_from_cbcgcdne.c

chi3_from_cbcgcdoe1.o: chi3_from_cbcgcdoe1.c $(HEADERS)
	$(CC) -c $(CCOPT) chi3_from_cbcgcdoe1.c

chi3_from_cbcgcdce.o: chi3_from_cbcgcdce.c $(HEADERS)
	$(CC) -c $(CCOPT) chi3_from_cbcgcdce.c

chi3_from_cbcgsdce.o: chi3_from_cbcgsdce.c $(HEADERS)
	$(CC) -c $(CCOPT) chi3_from_cbcgsdce.c

chi4_from_cgcdnecz.o: chi4_from_cgcdnecz.c $(HEADERS)
	$(CC) -c $(CCOPT) chi4_from_cgcdnecz.c

chi4_from_cgcdcenz.o: chi4_from_cgcdcenz.c $(HEADERS)
	$(CC) -c $(CCOPT) chi4_from_cgcdcenz.c

chi5_from_cdnecznh1.o: chi5_from_cdnecznh1.c $(HEADERS)
	$(CC) -c $(CCOPT) chi5_from_cdnecznh1.c

is_part_of_ring.o: is_part_of_ring.c $(HEADERS)
	$(CC) -c $(CCOPT) is_part_of_ring.c

edit_bond.o: edit_bond.c $(HEADERS)
	$(CC) -c $(CCOPT) edit_bond.c

edit_chi2.o: edit_chi2.c $(HEADERS)
	$(CC) -c $(CCOPT) edit_chi2.c

extract_cacb.o: extract_cacb.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_cacb.c

edit_chi1.o: edit_chi1.c $(HEADERS)
	$(CC) -c $(CCOPT) edit_chi1.c

extract_cbxg.o: extract_cbxg.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_cbxg.c

resize_complex.o: resize_complex.c $(HEADERS)
	$(CC) -c $(CCOPT) resize_complex.c

membrane.o: membrane.c $(HEADERS)
	$(CC) -c $(CCOPT) membrane.c

inform_user.o: inform_user.c $(HEADERS)
	$(CC) -c $(CCOPT) inform_user.c

alpha_membrane.o: alpha_membrane.c $(HEADERS)
	$(CC) -c $(CCOPT) alpha_membrane.c

extract_ca.o: extract_ca.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_ca.c

extract_vector.o: extract_vector.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_vector.c

alpha_membrane_center.o: alpha_membrane_center.c $(HEADERS)
	$(CC) -c $(CCOPT) alpha_membrane_center.c

init_membrane.o: init_membrane.c $(HEADERS)
	$(CC) -c $(CCOPT) init_membrane.c

alpha_normal.o: alpha_normal.c $(HEADERS)
	$(CC) -c $(CCOPT) alpha_normal.c

project_membranes.o: project_membranes.c $(HEADERS)
	$(CC) -c $(CCOPT) project_membranes.c

draw_membranes.o: draw_membranes.c $(HEADERS)
	$(CC) -c $(CCOPT) draw_membranes.c

rotate_membrane.o: rotate_membrane.c $(HEADERS)
	$(CC) -c $(CCOPT) rotate_membrane.c

translate_membrane.o: translate_membrane.c $(HEADERS)
	$(CC) -c $(CCOPT) translate_membrane.c

select_triplet.o: select_triplet.c $(HEADERS)
	$(CC) -c $(CCOPT) select_triplet.c

set.o: set.c $(HEADERS)
	$(CC) -c $(CCOPT) set.c

set_phi.o: set_phi.c $(HEADERS)
	$(CC) -c $(CCOPT) set_phi.c

set_psi.o: set_psi.c $(HEADERS)
	$(CC) -c $(CCOPT) set_psi.c

set_omega.o: set_omega.c $(HEADERS)
	$(CC) -c $(CCOPT) set_omega.c

set_chi1.o: set_chi1.c $(HEADERS)
	$(CC) -c $(CCOPT) set_chi1.c

set_chi2.o: set_chi2.c $(HEADERS)
	$(CC) -c $(CCOPT) set_chi2.c

set_chi3.o: set_chi3.c $(HEADERS)
	$(CC) -c $(CCOPT) set_chi3.c

extract_cgxd.o: extract_cgxd.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_cgxd.c

set_chi4.o: set_chi4.c $(HEADERS)
	$(CC) -c $(CCOPT) set_chi4.c

extract_cdxe.o: extract_cdxe.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_cdxe.c

set_chi5.o: set_chi5.c $(HEADERS)
	$(CC) -c $(CCOPT) set_chi5.c

extract_necz.o: extract_necz.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_necz.c

pick_bond.o: pick_bond.c $(HEADERS)
	$(CC) -c $(CCOPT) pick_bond.c

nice_bonds.o: nice_bonds.c $(HEADERS)
	$(CC) -c $(CCOPT) nice_bonds.c

covalent.o: covalent.c $(HEADERS)
	$(CC) -c $(CCOPT) covalent.c

covalent2.o: covalent2.c $(HEADERS)
	$(CC) -c $(CCOPT) covalent2.c

half_nice_bond.o: half_nice_bond.c $(HEADERS)
	$(CC) -c $(CCOPT) half_nice_bond.c

probe.o: probe.c $(HEADERS)
	$(CC) -c $(CCOPT) probe.c

small_atoms.o: small_atoms.c $(HEADERS)
	$(CC) -c $(CCOPT) small_atoms.c

small_atoms2.o: small_atoms2.c $(HEADERS)
	$(CC) -c $(CCOPT) small_atoms2.c

draw_sticks.o: draw_sticks.c $(HEADERS)
	$(CC) -c $(CCOPT) draw_sticks.c

half_stick.o: half_stick.c $(HEADERS)
	$(CC) -c $(CCOPT) half_stick.c

command_refresh.o: command_refresh.c $(HEADERS)
	$(CC) -c $(CCOPT) command_refresh.c

acceptor_dipole.o: acceptor_dipole.c $(HEADERS)
	$(CC) -c $(CCOPT) acceptor_dipole.c

donor_dipole.o: donor_dipole.c $(HEADERS)
	$(CC) -c $(CCOPT) donor_dipole.c

sticks.o: sticks.c $(HEADERS)
	$(CC) -c $(CCOPT) sticks.c

balls.o: balls.c $(HEADERS)
	$(CC) -c $(CCOPT) balls.c

blur.o: blur.c $(HEADERS)
	$(CC) -c $(CCOPT) blur.c

blur_main_window.o: blur_main_window.c $(HEADERS)
	$(CC) -c $(CCOPT) blur_main_window.c

group.o: group.c $(HEADERS)
	$(CC) -c $(CCOPT) group.c

beta_membrane.o: beta_membrane.c $(HEADERS)
	$(CC) -c $(CCOPT) beta_membrane.c

extract_cg.o: extract_cg.c $(HEADERS)
	$(CC) -c $(CCOPT) extract_cg.c

select_tm.o: select_tm.c $(HEADERS)
	$(CC) -c $(CCOPT) select_tm.c

parse_pattern.o: parse_pattern.c $(HEADERS)
	$(CC) -c $(CCOPT) parse_pattern.c

select_pattern.o: select_pattern.c $(HEADERS)
	$(CC) -c $(CCOPT) select_pattern.c

hyphob_function1.o: hyphob_function1.c $(HEADERS)
	$(CC) -c $(CCOPT) hyphob_function1.c

hyphob_function2.o: hyphob_function2.c $(HEADERS)
	$(CC) -c $(CCOPT) hyphob_function2.c

hyphob_function3.o: hyphob_function3.c $(HEADERS)
	$(CC) -c $(CCOPT) hyphob_function3.c

hyphob_function4.o: hyphob_function4.c $(HEADERS)
	$(CC) -c $(CCOPT) hyphob_function4.c

hyphob_function5.o: hyphob_function5.c $(HEADERS)
	$(CC) -c $(CCOPT) hyphob_function5.c

hyphob_function6.o: hyphob_function6.c $(HEADERS)
	$(CC) -c $(CCOPT) hyphob_function6.c

hyphob_function7.o: hyphob_function7.c $(HEADERS)
	$(CC) -c $(CCOPT) hyphob_function7.c

color_size.o: color_size.c $(HEADERS)
	$(CC) -c $(CCOPT) color_size.c

command_dump.o: command_dump.c $(HEADERS)
	$(CC) -c $(CCOPT) command_dump.c

is_aromatic.o: is_aromatic.c $(HEADERS)
	$(CC) -c $(CCOPT) is_aromatic.c

select_2c3.o: select_2c3.c $(HEADERS)
	$(CC) -c $(CCOPT) select_2c3.c

select_2cz3.o: select_2cz3.c $(HEADERS)
	$(CC) -c $(CCOPT) select_2cz3.c

select_2nz3.o: select_2nz3.c $(HEADERS)
	$(CC) -c $(CCOPT) select_2nz3.c

select_3c4.o: select_3c4.c $(HEADERS)
	$(CC) -c $(CCOPT) select_3c4.c

select_3cz4.o: select_3cz4.c $(HEADERS)
	$(CC) -c $(CCOPT) select_3cz4.c

select_3nz4.o: select_3nz4.c $(HEADERS)
	$(CC) -c $(CCOPT) select_3nz4.c

select_3c5.o: select_3c5.c $(HEADERS)
	$(CC) -c $(CCOPT) select_3c5.c

select_3cz5.o: select_3cz5.c $(HEADERS)
	$(CC) -c $(CCOPT) select_3cz5.c

select_3nz5.o: select_3nz5.c $(HEADERS)
	$(CC) -c $(CCOPT) select_3nz5.c

select_3p5.o: select_3p5.c $(HEADERS)
	$(CC) -c $(CCOPT) select_3p5.c

select_4c5.o: select_4c5.c $(HEADERS)
	$(CC) -c $(CCOPT) select_4c5.c

select_4cz5.o: select_4cz5.c $(HEADERS)
	$(CC) -c $(CCOPT) select_4cz5.c

select_4nz5.o: select_4nz5.c $(HEADERS)
	$(CC) -c $(CCOPT) select_4nz5.c

select_3c6.o: select_3c6.c $(HEADERS)
	$(CC) -c $(CCOPT) select_3c6.c

select_3cz6.o: select_3cz6.c $(HEADERS)
	$(CC) -c $(CCOPT) select_3cz6.c

select_3nz6.o: select_3nz6.c $(HEADERS)
	$(CC) -c $(CCOPT) select_3nz6.c

select_4c6.o: select_4c6.c $(HEADERS)
	$(CC) -c $(CCOPT) select_4c6.c

select_4cz6.o: select_4cz6.c $(HEADERS)
	$(CC) -c $(CCOPT) select_4cz6.c

select_4nz6.o: select_4nz6.c $(HEADERS)
	$(CC) -c $(CCOPT) select_4nz6.c

select_4p6.o: select_4p6.c $(HEADERS)
	$(CC) -c $(CCOPT) select_4p6.c

select_5c6.o: select_5c6.c $(HEADERS)
	$(CC) -c $(CCOPT) select_5c6.c
                
select_5cz6.o: select_5cz6.c $(HEADERS)
	$(CC) -c $(CCOPT) select_5cz6.c

select_5nz6.o: select_5nz6.c $(HEADERS)
	$(CC) -c $(CCOPT) select_5nz6.c

select_4c7.o: select_4c7.c $(HEADERS)
	$(CC) -c $(CCOPT) select_4c7.c

select_4cz7.o: select_4cz7.c $(HEADERS)
	$(CC) -c $(CCOPT) select_4cz7.c

select_4nz7.o: select_4nz7.c $(HEADERS)
	$(CC) -c $(CCOPT) select_4nz7.c

select_5c7.o: select_5c7.c $(HEADERS)
	$(CC) -c $(CCOPT) select_5c7.c

select_5cz7.o: select_5cz7.c $(HEADERS)
	$(CC) -c $(CCOPT) select_5cz7.c

select_5nz7.o: select_5nz7.c $(HEADERS)
	$(CC) -c $(CCOPT) select_5nz7.c

select_5p7.o: select_5p7.c $(HEADERS)
	$(CC) -c $(CCOPT) select_5p7.c

select_6p7.o: select_6p7.c $(HEADERS)
	$(CC) -c $(CCOPT) select_6p7.c

select_4c9.o: select_4c9.c $(HEADERS)
	$(CC) -c $(CCOPT) select_4c9.c

select_4cz9.o: select_4cz9.c $(HEADERS)
	$(CC) -c $(CCOPT) select_4cz9.c

select_4nz9.o: select_4nz9.c $(HEADERS)
	$(CC) -c $(CCOPT) select_4nz9.c

select_5c9.o: select_5c9.c $(HEADERS)
	$(CC) -c $(CCOPT) select_5c9.c

select_5cz9.o: select_5cz9.c $(HEADERS)
	$(CC) -c $(CCOPT) select_5cz9.c

select_5nz9.o: select_5nz9.c $(HEADERS)
	$(CC) -c $(CCOPT) select_5nz9.c

color_weighted.o: color_weighted.c $(HEADERS)
	$(CC) -c $(CCOPT) color_weighted.c


