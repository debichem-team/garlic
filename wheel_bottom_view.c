/* Copyright (C) 2002-2003 Damir Zucic */

/*=============================================================================

				wheel_bottom_view.c

Purpose:
	Draw  the helical wheel clockwise.  This wheel corresponds to the
	bottom view of the helix.

Input:
	(1) Pointer to RuntimeS structure, with some runtime data.
	(2) Pointer to GUIS structure, with GUI data.
	(3) Pointer to NearestAtomS structure, with information about the
	    atom occupying the given pixel.
	(4) The number of pixels in the main window free area.
	(5) The refreshI, used to check the  NearestAtomS associated with
	    a given pixel. It is currently unused in this function.

Output:
	(1) Helical wheel drawn clockwise to the main window.
	(2) Return value.

Return value:
	(1) The number of residues used to prepare the helical wheel.

Notes:
	(1) The initial drawing destination is the hidden pixmap, and the
	    content of this pixmap is  copied  later to  the main window.

	(2) Labels (residue names) are drawn for the last 18 residues.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>
#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		RefreshPixmap_ (Display *, Pixmap, GC,
				unsigned int, unsigned int, unsigned long);
int		ParseColor_ (RGBS *, unsigned long *, GUIS *, char *, char *);
double		Hydrophobicity_ (char *, int, int);
unsigned long	WeightColors_ (unsigned long, unsigned long, double, GUIS *);
int		DrawTitles_ (RuntimeS *, GUIS *);

/*======draw helical wheel clockwise:========================================*/

size_t WheelBottomView_ (RuntimeS *runtimeSP, GUIS *guiSP,
			 NearestAtomS *nearest_atomSP, size_t pixelsN,
			 unsigned int refreshI)
{
int		max_length;
int		residuesN, residueI;
RGBS		rgbS;
unsigned long	phob_colorID, neutral_colorID, phil_colorID;
int		scaleI;
double		min_hyphob, max_hyphob, threshold_hyphob;
double		delta_hyphob1, delta_hyphob2;
double		inverse_range1, inverse_range2;
char		*residue_nameP;
char		residue_nameA[RESNAMESIZE];
double		hyphob;
double		scale_factor;
unsigned long	residue_colorID;
int		square_width, screen_margin, screen_radius, half_screen_radius;
int		screen_x0, screen_y0;
int		screen_x1, screen_y1, screen_x2, screen_y2;
int		width, height;
int		helix_step, arc_start, arc_extent, arc_center;
int		serialI, range_startI, range_endI, residues_in_wheelN = 0;
int		wheelI = 0;
double		angle, delta_x, delta_y;
int		label_length;
char		labelA[SHORTSTRINGSIZE];
int		text_width, text_height;
int		ref_angle;
int		inner_screen_radius, thickness1, thickness2;
double		alpha0, delta_alpha, alpha1, alpha2;
XPoint		pointSA[7];
unsigned long	far_colorID, near_colorID;

/* The number of residues in sequence buffer: */
residuesN = runtimeSP->residuesN;
if (residuesN == 0)
	{
	strcpy (runtimeSP->messageA, "The sequence buffer is empty!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return 0;
	}

/* The wheel_clockwiseF decides in which direction the sequence should be scanned: */

/* The maximal residue name length: */
max_length = RESNAMESIZE - 1;

/* Residue array index range: */
range_startI = runtimeSP->range_startI;
range_endI   = runtimeSP->range_endI;

/* Count the number of residues in the specified range: */
for (residueI = residuesN - 1; residueI >= 0; residueI--)
	{
	/* Check is the residue array index inside the range: */
	if ((residueI < range_startI) || (residueI > range_endI)) continue;

	/* Increase the count of residues if current */
	/* residue  belongs to  the specified range: */
	residues_in_wheelN++;
	}

/* Check the number of residues in a wheel: */
if (residues_in_wheelN == 0)
	{
	strcpy (runtimeSP->messageA, "The specified range is empty!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return 0;
	}

/* Refresh the hidden pixmap, where drawing will be done: */
RefreshPixmap_ (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[0],
		guiSP->control_winS.x0, guiSP->input_winS.y0,
		guiSP->main_winS.bg_colorID);

/*------draw arcs:-----------------------------------------------------------*/

/* Initialize colors used for hydrophobicity: */
ParseColor_ (&rgbS, &phob_colorID,    guiSP, "RGB:FFFF/0000/0000", "white");
ParseColor_ (&rgbS, &neutral_colorID, guiSP, "RGB:FFFF/FFFF/FFFF", "white");
ParseColor_ (&rgbS, &phil_colorID,    guiSP, "RGB:0000/0000/FFFF", "black");

/* Hydrophobicity scale index: */
scaleI = runtimeSP->hydrophobicity_scaleI;

/* Minimal, maximal and threshold hydrophobicity for current complex: */
min_hyphob =       Hydrophobicity_ ("", scaleI, 1);
max_hyphob =       Hydrophobicity_ ("", scaleI, 2);
threshold_hyphob = Hydrophobicity_ ("", scaleI, 4);

/* Inverse hydrophobicity ranges: */
delta_hyphob1 = max_hyphob - threshold_hyphob;
if (delta_hyphob1 != 0.0) inverse_range1 = 1.0 / delta_hyphob1;
else inverse_range1 = 0.0;
delta_hyphob2 = threshold_hyphob - min_hyphob;
if (delta_hyphob2 != 0.0) inverse_range2 = 1.0 / delta_hyphob2;
else inverse_range2 = 0.0;

/* Prepare geometric parameters: */
square_width = guiSP->main_win_free_area_height;
if ((int) guiSP->main_win_free_area_width < square_width)
	{
	square_width = guiSP->main_win_free_area_width;
	}
screen_margin = guiSP->main_winS.text_line_height +
		guiSP->main_winS.text_line_height / 2 + 2;
screen_radius = square_width / 2 - screen_margin;
if (screen_radius < 0) screen_radius = square_width / 2;
half_screen_radius = screen_radius / 2;
screen_x1 = guiSP->main_win_free_area_width  / 2;
screen_y1 = guiSP->main_win_free_area_height / 2;
screen_x0 = screen_x1 - screen_radius;
screen_y0 = screen_y1 - screen_radius;
width = 2 * screen_radius;
height = width;

/* Prepare the helix step angle (degrees * 64): */
helix_step = runtimeSP->helix_step_angle * 64;

/* Prepare the arc extent angle, (degrees * 64): */
arc_extent = (int) runtimeSP->arc_angle * 64;

/* Initialize the arc start angle, (degrees * 64): */
arc_start = 5760 - arc_extent / 2;

/* Prepare the text color: */
XSetForeground (guiSP->displaySP, guiSP->theGCA[0],
		guiSP->main_winS.fg_colorID);

/* Prepare the arc edge color: */
XSetForeground (guiSP->displaySP, guiSP->theGCA[1], guiSP->black_colorID);

/* Scan the sequence: */
for (residueI = residuesN - 1; residueI >= 0; residueI--)
	{
	/* Pointer to the current residue name: */
	residue_nameP = runtimeSP->sequenceP + residueI * max_length;

	/* Check is the residue array index inside the range: */
	if ((residueI < range_startI) || (residueI > range_endI)) continue;

	/* Copy the residue name: */
	strncpy (residue_nameA, residue_nameP, max_length);
	residue_nameA[max_length] = '\0';

	/* The corresponding hydrophobicity: */
	hyphob = Hydrophobicity_ (residue_nameA, scaleI, 0);

	/* If hydrophobicity is below the threshold value: */
	if (hyphob <= threshold_hyphob)
		{
		scale_factor = (threshold_hyphob - hyphob) * inverse_range2;
		residue_colorID = WeightColors_ (neutral_colorID, phil_colorID,
						 scale_factor, guiSP);
		}

	/* If hydrophobicity is above the threshold value: */
	else
		{
		scale_factor = (hyphob - threshold_hyphob) * inverse_range1;
		residue_colorID = WeightColors_ (neutral_colorID, phob_colorID,
						 scale_factor, guiSP);
		}

	/* Prepare the color: */
	XSetForeground (guiSP->displaySP, guiSP->theGCA[2], residue_colorID);

	/* Draw filled arc: */
	XFillArc (guiSP->displaySP,
		  guiSP->main_hidden_pixmapID,
		  guiSP->theGCA[2],
		  screen_x0, screen_y0,
		  width, height,
		  arc_start, arc_extent);

	/* Draw the outer arc edge: */
	XDrawArc (guiSP->displaySP,
		  guiSP->main_hidden_pixmapID,
		  guiSP->theGCA[1],
		  screen_x0, screen_y0,
		  width, height,
		  arc_start, arc_extent);

	/* Draw two straight lines from the arc origin to the edge: */
	angle = DEG_TO_RAD * (double) arc_start / 64.0;
	delta_x = (double) screen_radius * cos (angle);
	delta_y = (double) screen_radius * sin (angle);
	delta_y *= -1;
	screen_x2 = screen_x1 + (int) delta_x;
	screen_y2 = screen_y1 + (int) delta_y;
	XDrawLine (guiSP->displaySP,
		   guiSP->main_hidden_pixmapID,
		   guiSP->theGCA[1],
		   screen_x1, screen_y1,
		   screen_x2, screen_y2);
	angle = DEG_TO_RAD * (double) (arc_start + arc_extent) / 64.0;
	delta_x = (double) screen_radius * cos (angle);
	delta_y = (double) screen_radius * sin (angle);
	delta_y *= -1;
	screen_x2 = screen_x1 + (int) delta_x;
	screen_y2 = screen_y1 + (int) delta_y;
	XDrawLine (guiSP->displaySP,
		   guiSP->main_hidden_pixmapID,
		   guiSP->theGCA[1],
		   screen_x1, screen_y1,
		   screen_x2, screen_y2);

	/* Draw labels for the last 18 residues (not for all!): */
	if (residues_in_wheelN - wheelI <= 18)
		{
		arc_center = arc_start + arc_extent / 2;
		angle = DEG_TO_RAD * (double) arc_center / 64.0;
		delta_x = (double) screen_radius * cos (angle);
		delta_y = (double) screen_radius * sin (angle);
		delta_y *= -1;
		screen_x2 = screen_x1 + (int) delta_x;
		screen_y2 = screen_y1 + (int) delta_y;
		serialI = *(runtimeSP->serialIP + residueI);
		sprintf (labelA, "%d:%s", serialI, residue_nameA);
		label_length = strlen (labelA);
		text_width = XTextWidth (guiSP->main_winS.fontSP,
					 labelA, label_length);
		text_height = guiSP->main_winS.font_height;
		if (screen_x2 < screen_x1) screen_x2 -= text_width - 1;
		if (screen_y2 > screen_y1) screen_y2 += text_height;
		else screen_y2 -= 2;
		XDrawString (guiSP->displaySP,
			     guiSP->main_hidden_pixmapID,
			     guiSP->theGCA[0],
			     screen_x2, screen_y2,
			     labelA, label_length);
		}

	/* Update the arc start angle: */
	arc_start += helix_step;
	if (arc_start > 23040) arc_start = arc_start - 23040;

	/* Update the wheel index: */
	wheelI++;
	}

/*------draw the hole in the wheel center:-----------------------------------*/

/* Prepare geometric parameters: */
screen_x0 = guiSP->main_win_free_area_width  / 2 - half_screen_radius;
screen_y0 = guiSP->main_win_free_area_height / 2 - half_screen_radius;
width = 2 * half_screen_radius;
height = width;
XSetForeground (guiSP->displaySP, guiSP->theGCA[0],
		guiSP->main_winS.bg_colorID);
XFillArc (guiSP->displaySP, guiSP->main_hidden_pixmapID, guiSP->theGCA[0],
	  screen_x0, screen_y0, width, height, 0, 23040);

/*------draw polygons which represent backbone:------------------------------*/

/* This should be done only if angle is not equal to zero: */

if (helix_step != 0)
	{
	/* Prepare far and near color: */
	ParseColor_ (&rgbS, &far_colorID, guiSP,
		     "RGB:4444/4444/4444", "white");
	ParseColor_ (&rgbS, &near_colorID, guiSP,
		     "RGB:FFFF/FFFF/FFFF", "white");
	if (residuesN < 3)
		{
		ParseColor_ (&rgbS, &far_colorID, guiSP,
			     "RGB:AAAA/AAAA/AAAA", "white");
		}

	/* Prepare the polygon edge color: */
	XSetForeground (guiSP->displaySP,
			guiSP->theGCA[2],
			guiSP->black_colorID);

	/* Prepare half of the helix step angle in radians: */
	delta_alpha = DEG_TO_RAD * (double) helix_step / 128.0;

	/* Prepare the inner radius: */
	inner_screen_radius = half_screen_radius * cos (delta_alpha);

	/* Prepare two thickness parameters: */
	thickness1 = screen_radius / 10;
	if (thickness1 < 4) thickness1 = 4;
	thickness2 = thickness1 * cos (delta_alpha);
	if (thickness2 == 0) thickness2 = thickness1;

	/* Initialize the reference angle (X11 units): */
	ref_angle = 5760;

	/* Reset the wheel index: */
	wheelI = 0;

	/* Scan the sequence again: */
	for (residueI = residuesN - 1; residueI >= 0; residueI--)
		{
		/* Check is this array index inside the range: */
		if ((residueI < range_startI) || (residueI > range_endI))
			{
			continue;
			}

		/* Convert the reference angle to radians: */
		alpha0 = DEG_TO_RAD * (double) ref_angle / 64.0;

		/* Prepare two additional angles: */
		alpha1 = alpha0 - delta_alpha;
		alpha2 = alpha0 + delta_alpha;

		/* Now prepare seven points. */

		/* The first point: */
		delta_x = (double) half_screen_radius * cos (alpha0);
		delta_y = (double) half_screen_radius * sin (alpha0);
		delta_y *= -1;
		screen_x2 = screen_x1 + (int) delta_x;
		screen_y2 = screen_y1 + (int) delta_y;
		pointSA[0].x = (short) screen_x2;
		pointSA[0].y = (short) screen_y2;

		/* The second point: */
		delta_x = (double) inner_screen_radius * cos (alpha1);
		delta_y = (double) inner_screen_radius * sin (alpha1);
		delta_y *= -1;
		screen_x2 = screen_x1 + (int) delta_x;
		screen_y2 = screen_y1 + (int) delta_y;
		pointSA[1].x = (short) screen_x2;
		pointSA[1].y = (short) screen_y2;

		/* The third point: */
		delta_x = (double) (inner_screen_radius + thickness2) *
			  cos (alpha1);
		delta_y = (double) (inner_screen_radius + thickness2) *
			  sin (alpha1);
		delta_y *= -1;
		screen_x2 = screen_x1 + (int) delta_x;
		screen_y2 = screen_y1 + (int) delta_y;
		pointSA[2].x = (short) screen_x2;
		pointSA[2].y = (short) screen_y2;

		/* The fourth point: */
		delta_x = (double) (half_screen_radius + thickness1) *
			  cos (alpha0);
		delta_y = (double) (half_screen_radius + thickness1) *
			  sin (alpha0);
		delta_y *= -1;
		screen_x2 = screen_x1 + (int) delta_x;
		screen_y2 = screen_y1 + (int) delta_y;
		pointSA[3].x = (short) screen_x2;
		pointSA[3].y = (short) screen_y2;

		/* The fifth point: */
		delta_x = (double) (inner_screen_radius + thickness2) *
			  cos (alpha2);
		delta_y = (double) (inner_screen_radius + thickness2) *
			  sin (alpha2);
		delta_y *= -1;
		screen_x2 = screen_x1 + (int) delta_x;
		screen_y2 = screen_y1 + (int) delta_y;
		pointSA[4].x = (short) screen_x2;
		pointSA[4].y = (short) screen_y2;

		/* The sixth point: */
		delta_x = (double) inner_screen_radius * cos (alpha2);
		delta_y = (double) inner_screen_radius * sin (alpha2);
		delta_y *= -1;
		screen_x2 = screen_x1 + (int) delta_x;
		screen_y2 = screen_y1 + (int) delta_y;
		pointSA[5].x = (short) screen_x2;
		pointSA[5].y = (short) screen_y2;

		/* The seventh point is a copy of the first point: */
		pointSA[6].x = pointSA[0].x;
		pointSA[6].y = pointSA[0].y;

		/* Prepare the polygon interior color: */
		if (residues_in_wheelN > 1)
			{
			scale_factor = (double) wheelI /
				       (double) (residues_in_wheelN - 1);
			}
		else scale_factor = 0.0;
                residue_colorID = WeightColors_ (far_colorID, near_colorID,
						 scale_factor, guiSP);
		XSetForeground (guiSP->displaySP,
				guiSP->theGCA[2],
				residue_colorID);

		/* Draw the filled polygon: */
		XFillPolygon (guiSP->displaySP,
			      guiSP->main_hidden_pixmapID,
			      guiSP->theGCA[2],
			      pointSA, 6, Complex, CoordModeOrigin);

		/* Draw the polygon edge: */
		XDrawLines (guiSP->displaySP,
			    guiSP->main_hidden_pixmapID,
			    guiSP->theGCA[1],
			    pointSA, 7, CoordModeOrigin);

	        /* Update the reference angle (X11 units): */
		ref_angle += helix_step;
		if (ref_angle > 23040) ref_angle = ref_angle - 23040;

		/* Update the wheel index: */
		wheelI++;
		}
	}

/*------draw titles:---------------------------------------------------------*/

/* Draw titles: */
DrawTitles_ (runtimeSP, guiSP);

/*------copy the prepared image to the main window:--------------------------*/

/* Copy hidden pixmap to the main window: */
XCopyArea (guiSP->displaySP,
	   guiSP->main_hidden_pixmapID, guiSP->main_winS.ID,
	   guiSP->theGCA[0],
	   0, 0,
	   guiSP->main_win_free_area_width, guiSP->main_win_free_area_height,
	   0, 0);

return (size_t) residuesN;
}

/*===========================================================================*/


