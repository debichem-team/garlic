/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				fading.c

Purpose:
	Prepare the left, middle and right color for each atom in a complex.
	These are the only colors required by fast drawing styles. Some slow
	drawing styles may require additional colors. The AtomS structure is
	not capable  to  store  these  additional  colors,  so they  are not
	prepared here. Colors for hydrogen bonds are  not generated here. In
	fact,  this function does almost nothing - it just calls  the proper
	function which takes care about color fading.

Input:
	(1) Pointer to MolComplexS structure.
	(2) Number of macromolecular complexes.
	(3) Pointer to GUIS structure.

Output:
	(1) Colors (pixel values) initialized  for each atom in the complex.
	(2) Return value.

Return value:
	The number  of atoms which  have colors  different from near and far
	color.

=============================================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

size_t		NoFading_ (MolComplexS *, GUIS *);
size_t		PlanarFading_ (MolComplexS *, GUIS *);
size_t		SphereFading_ (MolComplexS *, GUIS *);
size_t		HalfSphereFading_ (MolComplexS *, GUIS *);
size_t		CylinFading_ (MolComplexS *, GUIS *);
size_t		HalfCylinFading_ (MolComplexS *, GUIS *);

/*======prepare atomic colors (apply fading):================================*/

size_t Fading_ (MolComplexS *mol_complexSP, int mol_complexesN, GUIS *guiSP)
{
size_t		atoms_in_sandwichN = 0;
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;

/* Check every macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/** Pointer to the current macromolecular complex: **/
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/** Do not prepare new colors if position has not changed: **/
	if (curr_mol_complexSP->position_changedF == 0) continue;

	/** Apply the proper fading: **/
	switch (curr_mol_complexSP->fading_modeI)
		{
		/*** No fading at all: ***/
		case 0:
			atoms_in_sandwichN +=
				NoFading_ (curr_mol_complexSP, guiSP);
			break;

		/*** Planar: ***/
		case 1:
			atoms_in_sandwichN +=
				PlanarFading_ (curr_mol_complexSP, guiSP);
			break;

		/*** Spherical: ***/
		case 2:
			atoms_in_sandwichN +=
				SphereFading_ (curr_mol_complexSP, guiSP);
			break;

		/*** Semi-spherical: ***/
		case 3:
			atoms_in_sandwichN +=
				HalfSphereFading_ (curr_mol_complexSP, guiSP);
			break;

		/*** Cylindrical: ***/
		case 4:
			atoms_in_sandwichN +=
				CylinFading_ (curr_mol_complexSP, guiSP);
			break;

		/*** Semi-cylindrical: ***/
		case 5:
			atoms_in_sandwichN +=
				HalfCylinFading_ (curr_mol_complexSP, guiSP);
			break;

		/*** Unknown fading mode: ***/
		default:
			;
		}
	}

/* Return the total number of atoms between the fading surfaces: */
return atoms_in_sandwichN;
}

/*===========================================================================*/


