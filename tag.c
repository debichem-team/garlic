/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				tag.c

Purpose:
	Execute tag command:  add the tag to the macromolecular complex
	which is currently handled. The maximal number of characters in
	the tag (including the terminal zero) is equal to TAGSIZE.

Input:
	(1) Pointer to MolComplexS structure.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to ConfigS structure.
	(5) Pointer to GUIS structure.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.

Output:
	(1) The specified tag written or hidden.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

Notes:
	(1) This function uses the original command string, to preserve
	    case in the tag string. The first token is the keyword tag.
	    The second token is  either the string,  enclosed in duoble
	    quotes, or the keyword OFF. The tag will be assigned to the
	    macromolecular complex which is  currently caught and drawn
	    above this complex. It moves along with the complex.

            -----------------------------------------------------------
            | Example                                Purpose          |
            -----------------------------------------------------------
	    | TAG "PORIN"  |  Add the tag to currently caught complex |
	    -----------------------------------------------------------
	    | TAG OFF      |  Hide the tag                            |
	    -----------------------------------------------------------

========includes:============================================================*/

#include <stdio.h>

#include <string.h>
#include <ctype.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute tag command:=================================================*/

int Tag_ (MolComplexS *mol_complexSP, int mol_complexesN,
	  RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
	  NearestAtomS *nearest_atomSP, size_t pixelsN,
	  unsigned int *refreshIP)
{
char		*stringP;
char		tokenA[STRINGSIZE];
char		*remainderP;
char		token_copyA[STRINGSIZE];
char		*P;
int		n;
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;
size_t		atomsN;
char		*dummyP;

/* Skip the first token: */
stringP = ExtractToken_ (tokenA, STRINGSIZE,
			 runtimeSP->curr_commandA, " \t\n");
if (!stringP) return ERROR_TAG;

/* Extract the second token for the first time: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, stringP, " \t\n");
if (!remainderP)
	{
	strcpy (runtimeSP->messageA, "Missing tag!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_TAG;
	}

/* Convert token to uppercase: */
strncpy (token_copyA, tokenA, STRINGSIZE - 1);
P = token_copyA;
while ((n = *P++) != '\0') *(P - 1) = toupper (n);

/* If second token is the keyword OFF, hide tag for all caught complexes: */
if (strncmp (token_copyA, "OFF", 3) == 0)
	{
	/** Hide tag for each caught complex: **/
	for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
		{
		/*** Pointer to the current macromolecular complex: ***/
		curr_mol_complexSP = mol_complexSP + mol_complexI;

		/*** Check is the current macromolecular complex caught: ***/
		if (curr_mol_complexSP->catchF == 0) continue;

		/*** Check the number of atoms in a macromol. complex: ***/
		atomsN = curr_mol_complexSP->atomsN;
		if (atomsN == 0) continue;

		/*** Hide tag (set flag to zero): ***/
		curr_mol_complexSP->tagF = 0;
		}

	/** Refresh the main window: **/
	(*refreshIP)++;
	MainRefresh_ (mol_complexSP, mol_complexesN,
		      runtimeSP, configSP, guiSP,
		      nearest_atomSP, pixelsN, *refreshIP);

	/** Refresh the control window: **/
	ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
			 configSP, guiSP);

	/** Return to the caller: **/
	return COMMAND_TAG;
	}

/* Extract the tag: */

/** At least one (single or double) quote should be present: **/
if ((strstr (stringP, "\"") == NULL) && (strstr (stringP, "'")  == NULL))
	{
	strcpy (runtimeSP->messageA, "The tag should be enclosed in qoutes!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_TAG;
	}

/** Extract the second token again (the string enclosed in qoutes): **/
remainderP = strstr (stringP, "\"") + 1;
dummyP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, "\"");

/* Assign (copy) this tag to each caught macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/** Pointer to the current macromolecular complex: **/
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/** Check is the current macromolecular complex caught: **/
	if (curr_mol_complexSP->catchF == 0) continue;

	/** Check the number of atoms in a macromol. complex: **/
	atomsN = curr_mol_complexSP->atomsN;
	if (atomsN == 0) continue;

	/** Assign (copy) tag: **/
	strncpy (curr_mol_complexSP->tagA, tokenA, TAGSIZE - 1);
	curr_mol_complexSP->tagA[TAGSIZE - 1] = '\0';

	/** Set flag: **/
	curr_mol_complexSP->tagF = 1;
	}

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return the command code: */
return COMMAND_TAG;
}

/*===========================================================================*/


