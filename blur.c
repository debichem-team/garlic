/* Copyright (C) 2003 Damir Zucic */

/*=============================================================================

				blur.c

Purpose:
	Execute blur command: set blur flag and define blur rectangle size.
	Blur will not affect labels, tags,  titles and the small coordinate
	system in the top left corner.

Input:
	(1) Pointer to MolComplexS structure.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to ConfigS structure.
	(5) Pointer to GUIS structure.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
	(9) Pointer to  the remainder of  the command string.  This command
	    may be given without any keyword,  with the keyword OFF or with
	    two positive integers (blur rectangle width and height).

Output:
	(1) The blur flag set and (on request) the blur rectangle width and
	    height changed.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
int		ExtractTwoIntegers_ (int *, int *, char *);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute blur command:================================================*/

int Blur_ (MolComplexS *mol_complexSP, int mol_complexesN,
	   RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
	   NearestAtomS *nearest_atomSP, size_t pixelsN,
	   unsigned int *refreshIP, char *stringP)
{
char		*remainderP;
char		tokenA[SHORTSTRINGSIZE];
char		*P;
int		n;
int		width, height;

/* Set the blur flag to one. This may change */
/* later,  if the keyword OFF will be found. */
runtimeSP->blurF = 1;

/* Extract the first token: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, stringP, " \t\n");

/* If available, the first token should contain */
/* the keyword  OFF  or two positive  integers: */
if (remainderP)
	{
	/* If keyword OFF is present, reset the blur flag to zero: */
	if (strstr (tokenA, "OFF") == tokenA)
		{
		/* Reset the blur flag: */
		runtimeSP->blurF = 0;
		}

	/* If the keyword  OFF is not present, */
	/* two positive integers are expected: */
	else
		{
		/* Replace each minus in the input string with space: */
		P = stringP;
		while ((n = *P++) != '\0')
			{
			if (n == '-') *(P - 1) = ' ';
			}

		/* If reading fails: */
		if (ExtractTwoIntegers_ (&width, &height, stringP) < 0)
			{
			strcpy (runtimeSP->messageA,
				"Two positive integers expected!");
			runtimeSP->message_length =
						strlen (runtimeSP->messageA);
			return ERROR_BLUR;
			}

		/* If two integers were successfully read, check them: */
		if ((width <= 0) || (height <= 0)) 
			{
			strcpy (runtimeSP->messageA,
				"Two positive integers expected!");
			runtimeSP->message_length =
						strlen (runtimeSP->messageA);
			return ERROR_BLUR;
			}

		/* Store the extracted width and height: */
		runtimeSP->blur_width  = width;
		runtimeSP->blur_height = height;
		}
	}

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return the command code: */
return COMMAND_BLUR;
}

/*===========================================================================*/


