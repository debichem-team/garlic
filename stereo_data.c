/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				stereo_data.c

Purpose:
	Prepare the stereo data. The right x and z coordinate have to be
	calculated.  Rotation axis is y;  the angle is given in  ConfigS
	structure. If this angle is positive,  rotation is right-handed.
	The original data are used for the left image  and this function
	prepares data for the right image.
	
Input:
	(1) Pointer to MolComplexS structure,  with macromolecular data.
	(2) Pointer  to  ConfigS  structure,  with  configuration  data,
	    including the rotation angle.

Output:
	(1) Data added to the array of MolComplexS structures.
	(2) Return value.

Return value:
	(1) Positive,  if macromolecular complex  contains  at least one
	    atom.
	(2) Zero, if there are no atoms.

Notes:
	(1) Positive  angle  is suitable for  "relaxed  eyes".  Negative
	    angle is suitable for "crossed eyes".

	(2) This function does  not  prepare stereo data  for the normal
	    vector, which defines the plane associated with the complex.

========includes:============================================================*/

#include <stdio.h>
#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======prepare stereo data:=================================================*/

int PrepareStereoData_ (MolComplexS *curr_mol_complexSP, ConfigS *configSP)
{
size_t		atomI, atomsN;
double		sin_angle, cos_angle;
double		x0, z0;
double		x, z;
static double	x_new, z_new;
AtomS		*curr_atomSP;

/* Return zero if there are no atoms: */
atomsN = curr_mol_complexSP->atomsN;
if (atomsN == 0) return 0;

/* Prepare the sine and cosine of the rotation angle: */
sin_angle = sin (configSP->stereo_angle);
cos_angle = cos (configSP->stereo_angle);

/* Copy the geometric center coordinates: */
x0 = curr_mol_complexSP->geometric_center_vectorS.x;
z0 = curr_mol_complexSP->geometric_center_vectorS.z;

/* Scan the whole complex; calculate right_x and right_z: */
for (atomI = 0; atomI < atomsN; atomI++)
	{
	/** Current AtomS pointer: **/
	curr_atomSP = curr_mol_complexSP->atomSP + atomI;

	/** Coordinates relative to the geometric center: **/
	x = curr_atomSP->raw_atomS.x[0] - x0;
	z = curr_atomSP->raw_atomS.z[0] - z0;

	/** Rotate around y: **/
	x_new =  x * cos_angle + z * sin_angle;
	z_new = -x * sin_angle + z * cos_angle;

	/** New coordinates (used for the right image): **/
	curr_atomSP->raw_atomS.x[1] = x_new + x0;
	curr_atomSP->raw_atomS.z[1] = z_new + z0;
	}

/* Force the projection: */
curr_mol_complexSP->position_changedF = 1;

/* Return positive value if complex contains at least one atom: */
return 1;
}

/*===========================================================================*/


