/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				text_background.c

Purpose:
	Execute tbg command:  change the background color for input and
	output window. 

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure, with some runtime data.
	(4) Pointer to ConfigS structure, with configuration data.
	(5) Pointer to GUIS structure, with GUI data.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.

Output:
	(1) Log file created.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
int		ParseColor_ (RGBS *, unsigned long *, GUIS *, char *, char *);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);

/*======execute tbg command:=================================================*/

int TextBackground_ (MolComplexS *mol_complexSP, int mol_complexesN,
		     RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
		     NearestAtomS *nearest_atomSP, size_t pixelsN,
		     unsigned int *refreshIP)
{
char		*remainderP;
char		tokenA[STRINGSIZE];
size_t		rgb_struct_size;

/* Skip the first token: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE,
			    runtimeSP->curr_commandA, " \t\n");
if (!remainderP) return ERROR_TBG;

/* The second token should contain the color: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, " \t\n");
if (!remainderP)
	{
	strcpy (runtimeSP->messageA, "Text background color missing!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_NO_COLOR;
	}

/* Prepare the text background color: */
strncpy (configSP->text_bg_colorA, tokenA, SHORTSTRINGSIZE - 1);
configSP->text_bg_colorA[SHORTSTRINGSIZE - 1] = '\0';
ParseColor_ (&guiSP->input_winS.bg_rgbS, &guiSP->input_winS.bg_colorID,
	     guiSP, configSP->text_bg_colorA, "black");

/* Don't forget the output window, it uses the same background color: */
rgb_struct_size = sizeof (RGBS);
memcpy (&guiSP->output_winS.bg_rgbS,
	&guiSP->input_winS.bg_rgbS, rgb_struct_size);
guiSP->output_winS.bg_colorID = guiSP->input_winS.bg_colorID;

XSetWindowBackground (guiSP->displaySP, guiSP->input_winS.ID,
		      guiSP->input_winS.bg_colorID);
XSetWindowBackground (guiSP->displaySP, guiSP->output_winS.ID,
		      guiSP->output_winS.bg_colorID);

/* Refresh the output window: */
XSetForeground (guiSP->displaySP, guiSP->theGCA[0],
		guiSP->output_winS.bg_colorID);
XFillRectangle (guiSP->displaySP, guiSP->output_winS.ID, guiSP->theGCA[0],
		0, 0, guiSP->output_winS.width, guiSP->output_winS.height);

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Return positive value on success: */
return COMMAND_TBG;
}

/*===========================================================================*/


