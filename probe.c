/* Copyright (C) 2002-2003 Damir Zucic */

/*=============================================================================

				probe.c

Purpose:
	Execute probe command: change the radius of the bond probe, for
	all caught complexes.  Do not change default probe radius. This
	probe is used  to draw nice bonds.  It is  not used to generate
	molecular  surfaces:  each surface has its  own probe.  Default
	(hard-coded)  value is  0.80 angstroms.  Each structure has its
	own probe.

Input:
        (1) Pointer to MolComplexS structure.
        (2) The number of macromolecular complexes.
        (3) Pointer to RuntimeS structure.
        (4) Pointer to ConfigS structure.
        (5) Pointer to GUIS structure.
        (6) Pointer to NearestAtomS structure.
        (7) The number of pixels in the main window free area.
        (8) Pointer to refreshI.
        (9) Pointer to the remainder of the command string.

Output:
	(1) The bond probe radius set.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>
#include <ctype.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute probe command:===============================================*/

int Probe_ (MolComplexS *mol_complexSP, int mol_complexesN,
	    RuntimeS *runtimeSP,
	    ConfigS *configSP, GUIS *guiSP,
	    NearestAtomS *nearest_atomSP, size_t pixelsN,
	    unsigned int *refreshIP, char *stringP)
{
char		*P;
int		n;
double		value;
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;

/* Replace each non-numeric character (except */
/* minus sign and  decimal point) with space: */
P = stringP;
while ((n = *P++) != '\0')
	{
	if (!isdigit (n) && (n != '-') && (n != '.')) *(P - 1) = ' ';
	}

/* Try to extract the bond probe radius: */
if (sscanf (stringP, "%lf", &value) != 1)
	{
	strcpy (runtimeSP->messageA,
		"Failed to extract the bond probe radius!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_PROBE;
	}

/* Check the bond probe radius (must be positive): */
if (value < 0.0)
	{
	strcpy (runtimeSP->messageA, "Positive value expected!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_PROBE;
	}

/* On success, set the bond probe radius for each caught complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	curr_mol_complexSP = mol_complexSP + mol_complexI;
	if (curr_mol_complexSP->catchF == 0) continue;
	curr_mol_complexSP->bond_probe_radius = value;
	}

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN, runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return the command code: */
return COMMAND_PROBE;
}

/*===========================================================================*/


