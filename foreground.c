/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				foreground.c

Purpose:
	Execute  foreground command:  change the main window foreground
	color. 

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure, with some runtime data.
	(4) Pointer to ConfigS structure, with configuration data.
	(5) Pointer to GUIS structure, with GUI data.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.

Output:
	(1) Log file created.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
int		ParseColor_ (RGBS *, unsigned long *, GUIS *, char *, char *);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);

/*======execute fg command:==================================================*/

int Foreground_ (MolComplexS *mol_complexSP, int mol_complexesN,
		 RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
		 NearestAtomS *nearest_atomSP, size_t pixelsN,
		 unsigned int *refreshIP)
{
char		*remainderP;
char		tokenA[STRINGSIZE];

/* Skip the first token: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE,
			    runtimeSP->curr_commandA, " \t\n");
if (!remainderP) return ERROR_FOREGROUND;

/* The second token should contain the color: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, " \t\n");
if (!remainderP)
	{
	strcpy (runtimeSP->messageA, "Main window foreground color missing!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_NO_COLOR;
	}

/* Prepare the main window foreground color: */
strncpy (configSP->fg_colorA, tokenA, SHORTSTRINGSIZE - 1);
configSP->fg_colorA[SHORTSTRINGSIZE - 1] = '\0';
ParseColor_ (&guiSP->main_winS.fg_rgbS, &guiSP->main_winS.fg_colorID,
	     guiSP, configSP->fg_colorA, "white");

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN, runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Return positive value on success: */
return COMMAND_FOREGROUND;
}

/*===========================================================================*/


