/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				interpolate_color.c

Purpose:
	Interpolate (calculate) additional color, using two old colors.

Input:
	(1) The first input color.
	(2) The second input color.
	(3) Pointer to GUIS structure.

Output:
	(1) Return value.

Return value:
	The pixel value calculated.


=============================================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======interpolate color:===================================================*/

unsigned long InterpolateColor_ (unsigned long color1ID,
				 unsigned long color2ID,
				 GUIS *guiSP)
{
unsigned long		colorID;
unsigned long		red_mask, green_mask, blue_mask;
unsigned long		red1, red2, red;
unsigned long		green1, green2, green;
unsigned long		blue1, blue2, blue;
double			r1, r2, g1, g2, b1, b2;
double			w = 0.5;
double			r, g, b;

/* Copy masks: */
red_mask   = guiSP->visual_infoS.red_mask;
green_mask = guiSP->visual_infoS.green_mask;
blue_mask  = guiSP->visual_infoS.blue_mask;

/* Extract input color components: */
red1   = color1ID & red_mask;
red2   = color2ID & red_mask;
green1 = color1ID & green_mask;
green2 = color2ID & green_mask;
blue1  = color1ID & blue_mask;
blue2  = color2ID & blue_mask;

/* Convert to doubles: */
r1 = (double) red1;
r2 = (double) red2;
g1 = (double) green1;
g2 = (double) green2;
b1 = (double) blue1;
b2 = (double) blue2;

/* Calculate new color components: */
r = w * (r1 + r2);
g = w * (g1 + g2);
b = w * (b1 + b2);
red   = ((unsigned long) r) & red_mask;
green = ((unsigned long) g) & green_mask;
blue  = ((unsigned long) b) & blue_mask;

/* Combine new color components: */
colorID = red | green | blue;

/* Return the pixel value: */
return colorID;
}

/*===========================================================================*/


