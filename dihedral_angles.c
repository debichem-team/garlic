/* Copyright (C) 2000, 2001 Damir Zucic */

/*=============================================================================

				dihedral_angles.c

Purpose:
	Calculate the angles phi, psi, omega, chi1, chi2, chi3 and chi4.
	Set the cis-trans flag  (0 = bad/undefined, 1 = trans, 2 = cis).
	Phi is  the angle of the right-handed rotation about  N-CA bond.
	Phi is to zero if CA-C bond is cis to C-N bond, where the C atom
	involved in the C-N bond belongs to the previous residue. Psi is
	the angle of the right-handed rotation about  CA-C bond.  Psi is
	zero if C-N bond is cis to N-CA bond,  where the N atom involved
	in the C-N bond belongs to the next residue.  Omega is the angle
	of the right-handed  rotation about  C-N bond.  Omega is zero if
	CA-C bond is  cis  to N-CA bond,  where both the CA atom and the
	C atom involved in the CA-C bond belong to the previous residue.
	Chi1 is the angle of the right-handed rotation about CA-CB bond.
	Chi1 is zero if CG is cis to N-CA bond. Chi2 is the angle of the
	right-handed rotation about  CB-CG bond,  chi3 about CG-CD bond,
	chi4 about CD-CE or CD-NE bond and chi5 is the angle about NE-CZ
	bond.

Input:
	(1) Pointer to MolComplexS structure.
	(2) Pointer to ConfigS structure.

Output:
	(1) Dihedral angles calculated, cis-trans flag set.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Zero, if complex contains no atoms.
	(3) Negative on failure.

Notes:
	(1) This should help you to understand  phi and psi definitions:

	    ........./................
	    | H * - * N              |
	    |        \    phi1 = 180 |
	    |         * CA           |  residue 1
	    |        /    psi1 = 180 |
	    | O * - * C              |
	    |........\...............|
	    |       N * - * H        |
	    |        /    phi2 = 180 |
	    |    CA *                |  residue 2
	    |        \    psi2 = 180 |
	    |       C * - * O        |
	    |......../...............|

	(2) By default, the peptide group has undefined conformation. To
	    define the peptide group,  the coordinates of four atoms are
	    required.

	(3) The peptide bond formed by residues  I and I + 1 is assigned
	    to the residue I + 1.  Thus,  the cis-trans flag assigned to
	    the residue  I  refers to  the peptide  bond  formed  by the
	    residues  I - 1 and  I.  No peptide bond is assigned  to the
	    first residue.  The omega angle for the first residue is not
	    available (undefined).

	(4) This function tryes to calculate the phi angle without using
	    the hydrogen atom coordinates. This means that C atom coord.
	    of the previous residue are required. It also means that phi
	    will be missing for the first residue in each chain.  In the
	    second passage,  this function  will look for hydrogen atoms
	    in an attempt to calculate the phi angle.  If hydrogen atoms
	    are missing, some phi values may be missing.

	(5) The table below contains the list of atoms which are used to
	    calculate the angles chi1, chi2, chi3, chi4 and chi5:

+-----+-------------+--------------+--------------+-------------+--------------+
| res.|    chi1     |     chi2     |     chi3     |     chi4    |     chi5     |
|-----+-------------+--------------+--------------+-------------+--------------|
| ALA |             |              |              |             |              |
| ARG | N-CA-CB-CG  | CA-CB-CG-CD  | CB-CG-CD-NE  | CG-CD-NE-NZ | CD-NE-CZ-NH1 |
| ASN | N-CA-CB-CG  | CA-CB-CG-OD1 |              |             |              |
| ASP | N-CA-CB-CG  | CA-CB-CG-OD1 |              |             |              |
| CYS | N-CA-CB-SG  |              |              |             |              |
| GLN | N-CA-CB-CG  | CA-CB-CG-CD  | CB-CG-CD-OE1 |             |              |
| GLU | N-CA-CB-CG  | CA-CB-CG-CD  | CB-CG-CD-OE1 |             |              |
| GLY |             |              |              |             |              |
| HIS | N-CA-CB-CG  | CA-CB-CG-ND1 |              |             |              |
| ILE | N-CA-CB-CG1 | CA-CB-CG1-CD |              |             |              |
| LEU | N-CA-CB-CD  | CA-CB-CG-CD1 |              |             |              |
| LYS | N-CA-CB-CG  | CA-CB-CG-CD  | CB-CG-CD-CE  | CG-CD-CE-NZ |              |
| MET | N-CA-CB-CG  | CA-CB-CG-SD  | CB-CG-SD-CE  |             |              |
| PHE | N-CA-CB-CG  | CA-CB-CG-CD1 |              |             |              |
| PRO | N-CA-CB-CG  | CA-CB-CG-CD  |              |             |              |
| SER | N-CA-CB-OG  |              |              |             |              |
| THR | N-CA-CB-OG1 |              |              |             |              |
| TRP | N-CA-CB-CG  | CA-CB-CG-CD1 |              |             |              |
| TYR | N-CA-CB-CG  | CA-CB-CG-CD1 |              |             |              |
| VAL | N-CA-CB-CG1 |              |              |             |              |
+-----+-------------+--------------+--------------+-------------+--------------+

========includes:============================================================*/

#include <stdio.h>

#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		ExtractNCAC_ (VectorS *, VectorS *, VectorS *,
			      AtomS *, size_t, size_t);
int		ExtractC_ (VectorS *, AtomS *, size_t, size_t);
double		PhiFromCNCAC_ (VectorS *, VectorS *, VectorS *, VectorS *,
			       ConfigS *);
int		ExtractH_ (VectorS *, AtomS *, size_t, size_t);
double		PhiFromHNCAC_ (VectorS *, VectorS *, VectorS *, VectorS *);
int             ExtractN_ (VectorS *, AtomS *, size_t, size_t);
double		PsiFromNCACN_ (VectorS *, VectorS *, VectorS *, VectorS *,
			       ConfigS *);
int		ExtractO_ (VectorS *, AtomS *, size_t, size_t);
double		PsiFromNCACO_ (VectorS *, VectorS *, VectorS *, VectorS *);
int		ExtractNCA_ (VectorS *, VectorS *, AtomS *, size_t, size_t);
int		ExtractCAC_ (VectorS *, VectorS *, AtomS *, size_t, size_t);
double		OmegaFromCACNCA_ (VectorS *, VectorS *, VectorS *, VectorS *,
				  ConfigS *);
int		IsStandard_ (char *);
double		Chi1FromNCACBCG_ (AtomS *, size_t, size_t);
double		Chi1FromNCACBSG_ (AtomS *, size_t, size_t);
double		Chi1FromNCACBCG1_ (AtomS *, size_t, size_t);
double		Chi1FromNCACBOG_ (AtomS *, size_t, size_t);
double		Chi1FromNCACBOG1_ (AtomS *, size_t, size_t);
double		Chi2FromCACBCGCD_ (AtomS *, size_t, size_t);
double		Chi2FromCACBCGOD1_ (AtomS *, size_t, size_t);
double		Chi2FromCACBCGND1_ (AtomS *, size_t, size_t);
double		Chi2FromCACBCG1CD_ (AtomS *, size_t, size_t);
double		Chi2FromCACBCGCD1_ (AtomS *, size_t, size_t);
double		Chi2FromCACBCGSD_ (AtomS *, size_t, size_t);
double		Chi3FromCBCGCDNE_ (AtomS *, size_t, size_t);
double		Chi3FromCBCGCDOE1_ (AtomS *, size_t, size_t);
double		Chi3FromCBCGCDCE_ (AtomS *, size_t, size_t);
double		Chi3FromCBCGSDCE_ (AtomS *, size_t, size_t);
double		Chi4FromCGCDNECZ_ (AtomS *, size_t, size_t);
double		Chi4FromCGCDCENZ_ (AtomS *, size_t, size_t);
double		Chi5FromCDNECZNH1_ (AtomS *, size_t, size_t);

/*======calculate dihedral angles and set the cis-trans flag:================*/

int DihedralAngles_ (MolComplexS *mol_complexSP, ConfigS *configSP)
{
int			residuesN, residueI;
size_t			vector_struct_size;
int			n;
ResidueS		*current_residueSP;
size_t			current_startI, current_endI;
static VectorS		N_vectorS, CA_vectorS, C_vectorS;
ResidueS		*previous_residueSP;
size_t			previous_startI, previous_endI;
static VectorS		previousCA_vectorS, previousC_vectorS;
static VectorS		H_vectorS;
ResidueS		*next_residueSP;
size_t			next_startI, next_endI;
static VectorS		nextN_vectorS;
static VectorS		O_vectorS;
double			cis_abs_omega_max;
double			trans_abs_omega_min;
double			abs_omega;
int			cis_transF = 0;
AtomS			*first_atomSP;
char			*residue_nameP;
int			residue_typeI;

/* Return zero if there are no atoms: */
if (mol_complexSP->atomsN == 0) return 0;

/* Prepare and check the number of residues - at least one is required: */
residuesN = mol_complexSP->residuesN;
if (residuesN < 1) return -1;

/* The size of VectorS structure: */
vector_struct_size = sizeof (VectorS);

/*---------------------------------------------------------------------------*/

/* The first passage: initialize dihedral angles and cis-trans flag. */
for (residueI = 0; residueI < residuesN; residueI++)
	{
	/* Pointer to the current residue: */
	current_residueSP = mol_complexSP->residueSP + residueI;

	/* Initialize all dihedral angles: */
	current_residueSP->phi   = BADDIHEDANGLE;
	current_residueSP->psi   = BADDIHEDANGLE;
	current_residueSP->omega = BADDIHEDANGLE;
	current_residueSP->chi1  = BADDIHEDANGLE;
	current_residueSP->chi2  = BADDIHEDANGLE;
	current_residueSP->chi3  = BADDIHEDANGLE;
	current_residueSP->chi4  = BADDIHEDANGLE;

	/* Initialize the cis-trans flag: */
	current_residueSP->cis_transF = 0;
	}

/*---------------------------------------------------------------------------*/

/* The second passage: calculate phi and psi. */
for (residueI = 0; residueI < residuesN; residueI++)
	{
	/* Pointer to the current residue: */
	current_residueSP = mol_complexSP->residueSP + residueI;

	/* Prepare the atomic index range for the current residue: */
	current_startI = current_residueSP->residue_startI;
	current_endI   = current_residueSP->residue_endI;

	/* Try to extract N, CA and C coordinates for the current residue: */
	n = ExtractNCAC_ (&N_vectorS, &CA_vectorS, &C_vectorS,
			  mol_complexSP->atomSP, current_startI, current_endI);

	/* If the coordinates of any atom  are missing, */
	/* it is not possible to calculate phi and psi: */
	if (n < 3) continue;

	/*------phi:---------------------------------------------------------*/

	/* The first attempt - use the coordinates  of the */
	/* C atom from the previous residue, if available: */
	do
		{
		/* Do nothing for the first residue: */
		if (residueI < 1) break;

		/* Pointer to the previous residue: */
		previous_residueSP = mol_complexSP->residueSP + residueI - 1;

		/* The atomic index range for the previous residue: */
		previous_startI = previous_residueSP->residue_startI;
		previous_endI   = previous_residueSP->residue_endI;

		/* Try to extract the C atom */
		/* from the previous residue: */
		n = ExtractC_ (&previousC_vectorS,
			       mol_complexSP->atomSP,
			       previous_startI, previous_endI);
		if (n < 1) break;

		/* If the C atom coordinates are available for */
		/* the previous residue, try to calculate phi: */
		current_residueSP->phi = PhiFromCNCAC_ (&previousC_vectorS,
							&N_vectorS,
							&CA_vectorS,
							&C_vectorS,
							configSP);
		} while (0);

	/* The second attempt - use the coordinates of the */
	/* H atom from the current residue,  if available: */
	do
		{
		/* If phi was determined before, */
		/* this attempt is not required: */
		if (current_residueSP->phi != BADDIHEDANGLE) break;

		/* Try to extract the H atom coordinates: */
		n = ExtractH_ (&H_vectorS,
			       mol_complexSP->atomSP,
			       current_startI, current_endI);
		if (n < 1) break;

		/* If the H atom coordinates are available for */
		/* the current residue,  try to calculate phi: */
		current_residueSP->phi = PhiFromHNCAC_ (&H_vectorS,
							&N_vectorS,
							&CA_vectorS,
							&C_vectorS);
		} while (0);

	/*------psi:---------------------------------------------------------*/

	/* The first attempt - use the coordinates of the */
	/* N  atom from  the next residue,  if available: */
	do
		{
		/* Do nothing for the last residue: */
		if (residueI >= residuesN - 1) break;

		/* Pointer to the next residue: */
		next_residueSP = mol_complexSP->residueSP + residueI + 1;

		/* The atomic index range for the next residue: */
		next_startI = next_residueSP->residue_startI;
		next_endI   = next_residueSP->residue_endI;

		/* Try to extract the N atom */
		/* from  the  next  residue: */
		n = ExtractN_ (&nextN_vectorS,
			       mol_complexSP->atomSP,
			       next_startI, next_endI);
		if (n < 1) break;

		/* If the N atom coordinates are available for */
		/* the next  residue,  try  to calculate  psi: */
		current_residueSP->psi = PsiFromNCACN_ (&N_vectorS,
							&CA_vectorS,
							&C_vectorS,
							&nextN_vectorS,
							configSP);
		} while (0);

	/* The second attempt - use the coordinates of the */
	/* O atom from the current residue,  if available: */
	do
		{
		/* If psi was determined before, */
		/* this attempt is not required: */
		if (current_residueSP->psi != BADDIHEDANGLE) break;

		/* Try to extract the O atom coordinates: */
		n = ExtractO_ (&O_vectorS,
			       mol_complexSP->atomSP,
			       current_startI, current_endI);
		if (n < 1) break;

		/* If the O atom coordinates are available for */
		/* the current residue,  try to calculate psi: */
		current_residueSP->psi = PsiFromNCACO_ (&N_vectorS,
							&CA_vectorS,
							&C_vectorS,
							&O_vectorS);
		} while (0);
	}

/*---------------------------------------------------------------------------*/

/* Initialize ranges for omega: */

/* Cis range: */
cis_abs_omega_max   = DEG_TO_RAD * OMEGA_TOLERANCE;
trans_abs_omega_min = DEG_TO_RAD * (180.0 - OMEGA_TOLERANCE);

/* The third passage:  calculate omega and */
/* cis-trans flag. Skip the first residue. */
for (residueI = 1; residueI < residuesN; residueI++)
	{
	/* Pointer to the current residue: */
	current_residueSP = mol_complexSP->residueSP + residueI;

	/* Prepare the atomic index range for the current residue: */
	current_startI = current_residueSP->residue_startI;
	current_endI   = current_residueSP->residue_endI;

	/* Extract N and CA coordinates for the current residue: */
	n = ExtractNCA_ (&N_vectorS, &CA_vectorS,
			 mol_complexSP->atomSP,
			 current_startI, current_endI);
	if (n < 2) continue;

	/* Pointer to the previous residue: */
	previous_residueSP = mol_complexSP->residueSP + residueI - 1;

	/* The atomic index range from the previous residue: */
	previous_startI = previous_residueSP->residue_startI;
	previous_endI   = previous_residueSP->residue_endI;

	/* Extract CA and C coordinates from the previous residue: */
	n = ExtractCAC_ (&previousCA_vectorS, &previousC_vectorS,
			 mol_complexSP->atomSP,
			 previous_startI, previous_endI);
	if (n < 2) continue;

	/* Try to calculate omega: */
	current_residueSP->omega = OmegaFromCACNCA_ (&previousCA_vectorS,
						     &previousC_vectorS,
						     &N_vectorS,
						     &CA_vectorS,
						     configSP);

	/* Prepare the cis-trans flag: */
	abs_omega = fabs (current_residueSP->omega);
	if (abs_omega < cis_abs_omega_max) cis_transF = 2;
	else if (abs_omega > trans_abs_omega_min) cis_transF = 1;
	else cis_transF = 0;
	current_residueSP->cis_transF = cis_transF;
	}

/*---------------------------------------------------------------------------*/

/* The fourth passage: calculate side chain torsion angles (chi1 etc.). */
for (residueI = 0; residueI < residuesN; residueI++)
	{
	/* Pointer to the current residue: */
	current_residueSP = mol_complexSP->residueSP + residueI;

	/* The atomic index range for the current residue: */
	current_startI = current_residueSP->residue_startI;
	current_endI   = current_residueSP->residue_endI;

	/* Pointer to the first atom of the current residue: */
	first_atomSP = mol_complexSP->atomSP + current_startI;

	/* The name of the current residue: */
	residue_nameP = first_atomSP->raw_atomS.pure_residue_nameA;

	/* Check is the current residue from */
	/* the set of  20 standard residues: */
	residue_typeI = IsStandard_ (residue_nameP);

	/*------chi1:--------------------------------------------------------*/

	/* Calculate chi1: */
	switch (residue_typeI)
		{
		/* Do nothing for ALA and GLY: */
		case  0:	/* ALA */
		case  7:	/* GLY */
			break;

		/* Use  N, CA, CB and CG  for  ARG, ASN, ASP, GLN, */
		/* GLU, HIS, LEU, LYS, MET, PHE, PRO, TRP and TYR: */
		case  1:	/* ARG */ 
		case  2:	/* ASN */
		case  3:	/* ASP */
		case  5:	/* GLN */
		case  6:	/* GLU */
		case  8:	/* HIS */
		case 10:	/* LEU */
		case 11:	/* LYS */
		case 12:	/* MET */
		case 13:	/* PHE */
		case 14:	/* PRO */
		case 17:	/* TRP */
		case 18:	/* TYR */
			current_residueSP->chi1 =
				Chi1FromNCACBCG_ (mol_complexSP->atomSP,
						  current_startI,
						  current_endI);
			break;

		/* Use N, CA, CB and SG for CYS: */
		case 4:		/* CYS */
			current_residueSP->chi1 =
				Chi1FromNCACBSG_ (mol_complexSP->atomSP,
						  current_startI,
						  current_endI);
			break;

		/* Use N, CA, CB and CG1 for ILE and VAL: */
		case  9:	/* ILE */
		case 19:	/* VAL */
			current_residueSP->chi1 =
				Chi1FromNCACBCG1_ (mol_complexSP->atomSP,
						   current_startI,
						   current_endI);
			break;

		/* Use N, CA, CB and OG for SER: */
		case 15:	/* SER */
			current_residueSP->chi1 =
				Chi1FromNCACBOG_ (mol_complexSP->atomSP,
						  current_startI,
						  current_endI);
			break;

		/* Use N, CA, CB and OG1 for THR: */
		case 16:	/* THR */
			current_residueSP->chi1 =
				Chi1FromNCACBOG1_ (mol_complexSP->atomSP,
						   current_startI,
						   current_endI);
			break;

		/* At present, chi1 is not calculated for exotic residues: */
		default:
			;
		}

	/*------chi2:--------------------------------------------------------*/

	/* Calculate chi2: */
	switch (residue_typeI)
		{
		/* Do nothing for ALA, CYS, GLY, SER, THR and VAL: */
		case  0:	/* ALA */
		case  4:	/* CYS */
		case  7:	/* GLY */
		case 15:	/* SER */
		case 16:	/* THR */
		case 19:	/* VAL */
			break;

		/* Use CA, CB, CG and CD for ARG, GLN, GLU, LYS and PRO: */
		case  1:	/* ARG */
		case  5:	/* GLN */
		case  6:	/* GLU */
		case 11:	/* LYS */
		case 14:	/* PRO */
			current_residueSP->chi2 =
				Chi2FromCACBCGCD_ (mol_complexSP->atomSP,
						   current_startI,
						   current_endI);
			break;

		/* Use CA, CB, CG and OD1 for ASN and ASP: */
		case  2:	/* ASN */
		case  3:	/* ASP */
			current_residueSP->chi2 =
				Chi2FromCACBCGOD1_ (mol_complexSP->atomSP,
						    current_startI,
						    current_endI);
			break;

		/* Use CA, CB, CG and ND1 for HIS: */
		case  8:	/* HIS */
			current_residueSP->chi2 =
				Chi2FromCACBCGND1_ (mol_complexSP->atomSP,
						    current_startI,
						    current_endI);
			break;

		/* Use CA, CB, CG1 and CD for ILE: */
		case  9:	/* ILE */
			current_residueSP->chi2 =
				Chi2FromCACBCG1CD_ (mol_complexSP->atomSP,
						    current_startI,
						    current_endI);
			break;

		/* Use CA, CB, CG and CD1 for LEU, PHE, TRP and TYR: */
		case 10:	/* LEU */
		case 13:	/* PHE */
		case 17:	/* TRP */
		case 18:	/* TYR */
			current_residueSP->chi2 =
				Chi2FromCACBCGCD1_ (mol_complexSP->atomSP,
						    current_startI,
						    current_endI);
			break;

		/* Use CA, CB, CG and SD for MET: */
		case 12:	/* MET */
			current_residueSP->chi2 =
				Chi2FromCACBCGSD_ (mol_complexSP->atomSP,
						   current_startI,
						   current_endI);
			break;

		/* At present, chi2 is not calculated for exotic residues: */
		default:
			;
		}

	/*------chi3:--------------------------------------------------------*/

	/* Calculate chi3: */
	switch (residue_typeI)
		{
		/* Do nothing  for  ALA, ASN, ASP, CYS, GLY, HIS, */
		/* ILE, LEU, PHE, PRO, SER THR, TRP, TYR and VAL: */
		case  0:        /* ALA */
		case  2:	/* ASN */
		case  3:	/* ASP */
		case  4:	/* CYS */
		case  7:        /* GLY */
		case  8:	/* HIS */
		case  9:	/* ILE */
		case 10:	/* LEU */
		case 13:	/* PHE */
		case 14:	/* PRO */
		case 15:        /* SER */
		case 16:        /* THR */
		case 17:	/* TRP */
		case 18:	/* TYR */
		case 19:        /* VAL */
			break;

		/* Use CB, CG, CD and NE for ARG: */
		case  1:	/* ARG */
			current_residueSP->chi3 =
				Chi3FromCBCGCDNE_ (mol_complexSP->atomSP,
						   current_startI,
						   current_endI);
			break;

		/* Use CB, CG, CD and OE1 for GLN and GLU: */
		case  5:	/* GLN */
		case  6:	/* GLU */
			current_residueSP->chi3 =
				Chi3FromCBCGCDOE1_ (mol_complexSP->atomSP,
						    current_startI,
						    current_endI);
			break;

		/* Use CB, CG, CD and CE for LYS: */
		case 11:	/* LYS */
			current_residueSP->chi3 =
				Chi3FromCBCGCDCE_ (mol_complexSP->atomSP,
						   current_startI,
						   current_endI);
			break;

		/* Use CB, CG, SD and CE for MET: */
		case 12:	/* MET */
			current_residueSP->chi3 =
				Chi3FromCBCGSDCE_ (mol_complexSP->atomSP,
						   current_startI,
						   current_endI);
			break;

		/* At present, chi3 is not calculated for exotic residues: */
		default:
			;
		}

	/*------chi4:--------------------------------------------------------*/

	/* Calculate chi4: */
	switch (residue_typeI)
		{
		/* Do nothing for ALA, ASN, ASP, CYS, GLN, GLU, GLY, HIS, */
		/* ILE, LEU, MET, PHE, PRO, SER, THR, TRP, TYR  and  VAL: */
		case  0:
		case  2:
		case  3:
		case  4:
		case  5:
		case  6:
		case  7:
		case  8:
		case  9:
		case 10:
		case 12:
		case 13:
		case 14:
		case 15:
		case 16:
		case 17:
		case 18:
		case 19:
			break;

		/* Use CG, CD, NE and CZ for ARG: */
		case  1:	/* ARG */
			current_residueSP->chi4 =
				Chi4FromCGCDNECZ_ (mol_complexSP->atomSP,
						   current_startI,
						   current_endI);
			break;

		/* Use CG, CD, NE and CZ for LYS: */
		case 11:
			current_residueSP->chi4 =
				Chi4FromCGCDCENZ_ (mol_complexSP->atomSP,
						   current_startI,
						   current_endI);

			break;

		/* At present, chi4 is not calculated for exotic residues: */
		default:
			;
		}

	/*------chi5:--------------------------------------------------------*/

	/* Calculate chi5: */
	switch (residue_typeI)
		{
		/* Do  nothing  for  ALA, ASN, ASP, CYS, GLN, GLU, GLY, HIS, */
		/* ILE, LEU, LYS, MET, PHE, PRO, SER, THR, TRP, TYR and VAL: */
		case  0:
		case  2:
		case  3:
		case  4:
		case  5:
		case  6:
		case  7:
		case  8:
		case  9:
		case 10:
		case 11:
		case 12:
		case 13:
		case 14:
		case 15:
		case 16:
		case 17:
		case 18:
		case 19:
			break;

		/* Use CD, NE, CZ and NH1 for ARG: */
		case  1:	/* ARG */
			current_residueSP->chi5 =
				Chi5FromCDNECZNH1_ (mol_complexSP->atomSP,
						    current_startI,
						    current_endI);
			break;

		/* At present, chi5 is not calculated for exotic residues: */
		default:
			;
		}
	}

/*---------------------------------------------------------------------------*/

/* If this point is reached, return the positive value (success indicator): */
return 1;
}

/*===========================================================================*/


