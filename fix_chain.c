/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

				fix_chain.c

Purpose:
	Fix the macromolecular chain. The newly created structure is very
	messy after the creation.  This function  has to move, rotate and
	connect residues.  The initial conformation will be extended. The
	structure is brought to the specified conformation  at the end of
	this function.

Input:
	(1) Pointer to MolComplexS structure.
	(2) Pointer to RuntimeS structure (secondary structure is there).

Output:
	(1) The conformation  of the specified structure will be changed.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int             ResetPhi_ (MolComplexS *);
int		ResetPsi_ (MolComplexS *);
int		BuildChain_ (MolComplexS *);
int		ApplyStructure_ (MolComplexS *, RuntimeS *);

/*======fix macromolecular chain:============================================*/

int FixChain_ (MolComplexS *mol_complexSP, RuntimeS *runtimeSP)
{
int		n;

/* Reset the phi angle in each residue (change it to -180 degrees): */
ResetPhi_ (mol_complexSP);

/* Reset the psi angle in each residue (change it to 180 degrees): */
ResetPsi_ (mol_complexSP);

/* Build the extended polypeptide chain: */
n = BuildChain_ (mol_complexSP);
if (n < 0) return n;

/* Apply the specified secondary structure: */
ApplyStructure_ (mol_complexSP, runtimeSP);

/* If this point is reached, return positive value (success indicator): */
return 1;
}

/*===========================================================================*/


