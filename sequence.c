/* Copyright (C) 2000, 2001 Damir Zucic */

/*=============================================================================

				sequence.c

Purpose:
	Execute  sequence command.  This command may be used to define or
	copy the sequence. The sequence is stored to the sequence buffer.
	The pointer to this buffer is a member of RuntimeS structure. The
	sequence command always requires at least one keyword.  Keywords:
	RESET,  =,  FROM, LOAD (READ) and SAVE.  All commands  (except =)
	may be abbreviated  to three characters.

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to the string with command parameters.

Output:
	(1) Sequence stored to the sequence buffer.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

Notes:
	(1) The sequence  is expected in  three letters code.  One letter
	    code may be missinterpreted as valid three letters code.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>
#include <ctype.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		ParseSequence_ (RuntimeS *, char *);
char		*ExtractToken_ (char *, int, char *, char *);
int		SequenceFrom_ (MolComplexS *, int, RuntimeS *, char *);
int		CopySequence_ (RuntimeS *);
int		LoadSequence_ (RuntimeS *);
int		SaveSequence_ (RuntimeS *);
int		SaveSequenceWithNumbers_ (RuntimeS *);

/*======execute sequence command:============================================*/

int Sequence_ (MolComplexS *mol_complexSP, int mol_complexesN,
	       RuntimeS *runtimeSP, char *stringP)
{
char		string_copyA[STRINGSIZE];
char		*P;
char		*sequenceP;
int		n;
char		*remainderP;
char		tokenA[SHORTSTRINGSIZE];

/* Copy the original command string: */
strncpy (string_copyA, runtimeSP->curr_commandA, STRINGSIZE - 1);
string_copyA[STRINGSIZE - 1] = '\0';

/* Check is the equality sign present in the original command string: */
if ((P = strstr (string_copyA, "=")) != NULL)
	{
	/* Prepare the pointer to the sequence string: */
	sequenceP = P + 1;

	/* Convert sequence to uppercase: */
	P = sequenceP;
	while ((n = *P++) != '\0') *(P - 1) = toupper (n);

	/* Parse and store the sequence: */
	n = ParseSequence_ (runtimeSP, sequenceP);
	if (n > 0) return COMMAND_SEQUENCE;
	else return ERROR_SEQUENCE;
	}

/* If this point is reached, the equality sign was not found */
/* in the original command string. Check for other keywords. */

/* Take the first token; it should be present: */
remainderP = ExtractToken_ (tokenA, SHORTSTRINGSIZE, stringP, " \t\n");
if (!remainderP)
	{
	strcpy (runtimeSP->messageA, "Command parameter(s) missing!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_SEQUENCE;
	}

/* Reset sequence, if requested by keyword RES (RESET): */
if (strstr (tokenA, "RES") == tokenA)
	{
	runtimeSP->residuesN = 0;
	return COMMAND_SEQUENCE;
	}

/* Copy sequence from structure, if requested by keyword FRO (FROM): */
else if (strstr (tokenA, "FRO") == tokenA)
	{
	n = SequenceFrom_ (mol_complexSP, mol_complexesN,
			   runtimeSP, remainderP);
	if (n > 0) return COMMAND_SEQUENCE;
	else return ERROR_SEQUENCE;
	}

/* Copy the sequence from the main sequence buffer to the reference buffer: */
else if (strstr (tokenA, "COP") == tokenA)
	{
	CopySequence_ (runtimeSP);
	return COMMAND_SEQUENCE;
	}

/* Load seq. from file, if requested by keyword LOA (LOAD) or REA (READ): */
else if ((strstr (tokenA, "LOA") == tokenA) ||
	 (strstr (tokenA, "REA") == tokenA))
	{
	n = LoadSequence_ (runtimeSP);
	if (n > 0) return COMMAND_SEQUENCE;
	else return ERROR_SEQUENCE;
	}

/* Save sequence as three letter code, if requested by keyword SAV: */
else if (strstr (tokenA, "SAV") == tokenA)
	{
	n = SaveSequence_ (runtimeSP);
	if (n > 0) return COMMAND_SEQUENCE;
	else return ERROR_SEQUENCE;
	}

/* Save sequence as three letter code, with serial numbers: */
else if (strstr (tokenA, "SWN") == tokenA)
	{
	n = SaveSequenceWithNumbers_ (runtimeSP);
	if (n > 0) return COMMAND_SEQUENCE;
	else return ERROR_SEQUENCE;
	}

/* If keyword recognition failed: */
else
	{
	strcpy (runtimeSP->messageA, "Keyword not recognized!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_SEQUENCE;
	}

/* Return the command code: */
return COMMAND_SEQUENCE;
}

/*===========================================================================*/


