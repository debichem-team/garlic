/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				tag_position.c

Purpose:
	Find the tag position in screen coordinates.

Input:
	(1) Pointer to horizontal position of the left image.
	(2) Pointer to horizontal position of the right image.
	(3) Pointer to vertical position.
	(4) Pointer to MolComplexS structure.
	(5) The number of images (1 for mono, 2 for stereo).

Output:
	(1) Horizontal position set (two values for stereo mode).
	(2) Vertical position set.
	(3) Return value.

Return value:
	(1) Positive if macromol. complex contains at least one atom.
	(2) Zero, if there are no atoms.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======find tag position:===================================================*/

int TagPosition_ (int *tag_screen_left_xP,
		  int *tag_screen_right_xP,
		  int *tag_screen_yP,
		  MolComplexS *curr_mol_complexSP,
		  int imagesN)
{
size_t		atomsN, atomI;
AtomS		*curr_atomSP;
int		screen_left_x_min, screen_left_x_max;
int		screen_right_x_min = 0, screen_right_x_max = 0;
int		screen_y_min, screen_y_max;
int		screen_left_x, screen_right_x, screen_y;

/* Return if there are no atoms: */
atomsN = curr_mol_complexSP->atomsN;
if (atomsN == 0) return 0;

/* Initialize extreme values using the screen coordinates of the first atom: */
screen_left_x_min = curr_mol_complexSP->atomSP->raw_atomS.screen_x[0];
screen_left_x_max = screen_left_x_min;
screen_y_min      = curr_mol_complexSP->atomSP->raw_atomS.screen_y;
screen_y_max      = screen_y_min;
if (imagesN == 2)
	{
	screen_right_x_min = curr_mol_complexSP->atomSP->raw_atomS.screen_x[1];
	screen_right_x_max = screen_right_x_min;
	}

/* Find new extreme values; skip the first atom: */
for (atomI = 1; atomI < atomsN; atomI++)
	{
	/** Current AtomS pointer: **/
	curr_atomSP = curr_mol_complexSP->atomSP + atomI;

	/** Min./max.: **/
	screen_left_x = curr_atomSP->raw_atomS.screen_x[0];
	screen_y      = curr_atomSP->raw_atomS.screen_y;
	if (screen_left_x < screen_left_x_min)
		{
		screen_left_x_min = screen_left_x;
		}
	if (screen_left_x > screen_left_x_max)
		{
		screen_left_x_max = screen_left_x;
		}
	if (screen_y < screen_y_min) screen_y_min = screen_y;
	if (screen_y > screen_y_max) screen_y_max = screen_y;
	if (imagesN == 2)
		{
		screen_right_x = curr_atomSP->raw_atomS.screen_x[1];
		if (screen_right_x < screen_right_x_min)
			{
			screen_right_x_min = screen_right_x;
			}
		if (screen_right_x > screen_right_x_max)
			{
			screen_right_x_max = screen_right_x;
			}
		}
	}

/* Set new values: */
*tag_screen_left_xP  = (screen_left_x_min  + screen_left_x_max)  / 2;
*tag_screen_right_xP = (screen_right_x_min + screen_right_x_max) / 2;
*tag_screen_yP       = screen_y_min;

/* Return 1 if this point is reached: */
return 1;
}

/*===========================================================================*/


