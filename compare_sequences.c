/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				compare_sequences.c

Purpose:
	Compare sequence fragment from the current macromolecular complex
	with the sequence from the sequence buffer.  The leading residues
	from both sequences were compared before,  so it is not necessary
	to compare these residues again.

Input:
	(1) Pointer to MolComplexS structure, with data about the current
	    macromolecular complex, including the sequence fragment which
	    should be compared with sequence from the sequence buffer.
	(2) The index of the first residue in the sequence fragment which
	    is currently checked.
	(3) Number of residues in  the sequence which  is associated with
	    the current  macromolecular complex.  The fragment  mentioned
	    above belongs to this sequence.
	(4) Pointer to the first residue in the sequence buffer.
	(5) Number of residues in the sequence buffer.

Output:
	Return value.

Return value:
	(1) Positive, if two sequences match.
	(2) Negative, if two sequences do not match.

Notes:
	(1) Don't forget  to check are there  enough residues in the seq.
	    fragment which is compared with the sequence buffer.  If this
	    is not checked, a memory overflow may occur.

	(2) Use strncmp to compare residue names because residue names in
	    the sequence buffer are not zero terminated!

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======compare sequences:===================================================*/

int CompareSequences_ (MolComplexS *curr_mol_complexSP,
		       size_t start1I, size_t residues1N,
		       char *first_name2P, size_t residues2N)
{
int		max_length;
size_t		residue1I, residue2I;
ResidueS	*curr_residue1SP;
AtomS		*atom1SP;
char		*name1P;
char		*name2P;

/* If there is only one residue in the sequence buffer, the sequences match: */
if (residues2N == 1) return 1;

/* Check is the sequence fragment long enough to be */
/* compared with sequence from the sequence buffer: */
if (start1I + residues2N > residues1N) return -1;

/* The maximal residue name length: */
max_length = RESNAMESIZE - 1;

/* Scan the sequence fragment: */
residue2I = 1;
for (residue1I = start1I + 1; residue1I < start1I + residues2N; residue1I++)
	{
	/* Pointer to the current residue: */
	curr_residue1SP = curr_mol_complexSP->residueSP + residue1I;

	/* Pointer to the first atom of the current residue: */
	atom1SP = curr_mol_complexSP->atomSP +
		  curr_residue1SP->residue_startI;

	/* Pointer to the name of the current residue: */
	name1P = atom1SP->raw_atomS.pure_residue_nameA;

	/* Pointer to the name of the corresponding residue in the buffer: */
	name2P = first_name2P + residue2I * max_length;

	/* Compare residue names; if they do */
	/* not match, return negative value: */
	if (strncmp (name1P, name2P, max_length) != 0) return -1;

	/* Update the residue index which is scanning the sequence buffer: */
	residue2I++;
	}

/* If this point is reached, two sequences match! */

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


