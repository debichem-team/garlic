/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

				copy_sec_structure.c

Purpose:
	Copy the secondary structure from the general purpose (main)
	buffer to the reference buffer.

Input:
	(1) Pointer to RuntimeS structure.

Output:
	(1) Secondary structure copyed to the reference buffer.
	(2) Return value.

Return value:
	(1) The number of residues in both buffers.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======copy secondary structure to reference buffer:========================*/

int CopySecondaryStructure_ (RuntimeS *runtimeSP)
{
int		sec_structure_length;
int		residueI;

/* Copy the number of residues: */
sec_structure_length = runtimeSP->sec_structure_length;
runtimeSP->reference_sec_str_length = sec_structure_length;

/* Initialize the reference buffer (use the code for extended conformation): */
for (residueI = 0; residueI < MAXRESIDUES; residueI++)
	{
	*(runtimeSP->reference_sec_structureP + residueI) = 'E';
	}

/* Copy the secondary structure codes: */
for (residueI = 0; residueI < sec_structure_length; residueI++)
	{
	*(runtimeSP->reference_sec_structureP + residueI) =
			*(runtimeSP->sec_structureP + residueI);
	}

/* Return the number of residues in both secondary structure buffers: */
return sec_structure_length;
}

/*===========================================================================*/


