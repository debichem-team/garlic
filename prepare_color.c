/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				prepare_color.c

Purpose:
	Prepare the color by mixing two colors, using the proper weighting.

Input:
	(1) Pointer to RGBS structure, with the first color.
	(2) Pointer to RGBS structure, with the second color.
	(3) Scale factor (double).
	(4) Pointer to GUIS structure.

Output:
	(1) Return value.

Return value:
	The pixel value for the new color.


=============================================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

unsigned long	PixelFromRGBS_ (RGBS *, GUIS *);

/*======mix two colors:======================================================*/

unsigned long PrepareColor_ (RGBS *rgb1SP, RGBS *rgb2SP,
			     double scale_factor, GUIS *guiSP)
{
unsigned long		colorID;
double			color1, delta_color, color;
RGBS			rgbS;

/* Red: */
color1  = (double) rgb1SP->red;
delta_color = (double) rgb2SP->red - (double) rgb1SP->red;
color = color1 + scale_factor * delta_color;
rgbS.red    = (unsigned short) color;

/* Green: */
color1  = (double) rgb1SP->green;
delta_color = (double) rgb2SP->green - (double) rgb1SP->green;
color = color1 + scale_factor * delta_color;
rgbS.green  = (unsigned short) color;

/* Blue: */
color1  = (double) rgb1SP->blue;
delta_color = (double) rgb2SP->blue - (double) rgb1SP->blue;
color = color1 + scale_factor * delta_color;
rgbS.blue   = (unsigned short) color;

/* Pixel value: */
colorID = PixelFromRGBS_ (&rgbS, guiSP);

/* Return the pixel value: */
return colorID;
}

/*===========================================================================*/


