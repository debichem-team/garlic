/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				show_backbone.c

Purpose:
	Show backbone for selected atoms: set hiddenF in each BackboneS
	structure.

Input:
	(1) Pointer to MolComplexS structure.
	(2) The number of macromolecular complexes.

Output:
	(1) The hiddenF set to zero for selected atoms.
	(2) Return value.

Return value:
	Positive always.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======show backbone:=======================================================*/

int ShowBackbone_ (MolComplexS *mol_complexSP, int mol_complexesN)
{
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;
size_t		c_alphaI, c_alphaN;
BackboneS	*curr_backboneSP;
AtomS		*curr_atomSP;

/* Scan every macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/** Pointer to the current macromolecular complex: **/
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/** Check is the current macromolecular complex caught: **/
	if (curr_mol_complexSP->catchF == 0) continue;

	/** The number of CA atoms in the current complex: **/
	c_alphaN = curr_mol_complexSP->c_alphaN;

	/** Scan the array of BackboneS structures: **/
	for (c_alphaI = 0; c_alphaI < c_alphaN; c_alphaI++)
		{
		/*** Pointer to the current CA atom: ***/
		curr_backboneSP = curr_mol_complexSP->backboneSP + c_alphaI;
		curr_atomSP = curr_mol_complexSP->atomSP +
			      curr_backboneSP->c_alphaI;

		/*** Check is the current CA atom selected and set ***/
		/*** hiddenF.  If the current CA atom is selected, ***/
		/*** the backbone element should be  made visible. ***/
		if (curr_atomSP->selectedF) curr_backboneSP->hiddenF = 0;
		}
	}

/* Return positive value (trivial): */
return 1;
}

/*===========================================================================*/


