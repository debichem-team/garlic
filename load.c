/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

				load.c

Purpose:
	Execute load command: load the specified macromolecular complex
	from file.

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) Pointer to the number of macromolecular complexes.
	(3) Pointer to the next macromolecular complex identifier.
	(4) Pointer to RuntimeS structure, with some runtime data.
	(5) Pointer to ConfigS structure, with configuration data.
	(6) Pointer to GUIS structure, with GUI data.
	(7) Pointer to NearestAtomS structure.
	(8) The number of pixels in the main window free area.
	(9) Pointer to refreshI.

Output:
	(1) New macromolecular complex loaded into memory.
	(2) The number of complexes updated.
	(3) The next macromolecular complex identifier updated.
	(4) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

Notes:
	(1) The value of refreshI  (*refreshIP)  may be changed in this
	    function or in some function called by this function. It is
	    checked in  EventLoop_  (see event_loop.c).  It is  assumed
	    that  refreshI  is updated no more  than once in each event
	    handling function. If this is not the case, the care should
	    be taken to avoid refreshI overflow.

	(2) This functions resets the edit mode index.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
int		LoadComplex_ (int *,
			      MolComplexS *, int *, int *,
			      char *, ConfigS *, GUIS *);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute load command:================================================*/

int Load_ (MolComplexS *mol_complexSP,
	   int *mol_complexesNP, int *next_mol_complexIDP,
	   RuntimeS *runtimeSP,
	   ConfigS *configSP, GUIS *guiSP,
	   NearestAtomS *nearest_atomSP, size_t pixelsN,
	   unsigned int *refreshIP)
{
char		*remainderP;
char		tokenA[STRINGSIZE];
static char	messageA[SHORTSTRINGSIZE] = "Loading file ...";
int		message_length, text_width;
int		frame_width, frame_height, screen_x0, screen_y0;
int		n;

/* Skip the first token: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE,
			    runtimeSP->curr_commandA, " \t\n");
if (!remainderP) return ERROR_LOAD;

/* The second token should contain the file name: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, " \t\n");
if (!remainderP)
	{
	strcpy (runtimeSP->messageA, "Missing input file name!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_NO_FILE_NAME;
	}

/* Inform user that loading may take some time: */
message_length = strlen (messageA);
text_width = XTextWidth (guiSP->main_winS.fontSP, messageA, message_length);
frame_width = text_width + 50;
frame_height = 4 * guiSP->main_winS.text_line_height;
screen_x0 = (guiSP->main_win_free_area_width - frame_width) / 2;
screen_y0 = (guiSP->main_win_free_area_height - frame_height) / 2;
XSetForeground (guiSP->displaySP, guiSP->theGCA[0], guiSP->dark_red_colorID);
XFillRectangle (guiSP->displaySP, guiSP->main_winS.ID, guiSP->theGCA[0],
		screen_x0, screen_y0, frame_width, frame_height);
XSetForeground (guiSP->displaySP, guiSP->theGCA[0], guiSP->yellow_colorID);
screen_x0 = (guiSP->main_win_free_area_width - text_width) / 2;
screen_y0 = (guiSP->main_win_free_area_height +
	     guiSP->main_winS.text_line_height) / 2;
XDrawString (guiSP->displaySP, guiSP->main_winS.ID, guiSP->theGCA[0],
	     screen_x0, screen_y0, messageA, message_length);
XFlush (guiSP->displaySP);

/* Try to read file: */
n = LoadComplex_ (&runtimeSP->groupF,
		  mol_complexSP, mol_complexesNP, next_mol_complexIDP,
		  tokenA, configSP, guiSP);
if (n < 0)
	{
	sprintf (runtimeSP->messageA, "Unable to open file %s!", tokenA);
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	(*refreshIP)++;
	MainRefresh_ (mol_complexSP, *mol_complexesNP,
		      runtimeSP, configSP, guiSP,
		      nearest_atomSP, pixelsN, *refreshIP);
	ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
			 configSP, guiSP);
	return ERROR_OPEN_FAILURE;
	}

/* The new complex will be a new default caught complex: */
if (*mol_complexesNP > 0)
	{
	runtimeSP->default_complexI = *mol_complexesNP - 1;
	}

/* Reset the editing mode index: */
runtimeSP->edit_modeI = 0;

/* Reset the edit_single_modeF flag: */
runtimeSP->edit_single_bondF = 0;

/* Reset the main window mode index: */
guiSP->main_window_modeI = 0;

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, *mol_complexesNP,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return positive value on success: */
return COMMAND_LOAD;
}

/*===========================================================================*/


