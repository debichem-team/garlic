/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

				draw_labels.c

Purpose:
	Draw atomic labels to the main window. Note that drawing is not
	done to the hidden pixmap!

Input:
	(1) Pointer to MolComplexS structure.
	(2) Number of macromolecular complexes.
	(3) Pointer to ConfigS structure.
	(4) Pointer to GUIS structure.

Output:
	(1) Labels drawn to hidden pixmap.
	(2) Return value.

Return value:
	The number of labels drawn.

=============================================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======draw labels:=========================================================*/

size_t DrawLabels_ (MolComplexS *mol_complexSP, int mol_complexesN,
		    ConfigS *configSP, GUIS *guiSP)
{
size_t			labels_drawnN = 0;
int			imageI, imagesN;
int			left_edge[2], right_edge[2];
int			mol_complexI;
MolComplexS		*curr_mol_complexSP;
size_t			atomsN, atomI;
AtomS			*curr_atomSP;
int			screen_x, screen_y;
static char		stringA[SHORTSTRINGSIZE];
int			string_length, text_width;

/* Number of images: */
if (configSP->stereoF) imagesN = 2;
else imagesN = 1;

/* Left and right image edge (in stereo mode there are two images): */
left_edge[0]  = configSP->image_screen_x0[0];
right_edge[0] = configSP->image_screen_x1[0];
left_edge[1]  = configSP->image_screen_x0[1];
right_edge[1] = configSP->image_screen_x1[1];

/* Prepare the text color: */
XSetForeground (guiSP->displaySP, guiSP->theGCA[0],
		guiSP->main_winS.fg_colorID);

/* Check every macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/* Pointer to current macromolecular complex: */
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/* Prepare and check the number of atoms: */
	atomsN = curr_mol_complexSP->atomsN;
	if (atomsN == 0) continue;

	/* Check every atom: */
	for (atomI = 0; atomI < atomsN; atomI++)
		{
		/* Pointer to the current atom: */
		curr_atomSP = curr_mol_complexSP->atomSP + atomI;

		/* Check flag: */
		if (curr_atomSP->labelF != 1) continue;

		/* Check is atom hidden: */
		if (curr_atomSP->hiddenF) continue;

		/* Check is atom inside slab: */
		if (!curr_atomSP->inside_slabF) continue;

		/* Check is atom inside window: */
		if (!curr_atomSP->inside_windowF) continue;

		/* Draw one (mono) or two labels (stereo): */
		for (imageI = 0; imageI < imagesN; imageI++)
			{
			/* Prepare the coordinates: */
			screen_x = curr_atomSP->raw_atomS.screen_x[imageI];
			screen_y = curr_atomSP->raw_atomS.screen_y;

			/* Check is this atom really visible: */
			if (screen_x <  left_edge[imageI])  continue;
			if (screen_x >= right_edge[imageI]) continue;

			/* Prepare label: */
			if (curr_atomSP->raw_atomS.chainID == ' ')
				{
				sprintf (stringA, "%s %d %s%c",
				    curr_atomSP->raw_atomS.residue_nameA,
				    curr_atomSP->raw_atomS.residue_sequenceI,
				    curr_atomSP->raw_atomS.pure_atom_nameA,
				    curr_atomSP->raw_atomS.alt_location);
				}
			else
				{
				sprintf (stringA, "%s %d%c %s%c",
				    curr_atomSP->raw_atomS.residue_nameA,
				    curr_atomSP->raw_atomS.residue_sequenceI,
				    curr_atomSP->raw_atomS.chainID,
				    curr_atomSP->raw_atomS.pure_atom_nameA,
				    curr_atomSP->raw_atomS.alt_location);
				}

			/* String length: */
			string_length = strlen (stringA);

			/* Prepare the string size in pixels: */
			text_width = XTextWidth (guiSP->main_winS.fontSP,
						 stringA, string_length);

			/* Draw label: */
			XDrawString (guiSP->displaySP,
				     guiSP->main_winS.ID,
				     guiSP->theGCA[0],
				     screen_x - text_width / 2, screen_y,
				     stringA, string_length);

			/* Update the counter: */
			labels_drawnN++;

			}	/* imageI loop */
		}		/* atomI  loop */
	}			/* mol_complexI loop */

/* Return the number of labels which were drawn: */
return labels_drawnN;
}

/*===========================================================================*/


