/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

				draw_membranes.c

Purpose:
	For each macromolecular complex,  draw the membrane,  if visible.

Input:
	(1) Pointer to MolComplexS structure.
	(2) Number of macromolecular complexes.
	(3) Pointer to ConfigS structure, with configuration data.
	(4) Pointer to GUIS structure.
	(5) Pointer to NearestAtomS structure, with information about the
	    atom occupying the given pixel.
	(6) The number of pixels in the main window free area.
	(7) The refreshI, used to check the  NearestAtomS associated with
	    a given pixel.

Output:
	(1) Membrane drawn for each  macromolecular complex,  if visible.
	(2) Return value.

Return value:
	(1) Positive always (trivial).

Notes:
	(1) Indentation is exceptionally 4 spaces.

	(2) First draw the first plane, then the second plane.

=============================================================================*/

#include <stdio.h>

#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

void		PlaneExtent_ (int *, int *, int *, int *,
			      PlaneS *, ConfigS *, int);
unsigned long	WeightColors_ (unsigned long, unsigned long, double, GUIS *);

/*======draw membranes:======================================================*/

int DrawMembranes_ (MolComplexS *mol_complexSP, int mol_complexesN,
		    ConfigS *configSP, GUIS *guiSP,
		    NearestAtomS *nearest_atomSP, size_t pixelsN,
		    unsigned int refreshI)
{
int		imagesN, imageI;
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;
size_t		atomsN;
PlaneS		*plane1SP, *plane2SP;
int		center_screen_x, center_screen_y;
double		angle, sin_angle, cos_angle;
double		reciprocal_a, reciprocal_b;
double		reciprocal_diameter;
double		coeff;
int		screen_x_min, screen_x_max, screen_y_min, screen_y_max;
int		screen_x, screen_y;
double		x_relative, y_relative, x_rotated, y_rotated;
double		ratio_x, ratio_y, d;
double		plane_delta_z, plane_z;
size_t		pixelI;
NearestAtomS	*curr_pixelSP;
double		scale_factor;
unsigned long	plane_colorID, old_colorID, colorID;

/* Number of images: */
if (configSP->stereoF) imagesN = 2;
else imagesN = 1;

/* Draw the membrane (two planes), if it is defined */
/* and visible,  for each  macromolecular  complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
    {
    /* Pointer to current macromolecular complex: */
    curr_mol_complexSP = mol_complexSP + mol_complexI;

    /* Check is the membrane defined: */
    if (curr_mol_complexSP->membraneS.definedF == 0) continue;

    /* Check is the membrane visible: */
    if (curr_mol_complexSP->membraneS.hiddenF) continue;

    /* Prepare and check the number of atoms: */
    atomsN = curr_mol_complexSP->atomsN;
    if (atomsN == 0) continue;

    /* Pointers to planes: */
    plane1SP = &curr_mol_complexSP->membraneS.plane1S;
    plane2SP = &curr_mol_complexSP->membraneS.plane2S;

    /*------draw the first plane:--------------------------------------------*/

    /* Draw one image (mono) or two images (stereo): */
    for (imageI = 0; imageI < imagesN; imageI++)
	{
	/* Plane center position: */
	center_screen_x = plane1SP->center_screen_x[imageI];
	center_screen_y = plane1SP->center_screen_y;

	/* The shifted phi angle, used to rotate */
	/* the rectangle  bounding the ellipsa : */
	angle = plane1SP->normal_phi[imageI] - 4.712389;

	/* Sine and cosine of this angle: */
	sin_angle = sin (angle);
	cos_angle = cos (angle);

	/* Reciprocal values of half axes: */
	if (plane1SP->screen_a == 0.0) reciprocal_a = 0.0;
	else reciprocal_a = 1.0 / plane1SP->screen_a;
	if (plane1SP->screen_b[imageI] == 0.0) reciprocal_b = 0.0;
	else reciprocal_b = 1.0 / plane1SP->screen_b[imageI];

	/* Reciprocal diameter (required for color fading effect): */
	if (plane1SP->circle_radius == 0.0) reciprocal_diameter = 0.0;
	else reciprocal_diameter = 0.5 / plane1SP->circle_radius;

	/* The coefficient used to calculate z value for a given pixel: */
	if (plane1SP->screen_b[imageI] == 0.0) coeff = 0.0;
	else
	    {
	    coeff = plane1SP->circle_radius *
		    sin (plane1SP->normal_theta[imageI]) /
		    plane1SP->screen_b[imageI];
	    if (plane1SP->normal_theta[imageI] >= 1.5707963) coeff *= -1;
	    }

	/* Find the plane extent: */
	PlaneExtent_ (&screen_x_min, &screen_x_max,
		      &screen_y_min, &screen_y_max,
		      plane1SP, configSP, imageI);

	/* Scan the rectangle bounding ellipse: */
	for (screen_x = screen_x_min; screen_x <= screen_x_max; screen_x++)
	    {
	    for (screen_y = screen_y_min; screen_y <= screen_y_max; screen_y++)
		{
		/* Pixel position relative to the plane center position: */
		x_relative = screen_x - center_screen_x;
		y_relative = screen_y - center_screen_y;

		/* Rotated pixel position (rotation is anticlockwise here): */
		x_rotated =  x_relative * cos_angle + y_relative * sin_angle;
		y_rotated = -x_relative * sin_angle + y_relative * cos_angle;

		/* Check is it inside bounding ellipse: */
		ratio_x = x_rotated * reciprocal_a;
		ratio_y = y_rotated * reciprocal_b;
		d = ratio_x * ratio_x + ratio_y * ratio_y;
		if (d > 1.0) continue;

		/* Calculate the z value for this pixel: */
		plane_delta_z = coeff * y_rotated;
		plane_z = plane1SP->center_z[imageI] + plane_delta_z;

		/* The current pixel index: */
		pixelI = guiSP->main_win_free_area_width * screen_y + screen_x;

		/* Check pixel index: */
		if (pixelI >= pixelsN) continue;

		/* Pointer to  NearestAtomS struct. */
		/* assigned to current coordinates: */
		curr_pixelSP = nearest_atomSP + pixelI;

		/* If something was drawn to the current */
		/* pixel in this step, compare z values: */
		if (curr_pixelSP->last_refreshI == refreshI)
		    {
		    /* If the point drawn before is closer to the */
		    /* observer than plane, do not draw anything: */
		    if (curr_pixelSP->z < plane_z)
			{
			continue;
			}

		    /* If the plane is closer than the */
		    /* point drawn before, mix colors: */
		    else
			{
			/* The plane color: */
			scale_factor = (plane_delta_z +
					plane1SP->circle_radius) *
					reciprocal_diameter;
			if (scale_factor < 0.0) scale_factor = 0.0;
			if (scale_factor > 1.0) scale_factor = 1.0;
			if (plane1SP->visible_sideI[imageI] == 0)
			    {
			    plane_colorID = WeightColors_ (
					plane1SP->top_near_colorID,
					plane1SP->top_far_colorID,
					scale_factor, guiSP);
			    }
			else
			    {
			    plane_colorID = WeightColors_ (
					plane1SP->bottom_near_colorID,
					plane1SP->bottom_far_colorID,
					scale_factor, guiSP);
			    }

			/* The color of the object which was */
			/* drawn  before  the current plane: */
			old_colorID = curr_pixelSP->colorID;

			/* The weighting factor which should be used */
			/* to mix the plane color and the old color: */
			scale_factor = plane1SP->transparency;

			/* Mix the plane color and the old color: */
			colorID = WeightColors_ (plane_colorID,
						 old_colorID,
						 scale_factor, guiSP);
			}
		    }

		/* If nothing was drawn to the current pixel */
		/* in this drawing step,  prepare the color: */
		else
		    {
		    /* Prepare the scale factor used to weight (mix) colors: */
		    scale_factor = (plane_delta_z +
				    plane1SP->circle_radius) *
				    reciprocal_diameter;
		    if (scale_factor < 0.0) scale_factor = 0.0;
		    if (scale_factor > 1.0) scale_factor = 1.0;

		    /* Prepare the color by weighting */
		    /* (mixing)  near and  far color: */
		    if (plane1SP->visible_sideI[imageI] == 0)
			{
			colorID = WeightColors_ (
					plane1SP->top_near_colorID,
					plane1SP->top_far_colorID,
					scale_factor, guiSP);
			}
		    else
			{
			colorID = WeightColors_ (
					plane1SP->bottom_near_colorID,
					plane1SP->bottom_far_colorID,
					scale_factor, guiSP);
			}

		    /* Set the style index associated with the current pixel */
		    /* to signal  that only plane  was drawn to  this pixel: */
		    curr_pixelSP->styleI = PLANE_STYLE;
		    }

		/* Draw point: */
		XSetForeground (guiSP->displaySP, guiSP->theGCA[0], colorID);
		XDrawPoint (guiSP->displaySP, guiSP->main_hidden_pixmapID,
			    guiSP->theGCA[0], screen_x, screen_y);

		/* Update refresh index,  z value and */
		/* color value for the current pixel: */
		curr_pixelSP->last_refreshI = refreshI;
		curr_pixelSP->z = plane_z;
		curr_pixelSP->colorID = colorID;
		}
	    }
	}

    /*------draw the second plane:-------------------------------------------*/

    /* Draw one image (mono) or two images (stereo): */
    for (imageI = 0; imageI < imagesN; imageI++)
	{
	/* Plane center position: */
	center_screen_x = plane2SP->center_screen_x[imageI];
	center_screen_y = plane2SP->center_screen_y;

	/* The shifted phi angle, used to rotate */
	/* the rectangle  bounding the ellipsa : */
	angle = plane2SP->normal_phi[imageI] - 4.712389;

	/* Sine and cosine of this angle: */
	sin_angle = sin (angle);
	cos_angle = cos (angle);

	/* Reciprocal values of half axes: */
	if (plane2SP->screen_a == 0.0) reciprocal_a = 0.0;
	else reciprocal_a = 1.0 / plane2SP->screen_a;
	if (plane2SP->screen_b[imageI] == 0.0) reciprocal_b = 0.0;
	else reciprocal_b = 1.0 / plane2SP->screen_b[imageI];

	/* Reciprocal diameter (required for color fading effect): */
	if (plane2SP->circle_radius == 0.0) reciprocal_diameter = 0.0;
	else reciprocal_diameter = 0.5 / plane2SP->circle_radius;

	/* The coefficient used to calculate z value for a given pixel: */
	if (plane2SP->screen_b[imageI] == 0.0) coeff = 0.0;
	else
	    {
	    coeff = plane2SP->circle_radius *
		    sin (plane2SP->normal_theta[imageI]) /
		    plane2SP->screen_b[imageI];
	    if (plane2SP->normal_theta[imageI] >= 1.5707963) coeff *= -1;
	    }

	/* Find the plane extent: */
	PlaneExtent_ (&screen_x_min, &screen_x_max,
		      &screen_y_min, &screen_y_max,
		      plane2SP, configSP, imageI);

	/* Scan the rectangle bounding ellipse: */
	for (screen_x = screen_x_min; screen_x <= screen_x_max; screen_x++)
	    {
	    for (screen_y = screen_y_min; screen_y <= screen_y_max; screen_y++)
		{
		/* Pixel position relative to the plane center position: */
		x_relative = screen_x - center_screen_x;
		y_relative = screen_y - center_screen_y;

		/* Rotated pixel position (rotation is anticlockwise here): */
		x_rotated =  x_relative * cos_angle + y_relative * sin_angle;
		y_rotated = -x_relative * sin_angle + y_relative * cos_angle;

		/* Check is it inside bounding ellipse: */
		ratio_x = x_rotated * reciprocal_a;
		ratio_y = y_rotated * reciprocal_b;
		d = ratio_x * ratio_x + ratio_y * ratio_y;
		if (d > 1.0) continue;

		/* Calculate the z value for this pixel: */
		plane_delta_z = coeff * y_rotated;
		plane_z = plane2SP->center_z[imageI] + plane_delta_z;

		/* The current pixel index: */
		pixelI = guiSP->main_win_free_area_width * screen_y + screen_x;

		/* Check pixel index: */
		if (pixelI >= pixelsN) continue;

		/* Pointer to  NearestAtomS struct. */
		/* assigned to current coordinates: */
		curr_pixelSP = nearest_atomSP + pixelI;

		/* If something was drawn to the current */
		/* pixel in this step, compare z values: */
		if (curr_pixelSP->last_refreshI == refreshI)
		    {
		    /* If the point drawn before is closer to the */
		    /* observer than plane, do not draw anything: */
		    if (curr_pixelSP->z < plane_z)
			{
			continue;
			}

		    /* If the plane is closer than the */
		    /* point drawn before, mix colors: */
		    else
			{
			/* The plane color: */
			scale_factor = (plane_delta_z +
					plane2SP->circle_radius) *
					reciprocal_diameter;
			if (scale_factor < 0.0) scale_factor = 0.0;
			if (scale_factor > 1.0) scale_factor = 1.0;
			if (plane2SP->visible_sideI[imageI] == 0)
			    {
			    plane_colorID = WeightColors_ (
					plane2SP->top_near_colorID,
					plane2SP->top_far_colorID,
					scale_factor, guiSP);
			    }
			else
			    {
			    plane_colorID = WeightColors_ (
					plane2SP->bottom_near_colorID,
					plane2SP->bottom_far_colorID,
					scale_factor, guiSP);
			    }

			/* The color of the object which was */
			/* drawn  before  the current plane: */
			old_colorID = curr_pixelSP->colorID;

			/* The weighting factor which should be used */
			/* to mix the plane color and the old color: */
			scale_factor = plane2SP->transparency;

			/* Mix the plane color and the old color: */
			colorID = WeightColors_ (plane_colorID,
						 old_colorID,
						 scale_factor, guiSP);
			}
		    }

		/* If nothing was drawn to the current pixel */
		/* in this drawing step,  prepare the color: */
		else
		    {
		    /* Prepare the scale factor used to weight (mix) colors: */
		    scale_factor = (plane_delta_z +
				    plane2SP->circle_radius) *
				    reciprocal_diameter;
		    if (scale_factor < 0.0) scale_factor = 0.0;
		    if (scale_factor > 1.0) scale_factor = 1.0;

		    /* Prepare the color by weighting */
		    /* (mixing)  near and  far color: */
		    if (plane2SP->visible_sideI[imageI] == 0)
			{
			colorID = WeightColors_ (
					plane2SP->top_near_colorID,
					plane2SP->top_far_colorID,
					scale_factor, guiSP);
			}
		    else
			{
			colorID = WeightColors_ (
					plane2SP->bottom_near_colorID,
					plane2SP->bottom_far_colorID,
					scale_factor, guiSP);
			}

		    /* Set the style index associated with the current pixel */
		    /* to signal  that only plane  was drawn to  this pixel: */
		    curr_pixelSP->styleI = PLANE_STYLE;
		    }

		/* Draw point: */
		XSetForeground (guiSP->displaySP, guiSP->theGCA[0], colorID);
		XDrawPoint (guiSP->displaySP, guiSP->main_hidden_pixmapID,
			    guiSP->theGCA[0], screen_x, screen_y);

		/* Update refresh index,  z value and */
		/* color value for the current pixel: */
		curr_pixelSP->last_refreshI = refreshI;
		curr_pixelSP->z = plane_z;
		curr_pixelSP->colorID = colorID;
		}
	    }
	}
    }

/* Return positive value (trivial): */
return 1;
}

/*===========================================================================*/


