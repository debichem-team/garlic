/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

				apply_structure.c

Purpose:
	Apply the specified secondary structure. Use the secondary structure
	stored to  the main  secondary structure  buffer.  Generate  and set
	dihedral angles.  This function assumes that all phi angles were set
	to  -180 degrees and all psi angles were set to  +180 degrees in two
	functions: ResetPhi_ () and ResetPsi_ ().

Input:
	(1) Pointer to MolComplexS structure.
	(2) Pointer to RuntimeS structure (secondary structure is there).

Output:
	(1) The secondary structure set.
	(2) Return value.

Return value:
	(1) Positive always (trivial).

========includes:============================================================*/

#include <stdio.h>

#include <stdlib.h>
#include <time.h>
#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		ExtractNCA_ (VectorS *, VectorS *, AtomS *, size_t, size_t);
int		GeneratePhiAndPsi_ (double *, double *, int);
int		RotateAtom_ (AtomS *, VectorS *, VectorS *, double);
int		ExtractC_ (VectorS *, AtomS *, size_t, size_t);

/*======apply the specified secondary structure:=============================*/

int ApplyStructure_ (MolComplexS *mol_complexSP, RuntimeS *runtimeSP)
{
int			residuesN;
size_t			atomsN;
time_t			elapsed_time;
unsigned long		long_time, short_time;
static unsigned int	random_seed;
int			residueI;
ResidueS		*curr_residueSP;
size_t			atom_startI, atom_endI, atomI;
int			n;
static VectorS		N_vectorS, CA_vectorS, C_vectorS;
int			sec_structure_code;
double			phi, psi;
double			delta_phi, delta_psi;
AtomS			*atomSP;

/* Copy the number of residues: */
residuesN = mol_complexSP->residuesN;

/* Copy the number of atoms: */
atomsN = mol_complexSP->atomsN;

/* Generate the pseudo-random seed using the time since */
/* 00:00:00 UTC, January 1, 1970,  measured in seconds: */
elapsed_time = time (NULL);
long_time = (unsigned long) elapsed_time;
short_time = long_time % 30000;
random_seed = (unsigned int) short_time;
srand (random_seed);

/* Scan the structure: */ 
for (residueI = 0; residueI < residuesN; residueI++)
	{
	/* Pointer to the current residue: */
	curr_residueSP = mol_complexSP->residueSP + residueI;

	/* Prepare the atomic index range: */
	atom_startI = curr_residueSP->residue_startI;
	atom_endI   = curr_residueSP->residue_endI;

	/* Extract N and CA coordinates: */
	n = ExtractNCA_ (&N_vectorS, &CA_vectorS,
			 mol_complexSP->atomSP, atom_startI, atom_endI);
	if (n < 2) continue;

	/* The secondary structure code associated with this residue: */
	sec_structure_code = *(runtimeSP->sec_structureP + residueI);

	/* Generate dihedral angles: */
	GeneratePhiAndPsi_ (&phi, &psi, sec_structure_code);

	/* Set phi angle for the current residue; rotate */
	/* all atoms except H, N and CA about N-CA bond. */

	/* Rotation angle (the old phi value should be equal to -3.1415927: */
	delta_phi = phi + 3.1415927;

	/* Scan all atoms of the current residue: */
	for (atomI = atom_startI; atomI <= atom_endI; atomI++)
		{
		/* Pointer to the current atom: */
		atomSP = mol_complexSP->atomSP + atomI;

		/* Do not rotate H, N and CA: */
		if ((strcmp (atomSP->raw_atomS.pure_atom_nameA, "H")  == 0) ||
		    (strcmp (atomSP->raw_atomS.pure_atom_nameA, "N")  == 0) ||
		    (strcmp (atomSP->raw_atomS.pure_atom_nameA, "CA") == 0))
			{
			continue;
			}

		/* Rotate the current atom about N-CA bond: */
		RotateAtom_ (atomSP, &N_vectorS, &CA_vectorS, delta_phi);
		}

	/* Rotate all atoms after the current residue about N-CA bond: */
	for (atomI = atom_endI + 1; atomI < atomsN; atomI++)
		{
		/* Pointer to the current atom: */
		atomSP = mol_complexSP->atomSP + atomI;

		/* Rotate atom: */
		RotateAtom_ (atomSP, &N_vectorS, &CA_vectorS, delta_phi);
		}

	/* Now extract the new C coordinate; it is required to set psi: */
	n = ExtractC_ (&C_vectorS,
		       mol_complexSP->atomSP, atom_startI, atom_endI);
	if (n < 1) continue;

	/* Set psi angle  for the current residue, */
	/* i.e. rotate the O atom about CA-C bond. */

	/* Rotation angle (the old psi value should be equal to 3.1415927): */
	delta_psi = psi - 3.1415927;

	/* Scan all atoms of the current residue: */
	for (atomI = atom_startI; atomI <= atom_endI; atomI++)
		{
		/* Pointer to the current atom: */
		atomSP = mol_complexSP->atomSP + atomI;

		/* Rotate only the O atom: */
		if (strcmp (atomSP->raw_atomS.pure_atom_nameA, "O") != 0)
			{
			continue;
			}

		/* If this point is reached, rotate O atom about CA-C bond: */
		RotateAtom_ (atomSP, &CA_vectorS, &C_vectorS, delta_psi);
		}

	/* Rotate all atoms after the current residue about CA-C bond: */
	for (atomI = atom_endI + 1; atomI < atomsN; atomI++)
		{
		/* Pointer to the current atom: */
		atomSP = mol_complexSP->atomSP + atomI;

		/* Rotate atom: */
		RotateAtom_ (atomSP, &CA_vectorS, &C_vectorS, delta_psi);
		}
	}

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


