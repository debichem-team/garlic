/* Copyright (C) 2002 Damir Zucic */

/*=============================================================================

				draw_sticks.c

Purpose:
	Draw sticks (sort of spacefill style for bonds). Half of each bond
	is drawn and another half is assigned to the partner.  Find the  z
	value for each pixel. A bond is visible only if both atoms forming
	this bond are visible. Drawing direction: 1 -> 2.

Input:
	(1) Pointer to MolComplexS structure.
	(2) Number of macromolecular complexes.
	(3) Pointer to ConfigS structure.
	(4) Pointer to GUIS structure.
	(5) Pointer to NearestAtomS structure,  with information about the
	    atom occupying the given pixel.
	(6) The number of pixels in the main window free area.
	(7) The refreshI,  used to check the  NearestAtomS associated with
	    a given pixel.

Output:
	(1) Sticks drawn to the hidden pixmap.
	(2) Return value.

Return value:
	(1) The number of bonds drawn.

Notes:
	(1) Indentation is exceptionally 4 spaces.

	(2) This drawing style is not allowed for hydrogen bonds.

=============================================================================*/

#include <stdio.h>

#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		HalfStick_ (Aux2S *, int);

/*======draw bonds as sticks:================================================*/

size_t DrawSticks_ (MolComplexS *mol_complexSP, int mol_complexesN,
		    ConfigS *configSP, GUIS *guiSP,
		    NearestAtomS *nearest_atomSP, size_t pixelsN,
		    unsigned int refreshI)
{
static size_t		bonds_drawnN = 0;
static int		imageI, imagesN;
static int		mol_complexI;
static MolComplexS	*curr_mol_complexSP, *partner_mol_complexSP;
static size_t		atomsN, atomI;
static AtomS		*curr_atomSP, *partner_atomSP;
static int		bondsN, bondI;
static TrueBondS	*curr_bondSP;
static Aux2S		aux2S;

/* Return if there are no structures: */
if (mol_complexesN == 0) return 0;

/* Number of images: */
if (configSP->stereoF) imagesN = 2;
else imagesN = 1;

/* Copy some important pointers and parameters: */
aux2S.configSP = configSP;
aux2S.guiSP = guiSP;
aux2S.nearest_atomSP = nearest_atomSP;
aux2S.pixelsN = pixelsN;
aux2S.refreshI = refreshI;

/* Draw each macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
    {
    /* Pointer to current macromolecular complex: */
    curr_mol_complexSP = mol_complexSP + mol_complexI;

    /* Prepare and check the number of atoms: */
    atomsN = curr_mol_complexSP->atomsN;
    if (atomsN == 0) continue;

    /* Copy the macromolecular complex index: */
    aux2S.mol_complexI = mol_complexI;

    /* Copy the stick radius: */
    aux2S.stick_radius = curr_mol_complexSP->stick_radius;

    /* Draw bonds which have the given style: */
    for (atomI = 0; atomI < atomsN; atomI++)
	{
	/* Pointer to the current atom: */
	curr_atomSP = curr_mol_complexSP->atomSP + atomI;

	/* Check is atom hidden: */
	if (curr_atomSP->hiddenF) continue;

	/* Check is atom inside slab: */
	if (!curr_atomSP->inside_slabF) continue;

	/* Check is atom inside window: */
	if (!curr_atomSP->inside_windowF) continue;

	/* Copy the pointer to the current atom: */
	aux2S.atom1SP = curr_atomSP;

	/* Copy the atomic index: */
	aux2S.atomI = atomI;

	/* Number of bonds: */
	bondsN = curr_atomSP->bondsN;

	/* Bond loop: */
	for (bondI = 0; bondI < bondsN; bondI++)
	    {
	    /* Pointer to the structure with data about current bond: */
	    curr_bondSP = curr_atomSP->true_bondSA + bondI;

	    /* Check bond drawing style: */
	    if (curr_bondSP->bond_styleI != STICK_BONDS) continue;

	    /* If the current bond is hydrogen bond, skip it: */
	    if (curr_bondSP->bond_typeI == 0) continue;

	    /* The complex which contains the atom forming the bond: */
	    partner_mol_complexSP = mol_complexSP +
				    curr_bondSP->neighbor_mol_complexI;

	    /* Pointer to the bond partner: */
	    partner_atomSP = partner_mol_complexSP->atomSP +
			     curr_bondSP->neighbor_arrayI;

	    /* If partner is not visible, do not draw this bond: */
	    if (partner_atomSP->hiddenF) continue;

	    /* Check is the partner inside the slab: */
	    if (!partner_atomSP->inside_slabF) continue;

	    /* Check is the partner inside the window: */
	    if (!partner_atomSP->inside_windowF) continue;

	    /* Copy the bond index: */
	    aux2S.bondI = bondI;

	    /* Copy the pointer to the current bond: */
	    aux2S.curr_bondSP = curr_bondSP;

	    /* Copy the pointer to the bond partner: */
	    aux2S.atom2SP = partner_atomSP;

	    /* Draw one (mono) or two bonds (stereo): */
	    for (imageI = 0; imageI < imagesN; imageI++)
		{
		/* Copy the image index: */
		aux2S.imageI = imageI;

		/* Draw half of a stick: */
		HalfStick_ (&aux2S, bondI);

		/* Update the counter: */
		bonds_drawnN++;
		}	/* imageI loop */
	    }		/* bondI  loop */
	}		/* atomI  loop */
    }			/* mol_complexI loop */

/* Return the number of bonds which were drawn: */
return bonds_drawnN;
}

/*===========================================================================*/


