/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

				select_sphere.c

Purpose:
	Select atoms inside the sphere defined by the sphere radius and the
	geometric center  of current selection  calculated for all selected
	atoms.  Every caught macromolecular complex  is taken into account.

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) Number of macromolecular complexes.
	(3) Pointer to RuntimeS structure (sequence buffer is there).
	(4) Selection mode index  (0 = overwrite,  1 = restrict, 2 = expand
	    previous selection).

Output:
	(1) The  flag  selectedF  set to  one  for selected atoms  in every
	    caught macromolecular complex.
	(2) Return value.

Return value:
	(1) The number of selected atoms (zero or positive value).

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======select sphere:=======================================================*/

long SelectSphere_ (MolComplexS *mol_complexSP, int mol_complexesN,
		    RuntimeS *runtimeSP, int selection_modeI)
{
long		selected_atomsN = 0;
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;
size_t		atomsN, atomI;
AtomS		*curr_atomSP;
double		x = 0.0, y = 0.0, z = 0.0, denom;
double		center_x, center_y, center_z;
double		radius_squared;
double		delta_x, delta_y, delta_z, dist_squared;
int		sphereF;

/* Calculate the geometric center position for currently selected atoms: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/* Pointer to the current macromolecular complex: */
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/* Check is the current macromolecular complex caught: */
	if (curr_mol_complexSP->catchF == 0) continue;

	/* Number of atoms in a macromolecular complex: */
	atomsN = curr_mol_complexSP->atomsN;
	if (atomsN == 0) continue;

	/* Scan all atoms in the current complex: */
	for (atomI = 0; atomI < atomsN; atomI++)
		{
		/* Pointer to the current atom: */
		curr_atomSP = curr_mol_complexSP->atomSP + atomI;

		/* Check is this atom selected: */
		if (curr_atomSP->selectedF == 0) continue;

		/* Update the number of selected atoms: */
		selected_atomsN++;

		/* Prepare data required to find geometric center: */
		x += curr_atomSP->raw_atomS.x[0];
		y += curr_atomSP->raw_atomS.y;
		z += curr_atomSP->raw_atomS.z[0];
		}
	}

/* Check how many atoms are selected: */
if (selected_atomsN == 0) return (long) 0;

/* Calculate the geometric center position: */
denom = 1.0 / (double) selected_atomsN;
x *= denom;
y *= denom;
z *= denom;

/* Update the geometric center position: */
center_x = x;
center_y = y;
center_z = z;

/* Reset the number of selected atoms: */
selected_atomsN = 0;

/* Prepare the squared radius: */
radius_squared = runtimeSP->sphere_radius * runtimeSP->sphere_radius;

/* Select all atoms in a sphere: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/* Pointer to the current macromolecular complex: */
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/* Check is the current macromolecular complex caught: */
	if (curr_mol_complexSP->catchF == 0) continue;

	/* Number of atoms in a macromolecular complex: */
	atomsN = curr_mol_complexSP->atomsN;
	if (atomsN == 0) continue;

	/* Scan all atoms in the current complex: */
	for (atomI = 0; atomI < atomsN; atomI++)
		{
		/* Pointer to the current atom: */
		curr_atomSP = curr_mol_complexSP->atomSP + atomI;

		/* Copy the coordinates: */
		x = curr_atomSP->raw_atomS.x[0];
		y = curr_atomSP->raw_atomS.y;
		z = curr_atomSP->raw_atomS.z[0];

		/* The squared distance: */
		delta_x = x - center_x;
		delta_y = y - center_y;
		delta_z = z - center_z;
		dist_squared = delta_x * delta_x +
			       delta_y * delta_y +
			       delta_z * delta_z;

		/* Compare the squared distance with the squared radius: */
		sphereF = 1;
		if (dist_squared > radius_squared) sphereF = 0;

		/* Set the selection flag for the current atom: */
		switch (selection_modeI)
			{
			/* Overwrite the previous selection: */
			case 0:
				curr_atomSP->selectedF = sphereF;
				break;

			/* Restrict the previous selection: */
			case 1:
				curr_atomSP->selectedF &= sphereF;
				break;

			/* Expand the previous selection: */
			case 2:
				curr_atomSP->selectedF |= sphereF;
				break;

			default:
				;
			}

		/* Check the selection flag; increase */
		/* the count if flag is equal to one: */
		if (curr_atomSP->selectedF) selected_atomsN++;
		}
	}

/* Return the number of selected atoms: */
return selected_atomsN;
}

/*===========================================================================*/


