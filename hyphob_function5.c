/* Copyright (C) 2006 Damir Zucic */

/*=============================================================================

			    hyphob_function5.c

Purpose:
	Draw the hydrophobicity function F5.  The sequence stored to the
	main sequence buffer  is used  to calculate  the function value.

Input:
	(1) Pointer to the storage where the minimal function value will
	    be stored.
	(2) Pointer to the storage where the maximal function value will
	    be stored.
	(3) Pointer to RuntimeS structure.

Output:
	(1) Function F5 calculated and stored.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

Notes:
	(1) The function  F5 may be modified and used for many purposes.
	    Originally, it was introduced while searching for the method
	    which will be suitable for prediction of the porin secondary
	    structure.

========includes:============================================================*/

#include <stdio.h>

#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		HyphobFunction1_ (double *, double *, RuntimeS *);
int		HyphobFunction2_ (double *, double *, RuntimeS *);
int		HyphobFunction3_ (double *, double *, RuntimeS *);
int		HyphobFunction4_ (double *, double *, RuntimeS *);
void		InitHyphob_ (RuntimeS *);

/*======calculate the hydrophobicity function F5:============================*/

int HyphobFunction5_ (double *f5minP, double *f5maxP, RuntimeS *runtimeSP)
{
int		residuesN, residueI;   /* Do not use size_t instead of int ! */
int		max_length;
char		*residue_nameP;
char		residue_nameA[RESNAMESIZE];
double		f1, f1min, f1max, f1before;
double		f2, f2min, f2max, f2before;
double		f3, f3min, f3max, f3before;
double		f4, f4min, f4max;
double		f5;
int		windowI, combinedI;
double		left_value, central_value, right_value;
double		weightA[5] = {1.00, 1.00, 1.00, 1.00, 1.00};
double		average_value;

/*------prepare some parameters:---------------------------------------------*/

/* The number of residues in the sequence buffer: */
residuesN = (int) runtimeSP->residuesN;
if (residuesN == 0) return -1;

/* The maximal residue name length: */
max_length = RESNAMESIZE - 1;

/*------initialize (reset) F5:-----------------------------------------------*/

/* Initialize F5: */
for (residueI = 0; residueI < residuesN; residueI++)
        {
        /* Reset the function F5, it might be initialized before: */
        *(runtimeSP->function5P + residueI) = 0.0;
        }

/*------calculate the function F1:-------------------------------------------*/

/* Calculate the function F1: */
HyphobFunction1_ (&f1min, &f1max, runtimeSP);

/*------calculate the function F2:-------------------------------------------*/

/* Calculate the function F2: */
HyphobFunction2_ (&f2min, &f2max, runtimeSP);

/*------calculate the function F3:-------------------------------------------*/

/* Calculate the function F3: */
HyphobFunction3_ (&f3min, &f3max, runtimeSP);

/*------calculate the function F4:-------------------------------------------*/

/* Calculate the function F4: */ 
HyphobFunction4_ (&f4min, &f4max, runtimeSP);

/*------calculate the function F5:-------------------------------------------*/

/* Initialize the extreme values: */
*f5minP = +999999.0;
*f5maxP = -999999.0;

/* Scan the sequence and calculate F5: */
for (residueI = 0; residueI < residuesN; residueI++)
	{
	/* Prepare the pointer to the residue name: */
	residue_nameP = runtimeSP->sequenceP + residueI * max_length;

	/* Copy the residue name: */
	strncpy (residue_nameA, residue_nameP, max_length);
	residue_nameA[max_length] = '\0';

	/* Initialize the function value: */
	*(runtimeSP->function5P + residueI) = 0.0;

	/* Reset default F5 value: */
	f5 = 0.0;

	/* 20060608.1205: */
	/* Copy and use F1 and F3. */
	f1 = *(runtimeSP->function1P + residueI);
	f3 = *(runtimeSP->function4P + residueI);
	f5 = f3 - f1;

	/* Scale and store the function value: */
	*(runtimeSP->function5P + residueI) = 1.0 * f5;
	}

/*------find the extreme values of F5:---------------------------------------*/

/* Find the extreme values of F5: */
for (residueI = 0; residueI < residuesN; residueI++)
	{
	/* Find extreme values for F5: */
	f5 = *(runtimeSP->function5P + residueI);
	if (f5 < *f5minP) *f5minP = f5;
	if (f5 > *f5maxP) *f5maxP = f5;
	}

/*---------------------------------------------------------------------------*/

return 1;
}

/*===========================================================================*/


