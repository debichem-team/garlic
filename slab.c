/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				slab.c

Purpose:
	For each macromolecular complex,  check which atoms are inside slab.
	If slab mode is not recognized,  do nothing.  The slab flags will be
	left intact in this case.  Each macromolecular complex  may have its
	own slab mode.

Input:
	(1) Pointer to MolComplexS structure.
	(2) Number of macromolecular complexes.

Output:
	(1) Slab flag set for each atom in each macromolecular complex.  The
	    value zero  is assigned to all atoms outside the slab  and value
	    one to all atoms inside the slab.
	(2) Return value.

Return value:
	The number of atoms inside slab (zero or positive).

Notes:
	(1) For each slab mode a separate function is prepared.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

size_t		NoSlab_ (MolComplexS *);
size_t		PlanarSlab_ (MolComplexS *);
size_t		SphereSlab_ (MolComplexS *);
size_t		HalfSphereSlab_ (MolComplexS *);
size_t		CylinSlab_ (MolComplexS *);
size_t		HalfCylinSlab_ (MolComplexS *);

/*======cut the slab:========================================================*/

size_t Slab_ (MolComplexS *mol_complexSP, int mol_complexesN)
{
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;

/* Check every macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/** Pointer to the current macromolecular complex: **/
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/** Do not apply slab if position has not changed: **/
	if (curr_mol_complexSP->position_changedF == 0) continue;

	/** Apply the proper slab: **/
	switch (curr_mol_complexSP->slab_modeI)
		{
		/*** Slab not used: ***/
		case 0:
			return NoSlab_ (curr_mol_complexSP);
			break;

		/*** Planar: ***/
		case 1:
			return PlanarSlab_ (curr_mol_complexSP);
			break;

		/*** Spherical: ***/
		case 2:
			return SphereSlab_ (curr_mol_complexSP);
			break;

		/*** Semi-spherical: ***/
		case 3:
			return HalfSphereSlab_ (curr_mol_complexSP);
			break;

		/*** Cylindrical: ***/
		case 4:
			return CylinSlab_ (curr_mol_complexSP);
			break;

		/*** Semi-cylindrical: ***/
		case 5:
			return HalfCylinSlab_ (curr_mol_complexSP);
			break;

		/*** Unknown slab mode: ***/
		default:
			;
		}
	}

/* If this point is reached, slab mode was not recognized: */
return 0;
}

/*===========================================================================*/

