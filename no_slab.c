/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				no_slab.c

Purpose:
	Set the slab flag for each atom in a macromolecular complex to one.
	Return the total number of atoms in a complex.
	
Input:
	(1) Pointer to MolComplexS structure.

Output:
	(1) Slab flag set to one for each atom. This means that slab is not
	    not used, i.e. all atoms are inside the slab.
	(2) Return value.

Return value:
	The total number of atoms in a macromolecular complex.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======slab not used:=======================================================*/

size_t NoSlab_ (MolComplexS *curr_mol_complexSP)
{
size_t		atoms_inside_slabN;
size_t		atomsN, atomI;

/* The number of atoms in a complex: */
atomsN = curr_mol_complexSP->atomsN;
atoms_inside_slabN = atomsN;

/* Set both slab flags for each atom to one: */
for (atomI = 0; atomI < atomsN; atomI++)
	{
	(curr_mol_complexSP->atomSP + atomI)->inside_slabF = 1;
	(curr_mol_complexSP->atomSP + atomI)->inside_projected_slabF = 1;
	}

/* Return the total number of atoms in the macromolecular complex: */
return atoms_inside_slabN;
}

/*===========================================================================*/

