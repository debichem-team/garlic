/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				sequence_from.c

Purpose:
	Copy the sequence  from the specified macromolecular complex to
	the sequence buffer. Only the sequence for selected range(s) is
	copied.  A residue is treated as selected  if the first atom of
	this residue is selected.  For proteins,  this is typically  N.
	The original  residue  sequence  indices are copyed.  Insertion
	codes are, however, ignored,  so it is possible  that some res.
	serial indices appear more than once in the buffer.

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to the string which contains  the macromol. complex
	    identifier.

Output:
	(1) Sequence from the specified complex  copied to the sequence
	    buffer.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
void		InitHyphob_ (RuntimeS *);

/*======copy sequence from complex to sequence buffer:=======================*/

int SequenceFrom_ (MolComplexS *mol_complexSP, int mol_complexesN,
		   RuntimeS *runtimeSP, char *stringP)
{
int			max_length;
char			*remainderP;
char			tokenA[SHORTSTRINGSIZE];
int			complexID;
int			mol_complexI;
static MolComplexS	*curr_mol_complexSP;
int			job_doneF = 0;
size_t			residue1I, residues1N;
ResidueS		*curr_residueSP;
AtomS			*first_atomSP;
AtomS			*last_atomSP;
AtomS			*curr_atomSP;
size_t			offset;
char			*sourceP, *destP;
size_t			residue2I = 0;
int			bondsN, bondI;
TrueBondS		*curr_bondSP;
int			disulfideF;
int			*curr_disulfideFP;

/* The maximal residue name length: */
max_length = RESNAMESIZE - 1;

/* At least one macromolecular complex should be available: */
if (mol_complexesN == 0)
	{
	strcpy (runtimeSP->messageA, "No structure loaded!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return -1;
	}

/* Take the first token; it should be present: */
remainderP = ExtractToken_ (tokenA, SHORTSTRINGSIZE, stringP, " \t\n");
if (!remainderP)
	{
	strcpy (runtimeSP->messageA, "Complex identifier missing!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return -2;
	}

/* Extract the macromolecular complex identifier: */
if (sscanf (tokenA, "%d", &complexID) != 1)
	{
	strcpy (runtimeSP->messageA, "Bad macromolecular complex identifier!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return -3;
	}

/* Check is the requested complex available at all: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/** Prepare the pointer to the current macromolecular complex: **/
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/* Check the number of atoms;  for bad */
	/* and discarded complexes it is zero: */
	if (curr_mol_complexSP->atomsN == 0) continue;

	/* If macromol. complex is recognized, set the catch flag to one: */
	if (curr_mol_complexSP->mol_complexID == complexID)
		{
		job_doneF = 1;
		break;
		}
	}

/* If the requested complex is not available, return negative value: */
if (job_doneF == 0)
	{
	strcpy (runtimeSP->messageA,
		"The requested complex is not available!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return -4;
	}

/* Copy the sequence: */
residues1N = curr_mol_complexSP->residuesN;
for (residue1I = 0; residue1I < residues1N; residue1I++)
	{
	/* Pointer to the current residue: */
	curr_residueSP = curr_mol_complexSP->residueSP + residue1I;

	/* Pointer to the first atom: */
	first_atomSP = curr_mol_complexSP->atomSP +
		       curr_residueSP->residue_startI;

	/* Pointer to the last atom: */
	last_atomSP = curr_mol_complexSP->atomSP +
		      curr_residueSP->residue_endI;

	/* If this atom is not selected, do not copy the residue name: */
	if (first_atomSP->selectedF == 0) continue;

	/* Pointer to the residue name associated with this atom: */
	sourceP = first_atomSP->raw_atomS.pure_residue_nameA;

	/* Prepare and check the position in seq. */
	/* buffer where this name will be copied: */
	offset = max_length * residue2I;
	if (offset > runtimeSP->sequence_buffer_size - 10 * max_length)
		{
		strcpy (runtimeSP->messageA, "Sequence too long!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return -5;
		}

	/* Copy the residue name: */
	destP = runtimeSP->sequenceP + offset;
	strncpy (destP, sourceP, max_length);

	/* Copy the residue serial index: */
	*(runtimeSP->serialIP + residue2I) =
				first_atomSP->raw_atomS.residue_sequenceI;

	/* Now it is time to set disulfide flag for the current residue. */

	/* Pointer to disulfide flag for the current residue: */
	curr_disulfideFP = runtimeSP->disulfideFP + residue2I;

	/* Reset disulfide flag: */
	*curr_disulfideFP = 0;

	/* Reset the flag: */
	disulfideF = 0;

	/* Check is this residue involved in disulfide bond: */
	for (curr_atomSP = first_atomSP;
	     curr_atomSP <= last_atomSP;
	     curr_atomSP++)
		{
		/* The number of bonds: */
		bondsN = curr_atomSP->bondsN;

		/* Check is disulfide bond present: */
		for (bondI = 0; bondI < bondsN; bondI++)
			{
			/* Pointer to the current bond: */
			curr_bondSP = curr_atomSP->true_bondSA + bondI;

			/* If this bond is disulfide */
			/* bond, set flag and break: */
			if (curr_bondSP->bond_typeI == 2)
				{
				disulfideF = 1;
				break;
				}
			}

		/* If at least one disulfide bond is found, break from loop: */
		if (disulfideF != 0) break;
		}

	/* Update disulfide flag: */
	if (disulfideF) *curr_disulfideFP = 1;

	/* Update the output residue index: */
	residue2I++;
	}

/* Set the number of residues: */
runtimeSP->residuesN = residue2I;

/* Initialize hydrophobicity values: */
InitHyphob_ (runtimeSP);

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


