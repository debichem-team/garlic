/* Copyright (C) 2000-2004 Damir Zucic */

/*=============================================================================

				include.c

Purpose:
	Identify chains, residue ranges, residue names and atom names
	which have to be included into selection.

Input:
	(1) Pointer to SelectS structure, where data will be stored.
	(2) Pointer to the selection string.

Output:
	(1) SelectS structure filled with data.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
int		ExtractChains_ (SelectS *, char *);
int		ExtractResidueRanges_ (SelectS *, char *);
int		ExtractResidueNames_ (SelectS *, char *);
int		ExtractAtomNames_ (SelectS *, char *);

/*======identify included ranges and names:==================================*/

int Include_ (SelectS *include_selectSP, char *stringP)
{
char		string_copyA[STRINGSIZE];
char		*remainderP;
char		tokenA[STRINGSIZE];
char		*exceptP;

/* Copy the input string to preserve it: */
strncpy (string_copyA, stringP, STRINGSIZE - 1);
string_copyA[STRINGSIZE - 1] = '\0';

/* Pointer to the remainder of the string */
/* initially points to  the whole string: */
remainderP = stringP;

/* The first token should contain chain identifiers: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, "/");
if (remainderP == NULL) return -1;

/* Remove the exc (exclude, except) keyword and everything after it: */
if ((exceptP = strstr (tokenA, "EXC")) != NULL) *exceptP = '\0';

/* Extract chain identifiers: */
if (ExtractChains_ (include_selectSP, tokenA) < 0) return -2;

/* The second token should contain residue ranges: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, "/");
if (remainderP == NULL) return -3;

/* Remove the exc (exclude, except) keyword and everything after it: */
if ((exceptP = strstr (tokenA, "EXC")) != NULL) *exceptP = '\0';

/* Extract residue ranges: */
if (ExtractResidueRanges_ (include_selectSP, tokenA) < 0) return -4;

/* The third token should contain residue names: */ 
remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, "/");
if (remainderP == NULL) return -5;

/* Remove the exc (exclude, except) keyword and everything after it: */
if ((exceptP = strstr (tokenA, "EXC")) != NULL) *exceptP = '\0';

/* Extract residue names: */
if (ExtractResidueNames_ (include_selectSP, tokenA) < 0) return -6;

/* The fourth (the last) token should contain atom names: */ 
remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, "/");
if (remainderP == NULL) return -7;

/* Remove the exc (exclude, except) keyword and everything after it: */
if ((exceptP = strstr (tokenA, "EXC")) != NULL) *exceptP = '\0';

/* Extract atom names: */
if (ExtractAtomNames_ (include_selectSP, tokenA) < 0) return -8;

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


