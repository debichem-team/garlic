/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				compare.c

Purpose:
	Execute compare command:  compare the sequence from the general
	purpose buffer with the sequence from the reference buffer. The
	keywords are: OFF, ZOOM, CORNER.

Input:
	(1) Pointer to MolComplexS structure.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to ConfigS structure.
	(5) Pointer to GUIS structure.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
	(9) Pointer to the remainder of the command string. The command
	    may be given with a list of keywords.

Output:
	(1) The main window mode changed to 5 (default is zero).
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

Notes:
	(1) This command reinitializes the  NearestAtomS array,  except
	    if at additional keyword is not recognized.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
void		InitNearest_ (NearestAtomS *, size_t);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);
int		ExtractIndex_ (char *);
int		ExtractTwoIntegers_ (int *, int *, char *);

/*======execute compare command:=============================================*/

int Compare_ (MolComplexS *mol_complexSP, int mol_complexesN,
	      RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
	      NearestAtomS *nearest_atomSP, size_t pixelsN,
	      unsigned int *refreshIP, char *stringP)
{
char		*remainderP;
char		tokenA[SHORTSTRINGSIZE];
int		zoom_factor;
int		residue1I, residue2I;
int		segment_width, minimal_score;

/* Extract the first token: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, stringP, " \t\n");

/* If no token  is available,  set  the main window  drawing */
/* mode, refresh the main and the control window and return: */
if (!remainderP)
	{
	/* Set the main window drawing mode index: */
	guiSP->main_window_modeI = 5;
	}

/* If keyword OFF is present, switch to default drawing mode: */
else if (strstr (tokenA, "OFF") == tokenA)
	{
	/* Reset drawing mode index: */
	guiSP->main_window_modeI = 0;
	}

/* If keyword ZOOM is present: */
else if (strstr (tokenA, "ZOO") == tokenA)
	{
	/* Try to read the zoom factor: */
	zoom_factor = ExtractIndex_ (stringP);
	if (zoom_factor <= 0)
		{
		strcpy (runtimeSP->messageA, "Bad zoom factor!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_COMPARE;
		}

	/* Store the zoom factor: */
	runtimeSP->zoom_factor = zoom_factor;

	/* Set the main window drawing mode index: */
	guiSP->main_window_modeI = 5;
	}

/* If keyword CORNER is present: */
else if (strstr (tokenA, "COR") == tokenA)
	{
	/* Try to extract two offsets: */
	if (ExtractTwoIntegers_ (&residue1I, &residue2I, stringP) < 0)
		{
		strcpy (runtimeSP->messageA,
			"Unable to read corner position!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_COMPARE;
		}

	/* Check signs: */
	if ((residue1I < 0) || (residue2I < 0))
		{
		strcpy (runtimeSP->messageA, "Both indices must be positive!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_COMPARE;
		}

	/* Check upper limits: */
	if ((residue1I >= MAXRESIDUES) || (residue2I >= MAXRESIDUES))
		{
		strcpy (runtimeSP->messageA,
			"At least one index is too large!");
		return ERROR_COMPARE;
		}

	/* Store offsets: */
	runtimeSP->reference_offset = residue1I;
	runtimeSP->sequence_offset  = residue2I;

	/* Set the main window drawing mode index: */
	guiSP->main_window_modeI = 5;
	}

/* If keyword recognition  failes,  try to extract the segment width and */
/* the minimal score. Do not forget to set the main window drawing mode! */
else
	{
	/* Try to extract two integers: */
	if (ExtractTwoIntegers_ (&minimal_score, &segment_width, stringP) < 0)
		{
		strcpy (runtimeSP->messageA, "Syntax error!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_COMPARE;
		}

	/* Check values: */
	if ((segment_width < 1) || (minimal_score < 1))
		{
		strcpy (runtimeSP->messageA, "Positive integers expected!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_COMPARE;
		}
	else if (minimal_score > segment_width)
		{
		strcpy (runtimeSP->messageA, "The minimal score should not");
		strcat (runtimeSP->messageA, " exceed the segment width!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_COMPARE;
		}

	/* Store segment width and minimal score: */
	runtimeSP->minimal_score = minimal_score;
	runtimeSP->segment_width = segment_width;

	/* Set the main window drawing mode index: */
	guiSP->main_window_modeI = 5;
	}

/* Reinitialize the NearestAtomS array and refresh index: */
InitNearest_ (nearest_atomSP, pixelsN);
*refreshIP = 1;

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return the command code: */
return COMMAND_COMPARE;
}

/*===========================================================================*/


