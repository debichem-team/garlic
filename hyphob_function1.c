/* Copyright (C) 2006 Damir Zucic */

/*=============================================================================

			    hyphob_function1.c

Purpose:
	Draw the hydrophobicity function F1.  The sequence stored to the
	main sequence buffer  is used  to calculate  the function value.

Input:
	(1) Pointer to the storage where the minimal function value will
	    be stored.
	(2) Pointer to the storage where the maximal function value will
	    be stored.
	(3) Pointer to RuntimeS structure.

Output:
	(1) Function F1 calculated and stored.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

Notes:
	(1) The function  F1 may be modified and used for many purposes.
	    Originally, it was introduced while searching for the method
	    which will be suitable for prediction of the porin secondary
	    structure.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======calculate the hydrophobicity function F1:============================*/

int HyphobFunction1_ (double *min_functionP, double *max_functionP,
		      RuntimeS *runtimeSP)
{
int             residuesN, residueI;   /* Do not use size_t instead of int ! */
int		max_length;
int		count;
int		windowI, combinedI;
char		*residue_nameP;
char		residue_nameA[RESNAMESIZE];
double		function_value;

/*------prepare some parameters:---------------------------------------------*/

/* The number of residues in the sequence buffer: */
residuesN = (int) runtimeSP->residuesN;
if (residuesN == 0) return -1;

/* The maximal residue name length: */
max_length = RESNAMESIZE - 1;

/*------calculate the function F1:-------------------------------------------*/

/* Initialize the extreme values: */
*min_functionP = +999999.0;
*max_functionP = -999999.0;

/* 20060612.1446: */
/* Scan the window of 5 residues. Count ASP, ASN, GLU, GLN, ARG, LYS, HIS. */

/* Scan the sequence: */
for (residueI = 0; residueI < residuesN; residueI++)
	{
	/* Reset the function value, it might be initialized before: */
	*(runtimeSP->function1P + residueI) = 0.0;

	/* Reset the counter: */
	count = 0;

	/* Scan the sliding window: */
	for (windowI = -2; windowI <= 2; windowI++)
		{
		/* Prepare and check the combined index: */
		combinedI = residueI + windowI;
		if (combinedI < 0) continue;
		if (combinedI >= residuesN) continue;

		/* Pointer  to  the residue  name of */
		/* the residue defined by combinedI: */
		residue_nameP = runtimeSP->sequenceP + combinedI * max_length;

		/* Copy the residue name: */
		strncpy (residue_nameA, residue_nameP, max_length);
		residue_nameA[max_length] = '\0';

		/* Add +1 for charged and zwitterionic residues: */
		if ((strcmp (residue_nameA, "ASP") == 0) ||
		    (strcmp (residue_nameA, "ASN") == 0) ||
		    (strcmp (residue_nameA, "GLU") == 0) ||
		    (strcmp (residue_nameA, "GLN") == 0) ||
		    (strcmp (residue_nameA, "ARG") == 0) ||
		    (strcmp (residue_nameA, "LYS") == 0) ||
		    (strcmp (residue_nameA, "HIS") == 0))
			{
			count++;
			}
		}

	/* Store the function value: */
	function_value = (double) count;
	*(runtimeSP->function1P + residueI) = function_value;

	/* Find the extreme values: */
	if (function_value < *min_functionP) *min_functionP = function_value;
	if (function_value > *max_functionP) *max_functionP = function_value;

	/* End of residueI loop: */
	}

/*---------------------------------------------------------------------------*/

return 1;
}

/*===========================================================================*/


