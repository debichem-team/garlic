/* Copyright (C) 2004 Damir Zucic */

/*=============================================================================

				is_aromatic.c

Purpose:
	Check is the specified residue aromatic.

Input:
	(1) Residue name (purified, i.e. without spaces).

Output:
	(1) Return value.

Return value:
	(1) Zero, if residue is not aromatic.
	(2) One, if residue is aromatic.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

/*======check is the specified residue aromatic:=============================*/

int IsAromatic_ (char *pure_residue_nameP)
{
static char	aromatic_nameAA[4][4]  = {"HIS", "PHE", "TRP", "TYR"};
int		i;

/* Compare only the first three characters: */
for (i = 0; i < 4; i++)
	{
	if (!strcmp (aromatic_nameAA[i], pure_residue_nameP)) return 1;
	}

/* If this point is reached, the specified residue is not aromatic: */
return 0;
}

/*===========================================================================*/


