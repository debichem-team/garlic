/* Copyright (C) 2000-2006 Damir Zucic */

/*=============================================================================

				print_version.c

Purpose:
	Print version information.

Input:
	No input.

Output:
	Version info printed to stdout.

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>

/*======print version info:==================================================*/

void PrintVersion_ (void)
{
fprintf (stdout, "\n");
fprintf (stdout, "==================================\n");
fprintf (stdout, "garlic version 1.6\n");
fprintf (stdout, "Release date: 2006/X/X\n");
fprintf (stdout, "==================================\n");
fprintf (stdout, "\n");
fprintf (stdout, "Compliant with PDB version 2.1");
fprintf (stdout, " (draft, October 25, 1996).\n");
fprintf (stdout,
	 "Copyright (C) 2000-2006 Damir Zucic (zucic@garlic.mefos.hr).\n");
fprintf (stdout, "Source code, documentation and binaries:\n");
fprintf (stdout, "\n=====================================\n");
fprintf (stdout, "  http://garlic.mefos.hr/garlic-1.6\n");
fprintf (stdout, "=====================================\n\n");
}

/*===========================================================================*/


