/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

				parse_sec_structure.c

Purpose:
	Parse the string with secondary structure.

Input:
	(1) Pointer to RuntimeS structure.
	(2) Pointer to the string which contains the secondary structure.

Output:
	(1) Secondary structure stored to the secondary structure buffer.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

Notes:
	(1) Use one letter codes to define the secondary structure:
	    C = coil,
	    E = strand,
	    G = 3-10 helix,
	    H = alpha helix,
	    T = turn.

	(2) Space, comma and tab may be used as separators.


========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======parse string with secondary structure:===============================*/

int ParseSecondaryStructure_ (RuntimeS *runtimeSP, char *stringP)
{
int		i;
int		code;
char		*P;
int		codeI = 0;

/* Initialize the secondary structure buffer */
/* (use the code for extended conformation): */
runtimeSP->sec_structure_length = 0;
for (i = 0; i < MAXRESIDUES; i++) *(runtimeSP->sec_structureP + i) = 'E';

/* Parse input string, character by character: */
while ((code = *stringP++) != '\0')
	{
	/* Check the current code: */
	switch (code)
		{
		/* Valid codes: */
		case 'C':
		case 'E':
		case 'G':
		case 'H':
		case 'T':
			break;

		/* Separators: */
		case ' ':
		case ',':
		case '\t':
			continue;
			break;

		/* Bad codes: */
		default:
			sprintf (runtimeSP->messageA, "Bad code!");
			strcat (runtimeSP->messageA,
				" Valid codes: C E G H T;");
			strcat (runtimeSP->messageA,
				" separators: space, comma and tab.");
			runtimeSP->message_length =
				strlen (runtimeSP->messageA);
			return -1;
		}

	/* Store the secondary structure code: */
	P = runtimeSP->sec_structureP + codeI;
	*P = code;

	/* Increment the counter: */
	codeI++;
	}

/* Store the number of codes: */
runtimeSP->sec_structure_length = codeI;

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


