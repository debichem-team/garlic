/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				hybonds.c

Purpose:
	Execute hybonds command. Generate and show (or hide) hydrogen bonds.
	If hydrogen bonds  were generated before,  check is it  necessary to
	refresh the information about hydrogen bonds.

Input:
	(1) Pointer to MolComplexS structure,  with macromol. complexes.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure, with some runtime data.
	(4) Pointer to ConfigS structure, with configuration data.
	(5) Pointer to GUIS structure, with GUI data.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
	(9) Pointer to  the remainder of  the command string.  It is  either
	    empty or contains the keyword OFF.

Output:
	(1) Hydrogen bonds should be either visible or hidden.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

Notes:
	(1) Nice style is not available for hydrogen bonds.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
void		InformUser_ (GUIS *, char *);
unsigned long	GenerateHyBonds_ (MolComplexS *, int, ConfigS *, int);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute hybonds command:=============================================*/

int HydrogenBonds_ (MolComplexS *mol_complexSP, int mol_complexesN,
		    RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
		    NearestAtomS *nearest_atomSP, size_t pixelsN,
		    unsigned int *refreshIP, char *stringP)
{
char		*remainderP;
char		tokenA[STRINGSIZE];
int		styleI;
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;
long		l;

/* Initialize the styleI: */
styleI = 1;

/* Extract the first token: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, stringP, " \t\n");

/* If available, the first token should contain the bond drawing style: */
if (remainderP)
	{
	/** Extract the bond drawing style: **/
	if 	(strstr (tokenA, "OFF") == tokenA) styleI = -1;
	else if (strcmp (tokenA,   "0") == 0)      styleI =  0;
	else if (strcmp (tokenA,   "1") == 0)      styleI =  1;
	else if (strcmp (tokenA,   "2") == 0)      styleI =  2;
	else if (strcmp (tokenA,   "3") == 0)      styleI =  3;
	else if (strcmp (tokenA,   "4") == 0)      styleI =  4;
	else if (strcmp (tokenA,   "5") == 0)      styleI =  5;
	else
		{
		sprintf (runtimeSP->messageA,
			 "Hydrogen bond drawing style \"%s\" not available!",
			 tokenA);
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_HYBONDS;
		}
	}

/* If the first token was keyword OFF: */
if (styleI == -1)
	{
	/** Hide hydrogen bonds in every caught complex: **/
	for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
		{
		/*** Pointer to the current macromolecular complex: ***/
		curr_mol_complexSP = mol_complexSP + mol_complexI;

		/*** Check is the current macromol. complex caught: ***/
		if (curr_mol_complexSP->catchF == 0) continue;

		/*** Hide hydrogen bonds: ***/
		curr_mol_complexSP->hydrogen_bonds_hiddenF = 1;
		}

	/** Refresh the main window: **/
	(*refreshIP)++;
	MainRefresh_ (mol_complexSP, mol_complexesN,
		      runtimeSP, configSP, guiSP,
		      nearest_atomSP, pixelsN, *refreshIP);

	/** Refresh the control window: **/
	ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
			 configSP, guiSP);

	/** Return to caller: **/
	return COMMAND_HYBONDS;
	}

/* If this point is reached, make hydrogen bonds visible: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/** Pointer to the current macromolecular complex: **/
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/** Check is the current macromol. complex caught: **/
	if (curr_mol_complexSP->catchF == 0) continue;

	/** Make hydrogen bonds visible (unhide): **/
	curr_mol_complexSP->hydrogen_bonds_hiddenF = 0;
	}

/* Inform user that this function may take some time to execute: */
InformUser_ (guiSP, "This may take some time, please be patient!");

/* Generate hydrogen bonds, or just update the bond drawing style: */
l = GenerateHyBonds_ (mol_complexSP, mol_complexesN, configSP, styleI);

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN, runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return positive value on success: */
return COMMAND_HYBONDS;
}

/*===========================================================================*/


